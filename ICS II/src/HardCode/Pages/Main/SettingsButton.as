package HardCode.Pages.Main 
{
	import _Base.MyTextField;
	import flash.filters.BlurFilter;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import Resources.Resource;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SettingsButton extends BrightnessButton 
	{
		private var main:MainPage;
		public function SettingsButton(main:MainPage) 
		{
			this.main = main;
			var ren:RenderableObject = new RenderableObject();
			var text:MyTextField = Resource.GetMainMenuTextField("Settings");
			ren.addChild(text);
			super(ren, new CollisionRectangle(0, 0, text.textWidth, 40), 96, -96);
			Center = new Point(705 - Collision.Width / 2, 430);
		}
		public override function get IsReceivingControl():Boolean { return !main.IsShowingPopup && !Main.IsFading; }
		protected override function OnMouseOver():void
		{
			super.OnMouseOver();
			main.ShowDescription("Setting stuffs");
		}
		protected override function OnMouseLeave():void
		{
			super.OnMouseLeave();
			main.HideDescription();
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			main.OpenSettingsPopup();
			super.OnMouseLeave();
		}
	}

}