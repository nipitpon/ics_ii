package HardCode.Pages.Main 
{
	import _Base.MyTextField;
	import fl.transitions.easing.None;
	import fl.transitions.Tween;
	import flash.filters.BlurFilter;
	import flash.text.TextFormat;
	import Resources.Resource;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class MainPageDescription extends MyTextField 
	{
		private var tween:Tween;
		public function MainPageDescription() 
		{
			super();
			defaultTextFormat = new TextFormat("Pixel Font", 25, 0, true);
			embedFonts = true;
			x = 20;
			y = 330;
			htmlText = "";
			multiline = true;
			width = 800;
			height = 600;
		}
		public function Show(text:String):void
		{
			if (tween != null && tween.isPlaying) tween.stop();
			
			this.htmlText = text;
			tween = new Tween(this, "alpha", None.easeNone, 0, 1, Utility.SecondToFrame(0.2), false);
			tween.start();
		}
		public function Hide():void
		{
			if (tween != null && tween.isPlaying) return;
			
			tween = new Tween(this, "alpha", None.easeNone, 1, 0, Utility.SecondToFrame(0.5), false);
			tween.start();
		}
	}

}