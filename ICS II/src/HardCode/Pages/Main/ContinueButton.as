package HardCode.Pages.Main 
{
	import _Base.MyTextField;
	import flash.filters.BlurFilter;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import Resources.Resource;
	import System.GameSystem;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ContinueButton extends BrightnessButton 
	{
		private var main:MainPage;
		public function ContinueButton(main:MainPage) 
		{
			this.main = main;
			var ren:RenderableObject = new RenderableObject();
			var text:MyTextField = Resource.GetMainMenuTextField("Continue");
			ren.addChild(text);
			super(ren, new CollisionRectangle(0, 0, text.textWidth, 40), 96, 32);
			Center = new Point(525 - Collision.Width / 2, 60);
		}
		public override function get IsReceivingControl():Boolean 
		{ return super.IsReceivingControl && !main.IsShowingPopup && !Main.IsFading; }
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			GameSystem.SetGameMode(GameSystem.GameMode_Vanilla);
			Main.ChangeToGamePage();
			super.OnMouseLeave();
		}
		protected override function OnMouseOver():void
		{
			super.OnMouseOver();
			main.ShowDescription("Continue from the last checkpoint.");
		}
		protected override function OnMouseLeave():void
		{
			super.OnMouseLeave();
			main.HideDescription();
		}
		
	}

}