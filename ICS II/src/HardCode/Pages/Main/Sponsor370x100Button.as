package HardCode.Pages.Main 
{
	import flash.geom.Point;
	import Resources.lib.Other_pages.Sponsor300x80Image;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Sponsor370x100Button extends BrightnessButton 
	{
		private var main:MainPage;
		public function Sponsor370x100Button(main:MainPage) 
		{
			this.main = main;
			super(new Sponsor300x80Image(), new CollisionRectangle(0, 0, 370, 100), 96, 32);
			Center = new Point(205, 530);
		}
		public override function get IsReceivingControl():Boolean { return !main.IsShowingPopup && !Main.IsFading; }
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			
		}
	}

}