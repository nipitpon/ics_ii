package HardCode.Pages.Main 
{
	import _Base.MyTextField;
	import fl.transitions.easing.None;
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BitmapDataChannel;
	import flash.events.Event;
	import flash.filters.BlurFilter;
	import flash.filters.GlowFilter;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import HardCode.Pages.SettingsPage.GameSettings;
	import HardCode.Tiles.StaticGrassTile;
	import Resources.BlackMirror.Character.PlayerSprite;
	import Resources.lib.Effects.FireSprite;
	import Resources.lib.Other_pages.MainMenuBackground;
	import Resources.lib.Other_pages.SunRay;
	import Resources.lib.Sound.BGM.SunnyWeatherSound;
	import System.ControlCenter;
	import System.DataCenter;
	import System.MC.Tile;
	import System.Pages;
	import System.SoundCenter;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class MainPage extends Pages 
	{
		private var cont:ContinueButton;
		private var start:StartButton;
		private var settings:SettingsButton;
		private var settings_popup:GameSettings;
		private var achievement:AchievementButton;
		private var credit:CreditButton;
		private var description:MainPageDescription;
		private var version:MyTextField;
		
		private var player:PlayerSprite;
		private var fire:FireSprite;
		public function MainPage() 
		{
			super();
			
			addChild(new MainMenuBackground());
			var tile:StaticGrassTile = new StaticGrassTile(Vector.<Vector.<int>>([
				Vector.<int>([-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 37, 38, 38, 38, 38]),
				Vector.<int>([-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 08, 07, 07, 07, 07]),
				Vector.<int>([-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 37, 38, 40, 07, 07, 07, 07]),
				Vector.<int>([-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 37, 38, 40, 07, 07, 07, 07, 07, 07]),
				Vector.<int>([-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 08, 07, 07, 07, 07, 07, 07, 07, 07]),
				Vector.<int>([38, 38, 38, 38, 38, 38, 38, 38, 38, 38, 38, 38, 38, 38, 40, 07, 07, 07, 07, 07, 07, 07, 07]),
				Vector.<int>([07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07])
			]));
			tile.Top = Tile.Size.y * 10;
			player = new PlayerSprite();
			player.Left = Tile.Size.x;
			player.Bottom = Tile.Size.y * 15 + 28;
			player.transform.colorTransform = new ColorTransform(0, 0, 0, 1);
			fire = new FireSprite();
			fire.Left = player.Left;
			fire.Bottom = Tile.Size.y * 15;
			addChild(tile.View);
			addChild(fire);
			addChild(player);
			addChild((cont = new ContinueButton(this)).View);
			addChild((start = new StartButton(this)).View);
			addChild((settings = new SettingsButton(this)).View);
			addChild((achievement = new AchievementButton(this)).View);
			addChild((credit = new CreditButton(this)).View);
			if (VERSION::Mochi)
			{}
			else
			{
			}
			addChild(description = new MainPageDescription());
			addChild(settings_popup = new GameSettings(this, true));
			addChild(version = new MyTextField());
			
			AddControlReceiver(cont);
			AddControlReceiver(start);
			AddControlReceiver(settings);
			AddControlReceiver(credit);
			AddControlReceiver(achievement);
			if (VERSION::Mochi)
			{
				
			}
			else
			{
				
			}
			
			settings_popup.visible = false;
			//notes_popup.visible = false;
			version.text = Main.Version;
			version.embedFonts = false;
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		public override function get IsShowingPopup():Boolean 
		{ 
			return settings_popup.visible; 
		}
		public function Refresh():void
		{
			cont.View.visible = DataCenter.HasPlayerData;
		}
		public function ShowDescription(text:String):void { description.Show(text); }
		public function HideDescription():void { description.Hide(); }
		public override function Start():void
		{
			super.Start();
			SoundCenter.PlayBGM(SunnyWeatherSound);
		}
		public override function Stop():void
		{
			super.Stop();
			description.Hide();
		}
		public function OpenSettingsPopup():void 
		{ 
			settings_popup.visible = true; 
			var tween:Tween = new Tween(settings_popup, "alpha", None.easeNone, 0, 1, 0.2, true);
			tween.start();
		}
		public function CloseSettingsPopup():void 
		{ 
			var tween:Tween = new Tween(settings_popup, "alpha", None.easeNone, 1, 0, 0.2, true);
			tween.start();
			tween.addEventListener(TweenEvent.MOTION_FINISH, onSettingsFadedOut);
		}
		private function onSettingsFadedOut(e:Event):void
		{
			settings_popup.visible = false; 
		}
		private function onEnterFrame(e:Event):void
		{
			player.Update();
			fire.Update();
		}
	}

}