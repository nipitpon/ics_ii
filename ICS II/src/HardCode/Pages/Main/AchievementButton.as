package HardCode.Pages.Main 
{
	import _Base.MyTextField;
	import flash.filters.BlurFilter;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import Resources.Resource;
	import System.DataCenter;
	import System.GameSystem;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class AchievementButton extends BrightnessButton 
	{
		private var main:MainPage;
		public function AchievementButton(main:MainPage) 
		{
			this.main = main;
			var ren:RenderableObject = new RenderableObject();
			var text:MyTextField = Resource.GetMainMenuTextField("Achievements");
			ren.addChild(text);
			super(ren, new CollisionRectangle(0, 0, text.textWidth, 40), 96, -96);
			Center = new Point(705 - Collision.Width / 2, 470);
		}
		public override function get IsReceivingControl():Boolean { return !main.IsShowingPopup && !Main.IsFading; }
		protected override function OnMouseOver():void
		{
			super.OnMouseOver();
			main.ShowDescription("See achievements");
		}
		protected override function OnMouseLeave():void
		{
			super.OnMouseLeave();
			main.HideDescription();
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			Main.ChangeToAchievementPage();
			//Main.ChangeToFinishVanilla();
			super.OnMouseLeave();
		}
		
	}

}