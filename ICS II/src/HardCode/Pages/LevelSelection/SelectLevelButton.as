package HardCode.Pages.LevelSelection 
{
	import _Base.MyTextField;
	import Resources.lib.Other_pages.SelectLevelButtonBackground;
	import Resources.Resource;
	import System.DataCenter;
	import System.GamePage;
	import System.GameSystem;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SelectLevelButton extends BrightnessButton 
	{
		private var level:int;
		private var main:LevelSelectionPage;
		public function SelectLevelButton(main:LevelSelectionPage, level:int) 
		{
			this.main = main;
			this.level = level;
			super(new SelectLevelButtonBackground(level), new CollisionRectangle(0, 0, 64, 64), 64, -128, -64);
			
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			
			GameSystem.SetGameMode(level);
			//Main.ChangeToGamePage();
			GamePage.CloseLevelSelection();
			GameSystem.PlayerInstance.SetState(Player.WarpOut);
			OnMouseLeave();
		}
		protected override function OnMouseOver():void
		{
			super.OnMouseOver();
			main.ShowLevelDetail(level);
		}
		protected override function OnMouseLeave():void
		{
			super.OnMouseLeave();
			main.HideLevelDetail();
		}
		public override function set Enabled(val:Boolean):void { (View as SelectLevelButtonBackground).Enabled = val; }
		public override function get Enabled():Boolean { return (View as SelectLevelButtonBackground).Enabled; }
	}

}