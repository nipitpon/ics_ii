package HardCode.Pages.LevelSelection 
{
	import _Base.MyTextField;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import HardCode.Pages.Main.MainPage;
	import Resources.Animation;
	import Resources.lib.GUI.QuitButtonBackground;
	import System.GamePage;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IUpdatable;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.Pages;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class CloseButton extends BrightnessButton implements IUpdatable
	{
		private var main:LevelSelectionPage;
		private var sprite:IAnimatable;
		public function CloseButton(main:LevelSelectionPage) 
		{
			this.main = main;
			var ren:RenderableObject = new RenderableObject();
			ren.addChild((sprite = new QuitButtonBackground()) as QuitButtonBackground);
			var text:MyTextField = new MyTextField(40, 0xffffff, false);
			text.text = "GO BACK";
			text.x = 28;
			ren.addChild(text);
			super(ren, new CollisionRectangle(0, 0, 111, 20), 64, -64, -128);
			Left = 546;
			Top = 454;
			(sprite as RenderableObject).addEventListener(Animation.FinishAnimation, onFinishedAnimation);
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			GamePage.CloseLevelSelection();
		}
		public function Update():void
		{
			sprite.Update();
		}
		public function get UpdateWhenAdvancingTime():Boolean { return false; }
		protected override function OnMouseOver():void 
		{
			super.OnMouseOver();
			sprite.Freeze = false;
			sprite.SetAnimation(1);
		}
		protected override function OnMouseLeave():void
		{
			super.OnMouseLeave();
			sprite.SetAnimation(0);
			sprite.Freeze = false;
		}
		private function onFinishedAnimation(e:Event):void
		{
			sprite.Freeze = true;
			//sprite.SetAnimation(0);
		}
	}

}