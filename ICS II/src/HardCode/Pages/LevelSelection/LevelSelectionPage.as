package HardCode.Pages.LevelSelection 
{
	import _Base.MyTextField;
	import flash.events.Event;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import HardCode.Tiles.StaticGrassTile;
	import Resources.lib.Environment.SpikeSprite;
	import Resources.lib.Environment.TreasureSprite;
	import Resources.lib.Other_pages.MainMenuBackground;
	import Resources.lib.Other_pages.SelectLevelBackground;
	import Resources.lib.Other_pages.SelectLevelTreasureSprite;
	import Resources.lib.Sound.BGM.SunnyWeatherSound;
	import Resources.Resource;
	import System.DataCenter;
	import System.GameFrame;
	import System.Interfaces.IReceiveControl;
	import System.MC.Tile;
	import System.Pages;
	import System.SoundCenter;
	import System.V.DeathCount;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class LevelSelectionPage extends Pages
	{
		private var back:CloseButton;
		private var death_count:DeathCount;
		private var level_text:MyTextField;
		private var treasure_sprites:SelectLevelTreasureSprite;
		private var level_buttons:Vector.<SelectLevelButton>;
		public function LevelSelectionPage() 
		{
			super();
			level_buttons = new Vector.<SelectLevelButton>();
			var header:MyTextField = Resource.GetLevelSelectionPageHeader("SELECT LEVEL");
			header.x = 360 - header.width / 2;
			header.y = 20;
			//addChild(new MainMenuBackground());
			addChild(new SelectLevelBackground());
			var tile:StaticGrassTile = new StaticGrassTile(Vector.<Vector.<int>>([
				Vector.<int>([-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1]),
				Vector.<int>([-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1]),
				Vector.<int>([-1, -1, -1, -1, -1, -1, -1, -1, -1, 37, 38, 38, 38, 39, -1, -1, -1, -1, -1, -1, -1, -1, -1]),
				Vector.<int>([-1, -1, -1, -1, -1, -1, -1, -1, -1, 08, 07, 07, 07, 06, -1, -1, -1, -1, -1, -1, -1, -1, -1]),
				Vector.<int>([38, 38, 38, 38, 38, 38, 38, 38, 38, 40, 07, 07, 07, 41, 38, 38, 38, 38, 38, 38, 38, 38, 38]),
				Vector.<int>([07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07, 07])
			]));
			tile.Top = Tile.Size.y * 11;
			//addChild(tile.View);
			//addChild(header);
			addChild((back = new CloseButton(this)).View);
			var spike:SpikeSprite = new SpikeSprite();
			spike.Left = Tile.Size.x * 7;
			spike.Bottom = Tile.Size.y * 15;
			//addChild(spike);
			//treasure_sprites = new Vector.<TreasureSprite>();
			for (var i:int = 0; i < 50; i++)
			{
				var but:SelectLevelButton = new SelectLevelButton(this, i + 1);
				but.Center = new Point(64 + (i % 10) * 66, 134 + Math.floor(i / 10) * 66);
				addChild(but.View);
				if(CONFIG::release)
				{
					but.Enabled = (i == 0 || DataCenter.GetDeathCount(i) >= 0);
				}
				AddControlReceiver(but);
				level_buttons.push(but);
				
				//var treasure:TreasureSprite = new TreasureSprite(i + 1, false);
				//treasure.Left = 280;
				//treasure.Top = 373;
				//treasure_sprites.push(treasure);
				//treasure.filters = [GameFrame.objectGlow];
			}
			treasure_sprites = new SelectLevelTreasureSprite();
			treasure_sprites.x = 436;
			treasure_sprites.y = 442;
			addChild(treasure_sprites);
			AddControlReceiver(back);
			addChild(death_count = new DeathCount());
			death_count.Left = 244;
			death_count.Top = 446;
			level_text = new MyTextField();
			addChild(level_text);
			level_text.defaultTextFormat = new TextFormat("Pixel Font", 80, 0xffffff, true);
			level_text.autoSize = TextFieldAutoSize.LEFT;
			level_text.text = "LEVEL 99";
			level_text.x = 4;
			level_text.y = 446;
			HideLevelDetail();
		}
		public function ShowLevelDetail(level:int):void
		{
			death_count.visible = true;
			death_count.NumOfDeath = DataCenter.GetDeathCount(level);
			level_text.visible = true;
			level_text.text = "LEVEL " + (level < 10 ? "0" : "") + level;
			//for (var i:int = 0; i < treasure_sprites.length; i++)
			//	if (contains(treasure_sprites[i]))
			//		removeChild(treasure_sprites[i]);
			treasure_sprites.visible = true;
			if (DataCenter.SecretItemCollected(level))
			{
				treasure_sprites.transform.colorTransform = new ColorTransform();
				treasure_sprites.alpha = 1;
			}
			else
			{
				treasure_sprites.transform.colorTransform = new ColorTransform(1, 1, 1, 1, -128, -128, -128);
				treasure_sprites.alpha = 0.5;
			}
		}
		public function ShowBackDetail():void
		{
			HideLevelDetail();
		}
		public function HideBackDetail():void
		{
			HideLevelDetail();
		}
		public function HideLevelDetail():void
		{
			level_text.visible = false;
			death_count.visible = false;
			//for (var i:int = 0; i < treasure_sprites.length; i++)
			//	if (contains(treasure_sprites[i]))
			//		removeChild(treasure_sprites[i]);
			treasure_sprites.visible = false;
		}
		CONFIG::release
		{
			protected override function onAddedToStage(e:Event):void
			{
				super.onAddedToStage(e);
				for (var i:int = 0; i < level_buttons.length; i++)
				{
					level_buttons[i].Enabled = i == 0 || DataCenter.GetDeathCount(i) >= 0;
				}
			}
		}
	}

}