package HardCode.Pages.Preloader 
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Logo370x100 extends RenderableObject 
	{
		[Embed(source = "../../../../lib/Other pages/370x100_black.png")]
		private static const map:Class;
		public function Logo370x100() 
		{
			super();
			addChild(new map());
			buttonMode = true;
			addEventListener(MouseEvent.CLICK, onClick);
			Center = new Point(400, 450);
		}
		private function onClick(e:Event):void
		{
			navigateToURL(new URLRequest("http://www.facebook.com/knightfiftytwo/"), "_blank");
		}
	}

}