package HardCode.Pages.Preloader 
{
	import flash.geom.Point;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Sponsor500x500 extends RenderableObject 
	{
		[Embed(source = "../../../../lib/Other pages/Sponsor 370x300.png")]
		private var map:Class;
		public function Sponsor500x500() 
		{
			super();
			addChild(new map());
			Center = new Point(400, 250);
		}
	}
}