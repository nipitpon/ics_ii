package HardCode.Pages.Logo 
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	import Resources.lib.Sound.BGM.SunnyWeatherSound;
	import System.GameSystem;
	import System.Pages;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Logo extends Pages 
	{
		[Embed(source = "../../../../lib/Other pages/800x600_black.png")]
		private static const map:Class;
		private var logo:RenderableObject;
		private var count:int;
		public function Logo() 
		{
			super();
			logo = new RenderableObject(new Point(360, 270));
			logo.addChild(new map());
			addChild(logo);
			buttonMode = true;
			addEventListener(MouseEvent.CLICK, onClick);
		}
		public override function Start():void
		{
			super.Start();
			logo.MoveToLocationByScalingPoint(new Point(360, 270));
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
			count = 0;
			SoundCenter.PlayBGM(SunnyWeatherSound);
		}
		protected override function onRemovedFromStage(e:Event):void
		{
			super.onRemovedFromStage(e);
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		private function onClick(e:Event):void
		{
			//navigateToURL(new URLRequest("http://knight52.somee.com"), "_blank");
		}
		private function onEnterFrame(e:Event):void
		{
			if (++count == Utility.SecondToFrame(2))
			{
				//Main.ChangeToMainPage();
				GameSystem.SetGameMode(31);
				Main.ChangeToGamePage();
			}
		}
		
	}

}