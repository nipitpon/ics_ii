package HardCode.Pages.MainMenuSettings 
{
	import _Base.MyTextField;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import HardCode.Pages.Game.PauseMenuButton;
	import HardCode.Pages.Main.MainPage;
	import Resources.Animation;
	import Resources.lib.GUI.QuitButtonBackground;
	import System.GamePage;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IUpdatable;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.Pages;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class CloseButton extends PauseMenuButton
	{
		private var main:MainSettings;
		public function CloseButton(main:MainSettings) 
		{
			super(new QuitButtonBackground(), "GO BACK");
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			GamePage.CloseMainSettingsPopup(true);
			super.OnMouseLeave();
		}
		//public function Update():void
		//{
		//	sprite.Update();
		//}
		//public function get UpdateWhenAdvancingTime():Boolean { return false; }
		//protected override function OnMouseOver():void 
		//{
		//	super.OnMouseOver();
		//	sprite.Freeze = false;
		//	sprite.SetAnimation(1);
		//}
		//protected override function OnMouseLeave():void
		//{
		//	super.OnMouseLeave();
		//	sprite.SetAnimation(0);
		//	sprite.Freeze = false;
		//}
		//private function onFinishedAnimation(e:Event):void
		//{
		//	sprite.Freeze = true;
		//	//sprite.SetAnimation(0);
		//}
	}

}