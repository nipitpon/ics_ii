package HardCode.Pages.MainMenuSettings 
{
	import _Base.MyTextField;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import HardCode.Pages.Game.PauseMenuButton;
	import HardCode.Pages.Main.MainPage;
	import Resources.lib.GUI.ClearDataSprite;
	import Resources.lib.Sound.SFX.MissileExplode;
	import System.DataCenter;
	import System.GamePage;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ClearData extends PauseMenuButton 
	{
		private var main:MainSettings;
		public function ClearData(main:MainSettings) 
		{
			this.main = main;
			super(new ClearDataSprite(), "CLEAR DATA");
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			GamePage.CloseMainSettingsPopup(false);
			GamePage.OpenClearData();
		}
	}
}