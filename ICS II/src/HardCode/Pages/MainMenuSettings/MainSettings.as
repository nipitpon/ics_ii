package HardCode.Pages.MainMenuSettings 
{
	import flash.events.Event;
	import flash.geom.Point;
	import flash.system.System;
	import HardCode.Pages.SettingsPage.BGMBar;
	import HardCode.Pages.SettingsPage.SFXBar;
	import Resources.lib.GUI.TextPlatform;
	import Resources.lib.Other_pages.MainSettingsBackground;
	import Resources.lib.Sound.SFX.ButtonClick;
	import System.ControlCenter;
	import System.DataCenter;
	import System.Interfaces.IDescriptor;
	import System.MC.Buttons.Button;
	import System.MC.Buttons.GUIButton;
	import System.MC.Buttons.MusicButton;
	import System.MC.Buttons.QualityButton;
	import System.MC.Buttons.SoundButton;
	import System.Pages;
	import System.SoundCenter;
	import System.V.Bar;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class MainSettings extends Pages
	{
		private var main:Pages
		private var bgm_bar:BGMBar;
		private var sfx_bar:SFXBar;
		private var close:CloseButton;
		public function MainSettings(main:Pages) 
		{
			this.main = main;
			super();
			var bg:MainSettingsBackground = new MainSettingsBackground();
			addChild(bg);
			close = new CloseButton(this);
			close.Left = 134;
			close.Top = 360;
			addChild(close.View);
			AddControlReceiver(close);
			bgm_bar = new BGMBar();
			sfx_bar = new SFXBar();
			addChild(bgm_bar);
			addChild(sfx_bar);
			bgm_bar.x = 460;
			bgm_bar.y = 200;
			sfx_bar.x = 460;
			sfx_bar.y = 234;
			bgm_bar.addEventListener(Bar.ValueChanged, bgmBar_ValueChanged);
			sfx_bar.addEventListener(Bar.ValueChanged, sfxBar_ValueChanged);
			sfx_bar.CurrentValue = SoundCenter.SFXVolume;
			bgm_bar.CurrentValue = SoundCenter.BGMVolume;
			var but:Button = new QualityButton();
			but.Left = 134;
			but.Top = 166;
			addChild(but.View);
			AddControlReceiver(but);
			but = new MusicButton();
			but.Left = 134;
			but.Top = 200;
			addChild(but.View);
			AddControlReceiver(but);
			but = new SoundButton();
			but.Left = 134;
			but.Top = 234;
			addChild(but.View);
			AddControlReceiver(but);
			but = new ClearData(this);
			but.Left = 134;
			but.Top = 289;
			addChild(but.View);
			AddControlReceiver(but);
		}
		public override function set visible(val:Boolean):void
		{
			super.visible = val;
			if (val)
			{
				sfx_bar.CurrentValue = SoundCenter.SFXVolume;
				bgm_bar.CurrentValue = SoundCenter.BGMVolume;
			}
			else
			{
				SoundCenter.SFXVolume = sfx_bar.CurrentValue;
				SoundCenter.BGMVolume = bgm_bar.CurrentValue;
			}
		}
		private function bgmBar_ValueChanged(e:Event):void
		{
			SoundCenter.BGMVolume = bgm_bar.CurrentValue;
		}
		private function sfxBar_ValueChanged(e:Event):void
		{
			SoundCenter.SFXVolume = sfx_bar.CurrentValue;
			SoundCenter.PlaySFX(ButtonClick);
		}
		private function onEnterFrame(e:Event):void
		{
			
		}
	}

}