package HardCode.Pages.Game 
{
	import _Base.MyTextField;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.StageVideoAvailabilityEvent;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.text.TextDisplayMode;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import Resources.lib.GUI.PauseBackground;
	import Resources.lib.GUI.TextPlatform;
	import Resources.lib.Other_pages.Sponsor40x40;
	import System.ControlCenter;
	import System.GamePage;
	import System.GUI;
	import System.Interfaces.IDescriptor;
	import System.Interfaces.IReceiveControl;
	import System.Pages;
	import System.Time;
	import System.V.RenderableObject;
	import System.MC.Buttons.MusicButton;
	import System.MC.Buttons.QualityButton;
	import System.MC.Buttons.SoundButton;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class PauseMenu extends Pages implements IReceiveControl
	{
		private var resume:ResumeButton;
		private var quit:QuitButton;
		private var confirmQuit:ConfirmQuitButton;
		private var uSure:MyTextField;
		private var confirmCount:int;
		private var key_released:Boolean;
		private var restart:RestartButton;
		private var soundButton:SoundButton;
		private var musicButton:MusicButton;
		private var qualityButton:QualityButton;
		public function PauseMenu() 
		{
			super();
			
			soundButton = new SoundButton();
			musicButton = new MusicButton();
			qualityButton = new QualityButton();
			uSure = new MyTextField();
			uSure.defaultTextFormat = new TextFormat("Pixel Font", 15, 0xffffff, false, false, false, null, null, TextFormatAlign.CENTER);
			uSure.autoSize = TextFieldAutoSize.CENTER;
			uSure.embedFonts = true;
			uSure.text = "Are you sure?";
			uSure.x = 360 - uSure.textWidth / 2;
			uSure.y = 370;
			
			soundButton.Left = 184;
			soundButton.Top = 340;
			musicButton.Left = 184;
			musicButton.Top = 306;
			qualityButton.Left = 184;
			qualityButton.Top = 272;
			
			addChild(new PauseBackground());
			addChild((resume = new ResumeButton(this)).View);
			addChild((quit = new QuitButton(this)).View);
			addChild((confirmQuit = new ConfirmQuitButton()).View);
			addChild((restart = new RestartButton(this)).View);
			addChild(soundButton.View);
			addChild(musicButton.View);
			addChild(qualityButton.View);
			if (VERSION::Mochi)
			{
			}
			else
			{
				//addChild((sponsor = new Sponsor40x40Button()).View);
			}
			addChild(uSure);
			
			uSure.visible = false;
			confirmQuit.View.visible = false;
			AddControlReceiver(resume);
			AddControlReceiver(quit);
			AddControlReceiver(restart);
			AddControlReceiver(qualityButton);
			AddControlReceiver(musicButton);
			AddControlReceiver(soundButton);
		}
		public override function set visible(val:Boolean):void
		{
			if (val != super.visible)
			{
				if (val)
				{
					key_released = false;
					if (VERSION::Mochi)
					{}
					else
					{
						//ControlCenter.AddGameObject(sponsor);
					}
					if(!ControlCenter.IsReceivingControl)
						ControlCenter.Start();
					GUI.HideDescription();
				}
				else
				{
					if (VERSION::Mochi)
					{}
					else
					{
						//ControlCenter.RemoveGameObject(sponsor);
					}
					if (Time.IsAdvancing)
						ControlCenter.Stop();
				}
			}
			super.visible = val;
		}
		public override function KeyPress(code:int):void 
		{
			super.KeyPress(code);
			if (key_released && code == ControlCenter.Pause)
			{
				GUI.HidePauseMenu();
			}
		}
		public override function KeyRelease(code:int):void 
		{
			super.KeyRelease(code);
			if (code == ControlCenter.Pause)
				key_released = true;
		}
		public override function get IsReceivingControl():Boolean { return GUI.IsShowingPauseMenu && !GamePage.IsShowingPopup; }
		public function ConfirmQuit():void
		{
			uSure.visible = true;
			confirmQuit.View.visible = true;
			confirmCount = 0;
		}
		public function CloseConfirmQuit(e:Event = null):void
		{
			uSure.visible = false;
			confirmQuit.View.visible = false;
			confirmCount = 0;
		}
	}

}