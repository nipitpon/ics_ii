package HardCode.Pages.Game 
{
	import _Base.MyTextField;
	import flash.geom.Point;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import System.GamePage;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ConfirmQuitButton extends BrightnessButton 
	{
		
		public function ConfirmQuitButton() 
		{
			var ren:RenderableObject = new RenderableObject();
			var text:MyTextField = new MyTextField();
			text.defaultTextFormat = new TextFormat("Pixel Font", "15", 0, true, false, false, null, null, TextFormatAlign.CENTER);
			text.embedFonts = true;
			text.width = 100;
			text.height = 20;
			text.border = true;
			text.background = true;
			text.borderColor = 0;
			text.backgroundColor = 0xeaeaea;
			text.text = "Yah";
			ren.addChild(text);
			super(ren, new CollisionRectangle(0, 0, text.width, 20), 96, 32);
			Center = new Point(360, 408);
		}
		public override function get IsReceivingControl():Boolean { return super.IsReceivingControl && !GamePage.IsShowingPopup && !Main.IsFading; }
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			Main.ChangeToSelectionPage();
			super.OnMouseLeave();
		}
		
	}

}