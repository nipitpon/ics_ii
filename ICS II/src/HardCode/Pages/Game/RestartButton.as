package HardCode.Pages.Game 
{
	import _Base.MyTextField;
	import flash.geom.Point;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import Resources.lib.GUI.RestartButtonBackground;
	import System.GamePage;
	import System.GUI;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Level;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class RestartButton extends PauseMenuButton 
	{
		private var main:PauseMenu;
		public function RestartButton(main:PauseMenu) 
		{
			this.main = main;
			super(new RestartButtonBackground(), "RESTART");
			Left = 184;
			Top = 238;
		}
		public override function get IsReceivingControl():Boolean { return super.IsReceivingControl && !GamePage.IsShowingPopup; }
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			Main.ChangeToGamePage();
			Level.ClearObjectInfo();
			super.OnMouseLeave();
		}
	}

}