package HardCode.Pages.Game.GameOver 
{
	import _Base.MyTextField;
	import fl.transitions.easing.None;
	import fl.transitions.Tween;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.filters.BlurFilter;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.Timer;
	import System.ControlCenter;
	import System.GameSystem;
	import System.V.FloatingText;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class GameOver extends RenderableObject 
	{
		private var toMain:BackToMenuButton;
		private var cont:ContinueButton;
		private var num:FloatingText;
		private var text2:MyTextField;
		public function GameOver() 
		{
			super();
			graphics.beginFill(0, 1);
			graphics.drawRect(0, 0, 800, 600);
			graphics.endFill();
			addChild((toMain = new BackToMenuButton()).View);
			addChild((cont = new ContinueButton()).View);
			var text:MyTextField = new MyTextField();
			text.defaultTextFormat = new TextFormat("Tahoma", 60, 0xffffff, true);
			text.autoSize = TextFieldAutoSize.LEFT;
			text.text = "YOU ARE DEAD";
			text.filters = [new BlurFilter(2, 2)];
			text.x = 400 - text.textWidth / 2;
			text.y = 200;
			addChild(text);
			text2 = new MyTextField();
			text2.defaultTextFormat = new TextFormat("Tahoma", 35, 0xffffff, true);
			text2.autoSize = TextFieldAutoSize.CENTER;
			text2.filters = [new BlurFilter(2, 2)];
			text2.x = 400 - text2.width / 2;
			text2.y = 300;
			text2.multiline = true;
			addChild(text2);
			visible = false;
		}
		public function Show(dam:uint, position:Point):void
		{
			ControlCenter.AddGameObject(toMain);
			ControlCenter.AddGameObject(cont);
			visible = true;
			if (num != null && contains(num))
				removeChild(num);
			num = new FloatingText(position.x, position.y, 0xff8888, dam.toString());
			addChild(num);
			cont.View.visible = false;
			toMain.View.visible = false;
			text2.visible = false;
			var timer:Timer = new Timer(1000, 1);
			timer.addEventListener(TimerEvent.TIMER, onTick);
			timer.start();
		}
		public function Hide():void
		{
			ControlCenter.RemoveGameObject(toMain);
			ControlCenter.RemoveGameObject(cont);
			visible = false;
		}
		private function onTick(e:Event):void
		{
			e.currentTarget.removeEventListener(TimerEvent.TIMER, onTick);
			cont.View.visible = true;
			toMain.View.visible = true;
			text2.visible = true;
			cont.View.alpha = 0;
			toMain.View.alpha = 0;
			text2.alpha = 0;
			var tween:Tween = new Tween(cont.View, "alpha", None.easeNone, 0, 1, 1, true);
			tween.start();
			tween = new Tween(toMain.View, "alpha", None.easeNone, 0, 1, 1, true);
			tween.start();
			tween = new Tween(text2, "alpha", None.easeNone, 0, 1, 1, true);
		}
	}

}