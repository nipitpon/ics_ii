package HardCode.Pages.Game.GameOver 
{
	import _Base.MyTextField;
	import flash.filters.BlurFilter;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ContinueButton extends BrightnessButton 
	{
		
		public function ContinueButton() 
		{
			var ren:RenderableObject = new RenderableObject();
			var text:MyTextField = new MyTextField();
			text.defaultTextFormat = new TextFormat("Times New Roman", "30", 0xffffff, true, false, false, null, null);
			text.filters = [new BlurFilter(2, 2)];
			text.autoSize = TextFieldAutoSize.LEFT;
			text.text = "NEVER!!!!";
			ren.addChild(text);
			super(ren, new CollisionRectangle(0, 0, text.textWidth, 40), -32, -96);
			Center = new Point(400, 450);
		}
		public override function get IsReceivingControl():Boolean { return !Main.IsFading; }
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			Main.ChangeToGamePage();
			super.OnMouseLeave();
		}
		
	}

}