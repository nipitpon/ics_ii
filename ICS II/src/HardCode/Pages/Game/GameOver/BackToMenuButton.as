package HardCode.Pages.Game.GameOver 
{
	import _Base.MyTextField;
	import flash.filters.BlurFilter;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class BackToMenuButton extends BrightnessButton 
	{
		
		public function BackToMenuButton() 
		{
			var ren:RenderableObject = new RenderableObject();
			var text:MyTextField = new MyTextField();
			text.defaultTextFormat = new TextFormat("Times New Roman", "30", 0xffffff, true, false, false, null, null);
			text.filters = [new BlurFilter(2, 2)];
			text.autoSize = TextFieldAutoSize.LEFT;
			text.text = "Yeah, I guess...";
			ren.addChild(text);
			super(ren, new CollisionRectangle(0, 0, text.textWidth, 40), -32, -96);
			Center = new Point(400, 500);
		}
		public override function get IsReceivingControl():Boolean { return !Main.IsFading; }
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			Main.ChangeToMainPage();
			super.OnMouseLeave();
		}
		
	}

}