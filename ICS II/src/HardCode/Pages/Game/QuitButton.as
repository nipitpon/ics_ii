package HardCode.Pages.Game 
{
	import _Base.MyTextField;
	import flash.geom.Point;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import Resources.lib.GUI.QuitButtonBackground;
	import System.GamePage;
	import System.GameSystem;
	import System.GUI;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class QuitButton extends PauseMenuButton 
	{
		private var pauseMenu:PauseMenu;
		public function QuitButton(pause:PauseMenu) 
		{
			pauseMenu = pause;
			super(new QuitButtonBackground(), "RAGEQUIT");
			Left = 184;
			Top = 374;
		}
		public override function get IsReceivingControl():Boolean { return super.IsReceivingControl && !GamePage.IsShowingPopup; }
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			//pauseMenu.ConfirmQuit();
			GameSystem.SetGameMode(80);
			Main.ChangeToGamePage();
			super.OnMouseLeave();
		}
	}

}