package HardCode.Pages.Game 
{
	import _Base.MyTextField;
	import flash.geom.Point;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import Resources.lib.GUI.SettingsButtonBackground;
	import System.GamePage;
	import System.GUI;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SettingsButton extends BrightnessButton 
	{
		private var main:PauseMenu;
		public function SettingsButton(main:PauseMenu) 
		{
			this.main = main;
			super(new SettingsButtonBackground(), new CollisionRectangle(0, 0, 64, 64), 96, 32);
			Center = new Point(360, 358);
		}
		public override function get IsReceivingControl():Boolean { return super.IsReceivingControl && !GamePage.IsShowingPopup; }
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			GamePage.OpenSettingsPopup();
			OnMouseLeave();
		}
		protected override function OnMouseOver():void
		{
			super.OnMouseOver();
			//GUI.ShowDescriptionAsHtml(CenterX, CenterY, "Open Settings menu");
			main.SetText("SETTINGS");
		}
		protected override function OnMouseLeave():void
		{
			super.OnMouseLeave();
			//GUI.HideDescription();
			main.ClearText();
		}
		
	}

}