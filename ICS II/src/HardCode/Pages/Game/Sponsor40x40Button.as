package HardCode.Pages.Game 
{
	import flash.geom.Point;
	import Resources.lib.Other_pages.Sponsor40x40;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Sponsor40x40Button extends BrightnessButton 
	{
		
		public function Sponsor40x40Button() 
		{
			super(new Sponsor40x40(), new CollisionRectangle(0, 0, 40, 40), 96, 32);
			Center = new Point(205, 121);
		}
		
	}

}