package HardCode.Pages.Game 
{
	import _Base.MyTextField;
	import flash.events.Event;
	import flash.text.engine.RenderingMode;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import Resources.Animation;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IUpdatable;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class PauseMenuButton extends BrightnessButton implements IUpdatable
	{
		private var text_field:MyTextField;
		private var sprite:IAnimatable
		public function PauseMenuButton(view:RenderableObject, text:String) 
		{
			sprite = view as IAnimatable;
			var ren:RenderableObject = new RenderableObject();
			ren.addChild(view);
			super(ren, new CollisionRectangle(0, 0, 280, 24), 64, -64, -128);
			text_field = new MyTextField();
			text_field.defaultTextFormat = new TextFormat("Flamenco", 32, 0xffffff, true, null, null, null, null, TextFormatAlign.LEFT);
			text_field.text = text;
			text_field.x = 24;
			text_field.y = -6;
			text_field.width = 280;
			ren.addChild(text_field);
			view.addEventListener(Animation.FinishAnimation, onFinishedAnimation);
		}
		public function Update():void
		{
			sprite.Update();
		}
		public function get UpdateWhenAdvancingTime():Boolean { return false; }
		protected function SetText(text:String):void
		{
			text_field.text = text;
		}
		protected override function OnMouseOver():void 
		{
			super.OnMouseOver();
			sprite.SetAnimation(1);
		}
		protected override function OnMouseLeave():void
		{
			super.OnMouseLeave();
			sprite.SetAnimation(0);
		}
		private function onFinishedAnimation(e:Event):void
		{
			sprite.SetAnimation(0);
		}
	}

}