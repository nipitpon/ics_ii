package HardCode.Pages.Game 
{
	import _Base.MyTextField;
	import flash.geom.Point;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import Resources.lib.GUI.ResumeButtonBackground;
	import System.GamePage;
	import System.GUI;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ResumeButton extends PauseMenuButton 
	{
		private var main:PauseMenu;
		public function ResumeButton(main:PauseMenu) 
		{
			this.main = main;
			super(new ResumeButtonBackground(), "RESUME");
			Left = 184;
			Top = 204;
		}
		public override function get IsReceivingControl():Boolean { return super.IsReceivingControl && !GamePage.IsShowingPopup; }
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			GUI.HidePauseMenu();
			super.OnMouseLeave();
		}
	}

}