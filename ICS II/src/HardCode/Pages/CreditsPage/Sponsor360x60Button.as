package HardCode.Pages.CreditsPage 
{
	import flash.geom.Point;
	import Resources.lib.Sponsor360x60;
	import System.MC.Buttons.FrameButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Sponsor360x60Button extends FrameButton 
	{
		
		public function Sponsor360x60Button() 
		{
			super(new Sponsor360x60(), new CollisionRectangle(0, 0, 370, 60));
			Center = new Point(10 + Collision.Width / 2, 275);
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
		}
		
	}

}