package HardCode.Pages.CreditsPage 
{
	import _Base.MyTextField;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import HardCode.Pages.Main.MainPage;
	import Resources.lib.GUI.QuitButtonBackground;
	import Resources.lib.Other_pages.CreditBackButtonImage;
	import System.GamePage;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.Pages;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class CloseButton extends BrightnessButton 
	{
		public function CloseButton() 
		{
			var ren:RenderableObject = new RenderableObject();
			ren.addChild(new QuitButtonBackground());
			var text:MyTextField = new MyTextField(40, 0xffffff, false);
			text.text = "GO BACK";
			text.x = 28;
			ren.addChild(text);
			super(ren, new CollisionRectangle(0, 0, 111, 20), 64, -64, -128);
			Left = 188;
			Top = 440;
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			GamePage.CloseCredit();
			super.OnMouseLeave();
		}
	}

}