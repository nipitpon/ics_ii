package HardCode.Pages.CreditsPage 
{
	import _Base.MyTextField;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.filters.BlurFilter;
	import flash.geom.Rectangle;
	import flash.net.navigateToURL;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import System.MC.Buttons.FrameButton;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class CreditText extends RenderableObject 
	{
		private var frame:Shape;
		private var credit1:MyTextField;
		private var credit2:MyTextField;
		private var buttons:Vector.<FrameButton>;
		private var sponsor:Sponsor360x60Button;
		private static const credit2_link:Vector.<String> = Vector.<String>([
			"http://freesound.org/people/Bidone/sounds/66762/",
			"http://freesound.org/people/tc630/sounds/47835/",
			"http://freesound.org/people/samararaine/sounds/186374/",
			"http://freesound.org/people/Nic3_one/sounds/166542/",
			"http://freesound.org/people/Greencouch/sounds/106265/",
			"http://freesound.org/people/rombart/sounds/186747/",
			"http://freesound.org/people/AGFX/sounds/20438/"
			]);
		public function CreditText() 
		{
			super();
			
			x = 168;
			y = 55;
			buttons = new Vector.<FrameButton>();
			credit1 = new MyTextField();
			credit1.defaultTextFormat = new TextFormat("Pixel Font", 25, 0, false, false, false, null, null);
			credit1.autoSize = TextFieldAutoSize.LEFT;
			credit1.embedFonts = true;
			credit1.width = 368;
			credit1.height = 300;
			credit1.multiline = true;
			credit1.htmlText = 
				"<p>Graphics</p>" + 
				"<p><font size='20'><b>Miroko</b></font></p>" + 
				"<br/><p>Design and Program</p><p><font size='20'><b>Nipitpon Wongsuparatkul</b></font></p>" + 
				"<br/><p>Music and Sound Effects:</p><p><font size='20'><b>Alec Shea</b></font></p>";
			if (VERSION::Mochi)
			{
				credit1.htmlText += "<br/>";
			}
			else
			{
				//credit1.htmlText += "<br/><p>Sponsored by</p><br/><br/><br/>";
			}
			//credit1.htmlText += "<p>Applications used</p>" +
			//	"<li><font size='20'><ul><a href='event:audacity'>Audacity</a></ul><br/><ul><a href='event:gimp'>GIMP 2</a></ul>" +
			//	"<br/><ul><a href='event:paint'>Paint.NET</a></ul><br/><ul><a href='event:flashdevelop'>FlashDevelop 4</a></ul>" +
			//	"<br/><ul><a href='event:inkscape'>InkScape</a></ul><br/><ul><a href='event:synthfont'>SynthFont</a></ul></font></li>";
			
			credit2 = new MyTextField();
			credit2.defaultTextFormat = new TextFormat("Pixel Font", 25, 0, false, false, false, null, null, TextFormatAlign.LEFT);
			credit2.embedFonts = true;
			credit2.width = 800;
			credit2.height = 600;
			credit2.multiline = true;
			credit2.y = credit1.textHeight;
			credit2.htmlText = "";
			createBorder();
			
			addChild(credit1);
			addChild(credit2);
			if (VERSION::Mochi)
			{}
			else
			{
				//addChild((sponsor = new Sponsor360x60Button()).View);
				//buttons.push(sponsor);
			}
		}
		public function get Buttons():Vector.<FrameButton> { return buttons; }
		private function text1_Click(e:TextEvent):void
		{
			switch(e.text)
			{
				case "Misho": navigateToURL(new URLRequest("http://miroko.newgrounds.com/"), "_blank"); break;
				case "knight52": navigateToURL(new URLRequest("https://www.facebook.com/knightfiftytwo"), "_blank"); break;
				case "alec": navigateToURL(new URLRequest("https://soundcloud.com/alecshea"), "_blank"); break;
				case "soundchris": navigateToURL(new URLRequest("http://soundchris.newgrounds.com/audio/"), "_blank"); break;
				case "gimp": navigateToURL(new URLRequest("http://www.gimp.org"), "_blank"); break;
				case "paint": navigateToURL(new URLRequest("http://www.getpaint.net/"), "_blank"); break;
				case "audacity": navigateToURL(new URLRequest("http://audacity.sourceforge.net/"), "_blank"); break;
				case "flashdevelop": navigateToURL(new URLRequest("http://www.flashdevelop.org/‎"), "_blank"); break;
				case "inkscape": navigateToURL(new URLRequest("http://www.inkscape.org/‎"), "_blank"); break;
				case "synthfont": navigateToURL(new URLRequest("http://www.synthfont.com/‎"), "_blank"); break;
			}
		}
		private function text2_Click(e:TextEvent):void
		{
			switch(e.text)
			{
				case "freesound": navigateToURL(new URLRequest("http://www.freesound.org"), "_blank"); break;
				default: navigateToURL(new URLRequest(credit2_link[int(e.text)]), "_blank"); break;
			}
		}
		private function createBorder():void
		{
				createButtonForText(credit1, Vector.<String>([
					"Miroko",
					"Nipitpon Wongsuparatkul",
					"Alec Shea"
				]));
		}
		private function createButtonForText(tf:MyTextField, texts:Vector.<String>):void
		{
			for each(var text:String in texts)
			{
				var startIndex:Number = tf.text.indexOf(text);
				var start:Rectangle = tf.getCharBoundaries(startIndex);
				var end:Rectangle = tf.getCharBoundaries(startIndex + text.length - 1);
				var border:FrameButton = new FrameButton(new RenderableObject(),
					new CollisionRectangle(x + tf.x + start.x, y + tf.y + start.y, end.x - start.x + end.width, end.height));
				buttons.push(border);
				//border.View.filters = [new BlurFilter(2, 2)];
				addChild(border.View);
			}
		}
	}

}