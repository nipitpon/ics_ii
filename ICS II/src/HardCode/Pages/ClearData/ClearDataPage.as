package HardCode.Pages.ClearData 
{
	import _Base.MyTextField;
	import flash.geom.Point;
	import Resources.lib.Other_pages.ClearDataBackground;
	import Resources.lib.Other_pages.MainSettingsBackground;
	import System.MC.Buttons.Button;
	import System.Pages;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ClearDataPage extends Pages 
	{
		
		public function ClearDataPage() 
		{
			super();
			var bg:MainSettingsBackground = new MainSettingsBackground();
			//bg.Center = new Point(360, 270);
			addChild(bg);
			var text:MyTextField = new MyTextField(40, 0xffffff, false);
			text.text = "Are you sure you want to erase your save data?\nYou'll lose everything!";
			text.x = 134;
			text.y = 166;
			text.width = text.textWidth;
			addChild(text);
			var but:Button = new ClearDataYes();
			but.Left = 134;
			but.Top = 289;
			addChild(but.View);
			AddControlReceiver(but);
			but = new ClearDataNo();
			but.Left = 134;
			but.Top = 323;
			addChild(but.View);
			AddControlReceiver(but);
		}
		
	}

}