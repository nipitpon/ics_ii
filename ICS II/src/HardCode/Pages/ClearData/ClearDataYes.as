package HardCode.Pages.ClearData 
{
	import HardCode.Tiles.DoorTile;
	import Resources.lib.Other_pages.ClearDataYesButtonImage;
	import Resources.lib.Sound.SFX.EraseDataSound;
	import System.DataCenter;
	import System.GamePage;
	import System.GameSystem;
	import System.Interfaces.INamable;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ClearDataYes extends BrightnessButton 
	{
		public function ClearDataYes() 
		{
			super(new ClearDataYesButtonImage(), new CollisionRectangle(0, 0, 60, 40), 64, -64, -128);
			
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			DataCenter.ClearSaveData(true);
			GamePage.CloseClearData(false);
			SoundCenter.PlaySFX(EraseDataSound);
			Main.ChangeToGamePage();
		}
	}

}