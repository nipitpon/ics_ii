package HardCode.Pages.ClearData 
{
	import Resources.lib.Other_pages.ClearDataNoButtonImage;
	import Resources.lib.Other_pages.ClearDataYesButtonImage;
	import System.GamePage;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ClearDataNo extends BrightnessButton 
	{
		public function ClearDataNo() 
		{
			super(new ClearDataNoButtonImage(), new CollisionRectangle(0, 0, 60, 40), 64, -64, -128);
			
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			GamePage.CloseClearData(true);
		}
	}

}