package HardCode.Pages.Achievements 
{
	import Resources.lib.Other_pages.AchievementsBackgroundImage;
	import Resources.lib.Other_pages.AchievementsIcons;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class AchievementTrophy extends BrightnessButton 
	{
		private static const collision_list:Vector.<CollisionBox> = Vector.<CollisionBox>([
			new CollisionRectangle(0, 0, 38, 64),
			new CollisionRectangle(4, 0, 26, 44),
			new CollisionRectangle(4, 0, 26, 44),
			new CollisionRectangle(4, 0, 26, 44),
			new CollisionRectangle(10, 0, 40, 72),
			new CollisionRectangle(10, 0, 40, 72),
			new CollisionRectangle(10, 0, 40, 72),
			new CollisionRectangle(0, 0, 70, 106),
			new CollisionRectangle(0, 0, 70, 106),
			new CollisionRectangle(0, 0, 70, 106),
			new CollisionRectangle(0, 0, 70, 106),
			new CollisionRectangle(0, 0, 38, 60),
			new CollisionRectangle(0, 0, 38, 60),
			new CollisionRectangle(0, 0, 38, 60),
			new CollisionRectangle(0, 0, 38, 60),
			new CollisionRectangle(0, 0, 38, 64),
			new CollisionRectangle(16, 0, 28, 104),
			new CollisionRectangle(0, 0, 134, 84),
			new CollisionRectangle(0, 0, 62, 50),
			new CollisionRectangle(26, 0, 26, 76),
			new CollisionRectangle(26, 0, 26, 76),
			new CollisionRectangle(26, 0, 26, 76),
			new CollisionRectangle(0, 0, 82, 70),
			new CollisionRectangle(0, 0, 38, 60),
			new CollisionRectangle(0, 0, 38, 64),
			new CollisionRectangle(0, 0, 38, 64),
			new CollisionRectangle(0, 0, 70, 116),
			new CollisionRectangle(26, 0, 50, 110),
			new CollisionRectangle(0, 0, 38, 64),
			new CollisionRectangle(0, 0, 90, 88)
		]);
		private var id:int;
		private var main:AchievementPage;
		public function AchievementTrophy(main:AchievementPage, id:int) 
		{
			var ach:AchievementsIcons = new AchievementsIcons();
			super(ach, new CollisionRectangle(collision_list[id].Left,
				collision_list[id].Top, collision_list[id].Width, collision_list[id].Height), 32, 32);
			this.id = id;
			this.main = main;
			on_mouse_click_sound = null;
			on_mouse_over_sound = null;
			ach.SetAnimation(id);
			//View.alpha = 0.4;
		}
	}

}