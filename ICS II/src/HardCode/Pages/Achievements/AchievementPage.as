package HardCode.Pages.Achievements 
{
	import _Base.MyTextField;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import HardCode.Pages.Main.AchievementButton;
	import HardCode.Tiles.StaticGrassTile;
	import Resources.BlackMirror.Character.GoatSprite;
	import Resources.lib.Other_pages.AchievementLockedSprite;
	import Resources.lib.Other_pages.AchievementsBackgroundImage;
	import Resources.lib.Other_pages.AchievementsIcons;
	import Resources.lib.Other_pages.MainMenuBackground;
	import Resources.lib.RocketLauncherSprite;
	import System.ControlCenter;
	import System.DataCenter;
	import System.MC.Buttons.FrameButton;
	import System.MC.Tile;
	import System.Pages;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class AchievementPage extends Pages 
	{
		//private static const trophy_locations:Vector.<Point> = Vector.<Point>([
		//	new Point(218, 148),new Point(250, 228),new Point(222, 232),new Point(194, 236),new Point(372, 200),
		//	new Point(280, 200),new Point(326, 204),new Point(536, 310),new Point(444, 174),new Point(516, 172),
		//	new Point(588, 174),new Point(102, 220),new Point(386, 356),new Point(478, 356),new Point(432, 354),
		//	new Point(438, 10), new Point(400, 32), new Point(482, 58), new Point(318, 368),new Point(258, 340),
		//	new Point(178, 340),new Point(98, 340), new Point(224, 72), new Point(148, 214),new Point(622, 286),
		//	new Point(334, 286),new Point(154, 24), new Point(302, 34), new Point(64, 10), 	new Point(64, 54)
		//]);
		private static const Names:Vector.<String> = Vector.<String>([
			"Thanks", 			"Half Row", 		"One Row", 			"One and Half Rows","Two Rows",
			"Two and Half Rows","All Rows", 		"Passing By", 		"Getting Addicted", "Fully Addicted",
			"",					"Sun Survivor"
		]);
		private static const Descriptions:Vector.<String> = Vector.<String>([
			"Finish 5 levels",
			"Finish 10 levels",
			"Finish 15 levels",
			"Finish 20 levels",
			"Finish 25 levels",
			"Finish 30 levels",
			"Finish 35 levels",
			"Finish 40 levels",
			"Finish 45 levels",
			"Finish 50 levels",
			"Play for 15 minutes",
			"Play for 30 minutes",
			"Play for 45 minutes",
			"Play for 60 minutes",
			"Burn 5 monsters",
			"Burn 20 monsters",
			"Burn 50 monsters",
			"Die",
			"Die 10 times",
			"Die 50 times",
			"Die 10 times in a single level",
			"Die from spike while burning",
			"Collect 10 treasure chests",
			"Collect 20 treasure chests!",
			"Collect 30 treasure chests",
			"Collect 40 treasure chests",
			"Collect 50 treasure chests",
			"Finish special level",
			"Visit credit page",
			"Visit sponsor's website",
		]);
		private var back:CloseButton;
		private var icon:AchievementsIcons;
		private var trophies:Vector.<AchievementTrophy>;
		private var descriptions:Vector.<MyTextField>;
		private var lock_sprite:Vector.<AchievementLockedSprite>;
		private var current_page:int;
		private var page_count:MyTextField;
		private var left_arrow:LeftArrow;
		private var right_arrow:RightArrow;
		public function AchievementPage() 
		{
			trophies = new Vector.<AchievementTrophy>(30);
			descriptions = new Vector.<MyTextField>(30);
			lock_sprite = new Vector.<AchievementLockedSprite>();
			page_count = new MyTextField();
			super();
			addChild(new AchievementsBackgroundImage());
			addChild((back = new CloseButton()).View);
			AddControlReceiver(back);
			for (var i:int = 0; i < 30; i++)
			{
				var ach:AchievementTrophy = new AchievementTrophy(this, i);
				ach.Left = (i % 2 == 0) ? 150 : 450;
				ach.Top = int((i % 10) / 2) * 50 + 120;
				//addChild(ach.View);
				//AddControlReceiver(ach);
				trophies[i] = ach;
				
				var des:MyTextField = new MyTextField(30, 0xffffff, false);
				des.x = ach.Right + 10;
				des.y = ach.Top + 10;
				des.text = Descriptions[i];
				des.width = 200;
				des.wordWrap = true;
				descriptions[i] = des;
				
				lock_sprite[i] = new AchievementLockedSprite();
				lock_sprite[i].x = ach.Left;
				lock_sprite[i].y = ach.Top;
			}
			page_count.autoSize = TextFieldAutoSize.CENTER;
			page_count.text = "3/3";
			page_count.x = 360;
			page_count.y = 392;
			addChild(page_count);
			left_arrow = new LeftArrow(this);
			left_arrow.Right = 360;
			left_arrow.Top = 387;
			right_arrow = new RightArrow(this);
			right_arrow.Left = 363 + page_count.textWidth;
			right_arrow.Top = 387;
			addChild(icon = new AchievementsIcons());
			icon.visible = false;
			icon.Left = 52;
			icon.Top = 432;
			current_page = 1;
			addChild(right_arrow.View);
			AddControlReceiver(right_arrow);
			addChild(left_arrow.View);
			AddControlReceiver(left_arrow);
			RefreshPage();
		}
		public function HideIcon():void
		{
			icon.visible = false;
		}
		public function MovePageRight():void
		{
			current_page = Math.min(3, current_page + 1);
			RefreshPage();
		}
		public function MovePageLeft():void
		{
			current_page = Math.max(0, current_page-1);
			RefreshPage();
		}
		protected override function onAddedToStage(e:Event):void
		{
			super.onAddedToStage(e);
			for (var i:int = 0; i < trophies.length; i++)
			{
				trophies[i].Enabled = DataCenter.IsAchievementUnlocked(i);
				descriptions[i].alpha = trophies[i].Enabled ? 1 : 0.5;
			}
			RefreshPage();
			HideIcon();
		}
		private function RefreshPage():void
		{
			var i:int = 0;
			for (i = 0; i < trophies.length; i++)
			{
				if (contains(trophies[i].View))
					removeChild(trophies[i].View);
				if (contains(descriptions[i]))
					removeChild(descriptions[i]);
				if (contains(lock_sprite[i]))
					removeChild(lock_sprite[i]);
			}
			for (i = 10 * (current_page - 1); i < 10 * current_page; i++)
			{
				addChild(trophies[i].View);
				addChild(descriptions[i]);
				if (!trophies[i].Enabled)
				{
					addChild(lock_sprite[i]);
				}
			}
			page_count.text = current_page + "/3";
			
			left_arrow.Enabled = current_page > 1;
			right_arrow.Enabled = current_page < 3;
		}
	}

}