package HardCode.Pages.Achievements 
{
	import _Base.MyTextField;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import HardCode.Pages.Main.MainPage;
	import Resources.Animation;
	import Resources.lib.GUI.QuitButtonBackground;
	import Resources.lib.Other_pages.AchievementCloseImage;
	import System.GamePage;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IUpdatable;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.Pages;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class CloseButton extends BrightnessButton implements IUpdatable
	{
		private var sprite:IAnimatable;
		public function CloseButton() 
		{
			var ren:RenderableObject = new RenderableObject();
			ren.addChild((sprite = new QuitButtonBackground()) as QuitButtonBackground);
			var text:MyTextField = new MyTextField(40, 0xffffff, false);
			text.text = "GO BACK";
			text.x = 28;
			ren.addChild(text);
			super(ren, new CollisionRectangle(0, 0, 111, 20), 64, -64, -128);
			Left = 318;
			Top = 455;
			(sprite as RenderableObject).addEventListener(Animation.FinishAnimation, onFinishedAnimation);
		}
		public function Update():void
		{
			sprite.Update();
		}
		public function get UpdateWhenAdvancingTime():Boolean { return false; }
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			GamePage.CloseAchievements();
			OnMouseLeave();
		}
		private function onFinishedAnimation(e:Event):void
		{
			sprite.Freeze = true;
			//sprite.SetAnimation(0);
		}
	}

}