package HardCode.Pages.Achievements 
{
	import Resources.lib.Other_pages.AchievementArrowLeftSprite;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class LeftArrow extends BrightnessButton 
	{
		private var main:AchievementPage;
		public function LeftArrow(main:AchievementPage) 
		{
			super(new AchievementArrowLeftSprite(), new CollisionRectangle(0, 0, 24, 34), 64, -64, -128);
			this.main = main;
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			main.MovePageLeft();
		}
	}

}