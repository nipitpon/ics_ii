package HardCode.Pages.Achievements 
{
	import _Base.MyTextField;
	import flash.filters.DropShadowFilter;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class AchievementDescription extends MyTextField 
	{
		
		public function AchievementDescription() 
		{
			super();
			defaultTextFormat = new TextFormat("Pixel Font", 25, 0xe3e0b2, true, false);
			x = 172;
			y = 436;
			width = 480;
			height = 92;
			wordWrap = true;
			filters = [new DropShadowFilter(1, -90, 0xCEA261, 1, 1, 1, 1)];
		}
		
	}

}