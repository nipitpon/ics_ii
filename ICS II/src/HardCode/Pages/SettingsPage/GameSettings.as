package HardCode.Pages.SettingsPage 
{
	import _Base.MyTextField;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import HardCode.Pages.Game.SettingsButton;
	import HardCode.Pages.Main.MainPage;
	import Resources.lib.GUI.TextPlatform;
	import Resources.lib.Sound.SFX.ButtonClick;
	import System.ControlCenter;
	import System.DataCenter;
	import System.GamePage;
	import System.Pages;
	import System.SoundCenter;
	import System.V.Bar;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class GameSettings extends Pages 
	{
		private var main:Pages;
		private var close:CloseButton;
		private var bgm_bar:BGMBar;
		private var sfx_bar:SFXBar;
		public function GameSettings(main:Pages, hasGrayBackground:Boolean) 
		{
			this.main = main;
			super();
			bgm_bar = new BGMBar();
			bgm_bar.addEventListener(Bar.ValueChanged, bgmBar_ValueChanged);
			bgm_bar.x = 295;
			bgm_bar.y = 342;
			sfx_bar = new SFXBar();
			sfx_bar.addEventListener(Bar.ValueChanged, sfxBar_ValueChanged);
			sfx_bar.x = 295;
			sfx_bar.y = 370;
			if (hasGrayBackground)
			{
				var shape:Shape = new Shape();
				shape.graphics.beginFill(0, 0.5);
				shape.graphics.drawRect(0, 0, 720, 540);
				shape.graphics.endFill();
				addChild(shape);
			}
			addChild((close = new CloseButton(this)).View);
			addChild(bgm_bar);
			addChild(sfx_bar);
			var text_platform:TextPlatform = new TextPlatform("SETTINGS");
			addChild(text_platform);
			text_platform.Left = 258;
			text_platform.Top = 466;
			sfx_bar.CurrentValue = SoundCenter.SFXVolume;
			bgm_bar.CurrentValue = SoundCenter.BGMVolume;
			AddControlReceiver(close);
		}
		public override function set visible(val:Boolean):void
		{
			super.visible = val;
			if (val)
			{
				sfx_bar.CurrentValue = SoundCenter.SFXVolume;
				bgm_bar.CurrentValue = SoundCenter.BGMVolume;
			}
			else
			{
				SoundCenter.SFXVolume = sfx_bar.CurrentValue;
				SoundCenter.BGMVolume = bgm_bar.CurrentValue;
			}
		}
		public override function get IsReceivingControl():Boolean { return super.IsReceivingControl && visible && alpha == 1; }
		private function bgmBar_ValueChanged(e:Event):void
		{
			SoundCenter.BGMVolume = bgm_bar.CurrentValue;
		}
		private function sfxBar_ValueChanged(e:Event):void
		{
			SoundCenter.SFXVolume = sfx_bar.CurrentValue;
			SoundCenter.PlaySFX(ButtonClick);
		}
		public function Close():void
		{
			if (main is MainPage)
				(main as MainPage).CloseSettingsPopup();
			else if (main is GamePage)
				GamePage.CloseSettingsPopup();
		}		
	}

}