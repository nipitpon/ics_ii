package HardCode.Pages.SettingsPage 
{
	import _Base.MyTextField;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import HardCode.Pages.Main.MainPage;
	import Resources.lib.GUI.ClearDataSprite;
	import Resources.lib.Sound.SFX.MissileExplode;
	import System.DataCenter;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ClearData extends BrightnessButton 
	{
		private var main:SettingsMain;
		public function ClearData(main:SettingsMain) 
		{
			this.main = main;
			super(new ClearDataSprite(), new CollisionRectangle(0, 0, 64, 64), 96, 32);
			Center = new Point(64, 450);
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			main.ToggleConfirmClearData();
		}
	}
}