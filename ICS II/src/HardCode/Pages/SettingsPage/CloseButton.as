package HardCode.Pages.SettingsPage 
{
	import _Base.MyTextField;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import HardCode.Pages.Main.MainPage;
	import Resources.lib.GUI.QuitButtonBackground;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.Pages;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class CloseButton extends BrightnessButton 
	{
		private var main:GameSettings;
		public function CloseButton(main:GameSettings) 
		{
			this.main = main;
			super(new QuitButtonBackground(), new CollisionRectangle(0, 0, 64, 64), 96, -96);
			Center = new Point(360, 288);
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			main.Close();
			super.OnMouseLeave();
		}
	}

}