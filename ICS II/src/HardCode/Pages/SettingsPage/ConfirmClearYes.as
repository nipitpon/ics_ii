package HardCode.Pages.SettingsPage 
{
	import _Base.MyTextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import System.DataCenter;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ConfirmClearYes extends BrightnessButton 
	{
		private var main:SettingsMain;
		public function ConfirmClearYes(main:SettingsMain) 
		{
			this.main = main;
			var ren:RenderableObject = new RenderableObject();
			var text:MyTextField = new MyTextField();
			text.defaultTextFormat = new TextFormat("Pixel Font", 20, 0xdadada);
			text.autoSize = TextFieldAutoSize.LEFT;
			text.text = "Yes";
			ren.addChild(text);
			super(ren, new CollisionRectangle(0, 0, text.textWidth, 25), 96, -96);
			
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			main.ToggleConfirmClearData();
			DataCenter.ClearSaveData(true);
			OnMouseLeave();
		}
	}

}