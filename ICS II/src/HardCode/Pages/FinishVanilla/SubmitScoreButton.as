package HardCode.Pages.FinishVanilla 
{
	import _Base.MyTextField;
	import com.spilgames.gameapi.GameAPI;
	import flash.events.Event;
	import flash.filters.BlurFilter;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import Resources.lib.Other_pages.SubmitScoreSprite;
	import System.DataCenter;
	import System.GameSystem;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.ModelViewController;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SubmitScoreButton extends BrightnessButton 
	{
		
		public function SubmitScoreButton() 
		{
			var ren:RenderableObject = new SubmitScoreSprite();
			super(ren, new CollisionRectangle(0, 0, ren.width, ren.height), 64, -64);
		}
		public override function get IsReceivingControl():Boolean { return !Main.IsFading; }
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			GameAPI.getInstance().Score.submit(DataCenter.CalculateScore());
		}
		
	}

}