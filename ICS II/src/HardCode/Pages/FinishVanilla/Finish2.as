package HardCode.Pages.FinishVanilla 
{
	import _Base.MyTextField;
	import flash.geom.ColorTransform;
	import flash.net.URLRequestHeader;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import Resources.lib.Other_pages.Finish2Background;
	import Resources.lib.Other_pages.MainMenuBackground;
	import System.DataCenter;
	import System.Pages;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class Finish2 extends Pages 
	{
		private var score:MyTextField;
		public function Finish2() 
		{
			super();
			
			var text:MyTextField = new MyTextField();
			text.defaultTextFormat = new TextFormat("Pixel Font", "30", 0xffffff, true, false, false, null, null, TextFormatAlign.CENTER);
			text.embedFonts = true;
			text.autoSize = TextFieldAutoSize.CENTER;
			text.text = "Disappointing, eh? Sorry....\n" +
				"Coded by Knight52    Music by Alec Shea    Arts by Miroko\n\n" +
				"Your score: ";
			text.multiline = true;
			text.x = 360 - text.textWidth / 2;
			text.y = 300;
			
			score = new MyTextField();
			score.defaultTextFormat = new TextFormat("Pixel Font", "60", 0xffffff, true, false, false, null, null, TextFormatAlign.CENTER);
			score.embedFonts = true;
			score.autoSize = TextFieldAutoSize.CENTER;
			score.text = "666,666";
			score.x = 360 - score.textWidth / 2;
			score.y = text.y + text.textHeight + 10
			var submit_score:SubmitScoreButton = new SubmitScoreButton();
			submit_score.CenterX = 360;
			submit_score.Top = score.y + score.textHeight + 10;
			
			var back:BackButton;
			addChild(new Finish2Background());
			addChild((back = new BackButton()).View);
			//addChild(submit_score.View);
			addChild(score);
			addChild(text);
			
			AddControlReceiver(back);
			//AddControlReceiver(submit_score);
		}
		public override function Start():void
		{
			super.Start();
			
			this.score.text = DataCenter.CalculateScore().toString();
			this.score.x = 360 - this.score.textWidth / 2;
		}
	}

}