package HardCode.Pages.FinishVanilla 
{
	import _Base.MySound;
	import _Base.MyTextField;
	import flash.events.Event;
	import flash.geom.ColorTransform;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import HardCode.GUI.MoreGameButton;
	import HardCode.GUI.Sponsor300x80;
	import HardCode.Tiles.StaticGrassTile;
	import Resources.lib.Environment.MoonSprite;
	import Resources.lib.Environment.PlaneSprite;
	import Resources.lib.Environment.StarSprite;
	import Resources.lib.Other_pages.BronzeMedal;
	import Resources.lib.Other_pages.Finish1BackgroundSprite;
	import Resources.lib.Other_pages.GoldMedal;
	import Resources.lib.Other_pages.MainMenuBackground;
	import Resources.lib.Other_pages.SilverMedal;
	import Resources.lib.Sound.BGM.NightLevelBGM;
	import Resources.lib.Sound.BGM.SkyLevelBGM;
	import Resources.Resource;
	import System.DataCenter;
	import System.GameSystem;
	import HardCode.Pages.FinishVanilla.BackButton;
	import System.MC.etc.TimeAttackLevelInfo;
	import System.MC.Tile;
	import System.MC.WaterBlock;
	import System.Pages;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FinishVanilla extends Pages 
	{
		private var back:BackButton;
		private var text:MyTextField;
		private var header:MyTextField;
		private var score:MyTextField;
		public function FinishVanilla() 
		{
			super();
			
			var ambientAffected:RenderableObject = new RenderableObject();
			//ambientAffected.transform.colorTransform = new ColorTransform(0x47 / 255.0, 0x47 / 255.0, 0x40 / 255.0);
			text = new MyTextField();
			text.defaultTextFormat = new TextFormat("Pixel Font", "30", 0xffffff, true, false, false, null, null, TextFormatAlign.CENTER);
			text.embedFonts = true;
			text.autoSize = TextFieldAutoSize.CENTER;
			text.text = "So, he has reached city of Arterius,\n" +
				"where he will face his enemy in the fierceful death race!.\n" +
				"Too bad he wants to do it by himself.\n\n" +
				"But you can help him reach the ultimate ending by\n" +
				"collecting all secret items to unlock the secret level.\n\n" +
				"But no matter you want to collect them all or not,\n" +
				"thanks for playing!!\n\n\n" +
				"Your score:";
			text.multiline = true;
			text.x = 360 - text.textWidth / 2;
			text.y = 150;
			
			score = new MyTextField();
			score.defaultTextFormat = new TextFormat("Pixel Font", "60", 0xffffff, true, false, false, null, null, TextFormatAlign.CENTER);
			score.embedFonts = true;
			score.autoSize = TextFieldAutoSize.CENTER;
			score.text = "666,666";
			score.x = 360 - score.textWidth / 2;
			score.y = text.y + text.textHeight + 10;
			
			header = new MyTextField();
			header.defaultTextFormat = new TextFormat("Pixel Font", "70", 0xffffff, true, false, false, null, null, TextFormatAlign.CENTER);
			header.embedFonts = true;
			header.autoSize = TextFieldAutoSize.CENTER;
			header.text = "THE END";
			header.multiline = true;
			
			header.x = 360 - header.textWidth / 2;
			header.y = 80;
			
			var background:MainMenuBackground = new MainMenuBackground();
			background.transform.colorTransform = new ColorTransform(0, 0xA / 255.0, 0x35 / 255.0);
			addChild(ambientAffected);
			var moon:MoonSprite = new MoonSprite();
			var plane:PlaneSprite = new PlaneSprite();
			var submit_score:SubmitScoreButton = new SubmitScoreButton();
			submit_score.CenterX = 360;
			submit_score.Top = score.y + score.textHeight + 10;
			var more_game:MoreGameButton = new MoreGameButton();
			more_game.Left = 10;
			more_game.Top = 10;
			
			plane.Left = Tile.Size.x * -5;
			plane.Top = Tile.Size.y * 11.5;
			ambientAffected.addChild(background);
			ambientAffected.addChild(more_game.View);
			ambientAffected.addChild(moon);
			ambientAffected.addChild(new StarSprite());
			ambientAffected.addChild(plane);
			ambientAffected.addChild(new Finish1BackgroundSprite());
			moon.x = 50;
			moon.y = 50;
			//moon.NegateAmbient(0x47 / 255.0, 0x47 / 255.0, 0x40 / 255.0);
			//addChild(submit_score.View);
			addChild((back = new BackButton()).View);
			addChild(text);
			addChild(score);
			addChild(header);
			
			AddControlReceiver(back);
			//AddControlReceiver(submit_score);
			AddControlReceiver(more_game);
		}
		public override function Start():void
		{
			super.Start();
			SoundCenter.PlayBGM(SkyLevelBGM);
			
			this.score.text = DataCenter.CalculateScore().toString();
			this.score.x = 360 - this.score.textWidth / 2;
		}
	}

}