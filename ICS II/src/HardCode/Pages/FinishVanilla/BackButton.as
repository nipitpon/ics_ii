package HardCode.Pages.FinishVanilla 
{
	import _Base.MyTextField;
	import flash.filters.BlurFilter;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import System.GameSystem;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class BackButton extends BrightnessButton 
	{
		
		public function BackButton() 
		{
			var ren:RenderableObject = new RenderableObject();
			var text:MyTextField = new MyTextField();
			text.defaultTextFormat = new TextFormat("Pixel Font", Tile.Size.y, 0xdadada, true, false, false, null, null, TextFormatAlign.CENTER);
			text.embedFonts = true;
			text.autoSize = TextFieldAutoSize.LEFT;
			text.text = "Back to main menu";
			ren.addChild(text);
			super(ren, new CollisionRectangle(0, 0, text.textWidth, 40), 64, -64, -128);
			Center = new Point(360, 488);
		}
		public override function get IsReceivingControl():Boolean { return !Main.IsFading; }
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			GameSystem.SetGameMode(80);
			Main.ChangeToGamePage();
			super.OnMouseLeave();
		}
		
	}

}