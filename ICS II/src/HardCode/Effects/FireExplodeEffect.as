package HardCode.Effects 
{
	import flash.events.Event;
	import Resources.Animation;
	import Resources.lib.Effects.FireExplodeSprite;
	import System.GameFrame;
	import System.Interfaces.IUpdatable;
	import System.MC.Overlay;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FireExplodeEffect extends Overlay
	{
		
		public function FireExplodeEffect() 
		{
			super(new FireExplodeSprite(), Location_Foreground);
			View.addEventListener(Animation.FinishAnimation, onFinishAnimation);
		}
		private function onFinishAnimation(e:Event):void
		{
			GameFrame.RemoveChild(this);
		}
		
	}

}