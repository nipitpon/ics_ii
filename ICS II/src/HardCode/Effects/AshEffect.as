package HardCode.Effects 
{
	import fl.transitions.easing.None;
	import flash.geom.Point;
	import Resources.lib.Effects.AshSprite;
	import System.GameFrame;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.ICacher;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IOverlay;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.MovableObject;
	import System.MC.Overlay;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class AshEffect extends CollidableParticle
	{
		
		public function AshEffect(size:int, cacher:ICacher) 
		{
			super(new AshSprite(size), new CollisionRectangle(0, 0, size, size), cacher);
		}
	}

}