package HardCode.Effects 
{
	import flash.events.Event;
	import Resources.Animation;
	import Resources.lib.Effects.FireParticleSprite;
	import Resources.lib.Effects.SmokeEffectSprite;
	import System.GameFrame;
	import System.Interfaces.ICachable;
	import System.Interfaces.ICacher;
	import System.Interfaces.IUpdatable;
	import System.MC.Overlay;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FireParticle extends Overlay implements IUpdatable, ICachable
	{
		private var cacher:ICacher
		public function FireParticle(cacher:ICacher) 
		{
			this.cacher = cacher;
			super(new FireParticleSprite(), Location_CharacterForeground);
			View.addEventListener(Animation.FinishAnimation, onFinishAnimation);
		}
		public function Update():void
		{
			MoveByOffset(0, -4);
		}
		public function get UpdateWhenAdvancingTime():Boolean { return true; }
		private function onFinishAnimation(e:Event):void
		{
			GameFrame.RemoveChild(this);
			if(cacher != null)
				cacher.Cache(this);
		}
		
	}

}