package HardCode.Effects 
{
	import flash.events.Event;
	import Resources.Animation;
	import Resources.AnimationEvent;
	import Resources.lib.Effects.LandingDustSprite;
	import Resources.lib.Effects.WaterSprite;
	import Resources.Resource;
	import System.GameFrame;
	import System.Interfaces.IAnimatable;
	import System.MC.Effects.Effect;
	import System.MC.Overlay;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class LandingDustEffect extends Overlay 
	{
		
		public function LandingDustEffect() 
		{
			super(new LandingDustSprite(), Location_FloorForeground);
			View.addEventListener(Animation.FinishAnimation, onFinishAnimation);
			
		}
		private function onFinishAnimation(e:Event):void
		{
			View.removeEventListener(Animation.FinishAnimation, onFinishAnimation);
			GameFrame.RemoveChild(this);
		}
	}

}