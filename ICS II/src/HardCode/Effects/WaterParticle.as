package HardCode.Effects 
{
	import fl.transitions.easing.None;
	import flash.geom.Point;
	import Resources.lib.Effects.AshSprite;
	import Resources.lib.Effects.WaterEffectSprite;
	import System.GameFrame;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.ICacher;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IOverlay;
	import System.Interfaces.IUpdatable;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.MovableObject;
	import System.MC.Overlay;
	import System.MC.Tile;
	import System.Overseer;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class WaterParticle extends CollidableParticle
	{
		public function WaterParticle(force_x:Number, cacher:ICacher) 
		{
			super(new WaterEffectSprite(), new CollisionRectangle(6, 6, 8, 8), cacher);
			Push(force_x, -Utility.Random(18, 23));
		}
		public override function get DefyGravity():Boolean { return (View as IAnimatable).CurrentAnimation == 1; }
		public override function Hit(obj:ICollidable, movement:Point):void
		{
			if ((View as IAnimatable).CurrentAnimation == 1) return;
			else super.Hit(obj, movement);
		}
		protected override function StartCounting():void
		{
			super.StartCounting();
			(View as IAnimatable).SetAnimation(1);
			collide_with = Vector.<Class>([]);
		}
	}

}