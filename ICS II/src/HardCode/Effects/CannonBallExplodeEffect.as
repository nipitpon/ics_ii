package HardCode.Effects 
{
	import flash.events.Event;
	import Resources.Animation;
	import Resources.lib.Effects.CannonBallExplosionSprite;
	import Resources.lib.Effects.FireExplodeSprite;
	import System.GameFrame;
	import System.Interfaces.IUpdatable;
	import System.MC.Overlay;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class CannonBallExplodeEffect extends Overlay
	{
		
		public function CannonBallExplodeEffect() 
		{
			super(new CannonBallExplosionSprite(), Location_Foreground);
			View.addEventListener(Animation.FinishAnimation, onFinishAnimation);
		}
		private function onFinishAnimation(e:Event):void
		{
			GameFrame.RemoveChild(this);
		}
		
	}

}