package HardCode.Effects 
{
	import fl.transitions.easing.None;
	import flash.events.Event;
	import Resources.Animation;
	import Resources.lib.Effects.SlidingDustSprite;
	import Resources.lib.Effects.SmokeEffectSprite;
	import System.GameFrame;
	import System.Interfaces.ICachable;
	import System.Interfaces.ICacher;
	import System.Interfaces.IUpdatable;
	import System.MC.Overlay;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SlidingDustParticle extends Overlay implements ICachable
	{
		private var cacher:ICacher
		public function SlidingDustParticle(cacher:ICacher) 
		{
			this.cacher = cacher;
			super(new SlidingDustSprite(), Location_CharacterForeground);
			View.addEventListener(Animation.FinishAnimation, onFinishAnimation);
		}
		private function onFinishAnimation(e:Event):void
		{
			GameFrame.RemoveChild(this);
			if(cacher != null)
				cacher.Cache(this);
		}
		
	}

}