package HardCode.Effects 
{
	import flash.events.Event;
	import Resources.Animation;
	import Resources.lib.Effects.CannonBallExplosionSprite;
	import Resources.lib.Effects.FireExplodeSprite;
	import Resources.lib.Effects.TileBurntSprite;
	import System.GameFrame;
	import System.Interfaces.IUpdatable;
	import System.MC.Overlay;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class TileBurntEffect extends Overlay
	{
		
		public function TileBurntEffect() 
		{
			super(new TileBurntSprite(), Location_Floor);
			View.addEventListener(Animation.FinishAnimation, onFinishAnimation);
		}
		private function onFinishAnimation(e:Event):void
		{
			GameFrame.RemoveChild(this);
		}
		
	}

}