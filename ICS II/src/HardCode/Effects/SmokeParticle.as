package HardCode.Effects 
{
	import fl.transitions.easing.None;
	import flash.events.Event;
	import Resources.Animation;
	import Resources.lib.Effects.SmokeEffectSprite;
	import System.GameFrame;
	import System.Interfaces.ICachable;
	import System.Interfaces.ICacher;
	import System.Interfaces.IUpdatable;
	import System.MC.Overlay;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SmokeParticle extends Overlay implements IUpdatable, ICachable
	{
		private var cacher:ICacher
		public function SmokeParticle(cacher:ICacher) 
		{
			this.cacher = cacher;
			super(new SmokeEffectSprite(), Location_CharacterForeground);
			View.addEventListener(Animation.FinishAnimation, onFinishAnimation);
		}
		public function Update():void
		{
			MoveByOffset(0, -2);
		}
		public function get UpdateWhenAdvancingTime():Boolean { return true; }
		private function onFinishAnimation(e:Event):void
		{
			GameFrame.RemoveChild(this);
			if(cacher != null)
				cacher.Cache(this);
		}
	}
}