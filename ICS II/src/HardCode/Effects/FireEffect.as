package HardCode.Effects 
{
	import flash.events.Event;
	import Resources.Animation;
	import Resources.lib.Effects.FireSprite;
	import Resources.lib.Effects.TileFireEffectSprite;
	import Resources.lib.Sound.SFX.FireExhaustedSound;
	import System.GameFrame;
	import System.Interfaces.IAnimatable;
	import System.MC.Effects.Effect;
	import System.MC.Overlay;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FireEffect extends Overlay
	{
		
		public function FireEffect(box:Boolean) 
		{
			super((!box) ? new FireSprite() : new TileFireEffectSprite(), Overlay.Location_CharacterBackground);
			BottomMost = true;
		}
		public function Reset():void
		{
			(View as IAnimatable).SetAnimation(0);
			View.removeEventListener(Animation.FinishAnimation, onFinishAnimation);
		}
		public function Exhaust():void
		{
			if ((View as IAnimatable).CurrentAnimation != 1)
			{
				(View as IAnimatable).SetAnimation(1);
				View.addEventListener(Animation.FinishAnimation, onFinishAnimation);
			}
		}
		private function onFinishAnimation(e:Event):void
		{
			View.removeEventListener(Animation.FinishAnimation, onFinishAnimation);
			GameFrame.RemoveChild(this);
		}
	}

}