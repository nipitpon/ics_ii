package HardCode.Effects 
{
	import fl.transitions.easing.None;
	import flash.geom.Point;
	import System.GameFrame;
	import System.Interfaces.ICachable;
	import System.Interfaces.ICacher;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IOverlay;
	import System.Interfaces.IUpdatable;
	import System.MC.Collision.CollisionBox;
	import System.MC.MovableObject;
	import System.MC.Overlay;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class CollidableParticle extends MovableObject implements IOverlay, IUpdatable, ICachable
	{
		private static const DisappearCount:int = Utility.SecondToFrame(1);
		private var count:int;
		private var location:int;
		private var bottom_most:Boolean;
		private var cacher:ICacher;
		public function CollidableParticle(view:RenderableObject, collisionBox:CollisionBox, cahcer:ICacher) 
		{
			this.cacher = cacher;
			super(view, collisionBox);
			collide_with = Vector.<Class>([Tile]);
			count = -1;
			location = Overlay.Location_CharacterBackground
		}
		public override function Update():void
		{
			super.Update();
			if (count >= 0)
			{
				View.alpha = None.easeNone(DisappearCount - count, 1, -1, DisappearCount);
				if (--count < 0)
				{
					GameFrame.RemoveChild(this);
					if(cacher != null)
						cacher.Cache(this);
				}
			}
			else if (ForceY >= 0 && location != Overlay.Location_CharacterForeground)
			{
				GameFrame.RemoveChild(this);
				location = Overlay.Location_CharacterForeground;
				GameFrame.AddChild(this);
			}
		}
		public override function Intersects(box:CollisionBox, movement:Point):ICollidable
		{
			if (count >= 0) return null;
			else return super.Intersects(box, movement);
		}
		public override function get DefyGravity():Boolean { return count >= 0; }
		public function set BottomMost(val:Boolean):void { bottom_most = val; }
		public function get BottomMost():Boolean { return bottom_most; }
		public function set OverlayLevel(val:int):void { location = val; }
		public function get OverlayLevel():int { return location; }
		public override function Hit(obj:ICollidable, movement:Point):void
		{
			if (movement.y < 0)
			{
				movement.y = 0;
			}
			else if (movement.y > 0)
			{
				if (count < 0)
				{
					StartCounting();
				}
				ClearAllForce();
				collide_with = Vector.<Class>([]);
				movement.y = Math.max(obj.Collision.GetTopAtPos(Collision.Left, Collision.Right, Collision.CenterY + movement.y) - View.Bottom, -movement.y);
			}
		}
		protected function StartCounting():void
		{
			count = DisappearCount;
		}
		
	}

}