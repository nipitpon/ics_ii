package HardCode.Effects 
{
	import flash.events.Event;
	import flash.geom.Point;
	import Resources.lib.Effects.TreasureCoinSprite;
	import Resources.lib.Effects.TreasureLightSprite;
	import System.GameFrame;
	import System.Interfaces.IOverlay;
	import System.Interfaces.IUpdatable;
	import System.MC.Effects.Effect;
	import System.MC.Overlay;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class TreasureCoinEffect extends Effect implements IOverlay, IUpdatable 
	{
		private var coin:TreasureCoinSprite;
		private var light:TreasureLightSprite;
		private var count:int;
		public function TreasureCoinEffect() 
		{
			var ren:RenderableObject = new RenderableObject();
			ren.addChild(light = new TreasureLightSprite());
			ren.addChild(coin = new TreasureCoinSprite());
			coin.x = 17;
			coin.y = 17;
			super(ren);
			count = Utility.SecondToFrame(1);
		}
		public function Update():void 
		{
			if (--count < 0)
			{
				GameFrame.RemoveChild(this);
			}
			else
			{
				CenterY -= (3 * (count) / Utility.SecondToFrame(1));
				light.rotation += 3;
				if (count < 5)
					View.alpha = count / 5;
			}
		}
		
		public function get UpdateWhenAdvancingTime():Boolean { return false; }
		public function set BottomMost(value:Boolean):void { }
		public function get BottomMost():Boolean { return false; }
		public function get OverlayLevel():int { return Overlay.Location_Foreground; }
	}

}