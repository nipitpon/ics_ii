package HardCode.Effects 
{
	import flash.events.Event;
	import Resources.Animation;
	import Resources.lib.Effects.FireExplodeSprite;
	import Resources.lib.Effects.WaterballExplodeSprite;
	import System.GameFrame;
	import System.Interfaces.IUpdatable;
	import System.MC.Overlay;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class WaterballExplodeEffect extends Overlay
	{
		
		public function WaterballExplodeEffect() 
		{
			super(new WaterballExplodeSprite(), Location_Foreground);
			View.addEventListener(Animation.FinishAnimation, onFinishAnimation);
		}
		private function onFinishAnimation(e:Event):void
		{
			GameFrame.RemoveChild(this);
		}
		
	}

}