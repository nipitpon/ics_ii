package HardCode 
{
	import HardCode.BlackMirror.Level.Level1;
	import HardCode.Levels.IntroLevel1;
	import System.DataCenter;
	import System.GameSystem;
	import System.MC.Characters.Character;
	import System.MC.Skills.Skill;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Skill_StartVanilla extends Skill 
	{
		
		public function Skill_StartVanilla() 
		{
			super(null, 0, 0, 0, 0);
			
		}
		public override function Activate():void
		{
			GameSystem.SetLevel(new IntroLevel1(2));
		}
		public override function Deactivate():void
		{
		}
		
	}

}