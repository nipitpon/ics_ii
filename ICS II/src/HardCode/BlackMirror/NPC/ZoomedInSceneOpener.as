package HardCode.BlackMirror.NPC 
{
	import System.GameFrame;
	import System.GUI;
	import System.Interfaces.IInteractor;
	import System.MC.Collision.CollisionBox;
	import System.MC.etc.ZoomedInSceneInfo;
	import System.MC.Interactables.InteractableObject;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ZoomedInSceneOpener extends InteractableObject 
	{
		protected var scene:ZoomedInSceneInfo;
		public function ZoomedInSceneOpener(view:RenderableObject, collisionBox:CollisionBox, interactionText:String) 
		{
			super(view, collisionBox, interactionText);
		}
		public override function GetInteracted(interactor:IInteractor):void
		{
			if (scene != null)
			{
				GameFrame.ZoomIn(scene);
			}
		}
	}

}