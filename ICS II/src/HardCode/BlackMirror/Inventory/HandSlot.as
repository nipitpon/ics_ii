package HardCode.BlackMirror.Inventory 
{
	import HardCode.BlackMirror.ZoomedInScene.ZoomedInButton;
	import Resources.BlackMirror.Inventory.HandSlotBackground;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class HandSlot extends ZoomedInButton 
	{
		
		public function HandSlot() 
		{
			super(new HandSlotBackground(), new CollisionRectangle(0, 0, 54, 54), true);
			Top = 33;
			Left = 360;
		}
		
	}

}