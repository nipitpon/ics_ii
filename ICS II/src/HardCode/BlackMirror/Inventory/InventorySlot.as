package HardCode.BlackMirror.Inventory 
{
	import HardCode.BlackMirror.ZoomedInScene.ZoomedInButton;
	import Resources.BlackMirror.Inventory.SlotBackground;
	import System.DataCenter;
	import System.GameFrame;
	import System.GameSystem;
	import System.InventoryScene;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class InventorySlot extends ZoomedInButton 
	{
		private var item_layer:RenderableObject;
		private var item_code:int;
		public function InventorySlot(x:int, y:int) 
		{
			var view:RenderableObject = new RenderableObject();
			view.addChild(new SlotBackground());
			view.addChild(item_layer = new RenderableObject());
			super(view, new CollisionRectangle(0, 0, 54, 54), true);
			Top = 90 + (57 * y);
			Left = 303 + (57 * x);
			item_code = -1;
		}
		public function get HasItem():Boolean { return item_code >= 0; }
		public function get ItemCode():int { return item_code; }
		public function SetItem(code:int):void
		{
			item_code = code;
			item_layer.removeChildren();
			if (code >= 0)
			{
				item_layer.addChild(DataCenter.GetItemIcon(item_code));
			}
		}
		public function RemoveItem():void
		{
			SetItem( -1);
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			if (item_code >= 0)
			{
				if (!GameSystem.IsHoldingObject)
				{
					GameSystem.HoldObject(item_code);
					RemoveItem();
				}
				else
				{
					var code:int = GameSystem.HoldingObjectCode;
					GameSystem.HoldObject(item_code);
					SetItem(code);
				}
			}
			else if (GameSystem.IsHoldingObject)
			{
				SetItem(GameSystem.HoldingObjectCode);
				GameSystem.ReleaseObject();
			}
		}
		protected override function OnMouseOver():void
		{
			super.OnMouseOver();
			if(item_code >= 0)
				GameFrame.Inventory.CurrentText = DataCenter.GetItemName(item_code);
		}
		protected override function OnMouseLeave():void
		{
			super.OnMouseLeave();
			if (item_code >= 0 && GameFrame.Inventory.CurrentText == DataCenter.GetItemName(item_code))
			{
				GameFrame.Inventory.CurrentText = "";
			}
		}
	}
}