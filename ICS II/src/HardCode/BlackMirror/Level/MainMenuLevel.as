package HardCode.BlackMirror.Level 
{
	import _Base.MySound;
	import _Base.MyTextField;
	import flash.display.Bitmap;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.SecureSocket;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.ByteArray;
	import HardCode.Goat.Goat;
	import HardCode.Goat.GoatSpawnPoint;
	import HardCode.GUI.MoreGameButton;
	import HardCode.NPC.BlueLever;
	import HardCode.NPC.Desk;
	import HardCode.NPC.Documents;
	import HardCode.NPC.FinalWarpSpot;
	import HardCode.NPC.FirePotion;
	import HardCode.NPC.GreenLever;
	import HardCode.NPC.RedLever;
	import HardCode.NPC.Toolboard;
	import HardCode.NPC.TrophyShelf;
	import HardCode.NPC.WarpSpot;
	import HardCode.NPC.WaterBottle;
	import HardCode.NPC.WaterBucket;
	import HardCode.NPC.WeatherBender;
	import HardCode.Overlays.DirtOverlay;
	import HardCode.PressurePlatforms.BluePressurePlatform;
	import HardCode.PressurePlatforms.GreenPressurePlatform;
	import HardCode.PressurePlatforms.RedPressurePlatform;
	import HardCode.Tiles.BlueDoorTile;
	import HardCode.Tiles.BoxTile;
	import HardCode.Tiles.DoorTile;
	import HardCode.Tiles.DynamicGrassTile;
	import HardCode.Tiles.FadeOutOnTouchTile;
	import HardCode.Tiles.GreenDoorTile;
	import HardCode.Tiles.RedDoorTile;
	import HardCode.Tiles.RocketLauncherTile;
	import HardCode.Tiles.SpikeTile;
	import HardCode.Tiles.StaticGrassTile;
	import HardCode.Tiles.StaticTile;
	import Resources.BlackMirror.Character.GoatSprite;
	import Resources.BlackMirror.Level.Level1.CloseBackground1;
	import Resources.Image;
	import Resources.lib.DirtTileSprite;
	import Resources.lib.Doors.BlueDoorSprite;
	import Resources.lib.Environment.Vine1;
	import Resources.lib.Environment.Vine2;
	import Resources.lib.Environment.Weed;
	import Resources.lib.Other_pages.ControlImage;
	import Resources.lib.Other_pages.MainMenuBackgroundSprite;
	import Resources.lib.Other_pages.MainMenuForegroundSprite;
	import Resources.lib.Other_pages.MainMenuLevelNearBackground;
	import Resources.lib.Other_pages.PressingButtonSprite;
	import Resources.lib.Sound.BGM.MainMenuBGM;
	import System.ControlCenter;
	import System.DataCenter;
	import System.GamePage;
	import System.GameSystem;
	import System.Interfaces.IMovable;
	import System.Interfaces.IMVC;
	import System.Interfaces.INamable;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.GrassTile;
	import System.MC.Interactables.InteractableObject;
	import System.MC.Ladder.Ladder;
	import System.MC.Level;
	import System.MC.Overlay;
	import System.MC.SpawnPoint;
	import System.MC.Tile;
	import System.MC.WaterBlock;
	import System.MC.Weather;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class MainMenuLevel extends Level 
	{
		private static var visited:Boolean = false;
		[Embed(source = "../../../../lib/Maps/main_menu.json", mimeType = "application/octet-stream")]
		private static const map:Class;
		public function MainMenuLevel(start_hour:int=9, start_minute:int=0, start_second:int=0) 
		{
			super(start_hour, start_minute, start_second);
			
			var b:ByteArray = new map();
			var info:Object = JSON.parse(b.readUTFBytes(b.length));
			AddOverlay(new Overlay(new MainMenuBackgroundSprite(), Overlay.Location_FloorBackground));
			AddOverlay(new Overlay(new MainMenuForegroundSprite(), Overlay.Location_Foreground));
			LoadJSON(info, 55);
			
			//InsertTerrain();
			//starting_point = new Point(Tile.Size.x * 12 + 40, Tile.Size.y * 14);
			//
			//InsertInteractables();
			InsertOverlay();
			//InsertSpawnPoints();
			//InsertLadders();
			//InsertObjects();
			show_paused_button = false;
			AddIndoorArea(new Rectangle(0, 0, 1024, 768));
			
			var found:Boolean = false;
			CONFIG::release
			{
				for (var i:int = 1; i <= 50 && !found; i++)
				{
					if (!DataCenter.SecretItemCollected(i))
					{
						found = true;
					}
				}
			}
			if (!found)
			{
				var obj:Vector.<INamable> = GetObjectByNames("YellowDoor");
				for each(var door:INamable in obj)
				{
					(door as DoorTile).Activate();
				}
			}
		}   
		public override function get FreezeTime():Boolean { return true; }
		public override function get OverrideBGM():Class { return MainMenuBGM; }
		public override function Start():void
		{
			super.Start();
			if (!visited && DataCenter.GetDeathCount(1) < 0)
			{
				(GameSystem.GetObjectByName("settings") as InteractableObject).ShowNameTag();
				(GameSystem.GetObjectByName("desk") as InteractableObject).ShowNameTag();
				(GameSystem.GetObjectByName("credit") as InteractableObject).ShowNameTag();
				(GameSystem.GetObjectByName("achievements") as InteractableObject).ShowNameTag();
				visited = true;
			}
		}
		public override function Stop():void
		{
			super.Stop();
		}
		private function InsertLadders():void
		{
		}
		private function InsertObjects():void
		{
		}
		private function InsertSpawnPoints():void
		{
		}
		private function InsertTerrain():void
		{
			TerrainLayer.SetTileAt(0, 0, new StaticTile(Vector.<Vector.<int>>([
				Vector.<int>([-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]), //0
				Vector.<int>([-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]), //1
				Vector.<int>([-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]), //2
				Vector.<int>([-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]), //3
				Vector.<int>([-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]), //4
				Vector.<int>([-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]), //5
				Vector.<int>([-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,42,43,44,-1,-1]), //6
				Vector.<int>([-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]), //7
				Vector.<int>([-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]), //8
				Vector.<int>([-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]), //9
				Vector.<int>([43,44,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]), //10
				Vector.<int>([-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,08,08,08,08]), //11
				Vector.<int>([-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,08,08,08,08]), //12
				Vector.<int>([-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,08,08,08,08,08,08]), //13
				Vector.<int>([-1,-1,-1,-1,-1,-1,-1,-1,38,38,38,-1,-1,-8,-8,-1,-1,08,08,08,08,08,08]), //14
				Vector.<int>([38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38,38]), //15
				Vector.<int>([07,07,07,07,07,07,07,07,07,07,07,07,07,07,07,07,07,07,07,07,07,07,07])  //16
				//			  00             05             10             15             20             25             30             35             40             45             50
			])));
			AddIndoorArea(new Rectangle(0, 0, Tile.Size.x * 24, 540));
		}
		private function InsertInteractables():void
		{
		}
		private function InsertOverlay():void
		{
			var ovr:Overlay = new Overlay(new PressingButtonSprite(), Overlay.Location_FloorForeground);
			ovr.CenterX = Tile.Size.x * 3;
			ovr.Bottom = Tile.Size.y * 14;
			//AddOverlay(ovr);
			//ovr = new Overlay(new ControlImage(), Overlay.Location_FloorBackground);
			//ovr.Top = Tile.Size.y * 9;
			//ovr.Left = Tile.Size.x * 7.5;
			////AddOverlay(ovr);
			//if(!DataCenter.SecretItemCollected(31))
			//	AddOverlay(new Treasure(31), Tile.Size.x * -1.3, Tile.Size.y * 13);
		}
		
	}

}