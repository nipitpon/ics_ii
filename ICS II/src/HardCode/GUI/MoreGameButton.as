package HardCode.GUI 
{
	import _Base.MyTextField;
	import com.spilgames.gameapi.GameAPI;
	import flash.display.DisplayObject;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import Resources.lib.GUI.MoreGameButtonSprite;
	import Resources.lib.Other_pages.Sponsor300x80Image;
	import System.Achievements;
	import System.Interfaces.IAnimatable;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Buttons.GUIButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	import Resources.*;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class MoreGameButton extends BrightnessButton 
	{
		
		public function MoreGameButton() 
		{
			var ren:RenderableObject = new MoreGameButtonSprite();
			//var text:MyTextField = new MyTextField();
			//text.defaultTextFormat = new TextFormat("Pixel Font", 35, 0xffff00, true, false, false, null, null, TextFormatAlign.CENTER);
			//text.width = 220;
			//text.height = 80;
			//text.text = "MORE GAMES";
			//text.y = (80 - text.textHeight) / 2;
			//ren.addChild(text);
			if (!GameAPI.getInstance().Branding.isReady)
				ren = new RenderableObject();
			super(ren, new CollisionRectangle(0, 0, 266, 64), 96, -96, -64);
		}
		protected override function OnMouseLeave():void
		{
			super.OnMouseLeave();
			(View as IAnimatable).SetAnimation(0);
		}
		protected override function OnMouseOver():void
		{
			super.OnMouseOver();
			(View as IAnimatable).SetAnimation(1);
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			Achievements.VisitSponsor();
			//var link:String = Main.BrandManager.getMoreGamesLink();
			//navigateToURL(new URLRequest(link));
			
			GameAPI.getInstance().Branding.getLink("more_games").action();
		}
	}

}