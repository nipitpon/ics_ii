package HardCode.GUI 
{
	import Resources.BlackMirror.GUI.InventoryButtonImage;
	import System.GameFrame;
	import System.GUI;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class InventoryButton extends BrightnessButton 
	{
		
		public function InventoryButton() 
		{
			super(new InventoryButtonImage(), new CollisionRectangle(0, 0, 40, 40, CollisionBox.IntersectDirection_All), 96, -96);
		}
		protected override function OnMouseOver():void
		{
			super.OnMouseOver();
			var p:String = "";
			if (GameFrame.IsOpeningInventory)
				p = "Close";
			else
				p = "Open";
			GUI.ShowDescriptionAsHtml(CenterX, CenterY, p + " inventory");
		}
		protected override function OnMouseLeave():void
		{
			super.OnMouseLeave();
			GUI.HideDescription();
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			if (GameFrame.ZoomedScene.IsAnimating) return;
			if (!GameFrame.IsOpeningInventory)
			{
				GameFrame.ShowInventory()
			}
			else
			{
				GameFrame.HideInventory();
			}
			super.OnMouseOver();
		}
		
	}

}