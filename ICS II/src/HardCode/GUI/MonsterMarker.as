package HardCode.GUI 
{
	import flash.display.Shape;
	import flash.events.Event;
	import flash.geom.Point;
	import System.GameFrame;
	import System.GameSystem;
	import System.GUI;
	import System.Interfaces.IUpdatable;
	import System.MC.Characters.Monster;
	import System.MC.Effects.Effect;
	import System.MC.ModelViewController;
	import System.MC.Overlay;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class MonsterMarker extends Overlay implements IUpdatable 
	{
		private var mon:Monster;
		public function MonsterMarker(mon:Monster) 
		{
			this.mon = mon;
			var ren:RenderableObject = new RenderableObject(new Point(25, 25));
			super(ren, Overlay.Location_TopMost);
			var shape:Shape = new Shape();
			
			shape.graphics.lineStyle(1, 0x880000);
			shape.graphics.beginFill(0xff0000);
			shape.graphics.moveTo(25, 50);
			shape.graphics.lineTo(0, 25);
			shape.graphics.lineTo(10, 25);
			shape.graphics.lineTo(8, 0);
			shape.graphics.lineTo(42, 0);
			shape.graphics.lineTo(40, 25);
			shape.graphics.lineTo(50, 25);
			shape.graphics.lineTo(25, 50);
			shape.graphics.endFill();
			ren.addChild(shape);
			mon.addEventListener(ModelViewController.Removed, onMarkingMonRemoved);
		}
		public function Update():void
		{
			if (mon.Collision.CenterX < GameFrame.ScreenLeft)
			{
				View.visible = true;
				View.rotation = 90;
				View.Center = new Point(GameFrame.ScreenLeft + 75, mon.Collision.CenterY);
			}
			else if (mon.Collision.CenterX > GameFrame.ScreenRight)
			{
				View.visible = true;
				View.rotation = 270;
				View.Center = new Point(GameFrame.ScreenRight - View.width / 2, mon.Collision.CenterY);
			}
			else
			{
				View.visible = false;
			}
		}
		public function get UpdateWhenAdvancingTime():Boolean { return true; }
		private function onMarkingMonRemoved(e:Event):void
		{
			if(GameFrame.ContainChild(this))
				GameFrame.RemoveChild(this);
			mon.removeEventListener(ModelViewController.Removed, onMarkingMonRemoved);
		}
	}
}