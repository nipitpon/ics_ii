package HardCode.GUI 
{
	import com.spilgames.gameapi.components.Logo;
	import com.spilgames.gameapi.GameAPI;
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import Resources.lib.Other_pages.Sponsor300x80Image;
	import System.Achievements;
	import System.ControlCenter;
	import System.Interfaces.ICollidable;
	import System.MC.Buttons.BrightnessButton;
	import System.MC.Buttons.GUIButton;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.SoundCenter;
	import System.V.RenderableObject;
	import Resources.*;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Sponsor300x80 extends BrightnessButton 
	{
		public function Sponsor300x80() 
		{
			var ren:RenderableObject = new RenderableObject();// new Sponsor300x80Image();
			var logo:Logo = GameAPI.getInstance().Branding.getLogo();
			if(GameAPI.getInstance().Branding.isReady)
				ren.addChild(logo);
			logo.y = 10;
			super(ren, new CollisionRectangle(0, 10, 200, 80), 96, -96);
			logo.addEventListener(MouseEvent.CLICK, function(e:Event):void { Achievements.VisitSponsor(); } );
			logo.addEventListener(MouseEvent.ROLL_OVER, function(e:Event):void
			{
				logo.transform.colorTransform = new ColorTransform(1, 1, 1, 1, 64, 64, 64);
			});
			logo.addEventListener(MouseEvent.ROLL_OUT, function(e:Event):void
			{
				logo.transform.colorTransform = new ColorTransform();
			});
			logo.addEventListener(MouseEvent.MOUSE_DOWN, function(e:Event):void
			{
				logo.transform.colorTransform = new ColorTransform(1, 1, 1, 1, -64, -64, -64);
			});
			logo.addEventListener(MouseEvent.MOUSE_UP, function(e:Event):void
			{
				logo.transform.colorTransform = new ColorTransform(1, 1, 1, 1, 64, 64, 64);
			});
		}
		protected override function OnMouseOver():void 
		{
			
		}
		protected override function OnMouseDown():void {}
		protected override function OnMouseClick():void
		{
			
			
		}
		
		public override function KeyPress(id:int):void
		{
		}
	}

}