package HardCode.NPC 
{
	import HardCode.Tiles.DoorTile;
	import Resources.lib.Sound.SFX.SwitchOffSound;
	import Resources.lib.Sound.SFX.SwitchOnSound;
	import System.GameSystem;
	import System.Interfaces.IActivatable;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IInteractor;
	import System.Interfaces.INamable;
	import System.Interfaces.ISavable;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Interactables.InteractableObject;
	import System.MC.Tile;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Lever extends InteractableObject implements ISavable
	{
		public static var Enabled:Boolean = true;
		private var door_name:String;
		private var anim:IAnimatable;
		public function Lever(view:RenderableObject, door_name:String) 
		{
			super(view, new CollisionRectangle(0, 0, Tile.Size.x, Tile.Size.y), "Trigger");
			anim = view as IAnimatable;
			this.door_name = door_name;
		}
		public function SetInfo(obj:Object):void
		{
			anim.SetAnimation(obj["animation"]);
			Enabled = obj["enabled"];
		}
		public function GetInfo():Object { return { "animation":anim.CurrentAnimation, "enabled":Enabled }; }	
		public override function get IsInteractable():Boolean { return super.IsInteractable && Enabled; }
		public override function GetInteracted(interactor:IInteractor):void
		{
			super.GetInteracted(interactor);
			if (anim.CurrentAnimation == 0) 
			{
				anim.SetAnimation(1);
				SoundCenter.PlaySFXFromInstance(new SwitchOnSound());
			}
			else 
			{
				anim.SetAnimation(0);
				SoundCenter.PlaySFXFromInstance(new SwitchOffSound());
			}
			
			var doors:Vector.<INamable> = GameSystem.GetObjectByNames(door_name);
			for each(var namable:INamable in doors)
			{
				(namable as IActivatable).Activate();
			}
		}
	}

}