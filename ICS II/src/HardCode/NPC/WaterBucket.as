package HardCode.NPC 
{
	import flash.events.Event;
	import HardCode.Effects.WaterEffect;
	import HardCode.Effects.WaterParticle;
	import Resources.Animation;
	import Resources.lib.NPC.WaterBucketSprite;
	import Resources.lib.Sound.SFX.WaterBottleOpen;
	import Resources.lib.Sound.SFX.WaterFountainSound;
	import System.GameFrame;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IInteractor;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Effects.Effect;
	import System.MC.Interactables.InteractableObject;
	import System.MC.Tile;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class WaterBucket extends WaterCacher 
	{
		public function WaterBucket() 
		{
			super(new WaterBucketSprite(), new CollisionRectangle(0, 0, Tile.Size.x * 2, Tile.Size.y * 2), "Unburn");
		}
		public override function Update():void
		{
			super.Update();
			if ((View as IAnimatable).CurrentAnimation == 1)
			{
				for (var i:int = 0; i < 1; i++)
				{
					var ef:WaterParticle = GetWaterParticle(Utility.Random( -5, 5));
					ef.CenterX = CenterX;
					ef.Bottom = Bottom;
					GameFrame.AddChild(ef);
				}
			}
		}
		public override function GetInteracted(interactor:IInteractor):void
		{
			super.GetInteracted(interactor);
			(interactor as Player).ExtinguishFire();
			SoundCenter.PlaySFX(WaterFountainSound);
			(View as IAnimatable).SetAnimation(1);
			if (!View.hasEventListener(Animation.FinishAnimation))
				View.addEventListener(Animation.FinishAnimation, onFinishAnimation);
			//var effect:WaterEffect = new WaterEffect();
			//effect.Bottom = Bottom;
			//effect.CenterX = CenterX;
			//GameFrame.AddChild(effect);
		}
		private function onFinishAnimation(e:Event):void
		{
			(View as IAnimatable).SetAnimation(0);
			View.removeEventListener(Animation.FinishAnimation, onFinishAnimation);
		}
	}

}