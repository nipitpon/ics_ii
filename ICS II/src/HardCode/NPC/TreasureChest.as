package HardCode.NPC 
{
	import flash.events.Event;
	import HardCode.Effects.TreasureCoinEffect;
	import Resources.Animation;
	import Resources.lib.NPC.TreasureChestSprite;
	import Resources.lib.Sound.SFX.TreasureCollectedSound;
	import System.Achievements;
	import System.DataCenter;
	import System.GameFrame;
	import System.GamePage;
	import System.GameSystem;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IInteractor;
	import System.Interfaces.INamable;
	import System.Interfaces.ISavable;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Interactables.InteractableObject;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class TreasureChest extends InteractableObject implements INamable, ISavable
	{
		private var level:int;
		public function TreasureChest(view:RenderableObject, level:int) 
		{
			var names:Vector.<String> = Vector.<String>(["A chest", "A box", "Suspicious chest", "Stashed Coin"]);
			super(new TreasureChestSprite(), new CollisionRectangle(16, 32, 32, 32), names[Utility.Random(0, names.length)]);
			this.level = level;
		}
		public function get Name():String { return "TreasureChest"; }
		public override function get IsInteractable():Boolean { return (View as IAnimatable).CurrentAnimation == 0; }
		public function GetInfo():Object
		{
			return { "animation": (View as IAnimatable).CurrentAnimation };
		}
		public function SetInfo(obj:Object):void
		{
			(View as IAnimatable).SetAnimation(parseInt(obj["animation"]));
		}
		public override function GetInteracted(interactor:IInteractor):void
		{
			//GameFrame.RemoveChild(this);
			//GameSystem.CurrentLevel.RemoveObject(this);
			//GamePage.ShowSecretItem(level - 1);
			//DataCenter.CollectSecretItem(level);
			//Achievements.CollectOneSecretItem();
			SoundCenter.PlaySFXFromInstance(new TreasureCollectedSound());
			View.addEventListener(Animation.FinishAnimation, onOpened);
			(View as IAnimatable).SetAnimation(1);
			var effect:TreasureCoinEffect = new TreasureCoinEffect();
			effect.CenterX = Collision.CenterX;
			effect.CenterY = Collision.Top;
			GameFrame.AddChild(effect);
		}
		private function onOpened(e:Event):void
		{
			View.removeEventListener(Animation.FinishAnimation, onOpened);
			(View as IAnimatable).SetAnimation(2);
		}
	}

}