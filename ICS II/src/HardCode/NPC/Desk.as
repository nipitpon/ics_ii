package HardCode.NPC 
{
	import Resources.lib.Other_pages.DeskSprite;
	import System.GamePage;
	import System.Interfaces.IInteractor;
	import System.Interfaces.INamable;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Interactables.InteractableObject;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Desk extends InteractableObject implements INamable
	{
		
		public function Desk() 
		{
			super(new DeskSprite(), new CollisionRectangle(0, 0, 64, 64), "Play the game");
		}
		public function get Name():String { return "desk"; }	
		public override function GetInteracted(interactor:IInteractor):void
		{
			if(!(interactor as Player).IsWarpingOut)
				GamePage.OpenLevelSelection(true);
			//Main.ChangeToFinishVanilla();
		}
	}

}