package HardCode.NPC 
{
	import HardCode.Tiles.GreenDoorTile;
	import HardCode.Tiles.RedDoorTile;
	import Resources.lib.Levers.GreenLeverSprite;
	import Resources.lib.Levers.RedLeverSprite;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class GreenLever extends Lever 
	{
		
		public function GreenLever() 
		{
			super(new GreenLeverSprite(), GreenDoorTile.Name);
		}
		
	}

}