package HardCode.NPC 
{
	import HardCode.Tiles.RedDoorTile;
	import Resources.lib.Levers.RedLeverSprite;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class RedLever extends Lever 
	{
		
		public function RedLever() 
		{
			super(new RedLeverSprite(), RedDoorTile.Name);
		}
		
	}

}