package HardCode.NPC 
{
	import _Base.MySound;
	import HardCode.Effects.WaterParticle;
	import System.Interfaces.ICachable;
	import System.Interfaces.ICacher;
	import System.MC.Collision.CollisionBox;
	import System.MC.Interactables.InteractableObject;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class WaterCacher extends InteractableObject implements ICacher 
	{
		private static var cache:Vector.<WaterParticle> = new Vector.<WaterParticle>();
		public function WaterCacher(view:RenderableObject, collisionBox:CollisionBox, interactionText:String) 
		{
			super(view, collisionBox, interactionText);
			
		}
		public function Cache(obj:ICachable):void 
		{
			cache.push(obj as WaterParticle);
		}
		protected function GetWaterParticle(force_x:int):WaterParticle
		{
			if (cache.length > 0) return cache.shift();
			return new WaterParticle(force_x, this);
		}
	}

}