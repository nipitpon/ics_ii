package HardCode.NPC 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import Resources.lib.NPC.FirePotionSprite;
	import Resources.lib.Sound.SFX.FireBottleOpen;
	import System.Achievements;
	import System.DataCenter;
	import System.GameFrame;
	import System.GameSystem;
	import System.GUI;
	import System.Interfaces.IInteractor;
	import System.Interfaces.ISavable;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Interactables.InteractableObject;
	import System.MC.Tile;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FirePotion extends InteractableObject implements ISavable
	{
		
		public function FirePotion(sprite:BitmapData = null) 
		{
			var ren:RenderableObject = null;
			if (sprite != null)
			{
				ren = new RenderableObject();
				var bmp:Bitmap = new Bitmap(sprite);
				bmp.scaleX = 2;
				bmp.scaleY = 2;
				ren.addChild(bmp);
			}
			else
			{
				ren = new FirePotionSprite();
			}
			super(ren, new CollisionRectangle(0, 0, Tile.Size.x, Tile.Size.y), "Burn yourself");
			
		}
		public function SetInfo(obj:Object):void { }
		public function GetInfo():Object { return { }; }	
		public override function GetInteracted(interactor:IInteractor):void
		{
			(interactor as Player).Burn();
			GameSystem.CurrentLevel.RemoveObject(this);
			SoundCenter.PlaySFXFromInstance(new FireBottleOpen());
			GameFrame.RemoveChild(this);
			GUI.HideInteractionText();
			DataCenter.UseFirePotion();
			Achievements.UseFirePotion();
		}
		
	}

}