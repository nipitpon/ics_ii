package HardCode.NPC 
{
	import HardCode.Tiles.BlueDoorTile;
	import HardCode.Tiles.RedDoorTile;
	import Resources.lib.Levers.BlueLeverSprite;
	import Resources.lib.Levers.RedLeverSprite;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class BlueLever extends Lever 
	{
		
		public function BlueLever() 
		{
			super(new BlueLeverSprite(), BlueDoorTile.Name);
		}
		
	}

}