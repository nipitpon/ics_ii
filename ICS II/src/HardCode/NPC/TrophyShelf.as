package HardCode.NPC 
{
	import Resources.lib.Other_pages.TrophyShelfImage;
	import System.GamePage;
	import System.Interfaces.IInteractor;
	import System.Interfaces.INamable;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Interactables.InteractableObject;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class TrophyShelf extends InteractableObject implements INamable
	{
		
		public function TrophyShelf(view:RenderableObject) 
		{
			super(view, new CollisionRectangle(0, 0, 64, 64), "Achievements");
			
		}
		public function get Name():String { return "achievements"; }
		public override function GetInteracted(interactor:IInteractor):void
		{
			GamePage.OpenAchievements(true);
		}
	}

}