package HardCode.NPC 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import HardCode.Effects.WaterEffect;
	import HardCode.Effects.WaterParticle;
	import Resources.lib.NPC.WaterBottleSprite;
	import Resources.lib.NPC.WaterBucketSprite;
	import Resources.lib.Sound.SFX.WaterBottleOpen;
	import System.GameFrame;
	import System.GameSystem;
	import System.GUI;
	import System.Interfaces.IInteractor;
	import System.Interfaces.ISavable;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Interactables.InteractableObject;
	import System.MC.Tile;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class WaterBottle extends WaterCacher implements ISavable
	{
		
		public function WaterBottle(sprite:BitmapData = null) 
		{
			var ren:RenderableObject = null;
			if (sprite != null)
			{
				ren = new RenderableObject();
				var bmp:Bitmap = new Bitmap(sprite);
				bmp.scaleX = 2;
				bmp.scaleY = 2;
				ren.addChild(bmp);
			}
			else
			{
				ren = new WaterBottleSprite();
			}
			super(ren, new CollisionRectangle(0, 0, Tile.Size.x, Tile.Size.y), "Use water bottle");
			
		}
		public function SetInfo(obj:Object):void { }
		public function GetInfo():Object { return { }; }	
		public override function GetInteracted(interactor:IInteractor):void
		{
			(interactor as Player).ExtinguishFire();
			GameSystem.CurrentLevel.RemoveObject(this);
			SoundCenter.PlaySFX(WaterBottleOpen);
			GameFrame.RemoveChild(this);
			GUI.HideInteractionText();
			for (var i:int = 0; i < 4; i++)
			{
				var ef:WaterParticle = GetWaterParticle(i - 2);
				ef.CenterX = CenterX;
				ef.CenterY = CenterY;
				GameFrame.AddChild(ef);
			}
			//var effect:WaterEffect = new WaterEffect();
			//effect.Bottom = Bottom;
			//effect.CenterX = CenterX;
			//GameFrame.AddChild(effect);
		}
	}

}