package HardCode.NPC 
{
	import Resources.lib.NPC.OptionNPCSprite;
	import Resources.lib.Other_pages.ToolboxSprite;
	import System.GamePage;
	import System.GameSystem;
	import System.Interfaces.IInteractor;
	import System.Interfaces.INamable;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Interactables.InteractableObject;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Toolboard extends InteractableObject implements INamable
	{
		
		public function Toolboard(view:RenderableObject) 
		{
			super(new OptionNPCSprite(), new CollisionRectangle(0, 0, 64, 64), "Settings");
			
		}
		public function get Name():String { return "settings"; }
		public override function GetInteracted(interactor:IInteractor):void
		{
			GamePage.OpenMainSettingsPopup(true);
		}
	}

}