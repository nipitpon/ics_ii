package HardCode.NPC 
{
	import com.spilgames.gameapi.GameAPI;
	import Resources.lib.NPC.WarpSpotSprite;
	import System.Achievements;
	import System.ControlCenter;
	import System.DataCenter;
	import System.GamePage;
	import System.GameSystem;
	import System.GUI;
	import System.Interfaces.IInteractor;
	import System.MC.Characters.Character;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Interactables.InteractableObject;
	import System.MC.Tile;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class WarpSpot extends InteractableObject 
	{
		private static var entered:int = 0;
		private var code:int;
		public function WarpSpot(code:int, view:RenderableObject = null) 
		{
			if (view == null) view = new WarpSpotSprite();
			super(view, new CollisionRectangle(0, 0, Tile.Size.x * 2, Tile.Size.y * 2), "Next level");
			this.code = code;
		}
		public override function GetInteracted(interactor:IInteractor):void
		{
			if (!(interactor as Player).IsBurning && !(interactor as Player).IsWarpingOut &&
				!(interactor as Player).IsJumping)
			{
				if (false && ++entered < 4)
				{
					onResume();
				}
				else
				{
					var mute_bgm:Boolean = SoundCenter.IsMutingBGM;
					var mute_sfx:Boolean = SoundCenter.IsMutingSFX;
					entered = 0;
					GameAPI.getInstance().GameBreak.request(function():void
					{
						GameSystem.Freeze();
					}, function():void { 
						if (!mute_bgm) SoundCenter.UnmuteBGM();
						if (!mute_sfx) SoundCenter.UnmuteSFX();
						onResume();
					});
				}
			}
		}
		private function onResume():void
		{
			GameSystem.Unfreeze();
			GameSystem.FinishLevel();
			if (GUI.IsShowingPauseMenu)
				GUI.HidePauseMenu();
			if (code > 0)
			{
				GameSystem.SetGameMode(code);
				//55 is fireballMonstrocityLevel, from main menu. Which doesn't have secret items, nor the index for collecting the item
				var chest:TreasureChest = GameSystem.GetObjectByName("TreasureChest") as TreasureChest;
				if (code != 55 && !DataCenter.SecretItemCollected(code - 1) && (chest == null || !chest.IsInteractable))
				{
					//GamePage.ShowSecretItem(code - 2);
					DataCenter.CollectSecretItem(code-1);
					Achievements.CollectOneSecretItem();
				}
				ControlCenter.ReleaseAllKey();
				ControlCenter.Stop();
				GameSystem.PlayerInstance.ExtinguishFire();
				GameSystem.PlayerInstance.ClearForceX();
				GameSystem.PlayerInstance.SetState(Player.WarpOut);
			}
			else if (code == -1)
			{
				Main.ChangeToFinishVanilla();
			}
			else if (code == -2)
			{
				Main.ChangeToFinish2();
			}
		}
	}

}