package HardCode.NPC 
{
	import Resources.lib.NPC.FinalWarpSprite;
	import Resources.lib.NPC.WarpSpotSprite;
	import System.GameSystem;
	import System.Interfaces.IInteractor;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Interactables.InteractableObject;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FinalWarpSpot extends InteractableObject 
	{
		public function FinalWarpSpot() 
		{
			super(new FinalWarpSprite(), new CollisionRectangle(0, 0, Tile.Size.x * 4, Tile.Size.y * 4), "To Arterius");
		}
		public override function GetInteracted(interactor:IInteractor):void
		{
			GameSystem.FinishLevel();
			Main.ChangeToFinishVanilla();
		}
		
	}

}