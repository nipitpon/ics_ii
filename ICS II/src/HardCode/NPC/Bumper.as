package HardCode.NPC 
{
	import flash.events.Event;
	import flash.geom.Point;
	import flash.globalization.NumberParseResult;
	import flash.system.System;
	import Resources.Animation;
	import Resources.lib.Sound.SFX.BumperSound;
	import System.GameSystem;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IOverlay;
	import System.MC.Characters.Character;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Overlay;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class Bumper extends CollidableObject implements IOverlay 
	{
		
		public function Bumper(view:RenderableObject) 
		{
			super(view, new CollisionRectangle(0, 16, 32, 16));
			collide_with = Vector.<Class>([Player]);
		}
		
		public function set BottomMost(value:Boolean):void { }
		public function get BottomMost():Boolean { return false; }
		public function set OverlayLevel(value:int):void { }
		public function get OverlayLevel():int { return Overlay.Location_FloorBackground; }
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			super.TakenHit(obj, movement);
			var player:Player = obj as Player;
			if (player != null && !player.IsDying)
			{
				var animatable:IAnimatable = View as IAnimatable;
				if (player.Collision.Right > Collision.CenterX && player.Collision.Left < Collision.CenterX)
				{
					player.SetState(Character.Idle);
					player.ClearForceY();
					player.BeginJump( -60);
					SoundCenter.PlaySFXFromInstance(new BumperSound());
					movement.y = -15;
					animatable.SetAnimation(2);
				}
				else if (animatable.CurrentAnimation == 0)
				{
					animatable.SetAnimation(1);
				}
				if(!View.hasEventListener(Animation.FinishAnimation))
					View.addEventListener(Animation.FinishAnimation, onFinishAnimation);
			}
		}
		private function onFinishAnimation(e:Event):void
		{
			var animatable:IAnimatable = View as IAnimatable;
			if (animatable.CurrentAnimation == 2)
			{
				View.removeEventListener(Animation.FinishAnimation, onFinishAnimation);
				animatable.SetAnimation(0);
			}
			else if (animatable.CurrentAnimation == 1 && Intersects(GameSystem.PlayerInstance.Collision, new Point(0, 1)) == null)
			{
				View.removeEventListener(Animation.FinishAnimation, onFinishAnimation);
				animatable.SetAnimation(0);
			}
		}
	}

}