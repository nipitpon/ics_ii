package HardCode.NPC 
{
	import Resources.lib.Other_pages.DocumentSprite;
	import System.GamePage;
	import System.Interfaces.IInteractor;
	import System.Interfaces.INamable;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Interactables.InteractableObject;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Documents extends InteractableObject implements INamable
	{
		
		public function Documents(view:RenderableObject) 
		{
			super(view, new CollisionRectangle(0, 0, 64, 64), "Credits");
			
		}
		public function get Name():String { return "credit"; }
		public override function GetInteracted(interactor:IInteractor):void
		{
			GamePage.OpenCredit(true);
			//Main.ChangeToFinishVanilla();
			//Main.ChangeToFinish2();
		}
	}
}