package HardCode.NPC 
{
	import HardCode.Weather.Weather_Rainy;
	import HardCode.Weather.Weather_Sunny;
	import Resources.lib.NPC.WeatherBenderSprite;
	import Resources.lib.Sound.SFX.FireExhaustedSound;
	import System.GameSystem;
	import System.Interfaces.IInteractor;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Interactables.InteractableObject;
	import System.MC.Tile;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class WeatherBender extends InteractableObject 
	{
		
		public function WeatherBender() 
		{
			super(new WeatherBenderSprite(), new CollisionRectangle(0, 0, Tile.Size.x, Tile.Size.y), "Start the rain");
		}
		public override function get IsInteractable():Boolean { return GameSystem.CurrentWeather is Weather_Sunny; }
		public override function GetInteracted(interactor:IInteractor):void
		{
			super.GetInteracted(interactor);
			if (GameSystem.PlayerInstance.IsBurning)
				SoundCenter.PlaySFXFromInstance(new FireExhaustedSound());
			GameSystem.SetWeather(new Weather_Rainy());
		}
	}

}