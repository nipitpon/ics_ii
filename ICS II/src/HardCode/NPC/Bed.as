package HardCode.NPC 
{
	import flash.geom.Point;
	import Resources.lib.Sound.SFX.ClockTickingSound;
	import System.GameSystem;
	import System.Interfaces.IInteractor;
	import System.Interfaces.ISavable;
	import System.MC.Characters.Character;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Interactables.InteractableObject;
	import System.SoundCenter;
	import System.Time;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class Bed extends InteractableObject implements ISavable
	{
		private static var used:Boolean;
		public function Bed(view:RenderableObject) 
		{
			super(view, new CollisionRectangle(0, 0, 64, 32), "Sleep");
			
		}
		public override function get IsInteractable():Boolean { return !Bed.Used; }
		public function SetInfo(obj:Object):void
		{
			used = obj["used"];
			Time.SetTime(obj["hour"], obj["minute"], obj["second"]);
		}
		public function GetInfo():Object 
		{ 
			return { 
				"used": used,
				"hour": Time.Hour,
				"minute": Time.Minute,
				"second": Time.Second
			}; 
		}
		public override function GetInteracted(interactor:IInteractor):void
		{
			super.GetInteracted(interactor);
			if (Time.Hour > 6)
				Time.AdvanceTo(0);
			else
				Time.AdvanceTo(12);
			(interactor as Player).Center = new Point(Collision.CenterX, interactor.CenterY);
			(interactor as Player).SetState(Player.Sleep);
			Bed.used = true;
			SoundCenter.PlaySFXFromInstance(new ClockTickingSound());
		}
		public static function set Used(val:Boolean):void { used = val; }
		public static function get Used():Boolean { return used; }
	}

}