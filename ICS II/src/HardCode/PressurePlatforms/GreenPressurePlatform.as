package HardCode.PressurePlatforms 
{
	import HardCode.Tiles.GreenDoorTile;
	import Resources.lib.Pressure_Platforms.GreenPressurePlatformSprite;
	import System.MC.PressurePlatform.PressurePlatform;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class GreenPressurePlatform extends PressurePlatform 
	{
		
		public function GreenPressurePlatform() 
		{
			super(new GreenPressurePlatformSprite(), GreenDoorTile.Name);
			
		}
		
	}

}