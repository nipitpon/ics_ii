package HardCode.PressurePlatforms 
{
	import HardCode.Tiles.BlueDoorTile;
	import Resources.lib.Pressure_Platforms.BluePressurePlatformSprite;
	import System.MC.PressurePlatform.PressurePlatform;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class BluePressurePlatform extends PressurePlatform 
	{
		
		public function BluePressurePlatform() 
		{
			super(new BluePressurePlatformSprite(), BlueDoorTile.Name);
			
		}
		
	}

}