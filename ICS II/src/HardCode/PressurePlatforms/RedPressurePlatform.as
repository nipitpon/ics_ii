package HardCode.PressurePlatforms 
{
	import HardCode.Tiles.RedDoorTile;
	import Resources.lib.Pressure_Platforms.RedPressurePlatformSprite;
	import System.MC.PressurePlatform.PressurePlatform;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class RedPressurePlatform extends PressurePlatform 
	{
		
		public function RedPressurePlatform() 
		{
			super(new RedPressurePlatformSprite(), RedDoorTile.Name);
			
		}
		
	}

}