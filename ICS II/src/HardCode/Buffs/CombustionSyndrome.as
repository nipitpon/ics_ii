package HardCode.Buffs 
{
	import flash.display.StageQuality;
	import flash.filters.BlurFilter;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import HardCode.Effects.AshEffect;
	import HardCode.Effects.FireEffect;
	import HardCode.Effects.FireParticle;
	import HardCode.Effects.SmokeParticle;
	import Resources.lib.Sound.BGM.OnFireBGM;
	import Resources.lib.Sound.SFX.FireBurning;
	import Resources.lib.Sound.SFX.FireIgniteSound;
	import Resources.lib.Sound.SFX.FireStartingSound;
	import System.DataCenter;
	import System.GameFrame;
	import System.GameSystem;
	import System.GUI;
	import System.Interfaces.IBuffHolder;
	import System.Interfaces.ICachable;
	import System.Interfaces.ICacher;
	import System.Interfaces.ISound;
	import System.MC.Buffs.ToggleBuff;
	import System.MC.Characters.Character;
	import System.MC.Characters.Player;
	import System.MC.Effects.Effect;
	import System.SoundCenter;
	import System.Time;
	import System.V.MonsterHealthBar;
	import System.V.ProgressBar;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class CombustionSyndrome extends ToggleBuff implements ICacher
	{
		private static const MaxCount:int = Utility.SecondToFrame(3);
		private var owner:Character;
		private var count:int;
		private var burn_count:int;
		private var effect:FireEffect;
		private var health_bar_fx:Effect;
		private var burn_bar_fx:Effect;
		private var health_bar:MonsterHealthBar;
		private var burn_bar:ProgressBar;
		private var fire_burning_sound:ISound;
		private var fire_starting_sound:ISound;
		private var ash_count:int;
		private var smoke_count:int;
		private static var ash_cache:Vector.<AshEffect> = new Vector.<AshEffect>();
		private static var smoke_cache:Vector.<SmokeParticle> = new Vector.<SmokeParticle>();
		private static var fire_cache:Vector.<FireParticle> = new Vector.<FireParticle>();
		public function CombustionSyndrome(owner:IBuffHolder) 
		{
			super(owner, DataCenter.Buff_CombustionSyndrome);
			this.owner = owner as Character;
			effect = new FireEffect(false);
			health_bar_fx = new Effect(health_bar = new MonsterHealthBar(60, 10));
			fire_starting_sound = new FireStartingSound();
			burn_bar_fx = new Effect(burn_bar = new ProgressBar(60, 10, 4, (178 << 16) | (172 << 8) | 130, 0xFF0B13));
		}
		public function Cache(cache:ICachable):void
		{
			if (cache is AshEffect)
				ash_cache.push(cache as AshEffect);
			else if (cache is FireParticle)
				fire_cache.push(cache as FireParticle);
			else
				smoke_cache.push(cache as SmokeParticle);
		}
		public function get IsBurning():Boolean { return burn_count >= 0; }
		public override function Update():void
		{
			super.Update();
			if (burn_count < 0)
			{
				if (!GameSystem.IsIndoor(owner.Center) && (Time.Hour >= 6 && Time.Hour < 18))
				{
					if (count < MaxCount)
					{
						count++;
						burn_bar.visible = true;
						burn_bar.CurrentValue = count * 100 / MaxCount;
						GUI.BurningScreenAlpha = count / MaxCount;
						owner.View.transform.colorTransform = new ColorTransform(1, 1, 1, 1, count * 255 / MaxCount);
						SoundCenter.SetSoundVolumeMultiplier(fire_starting_sound, (count) / MaxCount);
					}
					else if (GameSystem.GetIntersectedWaterBlock(owner.Collision) == null) Burn();
				}
				else
				{
					if (count > 0)
					{
						count--;
						GUI.BurningScreenAlpha = count / MaxCount;
						owner.View.transform.colorTransform = new ColorTransform(1, 1, 1, 1, count * 255 / MaxCount);
						burn_bar.CurrentValue = count * 100 / MaxCount;
						SoundCenter.SetSoundVolumeMultiplier(fire_starting_sound, (count) / MaxCount);
					}
					else
					{
						burn_bar.visible = false;
						SoundCenter.SetSoundVolumeMultiplier(fire_starting_sound, 0);
					}
				}
				if (count > 0 && Utility.Random(0, MaxCount) < count)
				{
					var smoke:SmokeParticle = null;
					
					if (smoke_cache.length == 0) smoke = new SmokeParticle(this);
					else smoke = smoke_cache.shift();
					
					smoke.CenterX = Utility.Random(Owner.CenterX - 20, Owner.CenterX + 20);
					smoke.Bottom = Utility.Random(Owner.CenterY - 30, Owner.CenterY + 30);
					GameFrame.AddChild(smoke);
				}
			}
			else if(burn_count < MaxCount)
			{
				if (GameSystem.GetIntersectedWaterBlock(owner.Collision) != null)
				{
					if (GameFrame.ContainChild(effect))
					{
						effect.Exhaust();
						GameSystem.CurrentLevel.CheckAndResumeBGM();
					}
					
					burn_count = -1;
					if (fire_burning_sound != null)
					{
						fire_burning_sound.Stop();
						fire_burning_sound = null;
					}
					owner.View.transform.colorTransform = new ColorTransform(1, 1, 1, 1, 255);
					owner.DetachEffect(effect);
					count = MaxCount;
					SoundCenter.SetSoundVolumeMultiplier(fire_starting_sound, 1);
					health_bar.visible = false;
					burn_bar.visible = true;
				}
				else
				{
					burn_bar.visible = false;
					burn_count++;
					health_bar.CurrentValue = 100 - (burn_count * 100 / MaxCount);
					health_bar.visible = true;
					ash_count--;
					if (ash_count == 0)
					{
						var ash_effect:AshEffect = new AshEffect(Utility.Random(4, 10), this);
						ash_effect.CenterX = Utility.Random(Owner.CenterX - 10, Owner.CenterX + 10);
						ash_effect.Bottom = Utility.Random(Owner.CenterY - 10, Owner.CenterY + 10);
						GameFrame.AddChild(ash_effect);
						ResetAshParticleCount();
					}
					if (burn_count == MaxCount)
					{
						effect.Exhaust();
						GameSystem.PlayerInstance.DetachEffect(effect);
						GameSystem.PlayerInstance.View.transform.colorTransform = new ColorTransform();
						GameSystem.PlayerInstance.DieFromBurning();
					}
				}
			}
		}
		public override function Reset():void
		{
			super.Reset();
			burn_count = -1;
			ash_count = -1;
			count = 1;
			burn_bar.visible = false;
			health_bar.visible = false;
			if (GameFrame.ContainChild(effect))
			{
				effect.Exhaust();
				(Owner as Player).DetachEffect(effect);
				if (fire_burning_sound != null)
				{
					fire_burning_sound.Stop();
					fire_burning_sound = null;
				}
			}
			Owner.View.transform.colorTransform = new ColorTransform();
			if (GameSystem.CurrentLevel.OverrideBGM == null)
				GameSystem.CurrentLevel.CheckAndResumeBGM();
			if(fire_starting_sound.IsPlaying)
				SoundCenter.SetSoundVolumeMultiplier(fire_starting_sound, 0);
		}
		public override function Activate():void
		{
			super.Activate();
			count = 0;
			burn_count = -1;
			health_bar_fx.Center = new Point(GameSystem.PlayerInstance.Collision.CenterX, GameSystem.PlayerInstance.Collision.Top - 20);
			burn_bar_fx.Center = health_bar_fx.Center;
			GameSystem.PlayerInstance.AttachEffect(health_bar_fx);
			GameSystem.PlayerInstance.AttachEffect(burn_bar_fx);
			GameFrame.AddChild(health_bar_fx);
			GameFrame.AddChild(burn_bar_fx);
			SoundCenter.PlaySFXFromInstance(fire_starting_sound, 0, 9999999);
			health_bar.visible = false;
			burn_bar.visible = false;
			burn_bar.Max = 100;
			health_bar.Max = 100;
			burn_bar.Min = 0;
			health_bar.Min = 0;
			GUI.BurningScreenAlpha = 0;
		}
		public override function Deactivate():void
		{
			Reset();
			super.Deactivate();
			(Owner as Player).DetachEffect(health_bar_fx);
			(Owner as Player).DetachEffect(burn_bar_fx);
			fire_starting_sound.Stop();
			ash_count = -1;
			GameFrame.RemoveChild(health_bar_fx);
			GameFrame.RemoveChild(burn_bar_fx);
			if (fire_burning_sound != null && fire_burning_sound.IsPlaying)
				fire_burning_sound.Stop();
			owner.View.transform.colorTransform = new ColorTransform();
		}
		public function Burn():void
		{
			if (GameFrame.ContainChild(effect)) return;
			
			owner.View.transform.colorTransform = new ColorTransform(0, 0, 0, 1, 55, 12, 31);
			effect.Center = new Point(owner.Collision.CenterX, owner.Collision.Bottom - effect.View.height / 2);
			effect.Reset();
			owner.AttachEffect(effect);
			GameFrame.AddChild(effect);
			SoundCenter.SetSoundVolumeMultiplier(fire_starting_sound, 0);
			SoundCenter.PlaySFX(FireIgniteSound);
			if (fire_burning_sound != null)
				fire_burning_sound.Stop();
			GUI.BurningScreenAlpha = 1;
			SoundCenter.PlaySFXFromInstance(fire_burning_sound = new FireBurning(), 1, 999999);
			if (GameSystem.CurrentLevel.OverrideBGM == null)
				SoundCenter.PlayBGM(OnFireBGM);
			burn_count = 0;
			count = MaxCount;
			burn_bar.CurrentValue = count * 100 / MaxCount;
			ResetAshParticleCount();
			for (var i:int = 0; i < 5; i++)
			{
				var ef:FireParticle = null;
				if (fire_cache.length == 0) ef = new FireParticle(this);
				else ef = fire_cache.shift();
				
				ef.CenterX = Owner.CenterX + ((Owner.View.width / 10) * (i - 2));
				ef.Bottom = Utility.Random(Owner.CenterY - 20, Owner.CenterY + 20);
				GameFrame.AddChild(ef);
			}
		}
		private function ResetAshParticleCount():void
		{
			var start:int = 10;
			var end:int = 20;
			switch(Owner.View.stage.quality.toLowerCase())
			{
				case StageQuality.HIGH: start = 1; end = 2; break;
				case StageQuality.MEDIUM: start = 5; end = 10; break;
				case StageQuality.LOW: start = 10; end = 20; break;
			}
			ash_count = Utility.SecondToFrame(Utility.Random(start, end) / 10.0);
		}
	}
}