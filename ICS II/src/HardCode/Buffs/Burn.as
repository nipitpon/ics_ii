package HardCode.Buffs 
{
	import _Base.MySound;
	import flash.geom.ColorTransform;
	import HardCode.Effects.FireEffect;
	import HardCode.GUI.MonsterMarker;
	import HardCode.Tiles.BurnableTiles;
	import Resources.lib.Sound.SFX.FireIgniteSound;
	import System.DataCenter;
	import System.GameFrame;
	import System.Interfaces.IBuffHolder;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IMVC;
	import System.Interfaces.IStatusHolder;
	import System.Interfaces.IUpdatable;
	import System.MC.Buffs.ToggleBuff;
	import System.MC.Characters.Monster;
	import System.MC.Collision.CollidableObject;
	import System.SoundCenter;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Burn extends ToggleBuff
	{
		private var effect:FireEffect;
		public function Burn(owner:IBuffHolder) 
		{
			super(owner, DataCenter.Buff_Burn);
			effect = new FireEffect(owner is BurnableTiles);
		}
		public override function Update():void
		{
			super.Update();
			(Owner as IStatusHolder).TakeDamage(2, null, false, null);
		}
		public override function Activate():void
		{
			super.Activate();
			SoundCenter.PlaySFXFromInstance(new FireIgniteSound());
			if (Owner is BurnableTiles)
				effect.Bottom = (Owner as ICollidable).Bottom + 4;
			else
				effect.Bottom = (Owner as ICollidable).Bottom;
			effect.CenterX = (Owner as ICollidable).CenterX;
			(Owner as CollidableObject).AttachEffect(effect);
			GameFrame.AddChild(effect);
			(Owner as CollidableObject).View.transform.colorTransform = 
				new ColorTransform(0, 0, 0, 1, 55, 12, 31);
		}
		public override function Deactivate():void
		{
			super.Deactivate();
			(Owner as CollidableObject).DetachEffect(effect);
			(Owner as CollidableObject).View.transform.colorTransform = new ColorTransform();
			effect.Exhaust();
		}
	}

}