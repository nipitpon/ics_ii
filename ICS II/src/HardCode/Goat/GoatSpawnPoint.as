package HardCode.Goat 
{
	import flash.geom.Point;
	import System.MC.SpawnPoint;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class GoatSpawnPoint extends SpawnPoint 
	{
		
		public function GoatSpawnPoint(location:Point) 
		{
			super(location, 100, 1);
			SetSpawns(Vector.<Class>([Goat]), Vector.<Class>([Goat]));
		}
		
	}

}