package HardCode.Goat 
{
	import flash.events.Event;
	import flash.geom.Point;
	import Resources.BlackMirror.Character.WalldoSprite;
	import System.GameSystem;
	import System.Interfaces.ICollidable;
	import System.MC.Characters.Monster;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Items.ItemSet;
	import System.MC.ModelViewController;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class Walldo extends Monster 
	{
		
		public function Walldo() 
		{
			super(new WalldoSprite(), new CollisionRectangle(0, 0, 64, 64), 2, 2, 2, 2, 2, 2, Vector.<ItemSet>([]));
			addEventListener(ModelViewController.Added, onAdded);
			addEventListener(ModelViewController.Removed, onRemoved);
		}
		public override function get MaxHP():Number { return 99999; }
		private function onAdded(e:Event):void
		{
			GameSystem.CurrentLevel.AddIndoorArea(Collision);
		}
		private function onRemoved(e:Event):void
		{
			GameSystem.CurrentLevel.RemoveIndoorArea(Collision);
		}
	}

}