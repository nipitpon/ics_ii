package HardCode.Goat 
{
	import flash.geom.Point;
	import HardCode.Buffs.Burn;
	import Resources.BlackMirror.Character.YellowGegonSprite;
	import Resources.lib.Sound.SFX.GoatAttackSound;
	import Resources.lib.Sound.SFX.GoatBurnSound;
	import System.Interfaces.IBurnable;
	import System.Interfaces.ICollidable;
	import System.MC.Characters.Monster;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Items.ItemSet;
	import System.MC.Tile;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class YellowGegon extends Monster implements IBurnable
	{
		
		public function YellowGegon() 
		{
			super(new YellowGegonSprite(), new CollisionRectangle(8, 34, 48, 30),
				1, 1, 1, 200, 1, 2, Vector.<ItemSet>([]));
			
		}
		public override function get MaxHP():Number { return 40; }
		public override function Hit(obj:ICollidable, movement:Point):void
		{
			super.Hit(obj, movement);
			if (!IsAlive) return;
			
			if (obj is Player && (obj as Player).IsAlive && !(obj as Player).IsDying && !(obj as Player).IsWarpingOut)
			{
				if (!(obj as Player).IsBurning)
				{
					SoundCenter.PlaySFXFromInstance(new GoatAttackSound());
					(obj as Player).DieFromMonster();
				}
				else if(!IsBurning)
				{
					Burn();
				}
			}
			
		}
		public override function Burn():void
		{
			if (!HasBuff(HardCode.Buffs.Burn) && !IsDying)
				SoundCenter.PlaySFXFromInstance(new GoatBurnSound());
			super.Burn();
			
		}
	}

}