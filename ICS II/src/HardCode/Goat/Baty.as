package HardCode.Goat 
{
	import flash.events.Event;
	import flash.geom.Point;
	import HardCode.Levels.SkyPlane;
	import Resources.BlackMirror.Character.BatySprite;
	import Resources.lib.Sound.SFX.BatySound;
	import Resources.lib.Sound.SFX.GoatAttackSound;
	import System.Interfaces.IBurnable;
	import System.Interfaces.ICollidable;
	import System.MC.Characters.Monster;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Items.ItemSet;
	import System.MC.Tile;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class Baty extends Monster implements IBurnable 
	{
		
		public function Baty() 
		{
			super(new BatySprite(), new CollisionRectangle(12, 26, 40, 20),
				1, 1, 1, 200, 1, 0, Vector.<ItemSet>([]));
		}
		public override function get MaxHP():Number { return 40; }
		public override function get DefyGravity():Boolean { return !IsBurning; }
		public override function Intersects(box:CollisionBox, movement:Point):ICollidable
		{
			if (IsBurning) return null;
			return super.Intersects(box, movement);
		}
		public override function Hit(obj:ICollidable, movement:Point):void
		{
			if (!IsBurning) 
				super.Hit(obj, movement);
		}
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			if (!IsAlive || obj is SkyPlane) return;
			super.TakenHit(obj, movement);
			
			if (obj is Player && (obj as Player).IsAlive && !(obj as Player).IsDying && !(obj as Player).IsWarpingOut)
			{
				SoundCenter.PlaySFXFromInstance(new BatySound());
				if (!(obj as Player).IsBurning)
				{
					(obj as Player).DieFromMonster();
				}
				else if(!IsBurning)
				{
					Burn();
				}
			}
			
		}
	}

}