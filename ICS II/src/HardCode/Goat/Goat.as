package HardCode.Goat 
{
	import Resources.BlackMirror.Character.GoatSprite;
	import Resources.lib.Sound.SFX.GoatBurnSound;
	import System.Interfaces.IBurnable;
	import System.MC.Characters.Monster;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Items.ItemSet;
	import System.MC.Tile;
	import System.SoundCenter;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Goat extends Monster implements IBurnable
	{
		
		public function Goat() 
		{
			super(new GoatSprite(), new CollisionRectangle(Tile.Size.x * 0.2, Tile.Size.y, Tile.Size.x * 1.6, Tile.Size.y),
				1, 1, 1, 200, 1, 2, Vector.<ItemSet>([]));
		}
		public override function get MaxHP():Number { return 40; }
		public override function Burn():void
		{
			super.Burn();
			
			SoundCenter.PlaySFXFromInstance(new GoatBurnSound());
		}
	}
}