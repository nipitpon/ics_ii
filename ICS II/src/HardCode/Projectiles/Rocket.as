package HardCode.Projectiles 
{
	import flash.geom.Point;
	import HardCode.Effects.FireExplodeEffect;
	import HardCode.NPC.Lever;
	import HardCode.Tiles.Cannon;
	import HardCode.Tiles.SpikeTile;
	import Resources.lib.Projectiles.RocketSprite;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.IBurnable;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IInteractable;
	import System.Interfaces.IProjectileLauncher;
	import System.Interfaces.IStatusHolder;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Projectiles.AttackTargetProjectile;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Rocket extends AttackTargetProjectile 
	{
		public function Rocket(owner:IProjectileLauncher) 
		{
			super(new RocketSprite(), new CollisionRectangle(12, 12, 8, 8), 1000, owner, 1, 0, 0);
			speed = 10;
		}
		protected override function onHitTile(tile:Tile):void
		{
			super.onHitTile(tile);
			var effect:FireExplodeEffect = new FireExplodeEffect();
			effect.View.rotation = View.rotation;
			effect.View.x = View.x;
			effect.View.y = View.y;
			GameFrame.AddChild(effect);
		}
		protected override function onHitTarget(target:IBurnable):void 
		{ 
			
			if (!target.IsBurning)
			{
				var box:CollisionBox = new CollisionRectangle(Collision.CenterX - (Tile.Size.x * 0.25), 
					Collision.CenterY - (Tile.Size.y * 0.25), 
					Tile.Size.x / 2, Tile.Size.y / 2);
				var collide:Vector.<ICollidable> = GameSystem.GetIntersectedCollidable(box, CalculateMovement());
				for each(var col:ICollidable in collide)
				{
					if (col is IBurnable && !(col as IBurnable).IsBurning)
					{
						(col as IBurnable).Burn();
					}
					if (col is Lever)
					{
						(col as Lever).GetInteracted(null);
					}
				}
			}
			if(!(target is Cannon))
				super.onHitTarget(target);
		}
	}
}