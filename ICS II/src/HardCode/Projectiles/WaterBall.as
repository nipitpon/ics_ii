package HardCode.Projectiles 
{
	import flash.display3D.Context3DVertexBufferFormat;
	import HardCode.Effects.WaterballExplodeEffect;
	import HardCode.Tiles.SpikeTile;
	import Resources.lib.Projectiles.WaterBallSprite;
	import System.GameFrame;
	import System.Interfaces.IBurnable;
	import System.Interfaces.IProjectileLauncher;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Projectiles.AttackTargetProjectile;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class WaterBall extends AttackTargetProjectile 
	{
		
		public function WaterBall(owner:IProjectileLauncher) 
		{
			super(new WaterBallSprite(), new CollisionRectangle(12, 12, 8, 8), 1000, owner, 1, 0, 0);
			speed = 10;
		}
		protected override function onHitTile(tile:Tile):void
		{
			super.onHitTile(tile);
			var effect:WaterballExplodeEffect = new WaterballExplodeEffect();
			effect.View.rotation = View.rotation;
			effect.View.x = View.x;
			effect.View.y = View.y;
			GameFrame.AddChild(effect);
		}
		protected override function onHitTarget(target:IBurnable):void 
		{ 
			
			if(target is Player)
				(target as Player).ExtinguishFire();
			super.onHitTarget(target);
			var effect:WaterballExplodeEffect = new WaterballExplodeEffect();
			effect.View.rotation = View.rotation;
			effect.View.x = View.x;
			effect.View.y = View.y;
			GameFrame.AddChild(effect);
		}
		
	}

}