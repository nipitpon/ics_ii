package HardCode.Projectiles 
{
	import HardCode.Effects.CannonBallExplodeEffect;
	import HardCode.Effects.FireExplodeEffect;
	import HardCode.Tiles.BoxTile;
	import HardCode.Tiles.Cannon;
	import Resources.lib.Projectiles.CannonBallSprite;
	import Resources.lib.Sound.SFX.MissileExplode;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.IBurnable;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IInteractable;
	import System.Interfaces.IProjectileLauncher;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Collision.CollisionSphere;
	import System.MC.Projectiles.AttackTargetProjectile;
	import System.MC.Tile;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class CannonBall extends AttackTargetProjectile 
	{
		
		public function CannonBall(owner:IProjectileLauncher) 
		{
			super(new CannonBallSprite(), new CollisionRectangle(21, 21, 22, 22), 1000, owner, 1, 0, 0);
			speed = 10;
		}
		protected override function onHitTile(tile:Tile):void
		{
			Explode();
		}
		protected override function onHitTarget(target:IBurnable):void 
		{ 
			if (target is Cannon && !(target as Cannon).IsBurning)
				(target as Cannon).Burn();
			else 
				Explode();
		}
		private function Explode():void
		{
			if (!GameFrame.ContainChild(this)) return;
			
			var box:CollisionBox = new CollisionRectangle(Collision.CenterX - Tile.Size.x, Collision.CenterY - Tile.Size.y, Tile.Size.x * 2, Tile.Size.y * 2);
			var collide:Vector.<ICollidable> = GameSystem.GetIntersectedCollidable(box, null);
			for each(var col:ICollidable in collide)
			{
				if (col is IBurnable && !(col as IBurnable).IsBurning)
				{
					(col as IBurnable).Burn();
				}
				if (col is IInteractable)
				{
					(col as IInteractable).GetInteracted(null);
				}
			}
			GameFrame.RemoveChild(this);
			var effect:CannonBallExplodeEffect = new CannonBallExplodeEffect();
			effect.CenterX = Collision.CenterX + ForceX;
			effect.CenterY = Collision.CenterY + ForceY;
			GameFrame.AddChild(effect);
			SoundCenter.PlaySFXFromInstance(new MissileExplode());
		}
	}

}