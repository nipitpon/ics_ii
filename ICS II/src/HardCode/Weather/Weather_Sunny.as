package HardCode.Weather 
{
	import HardCode.Buffs.CombustionSyndrome;
	import Resources.lib.Sound.BGM.SunnyWeatherSound;
	import System.GameSystem;
	import System.Interfaces.IBuff;
	import System.MC.Characters.Player;
	import System.MC.Weather;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Weather_Sunny extends Weather 
	{
		private var buff:IBuff;
		public function Weather_Sunny() 
		{
			super(new RenderableObject());
		}
		public override function Start():void
		{
			super.Start();
			buff = GameSystem.PlayerInstance.AddBuff(CombustionSyndrome);
		}
		public override function Stop():void
		{
			super.Stop();
			buff.Deactivate();
		}
	}

}