package HardCode.Weather 
{
	import adobe.utils.CustomActions;
	import fl.transitions.easing.None;
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import flash.display.BlendMode;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.filters.ConvolutionFilter;
	import flash.geom.Rectangle;
	import Resources.ActionSet;
	import Resources.lib.Environment.Weather.RainSprite;
	import Resources.lib.Sound.BGM.RainSound;
	import Resources.lib.Sound.ThunderSound;
	import System.GameFrame;
	import System.GameSystem;
	import System.GUI;
	import System.Interfaces.IBuff;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IStatusHolder;
	import System.Interfaces.IStunnable;
	import System.Interfaces.IUpdatable;
	import System.MC.Weather;
	import System.SoundCenter;
	import System.Time;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Weather_Rainy extends Weather implements IUpdatable
	{
		private var rain:RainSprite;
		private var thunder_count:uint;
		private var tween:Tween;
		private var indoor_area:Vector.<Shape>;
		public function Weather_Rainy() 
		{
			super(new RenderableObject());
			
			View.addChild(new RainSprite());
			//View.addChild(new Cloud());
			View.blendMode = BlendMode.LAYER;
			indoor_area = new Vector.<Shape>();
		}
		public function AddIndoorArea(rect:Rectangle):void
		{
			var rMask:Shape = new Shape();
			rMask.graphics.beginFill(0xffffff);
			rMask.graphics.drawRect(rect.x, rect.y, rect.width, rect.height);
			rMask.graphics.endFill();
			rMask.x = 0;
			rMask.y = 0;
			View.addChild(rMask);
			rMask.blendMode = BlendMode.ERASE;
			indoor_area.push(rMask);
		}
		public function ClearIndoorArea():void
		{
			for each(var shape:Shape in indoor_area)
				View.removeChild(shape);
		}
		public override function get BackgroundMusic():Class { return RainSound; }
		public override function get AmbientStrength():Number { return 0.5; }
		public function ShowMask():void
		{
			for each(var shape:Shape in indoor_area)
				shape.visible = true;
			View.visible = true;
		}
		public function HideMask():void
		{
			for each(var shape:Shape in indoor_area)
				shape.visible = false;
			if (GameSystem.IsCompletelyIndoor)
				View.visible = false;
		}
		public override function Start():void
		{
			super.Start();
			if (tween != null)
			{
				tween.stop();
				if (tween.hasEventListener(TweenEvent.MOTION_FINISH))
					tween.removeEventListener(TweenEvent.MOTION_FINISH, onTweenFinish);
			}
			GUI.BurningScreenAlpha = 0;
			View.visible = true;
			View.alpha = 0;
			tween = new Tween(View, "alpha", None.easeNone, 0, 1, 5, true);
			tween.start();
			View.addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			thunder_count = 0;
		}
		public override function Stop():void
		{
			if (!View.visible) return;
			
			if (tween != null)
			{
				tween.stop();
				if (tween.hasEventListener(TweenEvent.MOTION_FINISH))
					tween.removeEventListener(TweenEvent.MOTION_FINISH, onTweenFinish);
			}
			tween = new Tween(View, "alpha", None.easeNone, 1, 0, 5, true);
			tween.start();
			tween.addEventListener(TweenEvent.MOTION_FINISH, onTweenFinish);
		}
		public function get UpdateWhenAdvancingTime():Boolean { return false; }
		public override function Update():void
		{
			super.Update();
			GameFrame.SkyLayer.DarkCloudRate = View.alpha;
			GameFrame.FarBackground.alpha = 1 - View.alpha;
			GameFrame.AmbientStrength = 1 - (View.alpha / 2);
			
			if (View.alpha == 1)
			{
				thunder_count++;
				if (thunder_count == Utility.SecondToFrame(10))
				{
					thunder_count = 0;
					if (Utility.Random(0, 100) < 50)
					{
						SoundCenter.PlaySFXFromInstance(new ThunderSound());
						GameFrame.Flash(255, 255, 255, 10);
						var col:Vector.<ICollidable> = GameSystem.GetCollidables();
						for each(var c:ICollidable in col)
						{
							if (c is IStunnable)
							{
								var s:IStunnable = c as IStunnable;
								if (s.IsStunnable && Utility.Random(0, 100) < 50)
								{
									s.Stun(5);
								}
							}
						}
					}
				}
			}
		}
		private function onRemovedFromStage(e:Event):void
		{
			//No reference to stop as it will conflict when attempt to remove the view again.
			if(tween != null && tween.isPlaying)
				tween.stop();
			if(tween != null && tween.isPlaying)
				onTweenFinish(null);
			else
			{
				GameFrame.SkyLayer.DarkCloudRate = 0;
				GameFrame.AmbientStrength = 1;
				View.removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			}
		}
		private function onTweenFinish(e:Event):void
		{
			GameFrame.SkyLayer.DarkCloudRate = 0;
			GameFrame.AmbientStrength = 1;
			tween.removeEventListener(TweenEvent.MOTION_FINISH, onTweenFinish);
			View.removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			if(e != null)//it's null if called from onRemovedFromStage
				super.Stop();
			View.visible = false;
		}
	}
}