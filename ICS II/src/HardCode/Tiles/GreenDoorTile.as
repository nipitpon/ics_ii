package HardCode.Tiles 
{
	import Resources.lib.Doors.GreenDoorSprite;
	import System.Interfaces.IAnimatable;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class GreenDoorTile extends DoorTile 
	{
		public static const Name:String = "GreenDoor";
		public function GreenDoorTile(activated:Boolean) 
		{
			super(GreenDoorTile.Name, new GreenDoorSprite(), activated);
			
		}
		
	}

}