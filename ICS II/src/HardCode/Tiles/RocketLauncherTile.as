package HardCode.Tiles 
{
	import HardCode.Projectiles.Rocket;
	import Resources.lib.RocketLauncherSprite;
	import System.GameFrame;
	import System.Interfaces.ILaunchable;
	import System.Interfaces.IOverlay;
	import System.Interfaces.ISavable;
	import System.Interfaces.IUpdatable;
	import System.MC.etc.ForceStruct;
	import flash.geom.Point;
	import System.Interfaces.IProjectileLauncher;
	import System.MC.Collision.CollisionBox;
	import System.MC.Overlay;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class RocketLauncherTile extends Tile implements IProjectileLauncher, IUpdatable, IOverlay, ISavable
	{
		private static const Interval:int = Utility.SecondToFrame(2);
		private var shoot_direction:Vector.<Boolean>;
		private var count:int;
		private var bullet_type:Class;
		public function RocketLauncherTile(top:Boolean, left:Boolean, bottom:Boolean, right:Boolean, bullet_type:Class = null, ren:RenderableObject = null) 
		{
			super(false);
			shoot_direction = Vector.<Boolean>([right, bottom, left, top]);
			if (ren == null)
				ren = new RocketLauncherSprite();
			View.addChild(ren);
			count = Interval;
			if (bullet_type == null)
				bullet_type = Rocket;
			this.bullet_type = bullet_type;
		}
		public function SetInfo(obj:Object):void
		{
			count = obj["count"];
		}
		public function GetInfo():Object { return { "count":count }; }
		public function get OverlayLevel():int { return Overlay.Location_FloorForeground; }
		public function get ReleasePoint():Point 
		{
			return Center;
		}
		public override function Update():void
		{
			super.Update();
			if (--count == 0)
			{
				count = Interval;
				for (var i:int = 0; i < 4; i++)
				{
					if (shoot_direction[i])
					{
						ReleaseProjectile(bullet_type, 90 * (i), null);
					}
				}
			}
		}
		public function get ProjectileHoming():Boolean  { return false; } 
		public function set ProjectileHoming(value:Boolean):void  { }
		public function ReleaseProjectile(type:Class, angleOffset:Number, pushBack:ForceStruct):Boolean 
		{
			var projectile:ILaunchable = new type(this);
			var releasePoint:Point = new Point(CenterX + ((Tile.Size.x / 2) * Math.cos(Utility.ToRadian(angleOffset))), 
				CenterY + ((Tile.Size.y / 2) * Math.sin(Utility.ToRadian(angleOffset))));//ReleasePoint;
			projectile.Center = releasePoint;
			
			projectile.View.rotation = angleOffset;
			if(pushBack != null)
				PushWithDecay(pushBack.x, pushBack.y, pushBack.decayRate);
			projectile.LaunchAt(releasePoint.x, releasePoint.y);
			GameFrame.AddChild(projectile);
			
			return true;
		}
		
	}

}