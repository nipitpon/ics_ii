package HardCode.Tiles 
{
	import flash.geom.Point;
	import Resources.Animation;
	import Resources.BlackMirror.Character.PlayerSprite;
	import System.Interfaces.IActivatable;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.ICollidable;
	import System.Interfaces.INamable;
	import System.Interfaces.IOverlay;
	import System.Interfaces.ISavable;
	import System.MC.Collision.CollisionBox;
	import System.MC.Overlay;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class DoorTile extends Tile implements INamable, IActivatable, IOverlay, ISavable
	{
		private var name:String;
		private var sprite:Animation;
		public function DoorTile(name:String, sprite:Animation, activated:Boolean) 
		{
			super(false, null);
			View.addChild(sprite);
			this.sprite = sprite;
			this.name = name;
			if (activated)
				Activate();
		}
		public function set OverlayLevel(val:int):void { }
		public function get OverlayLevel():int { return Overlay.Location_FloorForeground; }	
		public function SetInfo(obj:Object):void
		{
			sprite.SetAnimation(obj["animation"]);
		}
		public function GetInfo():Object { return { "animation": sprite.CurrentAnimation }; }	
		public function Activate():void
		{
			if (sprite.CurrentAnimation == 1)
			{
				sprite.SetAnimation(0);
			}
			else
			{
				sprite.SetAnimation(1);
			}
		}
		public override function Intersects(obj:CollisionBox, movement:Point):ICollidable
		{
			if (sprite.CurrentAnimation == 1) return null;
			else return super.Intersects(obj, movement);
		}
		public override function IntersectsLine(x1:Number, x2:Number, y1:Number, y2:Number, movement:Point):ICollidable
		{
			if (sprite.CurrentAnimation == 1) return null;
			else return super.IntersectsLine(x1, x2, y1, y2, movement);
		}
		public function get Name():String  { return name; }
	}

}