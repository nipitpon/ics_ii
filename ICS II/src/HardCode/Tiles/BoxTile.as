package HardCode.Tiles 
{
	import flash.geom.Point;
	import Resources.lib.Environment.BoxSprite;
	import System.GameSystem;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IMovable;
	import System.Interfaces.ISavable;
	import System.MC.Characters.Character;
	import System.MC.Characters.Monster;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class BoxTile extends Tile implements IMovable, ISavable
	{
		public function BoxTile(view:RenderableObject = null) 
		{
			super(false, null);
			collide_with.push(Tile, Player);
			if(view == null)
				View.addChild(new BoxSprite());
			else 
				View.addChild(view);
		}
		public function SetInfo(obj:Object):void
		{
			Center = obj["center"];
		}
		public function GetInfo():Object { return { "center":Center }; }	
		public override function get DefyGravity():Boolean { return false; }
		public override function Hit(obj:ICollidable, movement:Point):void
		{
			super.Hit(obj, movement);
			if (obj is Tile || obj is Player)
			{
				var cBox:CollisionBox = obj.Collision;
				if (movement.y != 0 && obj.Intersects(Collision, new Point(0, movement.y)))
				{
					var result:Number = cBox.GetTopAtPos(Collision.Left, Collision.Right, Collision.CenterY + movement.y);
					if (movement.y > 0)
					{
						force.y = 0;
						movement.y = Math.max(result - Collision.Bottom, 0);
					}
					else if (movement.y < 0)
					{
						force.y = 0;
						movement.y = Math.min(cBox.GetBottomAtPos(Collision.Left, Collision.Right, Collision.CenterY + movement.y) - Collision.Top, 0);
					}
				}
			}
			else if (obj is Monster && movement.y > GameSystem.Gravity)
			{
				(obj as Monster).SmashByBox();
			}
		}
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			super.TakenHit(obj, movement);
			if (obj is Player && obj.Collision.Bottom > Collision.CenterY && (obj as Player).IsMoving)
			{
				//ClearForceX();
				//PushWithDecay(movement.x * 0.75, 0, 0.5);
				var new_movement:Point = new Point((obj as Character).Speed * 0.5, 0);
				if ((obj as Character).IsMovingLeft)
					new_movement.x *= -1;
				
				var col:Vector.<ICollidable> = GameSystem.GetIntersectedCollidable(Collision, new_movement);
				for (var i:int = 0; i < col.length; i++)
				{
					if ((col[i] is Tile || col[i] is Monster) && col[i] != this)
					{
						if (new_movement.x > 0)
							new_movement.x = Math.min(col[i].Collision.GetLeftAtPos(Collision.CenterX + new_movement.x, Collision.Top + movement.y, Collision.Bottom) - Collision.Right, new_movement.x);
						else if (new_movement.x < 0)
							new_movement.x = Math.max(new_movement.x, col[i].Collision.GetRightAtPos(Collision.CenterX + new_movement.x, Collision.Top + movement.y, Collision.Bottom) - Collision.Left + 1);
					}
				}
				MoveByOffset(new_movement.x, new_movement.y);
				if(new_movement.x < 0)
					movement.x = Math.min(0, Collision.Right - (obj as Character).Collision.Left);
				else
					movement.x = Math.max(0, Collision.Left - (obj as Character).Collision.Right);
				if ((obj as Character).CurrentAnimation != Player.Box)
					(obj as Character).SetAnimation(Player.Box);
			}
		}
	}

}