package HardCode.Tiles 
{
	import Resources.lib.GrassTileSprite;
	/**
	 * ...
	 * @author Knight
	 */
	public class StaticGrassTile extends StaticTile 
	{
		
		public function StaticGrassTile(collision_info:Vector.<Vector.<int>>) 
		{
			super(collision_info);
			for (var i:int = 0; i < collision_info.length; i++) for (var j:int = 0; j < collision_info[i].length; j++)
			{
				if (collision_info[i][j] < 0) continue;
				
				var sprite:GrassTileSprite = new GrassTileSprite(collision_info[i][j]);
				sprite.x = Size.x * j;
				sprite.y = Size.y * i;
				View.addChild(sprite);
			}
		}
		
	}

}