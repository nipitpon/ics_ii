package HardCode.Tiles 
{
	import flash.geom.Point;
	import Resources.lib.Environment.SpikeSprite;
	import Resources.lib.Sound.SFX.SpikeSound;
	import System.GameSystem;
	import System.Interfaces.ICollidable;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Collision.CollisionTile;
	import System.MC.Tile;
	import System.SoundCenter;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SpikeTile extends Tile 
	{
		
		public function SpikeTile(length:* = 1) 
		{
			super(false, length is Vector.<Vector.<int>> ? new CollisionTile(length as Vector.<Vector.<int>>, uint.MAX_VALUE, 10) : 
				new CollisionRectangle(0, Tile.Size.y * 0.6, Tile.Size.x * length, Tile.Size.y * 0.4), 1, 1);
			if(length is int) for (var i:int = 0; i < length; i++)
			{
				var sprite:SpikeSprite = new SpikeSprite();
				sprite.x = Tile.Size.x * i;
				View.addChild(sprite);
				
			}
		}
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			super.TakenHit(obj, movement);
			if (obj is Player && movement.y > GameSystem.Gravity && !((obj as Player).IsDying))
			{
				(obj as Player).DieFromSpike();
				SoundCenter.PlaySFXFromInstance(new SpikeSound());
			}
		}
	}

}