package HardCode.Tiles 
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author ss
	 */
	public final class CannonRope 
	{
		public var next:CannonRope;
		public var start:Point;
		public var end:Point;
		public var interpolate_multiplier:Number;
		public function CannonRope(start:Point, end:Point) 
		{
			interpolate_multiplier = 1;
			this.start = start;
			this.end = end;
		}
		
	}

}