package HardCode.Tiles 
{
	import flash.geom.Point;
	import Resources.lib.GrassTileSprite;
	
	/**
	 * ...
	 * @author ...
	 */
	public class DynamicGrassTile extends DynamicTile 
	{
		
		public function DynamicGrassTile(collision_info:Vector.<Vector.<int>>, start:Point, end:Point, speed:Number) 
		{
			super(collision_info, start, end, speed);
			
			for (var i:int = 0; i < collision_info.length; i++) for (var j:int = 0; j < collision_info[i].length; j++)
			{
				if (collision_info[i][j] < 0) continue;
				
				var sprite:GrassTileSprite = new GrassTileSprite(collision_info[i][j]);
				sprite.x = Size.x * j;
				sprite.y = Size.y * i;
				View.addChild(sprite);
			}
		}
		
	}

}