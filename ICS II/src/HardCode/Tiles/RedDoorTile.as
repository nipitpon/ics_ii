package HardCode.Tiles 
{
	import Resources.lib.Doors.RedDoorSprite;
	import System.Interfaces.IAnimatable;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class RedDoorTile extends DoorTile 
	{
		public static const Name:String = "RedDoor";
		public function RedDoorTile(activated:Boolean) 
		{
			super(RedDoorTile.Name, new RedDoorSprite(), activated);
			
		}
		
	}

}