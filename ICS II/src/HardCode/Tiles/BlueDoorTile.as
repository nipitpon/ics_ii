package HardCode.Tiles 
{
	import Resources.lib.Doors.BlueDoorSprite;
	import System.Interfaces.IAnimatable;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class BlueDoorTile extends DoorTile 
	{
		public static const Name:String = "BlueDoor";
		public function BlueDoorTile(activated:Boolean) 
		{
			super(BlueDoorTile.Name, new BlueDoorSprite(), activated);
			
		}
		
	}

}