package HardCode.Tiles 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import HardCode.Effects.SmokeParticle;
	import HardCode.Projectiles.CannonBall;
	import HardCode.Projectiles.Rocket;
	import Resources.lib.Effects.FireParticleSprite;
	import Resources.lib.Sound.SFX.CannonFireSound;
	import Resources.lib.Sound.SFX.CannonFuseSound;
	import Resources.lib.Tiles;
	import System.GameFrame;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IBurnable;
	import System.Interfaces.ICachable;
	import System.Interfaces.ICacher;
	import System.Interfaces.ICollidable;
	import System.Interfaces.ILaunchable;
	import System.Interfaces.IOverlay;
	import System.Interfaces.IProjectileLauncher;
	import System.Interfaces.ISound;
	import System.Interfaces.IUpdatable;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.etc.ForceStruct;
	import System.MC.Level;
	import System.MC.ModelViewController;
	import System.MC.Overlay;
	import System.MC.Tile;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class Cannon extends CollidableObject implements IOverlay, IUpdatable, ICacher, IProjectileLauncher, IBurnable
	{
		private var rope:CannonRope;
		private var current_rope:CannonRope;
		private var count:Number;
		private var indicator_sprite:IAnimatable;
		private var cooldown_count:int;
		private var cannon_position:Point;
		private var cannon_direction:int;
		private var fire_sprite:FireParticleSprite;
		private var fuse_sound:ISound;
		private static var smoke_cache:Vector.<SmokeParticle> = new Vector.<SmokeParticle>();
		public function Cannon(data:Object, tilesets:Object) 
		{
			addEventListener(ModelViewController.Removed, onRemoved);
			count = -1;
			cooldown_count = 0;
			fuse_sound = new CannonFuseSound();
			var rope_start:CollisionBox = null;
			fire_sprite = new FireParticleSprite();
			var max_width:int;
			var max_height:int;
			var node:Object;
			
			var i:int = 0;
			var j:int = 0;
			var tile_number:int = 0;
			for each(tile_number in data.data)
			{	
				
				if (tile_number > 0)
				{
					if (j > max_height) max_height = j;
					if (i > max_width) max_width = i;
				}

				if (++i % data.width == 0 && i > 0)
				{
					i -= parseInt(data.width);
					j++;
				}
			}
			
			var dat:BitmapData = new BitmapData(max_width * Tile.Size.x, max_height * Tile.Size.y, true, 0);
			
			i = 0;
			j = 0;
			for each(tile_number in data.data)
			{
				if (tile_number > 0)
				{
					var map:Object = null;
					for each(var m:Object in tilesets)
					{
						if (parseInt(m.firstgid) <= tile_number && (map == null || parseInt(map.firstgid) < parseInt(m.firstgid)))
						{
							map = m;
						}
					}
					if (map != null && (map.tiles == null || map.tiles[(tile_number - map.firstgid).toString()] == null || 
						map.tiles[(tile_number - map.firstgid).toString()].animation == null))
					{
						var origin:BitmapData = Tiles.GetBitmapData(map.image);
						
						var pos:Point = Level.GetTilePosition(tile_number, map.firstgid, map.spacing, origin);
						dat.copyPixels(origin, new Rectangle(pos.x * ((Tile.Size.x / 2) + map.spacing), pos.y * ((Tile.Size.y / 2) + map.spacing), Tile.Size.x / 2, Tile.Size.y / 2),
							new Point(((Tile.Size.x / 2)) * (i % data.width), ((Tile.Size.y / 2)) * j));
							
						node = map.tileproperties[(tile_number - parseInt(map.firstgid)).toString()];
						if (node.position == "cannon_start")
						{
							var offset_x:Number = 0;
							var offset_y:Number = 0;
							if (node.direction == "0" || node.direction == "2")
								offset_x = 0.25;
							if (node.direction == "1" || node.direction == "3")
								offset_y = 0.25;
							if (node.direction == "1")
								offset_x = 0.5;
							if (node.direction == "2")
								offset_y = 0.5;
							rope_start = new CollisionRectangle(Tile.Size.x * ((i % data.width) + offset_x), Tile.Size.y * (j + offset_y), 16, 16);
							rope = new CannonRope(new Point(Tile.Size.x * (i + 0.5), Tile.Size.y * (j + 0.5)), 
								new Point(Tile.Size.x * (i + (offset_x * 2)), Tile.Size.y * (j + offset_y * 2)));
							rope.interpolate_multiplier = 0.5;
						}
						else if (node.position == "indicator")
						{
							indicator_sprite = Level.LoadObjectSpriteFromTile(tile_number, map.tileproperties, map.firstgid, origin) as IAnimatable;
							(indicator_sprite as RenderableObject).x = Tile.Size.x * i;
							(indicator_sprite as RenderableObject).y = Tile.Size.y * j;
							indicator_sprite.SetAnimation(0);
						}
						else if (node.position == "cannon")
						{
							cannon_position = new Point(Tile.Size.x * (i + 0.5), Tile.Size.y * (j + 0.5));
							cannon_direction = parseInt(node.direction);
						}
					}
				}
				if (++i % data.width == 0 && i > 0)
				{
					i -= parseInt(data.width);
					j++;
				}
			}
			
			var connect_rope:Function = function(current:CannonRope):void
			{
				var current_i:int = ((current.start.x + current.end.x) / 2) / Tile.Size.x;
				var current_j:int = ((current.start.y + current.end.y) / 2) / Tile.Size.y;
				
				var next_i:int = current_i;
				var next_j:int = current_j;
				if ((current_j + 0.5) * Tile.Size.y > current.end.y) next_j--;
				else if ((current_j + 0.5) * Tile.Size.y < current.end.y) next_j++;
				else if ((current_i + 0.5) * Tile.Size.x > current.end.x) next_i--;
				else if ((current_i + 0.5) * Tile.Size.x < current.end.x) next_i++;
				
				var tile_number:int = data.data[(next_j * data.width) + next_i];
				
				var map:Object = null;
				for each(var m:Object in tilesets)
				{
					if (parseInt(m.firstgid) <= tile_number && (map == null || parseInt(map.firstgid) < parseInt(m.firstgid)))
					{
						map = m;
					}
				}
				if (map != null)
				{
					node = map.tileproperties[(tile_number - parseInt(map.firstgid)).toString()];
					if (node.position == "rope")
					{
						var directions:Array = node.direction.split(',');
						var points:Vector.<Point> = Vector.<Point>([
							new Point(Tile.Size.x * next_i, Tile.Size.y * next_j), 
							new Point(Tile.Size.x * next_i, Tile.Size.y * next_j)])
						for (i = 0; i < 2; i++)
						{
							switch(parseInt(directions[i]))
							{
								case 0: points[i].x += Tile.Size.x / 2.0; break;
								case 1: 
									points[i].x += Tile.Size.x;
									points[i].y += Tile.Size.y / 2.0;
									break;
								case 2:
									points[i].x += Tile.Size.x / 2.0;
									points[i].y += Tile.Size.y;
									break;
								case 3: points[i].y += Tile.Size.y / 2.0; break;
							}
						}
						var new_part:CannonRope = new CannonRope(null, null);
						if (points[0].x == current.end.x && points[0].y == current.end.y)
						{
							new_part.start = points[0];
							new_part.end = points[1];
						}
						else
						{
							new_part.start = points[1];
							new_part.end = points[0];
						}
						current.next = new_part;
						connect_rope(new_part);
					}
				}
			};
			connect_rope(rope);
			
			var bmp:Bitmap = new Bitmap(dat);
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			var ren:RenderableObject = new RenderableObject();
			ren.addChild(bmp);
			ren.Left = 0;
			ren.Top = 0;
			ren.addChild(indicator_sprite as RenderableObject);
			super(ren, rope_start);
			collide_with = Vector.<Class>([IBurnable, Rocket]);
		}
		public function Update():void 
		{
			if (cooldown_count > 0)
			{
				cooldown_count--;
				if (cooldown_count == 0)
				{
					count = -1;
					indicator_sprite.SetAnimation(0);
				}
			}
			else if (count >= 0)
			{
				count += current_rope.interpolate_multiplier / 5;
				var pos:Point = Point.interpolate(current_rope.end, current_rope.start, count);
				fire_sprite.CenterX = pos.x;
				fire_sprite.CenterY = pos.y;
				if (Math.floor(count * 10) % 3 == 0)
				{
					var smoke:SmokeParticle = null;
					if (smoke_cache.length == 0)
						smoke = new SmokeParticle(this);
					else
						smoke = smoke_cache.shift();
					smoke.CenterX = pos.x;
					smoke.CenterY = pos.y;
					GameFrame.AddChild(smoke);
				}
				if (count >= 1)
				{
					current_rope = current_rope.next;
					if (current_rope == null)
					{
						FireCannon();
						View.removeChild(fire_sprite);
					}
					else
						count = 0;
				}
			}
		}
		public function FireCannon():void
		{
			fuse_sound.Stop();
			SoundCenter.PlaySFXFromInstance(new CannonFireSound());
			cooldown_count = Utility.SecondToFrame(1);
			indicator_sprite.SetAnimation(1);
			ReleaseProjectile(CannonBall, 90 * (cannon_direction - 1), null);
		}
		public function Cache(obj:ICachable):void
		{
			smoke_cache.push(obj as SmokeParticle);
		}
		public function ReleaseProjectile(type:Class, angleOffset:Number, pushBack:ForceStruct):Boolean 
		{
			var projectile:ILaunchable = new type(this);
			projectile.Center = cannon_position;
			
			projectile.View.rotation = angleOffset;
			projectile.LaunchAt(cannon_position.x, cannon_position.y);
			GameFrame.AddChild(projectile);
			
			return true;
		}
		public function get IsBurning():Boolean { return count >= 0 || cooldown_count > 0; }
		public function get ReleasePoint():Point { return cannon_position; }
		public function get ProjectileHoming():Boolean  { return false; } 
		public function set ProjectileHoming(value:Boolean):void  { }
		public function get UpdateWhenAdvancingTime():Boolean { return true; }
		public function set BottomMost(value:Boolean):void { }
		public function get BottomMost():Boolean { return false; }
		public function set OverlayLevel(value:int):void { }
		public function get OverlayLevel():int { return Overlay.Location_Floor; }
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			super.TakenHit(obj, movement);
			if (count < 0 && cooldown_count == 0 && ((obj is Rocket) || (obj as Player).IsBurning))
			{
				Burn();
			}
		}
		public function Burn():void
		{
			current_rope = rope;
			View.addChild(fire_sprite);
			fire_sprite.CenterX = current_rope.start.x;
			fire_sprite.CenterY = current_rope.start.y;
			count = 0;
			SoundCenter.PlaySFXFromInstance(fuse_sound, 1, 999999);
		}
		private function onRemoved(e:Event):void
		{
			if (fuse_sound.IsPlaying)
			{
				fuse_sound.Stop();
			}
		}
	}

}