package HardCode.Tiles 
{
	import _Base.MySound;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display3D.IndexBuffer3D;
	import flash.geom.Point;
	import HardCode.Buffs.Burn;
	import HardCode.Effects.TileBurntEffect;
	import HardCode.Levels.IntroToBurnable;
	import Resources.lib.DynamicFlipbook;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.IBuff;
	import System.Interfaces.IBuffHolder;
	import System.Interfaces.IBurnable;
	import System.Interfaces.ICollidable;
	import System.Interfaces.ISavable;
	import System.Interfaces.IUpdatable;
	import System.MC.Characters.Character;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Tile;
	import System.Time;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class BurnableTiles extends Tile implements IBurnable, IBuffHolder, IUpdatable, ISavable
	{
		private var buffs:Vector.<IBuff>;
		private var hp:int;
		public function BurnableTiles(dat:BitmapData) 
		{
			super(false);
			
			buffs = new Vector.<IBuff>();
			var bmp:Bitmap = new Bitmap(dat);
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			View.addChild(bmp);
			hp = 30;
		}
		public function get IsBurning():Boolean { return HasBuff(HardCode.Buffs.Burn); }
		public override function get UpdateWhenAdvancingTime():Boolean { return true; }
		public function SetInfo(obj:Object):void 
		{ 
			hp = obj["hp"]; 
			if (hp != 30)
				Burn();
		}
		public function GetInfo():Object { return { "hp": hp }; }
		public function Burn():void
		{
			AddBuff(HardCode.Buffs.Burn);
		}
		public override function Update():void
		{
			super.Update();
			if (IsBurning)
			{
				if (Time.IsAdvancing)
					hp -= 10;
				else
					hp--;
				if (hp <= 0)
				{
					var box:CollisionBox = new CollisionRectangle(Collision.Left - Tile.Size.x / 2,
						Collision.Top - Tile.Size.y / 2, Tile.Size.x * 2, Tile.Size.y * 2);
					var collidables:Vector.<ICollidable> = GameSystem.GetIntersectedCollidable(box, null);
					for each(var collidable:ICollidable in collidables)
					{
						var c:IBurnable = collidable as IBurnable;
						if (c != null && !(c is Player) && !c.IsBurning && 
							GameSystem.GetIntersectedWaterBlock(c.Collision) == null)
						{
							c.Burn();
						}
					}
					RemoveBuff(HardCode.Buffs.Burn).Deactivate();
					GameFrame.RemoveChild(this);
					GameSystem.CurrentLevel.RemoveObject(this);
					var effect:TileBurntEffect = new TileBurntEffect();
					effect.CenterX = Collision.CenterX;
					effect.CenterY = Collision.CenterY;
					GameFrame.AddChild(effect);
				}
			}
		}
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			super.TakenHit(obj, movement);
			if (obj is IBurnable)
			{
				if (!IsBurning && (obj as IBurnable).IsBurning)
				{
					Burn();
				}
				else if (IsBurning && !(obj as IBurnable).IsBurning)
				{
					(obj as IBurnable).Burn();
				}
			}
		}
		public function HasBuff(buff:Class):Boolean
		{
			for each(var b:IBuff in buffs)
			{
				if (b is buff) return true;
			}
			return false;
		}
		public function AddBuff(buff:Class):IBuff
		{
			for each(var buf:IBuff in buffs)
			{
				if (buf is buff)
				{
					buf.Deactivate();
					break;
				}
			}
			
			var b:IBuff = new buff(this);
			b.Activate();
			buffs.push(b);
			return b;
		}
		public function RemoveBuff(buff:Class):IBuff
		{
			for (var i:int = 0; i < buffs.length; i++)
			{
				if (buffs[i] is buff)
				{
					return buffs.splice(i, 1)[0];
				}
			}
			return null;
		}
	}

}