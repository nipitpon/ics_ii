package HardCode.Tiles 
{
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionTile;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class StaticTile extends Tile 
	{
		
		public function StaticTile(collision_info:Vector.<Vector.<int>>, direction:uint = uint.MAX_VALUE) 
		{
			super(false, new CollisionTile(collision_info, direction));
		}
	}
}