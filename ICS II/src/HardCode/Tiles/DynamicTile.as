package HardCode.Tiles 
{
	import fl.transitions.easing.None;
	import flash.events.Event;
	import flash.geom.Point;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IUpdatable;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionTile;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author ...
	 */
	public class DynamicTile extends Tile implements IUpdatable
	{
		private var start:Point;
		private var end:Point;
		private var dest:Point;
		private var count:int;
		public function DynamicTile(info:Vector.<Vector.<int>>, start:Point, end:Point, speed:Number) 
		{
			super(false, new CollisionTile(info));
			this.start = start;
			this.end = end;
			this.speed = speed;
			addEventListener(Added, onAdded);
			collide_with.push(Player);
			count = 0;
		}
		public override function Update():void
		{
			super.Update();
			if (ForceX == 0 && ForceY == 0)
			{
				count--;
				if (count == 0)
				{
					count = Utility.SecondToFrame(2);
					onAdded(null);
				}
			}
		}
		public override function Hit(obj:ICollidable, movement:Point):void
		{
			super.Hit(obj, movement);
			if (obj is Player)
			{
				if (movement.x < 0)
				{
					obj.MoveByOffset(obj.Collision.Right - Collision.Left, 0);
				}
				else if (movement.x > 0)
				{
					obj.MoveByOffset(Collision.Right - obj.Collision.Left, 0);
				}
			}
		}
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			super.TakenHit(obj, movement);
			if (obj.Collision.CenterY <= Collision.Top && obj.Collision.Right > Collision.Left && obj.Collision.Left < Collision.Right)
			{
				movement.y = Collision.Top - obj.Collision.Bottom + ForceY;
				movement.x += ForceX;
			}
		}
		public override function MoveByOffset(x:int, y:int):void
		{
			super.MoveByOffset(x, y);
			if (dest != null)
			{
				if ((x > 0 && CenterX >= dest.x) || (x < 0 && CenterX <= dest.x))
				{
					ClearForceX();
				}
				if ((y > 0 && CenterY >= dest.y) || (y < 0 && CenterY <= dest.y))
				{
					ClearForceY();
				}
			}
		}
		private function onAdded(e:Event):void
		{
			ClearAllForce();
			if (Point.distance(Center, start) > Point.distance(Center, end))
				dest = start;
			else
				dest = end;
			if (CenterX < dest.x)
				Push(speed, 0);
			else if(CenterX > dest.x)
				Push( -speed, 0);
			if (CenterY < dest.y)
				Push(0, speed);
			else if(CenterY > dest.y)
				Push(0, -speed);
			count = Utility.SecondToFrame(2);
		}
	}
	

}