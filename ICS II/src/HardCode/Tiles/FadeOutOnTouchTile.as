package HardCode.Tiles 
{
	import fl.transitions.easing.None;
	import fl.transitions.easing.Regular;
	import fl.transitions.easing.Strong;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IUpdatable;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	/**
	 * ...
	 * @author Knight
	 */
	public class FadeOutOnTouchTile extends StaticGrassTile implements IUpdatable
	{
		private static const MaxFadeCount:int = Utility.SecondToFrame(1);
		private static const MaxHideCount:int = Utility.SecondToFrame(2);
		private static const FlashingTransform:ColorTransform = new ColorTransform(1, 1, 1, 1, 255, 255, 255);
		private static const NonFlashingTransform:ColorTransform = new ColorTransform();
		private var fade_count:int;
		private var hide_count:int;
		private var fade_in:Boolean;
		private var flashed:Boolean;
		public function FadeOutOnTouchTile(collision_info:Vector.<Vector.<int>>) 
		{
			super(collision_info);
			fade_count = -1;
			hide_count = -1;
			fade_in = true;
		}
		
		public override function Update():void
		{
			super.Update();
			if (hide_count >= 0)
			{
				hide_count--;
				if (fade_in)
				{
					if (hide_count % 3 == 0)
					{
						if (flashed)
							View.transform.colorTransform = NonFlashingTransform;
						else
							View.transform.colorTransform = FlashingTransform;
						flashed = !flashed;
						//View.alpha = (View.alpha == 1) ? 0.5 : 1;
					}
				}
				if (hide_count < 0)
				{
					fade_count = MaxFadeCount;
					fade_in = !fade_in;
					View.transform.colorTransform = NonFlashingTransform;
					View.alpha = 1;
				}
			}
			
			if (fade_count >= 0)
			{
				if (fade_in)
					View.alpha = Strong.easeInOut(MaxFadeCount - fade_count, 0.5, 0.5, MaxFadeCount);
				else
					View.alpha = Strong.easeInOut(MaxFadeCount - fade_count, 1, -0.5, MaxFadeCount);
				fade_count--;
				if (fade_count < 0)
				{
					if (fade_in)
						hide_count = -1;
					else
						hide_count = MaxHideCount;
				}
			}
		}
		public override function Intersects(collision:CollisionBox, movement:Point):ICollidable
		{
			if (View.alpha == 1 || (hide_count >= 0 && fade_in)) return super.Intersects(collision, movement);
			else return null;
		}
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			if (obj is Player && movement.y >= 0 && fade_count < 0 && hide_count < 0 &&
				obj.Collision.Right > Collision.Left && obj.Collision.Left < Collision.Right)
			{
				flashed = false;
				hide_count = MaxHideCount;
			}
			super.TakenHit(obj, movement);
		}
	}

}