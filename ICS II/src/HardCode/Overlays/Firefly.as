package HardCode.Overlays 
{
	import fl.transitions.easing.None;
	import flash.events.Event;
	import Resources.lib.Environment.FireflySprite;
	import System.Interfaces.INegateAmbient;
	import System.Interfaces.IUpdatable;
	import System.MC.InvisibleGrassTile;
	import System.MC.Overlay;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Firefly extends Overlay implements IUpdatable
	{
		private var start:Number;
		private var end:Number;
		private var duration:int;
		private var count:int;
		public function Firefly(location:int) 
		{
			super(new FireflySprite(), location);
			addEventListener(Added, onAdded);
		}
		public function get UpdateWhenAdvancingTime():Boolean { return true; }
		public function Update():void
		{
			count++;
			CenterY = None.easeNone(count, start, end - start, duration);
			if (count == duration)
			{
				count = 0;
				var temp:int = start;
				start = end;
				end = temp;
			}
		}
		private function onAdded(e:Event):void
		{
			start = CenterY;
			end = CenterY + 10;
			duration = Utility.Random(Utility.SecondToFrame(4), Utility.SecondToFrame(8));
			count = Utility.Random(0, duration - 1);
		}
	}

}