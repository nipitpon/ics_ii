package HardCode.Overlays 
{
	import flash.events.Event;
	import flash.geom.Point;
	import Resources.Animation;
	import Resources.lib.NPC.CheckPointSprite;
	import Resources.lib.Sound.SFX.CheckPointSound;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IInteractor;
	import System.Interfaces.IMovable;
	import System.Interfaces.INamable;
	import System.Interfaces.IOverlay;
	import System.Interfaces.IUpdatable;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Interactables.InteractableObject;
	import System.MC.Level;
	import System.MC.Overlay;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class CheckPoint extends InteractableObject implements IUpdatable, INamable
	{
		private static var last_activate:CheckPoint = null;
		private var count:int;
		public function CheckPoint(view:RenderableObject) 
		{
			super(new CheckPointSprite(), new CollisionRectangle(16, 0, 32, 64), "Check Point");
			collide_with = Vector.<Class>([ Player]);
		}
		public function set BottomMost(value:Boolean):void { }
		public function get BottomMost():Boolean { return false; }
		public function get Name():String { return "CheckPoint"; }
		public function get OverlayLevel():int { return Overlay.Location_Character; }
		public override function Update():void
		{
			super.Update();
			if (last_activate == this && count > 0 && --count <= 0)
			{
				last_activate = null;
			}
		}
		public override function GetInteracted(interactor:IInteractor):void 
		{
			super.GetInteracted(interactor);
			SaveInfo();
		}
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			if (last_activate != this && IntersectsPoint(obj.Collision.Center, movement) != null)
			{
				last_activate = this;
				//SaveInfo();
				GetInteracted(obj as IInteractor);
			}
			count = Utility.SecondToFrame(0.5);
		}
		public static function ClearLastActivation():void
		{
			last_activate = null;
		}
		private function SaveInfo():void
		{
			GameSystem.CurrentLevel.SaveObjectInfo();
			SoundCenter.PlaySFXFromInstance(new CheckPointSound());
			var anim:IAnimatable = View as IAnimatable;
			if (anim.CurrentAnimation == 0)
			{
				anim.SetAnimation(1);
				View.addEventListener(Animation.FinishAnimation, onFinishAnimation);
			}
			var checkpoints:Vector.<INamable> = GameSystem.GetObjectByNames(Name);
			for each(var checkpoint:INamable in checkpoints)
			{
				if (checkpoint != this && ((checkpoint as CheckPoint).View as IAnimatable).CurrentAnimation != 0)
				{
					var cp:CheckPoint = checkpoint as CheckPoint;
					cp.View.removeEventListener(Animation.FinishAnimation, cp.onFinishAnimation);
					cp.TurnOff();
				}
			}
		}
		private function TurnOff():void
		{
			(View as IAnimatable).SetAnimation(3);
			View.addEventListener(Animation.FinishAnimation, onTurnedOff);
		}
		private function onFinishAnimation(e:Event):void
		{
			(View as IAnimatable).SetAnimation(2);
			View.removeEventListener(Animation.FinishAnimation, onFinishAnimation);
		}
		private function onTurnedOff(e:Event):void
		{
			(View as IAnimatable).SetAnimation(0);
			View.removeEventListener(Animation.FinishAnimation, onTurnedOff);
		}
	}

}