package HardCode.Overlays 
{
	import flash.display.Bitmap;
	import flash.geom.ColorTransform;
	import System.MC.Overlay;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Decoration extends Overlay 
	{
		
		public function Decoration(view_class:Class, layer:int = Location_FloorForeground) 
		{
			var bmp:Bitmap = new view_class();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			var ren:RenderableObject = new RenderableObject();
			ren.addChild(bmp);
			super(ren, layer);
			
		}
		
	}

}