package HardCode.Overlays 
{
	import Resources.lib.DirtTileSprite;
	import System.MC.Overlay;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class DirtOverlay extends Overlay 
	{
		
		public function DirtOverlay(info:Vector.<Vector.<int>>, y_offset:int = 0) 
		{
			var view:RenderableObject = new RenderableObject();
			super(view, Overlay.Location_FloorBackground);
			for (var i:int = 0; i < info.length; i++) for (var j:int = 0; j < info[i].length; j++)
			{
				if (info[i][j] >= 0)
				{
					var sprite:DirtTileSprite = new DirtTileSprite(info[i][j]);
					sprite.x = Tile.Size.x * j;
					sprite.y = Tile.Size.y * (i + y_offset);
					view.addChild(sprite);
				}
			}
		}
		
	}

}