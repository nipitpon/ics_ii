package HardCode.Overlays 
{
	import System.Interfaces.IUpdatable;
	import System.MC.Overlay;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class SpinnableTile extends Overlay implements IUpdatable 
	{
		private var rotation:Number;
		public function SpinnableTile(view:RenderableObject, location:int, rotation:Number) 
		{
			super(view, location);
			this.rotation = rotation;
		}
		public function Update():void 
		{
			View.rotation += rotation;
		}
		public function get UpdateWhenAdvancingTime():Boolean { return true; }
	}

}