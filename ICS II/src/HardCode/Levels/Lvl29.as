package HardCode.Levels 
{
	import flash.utils.ByteArray;
	import System.MC.Level;
	
	/**
	 * ...
	 * @author ss
	 */
	public class Lvl29 extends Level 
	{
		[Embed(source = "../../../lib/Maps/lvl_29.json", mimeType = "application/octet-stream")]
		private static const map:Class;
		public function Lvl29(next_level:int) 
		{
			super();
			var b:ByteArray = new map();
			var info:Object = JSON.parse(b.readUTFBytes(b.length));
			LoadJSON(info, next_level);
		}
		
	}

}