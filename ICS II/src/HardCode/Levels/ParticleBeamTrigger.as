package HardCode.Levels 
{
	import flash.display.Shape;
	import flash.geom.Point;
	import flash.text.engine.RenderingMode;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IOverlay;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Flags.EventTrigger;
	import System.MC.Overlay;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ParticleBeamTrigger extends CollidableObject implements IOverlay
	{
		
		public function ParticleBeamTrigger() 
		{
			var ren:RenderableObject = new RenderableObject();
			super(ren, new CollisionRectangle(0, 0, Tile.Size.x, Tile.Size.y * 20));
			collide_with = Vector.<Class>([Player]);
			Left = Tile.Size.x * 252;
		}
		public function set BottomMost(val:Boolean):void {}
		public function get BottomMost():Boolean { return false; }
		public function set OverlayLevel(val:int):void {}
		public function get OverlayLevel():int { return Overlay.Location_Floor; }
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			if (obj is Player)
			{
				GameFrame.RemoveChild(this);
				(GameSystem.GetObjectByName("ParticleBeam") as ParticleBeam).Activate();
			}
		}
	}

}