package HardCode.Levels 
{
	import flash.geom.Point;
	import flash.system.System;
	import flash.text.engine.RenderingMode;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IOverlay;
	import System.Interfaces.ISavable;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Overlay;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class RuinGardenTrigger2 extends CollidableObject implements IOverlay, ISavable
	{
		
		public function RuinGardenTrigger2() 
		{
			super(new RenderableObject(), new CollisionRectangle(0, 0, Tile.Size.x * 1, Tile.Size.y * 7));
			collide_with = Vector.<Class>([Player]);
		}
		public function set BottomMost(val:Boolean):void {}
		public function get BottomMost():Boolean { return false; }
		public function set OverlayLevel(val:int):void {}
		public function get OverlayLevel():int { return Overlay.Location_Floor; }
		public function SetInfo(obj:Object):void { }
		public function GetInfo():Object { return { }; }	
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			if (obj is Player)
			{
				GameFrame.RemoveChild(this);
				(GameSystem.GetObjectByName("RuinGardenBeam") as RuinGardenBeam).Activate();
			}
		}
	}

}