package HardCode.Levels 
{
	import flash.utils.ByteArray;
	import System.MC.Level;
	
	/**
	 * ...
	 * @author ss
	 */
	public class EntranceToRuin extends Level 
	{
		[Embed(source = "../../../lib/Maps/entrance_to_ruin.json", mimeType = "application/octet-stream")]
		private static const map:Class;
		public function EntranceToRuin(next_level:int) 
		{
			super();
			var b:ByteArray = new map();
			var info:Object = JSON.parse(b.readUTFBytes(b.length));
			LoadJSON(info, next_level);
			
		}
		
	}

}