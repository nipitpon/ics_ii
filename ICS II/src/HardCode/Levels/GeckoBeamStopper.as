package HardCode.Levels 
{
	import flash.events.Event;
	import flash.geom.Point;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IOverlay;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Overlay;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class GeckoBeamStopper extends CollidableObject implements IOverlay 
	{
		
		public function GeckoBeamStopper() 
		{
			super(new RenderableObject(), new CollisionRectangle(295 * Tile.Size.x, 28 * Tile.Size.y, Tile.Size.x * 2, Tile.Size.y * 18));
			collide_with = Vector.<Class>([Player]);
		}
		public function set BottomMost(value:Boolean):void { } 
		public function get BottomMost():Boolean { return false; }
		public function set OverlayLevel(value:int):void { }
		public function get OverlayLevel():int { return Overlay.Location_Floor; }
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			super.TakenHit(obj, movement);
			if (obj is Player)
			{
				var o:GeckoBeamScript = GameSystem.GetObjectByName("GeckoBeamScript") as GeckoBeamScript;
				o.DeactivateBeam();
				GameFrame.RemoveChild(this);
			}
		}
		
	}

}