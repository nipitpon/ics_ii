package HardCode.Levels 
{
	import flash.geom.Point;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IOverlay;
	import System.Interfaces.ISavable;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Overlay;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class RuinGardenTrigger extends CollidableObject implements IOverlay, ISavable
	{
		
		public function RuinGardenTrigger() 
		{
			super(new RenderableObject(), new CollisionRectangle(0, 0, Tile.Size.x, Tile.Size.y * 4));
			collide_with = Vector.<Class>([Player]);
		}
		public function set BottomMost(val:Boolean):void {}
		public function get BottomMost():Boolean { return false; }
		public function set OverlayLevel(val:int):void {}
		public function get OverlayLevel():int { return Overlay.Location_Floor; }
		public function GetInfo():Object { return { }; }
		public function SetInfo(obj:Object):void 
		{
			if (obj["center"] != null)
			{
				var beam:RuinGardenBeam = new RuinGardenBeam(); 
				beam.ID = ID;
				beam.SetInfo(obj);
				GameFrame.AddChild(beam);
				GameSystem.CurrentLevel.RemoveObject(this);
			}
		}
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			GameFrame.RemoveChild(this);
			super.TakenHit(obj, movement);
			var beam:RuinGardenBeam = new RuinGardenBeam(); 
			beam.Right = Tile.Size.x * 6;
			beam.ID = ID;
			GameSystem.CurrentLevel.AddObjects(beam);
			GameFrame.AddChild(beam);
			GameSystem.CurrentLevel.RemoveObject(this);
		}
		
	}

}