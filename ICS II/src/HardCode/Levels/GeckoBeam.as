package HardCode.Levels 
{
	import flash.display.InteractiveObject;
	import flash.events.Event;
	import flash.geom.Point;
	import HardCode.GUI.MonsterMarker;
	import Resources.Animation;
	import Resources.lib.ParticleBeamSprite;
	import Resources.lib.Sound.SFX.FireIgniteSound;
	import Resources.lib.Sound.SFX.FireStartingSound;
	import Resources.lib.Sound.SFX.SpikeSound;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IUpdatable;
	import System.MC.Characters.Monster;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Overlay;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class GeckoBeam extends Overlay implements IUpdatable
	{
		private var count:int;
		public function GeckoBeam() 
		{
			super(new ParticleBeamSprite(), Overlay.Location_WeatherEffect);
			View.addEventListener(Animation.FinishAnimation, onFinishFadeIn);
			count = 0;
		}
		public function Update():void
		{
			if ((View as IAnimatable).CurrentAnimation != 0 && ++count == 2)
			{
				View.visible = !View.visible;
				count = 0;
			}
		}
		public function get UpdateWhenAdvancingTime():Boolean { return false; }
		private function onFinishFadeIn(e:Event):void
		{
			(View as IAnimatable).SetAnimation(0);
			View.removeEventListener(Animation.FinishAnimation, onFinishFadeIn);
			View.addEventListener(Animation.FinishAnimation, onFinishFadeOut);
			View.visible = true;
			var rec:CollisionRectangle = new CollisionRectangle(Left, Top, View.width, View.height);
			var obj:Vector.<ICollidable> = GameSystem.GetIntersectedCollidable(rec);
			for each(var col:ICollidable in obj)
			{
				if (col is Player)
				{
					(col as Player).DieFromMonster();
					SoundCenter.PlaySFXFromInstance(new FireIgniteSound());
				}
			}
			GameFrame.Shake(new Point(0, 20), Utility.SecondToFrame(0.5), 0.9);
		}
		private function onFinishFadeOut(e:Event):void
		{
			View.removeEventListener(Animation.FinishAnimation, onFinishFadeOut);
			(View as IAnimatable).SetAnimation(2);
			View.addEventListener(Animation.FinishAnimation, onFinished);
		}
		private function onFinished(e:Event):void
		{
			(View as IAnimatable).SetAnimation(1);
			View.removeEventListener(Animation.FinishAnimation, onFinished);
			View.addEventListener(Animation.FinishAnimation, onFinishFadeIn);
			GameFrame.RemoveChild(this);
		}
	}

}