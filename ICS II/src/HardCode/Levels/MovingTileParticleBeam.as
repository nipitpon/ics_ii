package HardCode.Levels 
{
	import flash.events.Event;
	import flash.geom.Point;
	import Resources.Animation;
	import Resources.AnimationEvent;
	import Resources.lib.ParticleBeamSprite;
	import Resources.lib.Sound.BGM.SunnyWeatherSound;
	import Resources.lib.Sound.SFX.ParticleBeamSound;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.ICollidable;
	import System.Interfaces.INamable;
	import System.Interfaces.IOverlay;
	import System.Interfaces.ISound;
	import System.Interfaces.IUpdatable;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.ModelViewController;
	import System.MC.MovableObject;
	import System.MC.Overlay;
	import System.SoundCenter;
	import System.V.FloatingText;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class MovingTileParticleBeam extends MovableObject implements IOverlay, IUpdatable, INamable
	{
		private var activated_count:int;
		private var max_activated_count:int;
		private var sound:ISound;
		public function MovingTileParticleBeam() 
		{
			super(new ParticleBeamSprite(), new CollisionRectangle(18, 0, 156, 540));
			View.addEventListener(Animation.FinishAnimation, onFinishAnimation);
			View.addEventListener(AnimationEvent.Progress, onFading);
			addEventListener(ModelViewController.Added, onAdded);
			activated_count = -1;
			collide_with = Vector.<Class>([Player]);
			speed = 7;
			sound = new ParticleBeamSound();
		}
		public function get Name():String { return "ParticleBeam"; }
		public function Activate():void
		{
			activated_count = 0;
			var prev_speed:Number = speed;
			speed = ((9248 - Left) / Number(1376.0)) * 8;
			max_activated_count = 172 + Math.floor((speed - prev_speed) * 5);
			Push(speed, 0);
		}
		public override function Update():void
		{
			super.Update();
			if (activated_count >= 0)
			{
				if (activated_count < max_activated_count)
				{
					activated_count++;
					//Left = Utility.Interpolate(9248, start_pos, (activated_count++) / 172.0);
				}
				else
				{
					ClearAllForce();
					(View as IAnimatable).SetAnimation(2);
					sound.Stop(560);
					activated_count = -1;
					SoundCenter.PlayBGM(SunnyWeatherSound);
					View.addEventListener(Animation.FinishAnimation, onFadedOut);
					View.addEventListener(AnimationEvent.Progress, onFading);
				}
			}
			SoundCenter.SetSoundVolumeMultiplier(sound, 1 - Math.min(Math.max((GameFrame.ScreenLeft - Right) / 720.0, 0), 0.9));
			Top = GameFrame.ScreenTop;
		}
		public function set BottomMost(val:Boolean):void {}
		public function get BottomMost():Boolean { return false; }
		public function set OverlayLevel(val:int):void {}
		public function get OverlayLevel():int { return Overlay.Location_WeatherEffect; }
		private function onFinishAnimation(e:Event):void
		{
			var anim:IAnimatable = View as IAnimatable;
			if (anim.CurrentAnimation == 1)
			{
				(View as IAnimatable).SetAnimation(0);
				SoundCenter.SetSoundVolumeMultiplier(sound, 1);
				View.removeEventListener(AnimationEvent.Progress, onFading);
			}
			else if (anim.CurrentAnimation == 0)
			{
				View.removeEventListener(Animation.FinishAnimation, onFinishAnimation);
			}
		}
		private function onFading(e:AnimationEvent):void
		{
			if ((View as IAnimatable).CurrentAnimation == 1)
			{
				SoundCenter.SetSoundVolumeMultiplier(sound, e.Position / 12.0);
			}
		}
		private function onFadedOut(e:Event):void
		{
			GameFrame.RemoveChild(this);
			GameSystem.CurrentLevel.RemoveObject(this);
			View.removeEventListener(Animation.FinishAnimation, onFadedOut);
		}
		public override function Hit(obj:ICollidable, movement:Point):void
		{
			super.Hit(obj, movement);
			if (obj is Player && (View as IAnimatable).CurrentAnimation != 2)
			{
				var player:Player = obj as Player;
				if (!player.IsDying)
				{
					player.DieFromBurning();
					sound.Stop(560);
				}
			}
		}
		private function onAdded(e:Event):void
		{
			SoundCenter.PlaySFXFromInstance(sound, 0, 999999);
			addEventListener(ModelViewController.Removed, onRemoved);
			Activate();
		}
		private function onRemoved(e:Event):void
		{
			if (sound != null && sound.IsPlaying)
				sound.Stop();
		}
	}

}