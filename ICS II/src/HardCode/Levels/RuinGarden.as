package HardCode.Levels 
{
	import flash.utils.ByteArray;
	import Resources.lib.Sound.BGM.BossLevelBGM;
	import System.MC.Level;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author ss
	 */
	public class RuinGarden extends Level 
	{
		[Embed(source = "../../../lib/Maps/ruin_garden.json", mimeType = "application/octet-stream")]
		private static const map:Class;
		public function RuinGarden(next_level:int) 
		{
			super();
			var b:ByteArray = new map();
			var info:Object = JSON.parse(b.readUTFBytes(b.length));
			LoadJSON(info, next_level);
			AddObjects(new RuinGardenTrigger(), Tile.Size.x * 6, Tile.Size.y * 12);
			AddObjects(new RuinGardenTrigger2(), Tile.Size.x * 433, Tile.Size.y * 8);
		}
		public override function get OverrideBGM():Class { return BossLevelBGM; }
	}

}