package HardCode.Levels 
{
	import com.spilgames.gameapi.GameAPI;
	import HardCode.NPC.Lever;
	import HardCode.NPC.TreasureChest;
	import Resources.lib.Environment.PlaneSprite;
	import System.Achievements;
	import System.ControlCenter;
	import System.DataCenter;
	import System.GamePage;
	import System.GameSystem;
	import System.GUI;
	import System.Interfaces.IActivatable;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IInteractor;
	import System.Interfaces.INamable;
	import System.Interfaces.IOverlay;
	import System.Interfaces.IUpdatable;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Interactables.InteractableObject;
	import System.MC.Overlay;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class BarnPlane extends InteractableObject implements INamable, IActivatable, IUpdatable
	{
		private var count:int;
		public function BarnPlane() 
		{
			super(new PlaneSprite(), new CollisionRectangle(0, 0, 256, 96), "Hop in");
			count = -1;
		}
		public override function Update():void
		{
			super.Update();
			var anim:IAnimatable = View as IAnimatable;
			if (count >= 0)
			{
				MoveByOffset(2, 0);
				--count;
				if (count < 0)
				{
					anim.SetAnimation(2);
				}
			}
		}
		public override function get IsInteractable():Boolean { return (View as IAnimatable).CurrentAnimation == 2; }
		public override function GetInteracted(interactor:IInteractor):void
		{
			super.GetInteracted(interactor);
			GameSystem.FinishLevel();
			
			var mute_bgm:Boolean = SoundCenter.IsMutingBGM;
			var mute_sfx:Boolean = SoundCenter.IsMutingSFX;
			GameAPI.getInstance().GameBreak.request(function():void
			{
				//GamePage.ShowPauseMenu();
				GameSystem.Freeze();
			}, function():void
			{
				if (GUI.IsShowingPauseMenu)
					GUI.HidePauseMenu();
				if (!mute_bgm) SoundCenter.UnmuteBGM();
				if (!mute_sfx) SoundCenter.UnmuteSFX();
				GameSystem.Unfreeze();
				GameSystem.SetGameMode(50);
				var chest:TreasureChest = GameSystem.GetObjectByName("TreasureChest") as TreasureChest;
				if (!DataCenter.SecretItemCollected(49) && (chest == null || !chest.IsInteractable))
				{
					//GamePage.ShowSecretItem(code - 2);
					DataCenter.CollectSecretItem(49);
					Achievements.CollectOneSecretItem();
				}
				ControlCenter.ReleaseAllKey();
				ControlCenter.Stop();
				Main.ChangeToGamePage();
			});
		}
		public function get Name():String { return "RedDoor"; }
		public function Activate():void
		{
			var anim:IAnimatable = View as IAnimatable;
			if (anim.CurrentAnimation != 0) return;
			
			Lever.Enabled = false;
			count = 192;
			anim.SetAnimation(1);
		}
	}

}