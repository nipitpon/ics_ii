package HardCode.Levels 
{
	import flash.events.Event;
	import flash.geom.Point;
	import System.GameFrame;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IOverlay;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Overlay;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class MovingTileBeamTrigger extends CollidableObject implements IOverlay 
	{
		
		public function MovingTileBeamTrigger() 
		{
			super(new RenderableObject(), new CollisionRectangle(0, 0, Tile.Size.x * 1, Tile.Size.y * 1));
			collide_with = Vector.<Class>([Player]);
		}
		public function set BottomMost(value:Boolean):void {  }
		public function get BottomMost():Boolean  { return false; }
		public function set OverlayLevel(value:int):void {} 
		public function get OverlayLevel():int { return Overlay.Location_Floor; }
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			super.TakenHit(obj, movement);
			var beam:MovingTileParticleBeam = new MovingTileParticleBeam(); 
			beam.Right = Tile.Size.x * 248;
			GameFrame.AddChild(beam);
			GameFrame.RemoveChild(this);
		}
	}

}