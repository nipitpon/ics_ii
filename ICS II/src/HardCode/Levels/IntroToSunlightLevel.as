package HardCode.Levels 
{
	import flash.utils.ByteArray;
	import Resources.lib.Tutorials.TutorialRestartSprite;
	import System.DataCenter;
	import System.MC.Level;
	import System.MC.Overlay;
	import System.MC.Tile;
	import System.MC.WaterBlock;
	
	/**
	 * ...
	 * @author ss
	 */
	public class IntroToSunlightLevel extends Level 
	{
		[Embed(source = "../../../lib/Maps/Intro_to_sunlight.json", mimeType = "application/octet-stream")]
		private static const map:Class;
		public function IntroToSunlightLevel(next_level:int) 
		{
			super();
			var b:ByteArray = new map();
			var info:Object = JSON.parse(b.readUTFBytes(b.length));
			LoadJSON(info, next_level);
			if (DataCenter.GetDeathCount(next_level - 1) < 0)
			{
				AddOverlay(new Overlay(new TutorialRestartSprite(), Overlay.Location_Foreground), Tile.Size.x * 0.75, Tile.Size.y * 14.5);
			}
		}   
		
	}

}