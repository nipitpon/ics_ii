package HardCode.Levels 
{
	import flash.utils.ByteArray;
	import Resources.lib.Sound.BGM.BossLevelBGM;
	import System.MC.Level;
	
	/**
	 * ...
	 * @author ss
	 */
	public class FireBallMonstrosityLevel extends Level 
	{
		[Embed(source = "../../../lib/Maps/fireball1.json", mimeType = "application/octet-stream")]
		private static const map:Class;
		public function FireBallMonstrosityLevel(next_level:int) 
		{
			super();
			var b:ByteArray = new map();
			var info:Object = JSON.parse(b.readUTFBytes(b.length));
			LoadJSON(info, next_level);
			
		}
		public override function get OverrideBGM():Class { return BossLevelBGM; }
	}
}