package HardCode.Levels 
{
	import flash.utils.ByteArray;
	import Resources.lib.Sound.BGM.BossLevelBGM;
	import System.MC.Level;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ParticleBeamLevel extends Level 
	{
		[Embed(source = "../../../lib/Maps/beam_level.json", mimeType = "application/octet-stream")]
		private static const map:Class;
		public function ParticleBeamLevel(next_level:int) 
		{
			super();
			var b:ByteArray = new map();
			var info:Object = JSON.parse(b.readUTFBytes(b.length));
			LoadJSON(info, next_level);
			AddObjects(new ParticleBeam());
		}
		public override function get OverrideBGM():Class { return BossLevelBGM; }
		
	}

}