package HardCode.Levels 
{
	import _Base.MyTextField;
	import flash.utils.ByteArray;
	import System.DataCenter;
	import System.MC.Level;
	import System.MC.Overlay;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class SpikeJumping extends Level 
	{
		[Embed(source = "../../../lib/Maps/spike_jumping.json", mimeType = "application/octet-stream")]
		private static const map:Class;
		public function SpikeJumping(next_level:int) 
		{
			super();
			var b:ByteArray = new map();
			var info:Object = JSON.parse(b.readUTFBytes(b.length));
			LoadJSON(info, next_level);
			if (DataCenter.GetDeathCount(next_level - 1) < 0)
			{
				var ren:RenderableObject = new RenderableObject();
				var text:MyTextField = new MyTextField(30, 0xffffff, false);
				text.text = "There are secret items\nfind them to unlock secret level";
				text.width = 300;
				ren.addChild(text);
				AddOverlay(new Overlay(ren, Overlay.Location_Foreground), Tile.Size.x * 42.5, Tile.Size.y * 5.8);
			}
		}
		
	}

}