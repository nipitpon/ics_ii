package HardCode.Levels 
{
	import flash.utils.ByteArray;
	import System.MC.Level;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author ss
	 */
	public class TheBarn extends Level 
	{
		[Embed(source = "../../../lib/Maps/the_barn.json", mimeType = "application/octet-stream")]
		private static const map:Class;
		public function TheBarn(next_level:int) 
		{
			super();
			var b:ByteArray = new map();
			var info:Object = JSON.parse(b.readUTFBytes(b.length));
			AddObjects(new BarnPlane(), Tile.Size.x * 26, Tile.Size.y * 15);
			LoadJSON(info, next_level);
		}
		
	}

}