package HardCode.Levels 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import HardCode.Tiles.DynamicTile;
	import Resources.lib.Effects.MovableTileSpinnerSprite;
	import Resources.lib.Tiles;
	import System.Interfaces.IActivatable;
	import System.Interfaces.ICollidable;
	import System.Interfaces.INamable;
	import System.Interfaces.IOverlay;
	import System.Interfaces.ISavable;
	import System.Interfaces.IUpdatable;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Overlay;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author ss
	 */
	public class MovingTile extends Tile implements IUpdatable, IActivatable, IOverlay, ISavable
	{
		private var going_right:Boolean;
		private var spinner:MovableTileSpinnerSprite;
		public function MovingTile() 
		{
			super(false, new CollisionRectangle(0, 0, Tile.Size.x * 5, Tile.Size.y * 1));
			var bmp:BitmapData = Tiles.GetBitmapData("../Tiles/Interactables.png");
			var dat:BitmapData = new BitmapData(Tile.Size.x * 3, Tile.Size.y * 1, true, 0);
			dat.copyPixels(bmp, new Rectangle(0, 238, 16, 16), new Point());
			dat.copyPixels(bmp, new Rectangle(34, 238, 16, 16), new Point(Tile.Size.x / 2));
			dat.copyPixels(bmp, new Rectangle(17, 238, 16, 16), new Point(Tile.Size.x));
			dat.copyPixels(bmp, new Rectangle(34, 238, 16, 16), new Point(Tile.Size.x * 1.5));
			dat.copyPixels(bmp, new Rectangle(51, 238, 16, 16), new Point(Tile.Size.x * 2));
			var bitmap:Bitmap = new Bitmap(dat);
			bitmap.scaleX = 2;
			bitmap.scaleY = 2;
			View.addChild(bitmap);
			going_right = false;
			spinner = new MovableTileSpinnerSprite();
			spinner.CenterX = Tile.Size.x * 2.5;
			spinner.CenterY = Tile.Size.y * 0.5;
			View.addChild(spinner);
		}
		public function get OverlayLevel():int { return Overlay.Location_FloorForeground; }
		public function SetInfo(obj:Object):void
		{
			Center = obj["center"];
			Activate();
		}
		public function GetInfo():Object
		{
			return { "center":Center };
		}
		public override function Update():void
		{
			if(ForceX > 0 && Collision.CenterX >= Tile.GetLeft(291))
			{
				ClearForceX();
				spinner.SetAnimation(0);
			}
			spinner.Update();
		}
		public function Activate():void
		{
			going_right = !going_right;
			spinner.SetAnimation(1);
			ClearAllForce();
			if (going_right)
				Push(3, 0);
		}
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			super.TakenHit(obj, movement);
			if (!going_right)
				Activate();
			if (obj.Collision.CenterY <= Collision.Top && obj.Collision.Right > Collision.Left && obj.Collision.Left < Collision.Right)
			{
				movement.y = Collision.Top - obj.Collision.Bottom + ForceY;
				movement.x += ForceX;
			}
		}
	}

}