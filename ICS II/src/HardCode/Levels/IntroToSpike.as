package HardCode.Levels 
{
	import flash.utils.ByteArray;
	import HardCode.Tiles.SpikeTile;
	import Resources.lib.Tutorials.TutorialPauseSprite;
	import System.DataCenter;
	import System.MC.Level;
	import System.MC.Overlay;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author ss
	 */
	public class IntroToSpike extends Level 
	{
		[Embed(source = "../../../lib/Maps/Intro_to_spike.json", mimeType = "application/octet-stream")]
		private static const map:Class;
		public function IntroToSpike(next_level:int) 
		{
			super();
			var b:ByteArray = new map();
			var info:Object = JSON.parse(b.readUTFBytes(b.length));
			LoadJSON(info, next_level);
			if (DataCenter.GetDeathCount(next_level - 1) < 0)
			{
				AddOverlay(new Overlay(new TutorialPauseSprite(), Overlay.Location_Foreground), Tile.Size.x * 27, Tile.Size.y * 17);
			}
		}
		
	}

}