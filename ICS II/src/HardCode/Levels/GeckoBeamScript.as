package HardCode.Levels 
{
	import flash.geom.Point;
	import Resources.lib.Sound.BGM.SunnyWeatherSound;
	import Resources.lib.Sound.SFX.GeckoBeamSound;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.INamable;
	import System.Interfaces.IUpdatable;
	import System.SoundCenter;
	
	/**
	 * ...
	 * @author ss
	 */
	public class GeckoBeamScript implements IUpdatable, INamable
	{
		private var count:int;
		private var beam:GeckoBeam;
		private var stop:Boolean;
		public function GeckoBeamScript() 
		{
			count = Utility.SecondToFrame(2);
			beam = new GeckoBeam();
			stop = false;
		}
		public function Update():void 
		{
			if (!stop)
			{
				if (--count == 0)
				{
					beam.CenterX = GameSystem.PlayerInstance.CenterX;
					GameFrame.AddChild(beam);
					count = Utility.SecondToFrame(5);
					SoundCenter.PlaySFX(GeckoBeamSound);
				}
			}
				beam.Top = GameFrame.ScreenTop;
		}
		public function DeactivateBeam():void
		{
			stop = true;
			SoundCenter.PlayBGM(SunnyWeatherSound);
		}
		public function get Name():String { return "GeckoBeamScript"; }
		public function get UpdateWhenAdvancingTime():Boolean { return false; } 
	}
}