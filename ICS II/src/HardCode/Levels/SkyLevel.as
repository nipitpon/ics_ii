package HardCode.Levels 
{
	import flash.utils.ByteArray;
	import Resources.lib.Environment.BackgroundCloudImage;
	import Resources.lib.Sound.BGM.SkyLevelBGM;
	import System.GameFrame;
	import System.GameSystem;
	import System.MC.Level;
	import System.MC.Tile;
	import System.V.ForestBackground;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class SkyLevel extends Level 
	{
		[Embed(source = "../../../lib/Maps/sky_level.json", mimeType = "application/octet-stream")]
		private static const map:Class;
		public function SkyLevel(next_level:int) 
		{
			super();
			var b:ByteArray = new map();
			var info:Object = JSON.parse(b.readUTFBytes(b.length));
			AddObjects(new SkyPlane(), 0, Tile.Size.y * 18);
			LoadJSON(info, next_level);
			far_background = new ForestBackground(BackgroundCloudImage, 540);
			middle_background = new RenderableObject();
			near_background = new RenderableObject();
			close_background = new RenderableObject();
		}
		public override function get OverrideBGM():Class { return SkyLevelBGM; }
		public override function Stop():void
		{
			super.Stop();
			GameSystem.FocusAt(NaN, NaN);
		}
	}

}