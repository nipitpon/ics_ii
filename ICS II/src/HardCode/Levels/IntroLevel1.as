package HardCode.Levels 
{
	import flash.utils.ByteArray;
	import Resources.lib.Tutorials.TutorialExitSprite;
	import Resources.lib.Tutorials.TutorialJumpSprite;
	import Resources.lib.Tutorials.TutorialSlideSprite;
	import System.DataCenter;
	import System.MC.Level;
	import System.MC.Overlay;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author ss
	 */
	public class IntroLevel1 extends Level 
	{
		[Embed(source = "../../../lib/Maps/Introlevel1.json", mimeType = "application/octet-stream")]
		private static const map:Class;
		public function IntroLevel1(next_level:int) 
		{
			super();
			var b:ByteArray = new map();
			var info:Object = JSON.parse(b.readUTFBytes(b.length));
			LoadJSON(info, next_level);
			if (DataCenter.GetDeathCount(next_level - 1) < 0)
			{
				AddOverlay(new Overlay(new TutorialJumpSprite(), Overlay.Location_Foreground), Tile.Size.x * 15, Tile.Size.y * 16.5);
				AddOverlay(new Overlay(new TutorialSlideSprite(), Overlay.Location_Foreground), Tile.Size.x * 21, Tile.Size.y * 13.5);
				AddOverlayCenterXTop(new Overlay(new TutorialExitSprite(), Overlay.Location_Foreground), Tile.Size.x * 30.5, Tile.Size.y * 10.5);
			}
		}
		
	}

}