package HardCode.Levels 
{
	import flash.utils.ByteArray;
	import Resources.lib.Sound.BGM.BossLevelBGM;
	import System.MC.Level;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author ss
	 */
	public class MovingTileBeam extends Level 
	{
		[Embed(source = "../../../lib/Maps/moving_tile_beam.json", mimeType = "application/octet-stream")]
		private static const map:Class;
		public function MovingTileBeam(next_level:int) 
		{
			super();
			//TerrainLayer.SetTileAt(8, 9, new MovingTile());
			AddObjects(new MovingTile(), Tile.Size.x * 8, Tile.Size.y * 9);
			var b:ByteArray = new map();
			var info:Object = JSON.parse(b.readUTFBytes(b.length));
			LoadJSON(info, next_level);
			AddObjects(new MovingTileBeamTrigger(), Tile.Size.x * 251, Tile.Size.y * 6);
		}
		public override function get OverrideBGM():Class { return BossLevelBGM; }
		
	}

}