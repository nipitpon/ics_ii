package HardCode.Levels 
{
	import flash.utils.ByteArray;
	import System.MC.Level;
	
	/**
	 * ...
	 * @author ss
	 */
	public class StartsAtNight extends Level 
	{
		[Embed(source = "../../../lib/Maps/starts_at_night.json", mimeType = "application/octet-stream")]
		private static const map:Class;
		public function StartsAtNight(next_level:int) 
		{
			super(0);
			var b:ByteArray = new map();
			var info:Object = JSON.parse(b.readUTFBytes(b.length));
			LoadJSON(info, next_level);
		}
		
	}

}