package HardCode.Levels 
{
	import flash.utils.ByteArray;
	import Resources.lib.Sound.BGM.BossLevelBGM;
	import System.GameFrame;
	import System.GameSystem;
	import System.MC.Level;
	
	/**
	 * ...
	 * @author ss
	 */
	public class GeckoBoss extends Level 
	{
		[Embed(source = "../../../lib/Maps/gecko_boss_2.json", mimeType = "application/octet-stream")]
		private static const map:Class;
		
		private var beam:GeckoBeamScript;
		public function GeckoBoss(next_level:int) 
		{
			super();
			var b:ByteArray = new map();
			var info:Object = JSON.parse(b.readUTFBytes(b.length));
			LoadJSON(info, next_level);
			beam = new GeckoBeamScript();
		}
		public override function Start():void
		{
			super.Start();
			GameSystem.AddGameObject(beam);
			GameFrame.AddChild(new GeckoBeamStopper());
		}
		public override function Stop():void
		{
			super.Stop();
			GameSystem.RemoveGameObject(beam);
		}
		
		public override function get OverrideBGM():Class { return BossLevelBGM; }
	}

}