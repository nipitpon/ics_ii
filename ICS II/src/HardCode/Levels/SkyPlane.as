package HardCode.Levels 
{
	import com.spilgames.gameapi.GameAPI;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.geom.Point;
	import HardCode.NPC.TreasureChest;
	import Resources.lib.Environment.PlaneSprite;
	import System.Achievements;
	import System.ControlCenter;
	import System.DataCenter;
	import System.GameFrame;
	import System.GamePage;
	import System.GameSystem;
	import System.GUI;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IOverlay;
	import System.Interfaces.ISavable;
	import System.Interfaces.IUpdatable;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.ModelViewController;
	import System.MC.Overlay;
	import System.MC.Tile;
	import System.SoundCenter;
	
	/**
	 * ...
	 * @author ss
	 */
	public class SkyPlane extends Tile implements IUpdatable, IOverlay, ISavable
	{
		private var plane:PlaneSprite;
		public function SkyPlane() 
		{
			super(false, new CollisionRectangle(-32, 0, 756, 96));
			Push(4, 0);
			View.addChild(plane = new PlaneSprite());
			plane.SetAnimation(2);
		}
		public function get OverlayLevel():int { return Overlay.Location_FloorForeground; }
		public function SetInfo(obj:Object):void
		{
			Center = obj["center"];
		}
		public function GetInfo():Object { return { "center": Center }; }
		public override function Update():void 
		{
			if (Collision.Right >= Tile.Size.x * 600 && ForceX > 0)
			{
				ClearForceX();
				var mute_bgm:Boolean = SoundCenter.IsMutingBGM;
				var mute_sfx:Boolean = SoundCenter.IsMutingSFX;
				
				var chest:TreasureChest = GameSystem.GetObjectByName("TreasureChest") as TreasureChest;
				if (!DataCenter.SecretItemCollected(50) && (chest == null || !chest.IsInteractable))
				{
					//GamePage.ShowSecretItem(49);
					DataCenter.CollectSecretItem(50);
					Achievements.CollectOneSecretItem();
				}
				GameSystem.FocusAt(NaN, NaN);
				GameSystem.FinishLevel();
				
				GameAPI.getInstance().GameBreak.request(function():void
				{
					//GamePage.ShowPauseMenu();
					GameSystem.Freeze();
					SoundCenter.MuteBGM();
					SoundCenter.MuteSFX();
				}, function():void
				{
					if (GUI.IsShowingPauseMenu)
						GUI.HidePauseMenu();
					if (!mute_bgm) SoundCenter.UnmuteBGM();
					if (!mute_sfx) SoundCenter.UnmuteSFX();
					GameSystem.Unfreeze();
					Main.ChangeToFinishVanilla();
				});
			}
			else if(ForceX > 0)
			{
				plane.Update();
				super.Update();
				GameSystem.FocusAt(Collision.CenterX, NaN);
				plane.CenterX = GameSystem.PlayerInstance.CenterX - View.x - 70;
			}
		}
		public override function CalculateMovement():Point
		{
			var outgoing:Point = super.CalculateMovement();
			//if (GameSystem.PlayerInstance.CanStand())
			//{
			//	if (ControlCenter.PressingKey(ControlCenter.Drop))
			//	{
			//		if(Top < 576)
			//			outgoing.y += 3;
			//	}
			//} 
			//if ((ControlCenter.PressingKey(ControlCenter.PlayerJump) || ControlCenter.PressingKey(ControlCenter.PlayerClimb)) && Top > 270)
			//	outgoing.y -= 3;
			return outgoing;
		}
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			if (obj is Player && obj.Collision.CenterY <= Collision.Top && obj.Collision.Right > Collision.Left && obj.Collision.Left < Collision.Right)
			{
				movement.y = Collision.Top - obj.Collision.Bottom + ForceY;
				movement.x += ForceX;
			}
		}
	}

}