package 
{
	import _Base.MySprite;
	import _Base.MyTextField;
	import flash.display.StageScaleMode;
	import HardCode.Pages.FinishVanilla.Finish2;
	import Resources.lib.Other_pages.Finish2Background;
	import Resources.lib.Sound.BGM.SunnyWeatherSound;
	import fl.transitions.easing.None;
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.media.Sound;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.ui.Mouse;
	import flash.utils.Timer;
	import HardCode.Pages.Achievements.AchievementPage;
	import HardCode.Pages.CreditsPage.CreditPage;
	import HardCode.Pages.FinishVanilla.FinishVanilla;
	import HardCode.Pages.LevelSelection.LevelSelectionPage;
	import HardCode.Pages.Logo.Logo;
	import HardCode.Pages.Main.MainPage;
	import Resources.lib.ComicPage.PlaneDownImage;
	import Resources.Resource;
	import System.*;
	import System.Interfaces.IReceiveControl;
	import System.MC.Level;
	import System.V.CursorLayer;
	import System.V.InteractionText;
	import System.V.RenderableObject;
	/**
	 * ...
	 * @author Knight
	 */
	[Frame (factoryClass = "Preloader")]
	public class Main extends Sprite 
	{
		public static var Version:String = "0.9.10";
		
		private static var instance:Main;
		private var main:MainPage;
		private var credit:CreditPage;
		private var game:GamePage;
		private var finish_vanilla:FinishVanilla;
		private var level_selection:LevelSelectionPage;
		private var achievement:AchievementPage;
		private var current_page:Pages;
		private var to_change:Pages;
		private var timer:Timer;
		private var logo:Logo;
		private var finish2:Finish2;
		private var cursor_layer:CursorLayer;
		
		private var fader:Fader;
		public function Main():void 
		{
			instance = this;
			timer = new Timer(200, 1);
			cursor_layer = new CursorLayer();
			CONFIG::debug
			{ Version += " debug"; }
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		public static function RefreshMainPage():void
		{
			instance.main.Refresh();
		}
		public static function ChangeToGamePage():void
		{
			instance.ChangePage(instance.game);
		}
		public static function ChangeToLogo():void
		{
			instance.ChangePage(instance.logo);
		}
		public static function ChangeToSelectionPage():void
		{
			instance.ChangePage(instance.level_selection);
		}
		public static function ChangeToMainPage():void
		{
			RefreshMainPage();
			instance.ChangePage(instance.main);
		}
		public static function ChangeToCreditPage():void
		{
			instance.ChangePage(instance.credit);
		}
		public static function ChangeToAchievementPage():void
		{
			instance.ChangePage(instance.achievement);
		}
		public static function ChangeToFinishVanilla():void
		{
			SoundCenter.StopBGM();
			instance.ChangePage(instance.finish_vanilla);
		}
		public static function ChangeToFinish2():void
		{
			instance.ChangePage(instance.finish2);
		}
		public static function UseCustomCursor():void
		{
			Mouse.hide();
			instance.cursor_layer.visible = true;
			instance.cursor_layer.MouseMove(instance.stage.mouseX, instance.stage.mouseY);
		}
		public static function UseDefaultCursor():void
		{
			Mouse.show();
			instance.cursor_layer.visible = false;
		}
		public static function get IsFading():Boolean 
		{ 
			return (instance.timer.hasEventListener(TimerEvent.TIMER)) ||
				(instance.current_page == instance.game && (GamePage.IsFadingOut)); 
		}

		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoveFromStage);
			addEventListener(Event.RESIZE, onResize);
			
			// entry point
			Overseer.Initialize();
			ControlCenter.Initialize(stage);
			DataCenter.Initialize();
			SoundCenter.Initialize();
			
			fader = new Fader();
			main = new MainPage();
			game = new GamePage();
			credit = new CreditPage();
			finish_vanilla = new FinishVanilla();
			achievement = new AchievementPage();
			logo = new Logo();
			level_selection = new LevelSelectionPage();
			finish2 = new Finish2();
			ControlCenter.AddGameObject(cursor_layer);
			
			addChild(cursor_layer);
			UseCustomCursor();
			GameSystem.SetGameMode(80);
			fader.addEventListener(Fader.FaderFadedOut, tween_onFadedOut);
			fader.FadeOut();
			current_page = game;
			addChild(fader);
			addChildAt(current_page, numChildren - 2);
			current_page.Start();
			
			stage.scaleMode = StageScaleMode.EXACT_FIT;
		}
		private function onRemoveFromStage(e:Event):void
		{
			DataCenter.Flush();
			ControlCenter.Flush();
			Overseer.Flush();
			SoundCenter.Flush();
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemoveFromStage);
		}
		private function ChangePage(page:Pages):void
		{
			if (current_page != null)
			{
				current_page.Stop();
			}
			ControlCenter.Stop();
			fader.addEventListener(Fader.FaderFadedIn, tween_onFinish);
			var texts:Vector.<String> = Vector.<String>(["WARPING...", "DOING SOMETHING...", "LOADING...."]);
			fader.FadeIn(null, 0xffffff, texts[Utility.Random(0, texts.length)]);
			addChild(fader);
			to_change = page;
		}
		private function onResize(e:Event):void
		{
		}
		private function tween_onFinish(e:Event):void
		{
			if (current_page != null)
			{
				removeChild(current_page);
				//if (current_page is IReceiveControl)
				//	ControlCenter.RemoveGameObject(current_page as IReceiveControl);
			}
			ControlCenter.Start();
			fader.FadeOut();
			fader.removeEventListener(Fader.FaderFadedIn, tween_onFinish);
			fader.addEventListener(Fader.FaderFadedOut, tween_onFadedOut);
			current_page = to_change;
			addChildAt(current_page, numChildren - 2);
			current_page.Start();
		}
		private function tween_onFadedOut(e:Event):void
		{
			removeChild(fader);
			fader.removeEventListener(Fader.FaderFadedOut, tween_onFadedOut);
		}
		private function SetPage(page:Pages):void
		{
			if (current_page != null)
			{
				current_page.Stop();
				removeChild(current_page);
				//if (current_page is IReceiveControl)
				//	ControlCenter.RemoveGameObject(current_page as IReceiveControl);
			}
			//if (page is IReceiveControl)
			//	ControlCenter.AddGameObject(page as IReceiveControl);
			current_page = page;
			addChildAt(page, numChildren - 1);
			setChildIndex(cursor_layer, numChildren - 1);
			page.Start();
		}
	}
}