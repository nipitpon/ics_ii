package  
{
	import flash.display.DisplayObject;
	import flash.geom.Point;
	import flash.utils.describeType;
	import flash.utils.getQualifiedClassName;
	/**
	 * ...
	 * @author Knight
	 */
	public class Utility 
	{
		public static const FramePerSecond:Number = 28;
		public static const EmptyPoint:Point = new Point();
		public function Utility() 
		{
			throw new Error("Cannot instantiate Utility");
		}
		
		public static function Random(start:int, end:int):int
		{
			if (start == end) return start;
			
			return int(((Math.random() * 1000000) % (end - start)) + start);
		}
		public static function IsObjectVisible(obj:DisplayObject):Boolean
		{
			if (!obj.visible) return false;
			if (obj == obj.root) return true;
			else return IsObjectVisible(obj.parent);
		}
		public static function DotProduct(vec1:Point, vec2:Point):Number
		{
			return vec1.x * vec2.x + vec1.y * vec2.y;
		}
		public static function AngleBetweenVector(vec1:Point, vec2:Point):Number
		{
			return Utility.ToDegree(Math.atan2(vec2.x - vec1.x, vec2.y - vec1.y));
		}
		public static function ToDegree(val:Number):Number { return (180 / Math.PI) * val; }
		public static function ToRadian(val:Number):Number { return (Math.PI / 180) * val; }
		public static function MillisecToSecond(val:int):Number { return val / 1000; }
		public static function SecondToFrame(val:Number):int { return val * FramePerSecond; }
		public static function MillisecToFrame(val:int):int { return SecondToFrame(MillisecToSecond(val)); }
		public static function FrameToSecond(val:uint):Number { return val / FramePerSecond; }
		public static function Interpolate(v1:*, v2:*, weightTowardV1:*):Number
		{
			return (weightTowardV1) * v1 + (1 - weightTowardV1) * v2;
		}
		public static function IsClassInheritedFrom(toTest:Class, descendant:Class):Boolean
		{
			var type:XML = describeType(toTest);
			return type.factory.extendsClass.(@type == getQualifiedClassName(descendant)).length() > 0;
		}
	}

}