package Resources 
{
	import flash.display.MovieClip;
	import flash.geom.Point;
	import System.Interfaces.IAnimatable;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SWFAnimation extends Animation implements IAnimatable 
	{
		private var clip:MovieClip;
		public function SWFAnimation(clip:MovieClip, scaleCenter:Point=null) 
		{
			super(scaleCenter);
			this.clip = clip;
			Initialize();
		}
		private function Initialize():void
		{
			addChild(clip);
			clip.stop();
		}
		public override function Update():void
		{
			super.Update();
			
			if (Freeze || !IsNewFrame) return;
			
			var num:uint = AnimationImagePosition;
			clip.gotoAndStop(num + 1);
			clip.stop();
		}
		protected override function setCurrentAnimation(action:ActionSet):void
		{
			super.setCurrentAnimation(action);
			var pos:uint = AnimationImagePosition;
			clip.gotoAndStop(pos + 1);
		}
	}

}