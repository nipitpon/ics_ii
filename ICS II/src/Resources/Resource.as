package Resources 
{
	import _Base.MyTextField;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.filters.BlurFilter;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import Resources.lib.*;
	import Resources.lib.Other_pages.*;
	import System.MC.Tile;
	import System.V.RenderableObject;
	/**
	 * ...
	 * @author Knight
	 */
	public class Resource 
	{
		/*
		 *	Excluded list
		 * 
		 * 	Requires Argument(s)
		 * 	 - SkillButton_Background.as
		 *   - SkillShortcut_Image.as
		 * 
		 * 	etc.
		 * 	 - PlayerSprite.as - Doesn't contain any sprite by itself
		 */
		
		private static const images:Vector.<Class> = Vector.<Class>([FlagSprite, 
			MainMenuBackground, NoteDetailForeground]);
		
		private static var cache_id:int = 0;
		private static var cache_buffer:RenderableObject;
		public function Resource() 
		{
			throw new Error("Cannot instantiate Resource");
		}
		
		public static function Initialize():void
		{
			
		}
		public static function Flush():void
		{
			
		}
		public static function GetMainMenuTextField(t:String):MyTextField
		{
			var text:MyTextField = new MyTextField();
			text.defaultTextFormat = new TextFormat("Pixel Font", Tile.Size.y, 0xdadada, true, false, false, null, null);
			text.embedFonts = true;
			text.autoSize = TextFieldAutoSize.LEFT;
			text.text = t;
			return text;
		}
		public static function GetGameFinishedTextField(t:String):MyTextField
		{ 
			var text:MyTextField = new MyTextField();
			text.defaultTextFormat = new TextFormat("Pixel Font", "30", 0, true, false, false, null, null, TextFormatAlign.CENTER);
			text.embedFonts = true;
			text.autoSize = TextFieldAutoSize.CENTER;
			text.text = t;
			text.multiline = true;
			return text;
		}
		public static function GetLevelSelectionPageHeader(t:String):MyTextField
		{
			var text:MyTextField = new MyTextField();
			text.defaultTextFormat = new TextFormat("Pixel Font", "40", 0, true, false, false, null, null, TextFormatAlign.CENTER);
			text.embedFonts = true;
			text.autoSize = TextFieldAutoSize.CENTER;
			text.text = t;
			text.multiline = true;
			return text;
		}
		public static function GetLevelSelectionButtonText(t:String):MyTextField
		{
			var text:MyTextField = new MyTextField();
			text.defaultTextFormat = new TextFormat("Pixel Font", "30", 0, true, false, false, null, null, TextFormatAlign.CENTER);
			text.embedFonts = true;
			text.text = t;
			text.width = 50;
			text.height = 50;
			text.multiline = true;
			return text;
		}
		public static function GetSelectLevelButtonText(t:String):RenderableObject
		{
			if (t.length == 1) t = "0" + t;
			var text:MyTextField = new MyTextField();
			text.defaultTextFormat = new TextFormat("Pixel Font", "72", 0xffffff, true, false, false, null, null, TextFormatAlign.CENTER);
			text.embedFonts = true;
			text.text = t;
			text.width = 64;
			var matrix:Matrix = new Matrix();
			var dat:BitmapData = new BitmapData(text.width, 64, true, 0);
			matrix.translate(0, 12);
			dat.draw(text, matrix);
			var bmp:Bitmap = new Bitmap(dat);
			var ren:RenderableObject = new RenderableObject();
			ren.addChild(bmp);
			return ren;
		}
		public static function GetPlatformText(t:String):RenderableObject
		{
			var text:MyTextField = new MyTextField();
			text.defaultTextFormat = new TextFormat("Pixel Font", "31", 0xffffff, true, false, false, null, null, TextFormatAlign.CENTER);
			text.embedFonts = true;
			text.text = t;
			text.width = 204;
			var dat:BitmapData = new BitmapData(text.width, 40, true, 0);
			var mat:Matrix = new Matrix();
			mat.translate(0, 4);
			dat.draw(text, mat, new ColorTransform(0x96 / 255, 0x8d / 255, 0x63 / 255));
			mat.identity();
			mat.translate(0, 1);
			dat.draw(text, mat, new ColorTransform(0xbf / 255, 0xb8 / 255, 0x80 / 255));
			mat.identity();
			mat.translate(0, -2);
			dat.draw(text, mat, new ColorTransform(0xd3 / 255, 0xd0 / 255, 0xa2 / 255));
			var bmp:Bitmap = new Bitmap(dat);
			var ren:RenderableObject = new RenderableObject();
			ren.addChild(bmp);
			return ren;
		}
		public static function GetImage(id:uint):RenderableObject
		{
			if (id < images.length) return new images[id]();
			else throw new Error("Invalid image resource id: " + id);
		}
		public static function BeginCache(buffer:RenderableObject):void
		{
			cache_id = 0;
			cache_buffer = buffer;
			Cache(cache_id);
		}
		public static function get IsCached():Boolean { return cache_id >= images.length; }
		private static function Cache(id:int):void
		{
			if (id >= images.length) return;
			
			var obj:RenderableObject = new images[id]();
			obj.addEventListener(Event.ADDED_TO_STAGE, cache_onAddedToStage);
			cache_buffer.addChild(obj);
		}
		private static function cache_onAddedToStage(e:Event):void
		{
			(e.currentTarget as RenderableObject).removeEventListener(Event.ADDED_TO_STAGE, cache_onAddedToStage);
			Cache(++cache_id);
		}
	}

}