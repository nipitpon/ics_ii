package Resources
{
	
	import flash.display.Bitmap;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import System.Interfaces.IAnimatable;
	
	/**
	* ...
	* @author Knight
	*/
	public class SpriteSheetBook extends Animation implements IAnimatable
	{
	
		private var bitmaps:Vector.<Bitmap>;
		private var bitmap_offset:Vector.<int>;
		
		public function SpriteSheetBook(sprite:Vector.<Bitmap>, width:Number, height:Number, scaleCenter:Point) 
		{
			super(scaleCenter);
			bitmaps = sprite;
			scrollRect = new Rectangle(0, 0, width, height);
			bitmap_offset = new Vector.<int>();
			Initialize();
		}
		public function Initialize():void
		{
			var total:int = 0;
			for each(var bitmap:Bitmap in bitmaps)
			{
				addChild(bitmap);
				bitmap_offset.push(total);
				bitmap.visible = false;
				total += (int)((bitmap.width / width) * (bitmap.height / height));
			}
		}
		
		public function SetHeight(val:Number):Number
		{
			var rect:Rectangle = scrollRect;
			rect.height = val;
			scrollRect = rect;
			return val;
		}
		public function SetWidth(val:Number):Number
		{
			var rect:Rectangle = scrollRect;
			rect.width = val;
			scrollRect = rect;
			return val;
		}
		public override function get width():Number { return scrollRect.width; }
		public override function get height():Number { return scrollRect.height; }
		
		public override function Update():void
		{
			super.Update();
			
			if (Freeze || !IsNewFrame) return;
			
			var rect:Rectangle = scrollRect;
			var num:uint = AnimationImagePosition;
			HideBitmaps();
			var bitmap_post:int = GetProperBitmap(num);
			if (bitmap_post >= 0)
			{
				var bitmap:Bitmap = bitmaps[bitmap_post];
				bitmap.visible = true;
				var i:uint = (uint)((width) * ((num - bitmap_offset[bitmap_post]) % (bitmap.width / width)));
				var j:uint = (uint)((height) * Math.floor((num - bitmap_offset[bitmap_post]) / (bitmap.width / width)));
				rect.x = i;
				rect.y = j;
				scrollRect = rect;
			}
		}
		
		protected override function setCurrentAnimation(action:ActionSet):void
		{
			super.setCurrentAnimation(action);
			var pos:uint = AnimationImagePosition;
			var rect:Rectangle = scrollRect;
			HideBitmaps();
			var bitmap_pos:int = GetProperBitmap(pos);
			var bitmap:Bitmap = bitmaps[bitmap_pos];
			bitmap.visible = true;
			rect.y = height * Math.floor((pos - bitmap_offset[bitmap_pos]) / (bitmap.width / width));
			rect.x = width * (((pos - bitmap_offset[bitmap_pos]) % (bitmap.width / width)));
			scrollRect = rect;
		}
		private function HideBitmaps():void
		{
			for each(var bitmap:Bitmap in bitmaps)
				bitmap.visible = false;
		}
		private function GetProperBitmap(position:uint):int
		{
			var outgoing:int = -1;
			var total:int = 0;
			for (var i:int = 0; i < bitmaps.length; i++)
			{
				var bitmap:Bitmap = bitmaps[i];
				if (position + 1 <= (bitmap.width / width) * (bitmap.height / height) + total)
				{
					outgoing = i;
					break;
				}
				else
				{
					total += (int)((bitmap.width / width) * (bitmap.height / height));
				}
			}
			return outgoing;
		}
	}
	
}