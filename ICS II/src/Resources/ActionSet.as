package Resources 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import Resources.AnimationEvent;
	/**
	 * ...
	 * @author Knight
	 */
	public class ActionSet extends EventDispatcher
	{
		public var positions:Vector.<int>;
		private var num:int = 0;
		private var id:int = 0;
		private var interval:int = 0;
		public function ActionSet(id:int, arg:Vector.<int>, interval:int) 
		{
			positions = arg;
			this.id = id;
			this.interval = interval;
		}
		public function Reset():void { num = 0; }
		public function get Interval():int { return interval; }
		public function set Interval(val:int):void { interval = val; }
		public function SetPosition(pos:int):void
		{
			num = pos;
			if (num >= positions.length)
				throw new Error("Invalid position");
		}
		public function AnimateOneFrame():int
		{
			num++;
			dispatchEvent(new AnimationEvent(AnimationEvent.Progress, num));
			if (num >= positions.length)
			{
				dispatchEvent(new Event(Animation.FinishAnimation));	
				num = 0;
			}
			return num;
		}
		public function get CurrentPosition():int { return num; }
		public function get ID():int { return id; }
	}

}