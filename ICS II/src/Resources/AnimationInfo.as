package Resources 
{
	import _Base.MySprite;
	/**
	 * ...
	 * @author Knight
	 */
	public class AnimationInfo 
	{
		public static const Scale:String = "Scale";
		
		public var type:String;
		public var object:MySprite;
		public var count:int;
		public var length:int;
		public var begin:Number;
		public var end:Number;
		public function AnimationInfo() 
		{
			
		}
		
	}

}