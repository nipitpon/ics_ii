package Resources 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import System.Interfaces.IAnimatable;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Flipbook extends Animation implements IAnimatable 
	{
		private var images:Vector.<Bitmap>;
		private var current:int;
		public function Flipbook(images:Vector.<Bitmap>, scalePoint:Point = null) 
		{
			super(scalePoint);
			this.images = images;
			current = -1;
			Initialize();
		}
		public override function Update():void
		{
			super.Update();
			
			if (Freeze || !IsNewFrame) return;
			
			var num:uint = AnimationImagePosition;
			if (current >= 0) images[current].visible = false;
			images[num].visible = true;
			current = num;
		}
		
		protected override function get IsNewFrame():Boolean { return CurrentAction.positions.length > 1 && super.IsNewFrame; }
		protected override function setCurrentAnimation(action:ActionSet):void
		{
			super.setCurrentAnimation(action);
			var pos:uint = AnimationImagePosition;
			if (current >= 0) images[current].visible = false;
			images[pos].visible = true;
			current = pos;
		}
		private function Initialize():void
		{
			for each(var bmp:Bitmap in images)
			{
				bmp.visible = false;
				addChild(bmp);
			}
		}
	}

}