package Resources.lib 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import Resources.Image;
	import System.GameFrame;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class GrassTileSprite extends Image 
	{
		[Embed(source="../../../lib/Tiles/Tileset #1.png")]
		private static const map:Class;
		private static var main_instance:Bitmap = null;
		private static var bit_data:Array = new Array();
		public function GrassTileSprite(position:uint) 
		{
			super();
			if (main_instance == null) main_instance = new map();
			var _x:int = (position % 6) * 17;
			var _y:int = Math.floor(position / 6) * 17;
			var dat:BitmapData = null;
			
			if (bit_data[_x] == null) bit_data[_x] = new Array();
			if (bit_data[_x][_y] == null)
			{
				var mat:Matrix = new Matrix();
				mat.translate(_x, _y);
				dat = new BitmapData(16, 16, true, 0);
				dat.copyPixels(main_instance.bitmapData, new Rectangle(_x, _y, 16, 16), new Point());
				bit_data[_x][_y] = dat;
			}
			dat = bit_data[_x][_y] as BitmapData;
			
			var bmp:Bitmap = new Bitmap(dat);
			bmp.scaleX = Tile.Size.x / 16;
			bmp.scaleY = Tile.Size.y / 16;
			bmp.smoothing = false;
			addChild(bmp);
		}
		
	}

}