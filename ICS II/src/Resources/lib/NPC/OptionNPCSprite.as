package Resources.lib.NPC 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.SpriteSheetAnimation;
	
	/**
	 * ...
	 * @author ss
	 */
	public class OptionNPCSprite extends SpriteSheetAnimation 
	{
		[Embed(source = "../../../../lib/Other pages/Options Animation.png")]
		private static const map:Class;
		public function OptionNPCSprite() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			super(bmp, 64, 64, null);
			Interval = 1;
			RegisterAction(0, Vector.<int>([0, 1, 2, 3, 4, 5, 6, 7]));
			defaultAnimation = 0;
		}
		
	}

}