package Resources.lib.NPC 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.SpriteSheetBook;
	import System.Interfaces.INegateAmbient;
	
	/**
	 * ...
	 * @author ss
	 */
	public class CheckPointSprite extends SpriteSheetBook implements INegateAmbient
	{
		[Embed(source = "../../../../lib/Objects/Checkpoint/Checkpoint OFF.png")]
		private static const map1:Class;
		[Embed(source = "../../../../lib/Objects/Checkpoint/Checkpoint ON idle.png")]
		private static const map2:Class;
		[Embed(source = "../../../../lib/Objects/Checkpoint/Checkpoint turning ON.png")]
		private static const map3:Class;
		public function CheckPointSprite() 
		{
			var bmps:Vector.<Bitmap> = Vector.<Bitmap>([new map1(), new map2(), new map3()]);
			for each(var bmp:Bitmap in bmps)
			{
				bmp.scaleX = 2;
				bmp.scaleY = 2;
			}
			super(bmps, 64, 64, new Point(32, 32));
			Interval = 1;
			RegisterAction(0, Vector.<int>([0]));
			RegisterAction(1, Vector.<int>([7, 8, 9, 10]));
			RegisterAction(2, Vector.<int>([1, 2, 3, 4, 5, 6]));
			RegisterAction(3, Vector.<int>([10, 9, 8, 7]));
			defaultAnimation = 0;
		}
		
	}

}