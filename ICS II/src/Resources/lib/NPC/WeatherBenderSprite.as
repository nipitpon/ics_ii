package Resources.lib.NPC 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	import Resources.lib.GrassTileSprite;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class WeatherBenderSprite extends Image 
	{
		[Embed(source = "../../../../lib/Objects/Weather machine.png")]
		private static const map:Class;
		public function WeatherBenderSprite() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = Tile.Size.x / bmp.width;
			bmp.scaleY = bmp.scaleX;
			bmp.smoothing = false;
			addChild(bmp);
		}
		
	}

}