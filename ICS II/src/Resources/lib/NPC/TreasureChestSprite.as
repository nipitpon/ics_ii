package Resources.lib.NPC 
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.Point;
	import Resources.SpriteSheetAnimation;
	import Resources.SpriteSheetBook;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author ss
	 */
	public class TreasureChestSprite extends SpriteSheetAnimation 
	{
		[Embed(source = "../../../../lib/Secret Items/Chest Opening.png")]
		private static const map:Class;
		public function TreasureChestSprite() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			super(bmp, 64, 64, new Point(32, 32));
			Interval = 1;
			RegisterAction(0, Vector.<int>([0]));
			RegisterAction(1, Vector.<int>([0, 1, 2, 3, 4]));
			RegisterAction(2, Vector.<int>([4]));
			defaultAnimation = 0;
		}
		
	}

}