package Resources.lib.NPC 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class WaterBottleSprite extends Image 
	{
		[Embed(source = "../../../../lib/Objects/Water Potion.png")]
		private static const map:Class;
		public function WaterBottleSprite() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = Tile.Size.x / bmp.width;
			bmp.scaleY = bmp.scaleX;
			addChild(bmp);
		}
		
	}

}