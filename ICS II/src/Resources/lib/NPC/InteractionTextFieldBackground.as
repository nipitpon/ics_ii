package Resources.lib.NPC 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class InteractionTextFieldBackground extends Image 
	{
		[Embed(source = "../../../../lib/GUI/Left Side Textbox.png")]
		private static const left_map:Class;
		[Embed(source = "../../../../lib/GUI/Middle Textbox.png")]
		private static const middle_map:Class;
		[Embed(source = "../../../../lib/GUI/Right Side Textbox.png")]
		private static const right_map:Class;
		
		public function InteractionTextFieldBackground(width:Number) 
		{
			super();
			var bmp:Bitmap = new left_map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
			bmp = new right_map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			bmp.x = (width + 16) - bmp.width;
			addChild(bmp);
			bmp = new middle_map();
			bmp.scaleX = (width - 16) / bmp.width;
			bmp.scaleY = 2;
			bmp.x = 16;
			addChildAt(bmp, 0);
		}
		
	}

}