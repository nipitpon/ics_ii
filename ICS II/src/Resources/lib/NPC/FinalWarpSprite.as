package Resources.lib.NPC 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FinalWarpSprite extends Image 
	{
		[Embed(source = "../../../../lib/Arterius Sign.png")]
		private static const map:Class;
		public function FinalWarpSprite() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
		}
		
	}

}