package Resources.lib.NPC 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Flipbook;
	import Resources.Image;
	import Resources.lib.GrassTileSprite;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class WarpSpotSprite extends Flipbook 
	{
		[Embed(source = "../../../../lib/Objects/Teleporter/Next Level Door.png")] 
		private static const map1:Class;
		
		
		public function WarpSpotSprite() 
		{
			var bmps:Vector.<Bitmap> = Vector.<Bitmap>([new map1()]);
			for each(var bmp:Bitmap in bmps)
			{
				bmp.scaleX = Tile.Size.x * 2 / bmp.width;
				bmp.scaleY = bmp.scaleX;
				bmp.smoothing = false;
			}
			super(bmps);
			Interval = 0;
			RegisterAction(0, Vector.<int>([0]));
			defaultAnimation = 0;
		}
		
	}

}