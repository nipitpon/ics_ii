package Resources.lib.NPC 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FirePotionSprite extends Image 
	{
		[Embed(source = "../../../../lib/Objects/Fire Potion.png")]
		private static const map:Class;
		public function FirePotionSprite() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = Tile.Size.x / bmp.width;
			bmp.scaleY = bmp.scaleX;
			addChild(bmp);
		}
		
	}

}