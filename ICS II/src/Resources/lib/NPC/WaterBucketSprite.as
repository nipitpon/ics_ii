package Resources.lib.NPC 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import Resources.Flipbook;
	import Resources.Image;
	import Resources.lib.GrassTileSprite;
	import System.MC.GrassTile;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class WaterBucketSprite extends Flipbook
	{
		[Embed(source = "../../../../lib/Objects/Fountain 1.png")]
		private static const map1:Class;
		[Embed(source = "../../../../lib/Objects/Fountain 2.png")]
		private static const map2:Class;
		[Embed(source = "../../../../lib/Objects/Fountain 3.png")]
		private static const map3:Class;
		[Embed(source = "../../../../lib/Objects/Fountain use 1.png")] 
		private static const map4:Class;
		[Embed(source = "../../../../lib/Objects/Fountain use 2.png")] 
		private static const map5:Class;
		[Embed(source = "../../../../lib/Objects/Fountain use 3.png")] 
		private static const map6:Class;
		[Embed(source = "../../../../lib/Objects/Fountain use 4.png")] 
		private static const map7:Class;
		[Embed(source = "../../../../lib/Objects/Fountain use 5.png")] 
		private static const map8:Class;
		[Embed(source = "../../../../lib/Objects/Fountain use 6.png")] 
		private static const map9:Class;
		[Embed(source = "../../../../lib/Objects/Fountain use 7.png")] 
		private static const map10:Class;
		[Embed(source = "../../../../lib/Objects/Fountain use 8.png")] 
		private static const map11:Class;
		[Embed(source = "../../../../lib/Objects/Fountain use 9.png")] 
		private static const map12:Class;
		[Embed(source = "../../../../lib/Objects/Fountain use 10.png")]
		private static const map13:Class;
		[Embed(source = "../../../../lib/Objects/Fountain use 11.png")]
		private static const map14:Class;
		[Embed(source = "../../../../lib/Objects/Fountain use 12.png")]
		private static const map15:Class;
		[Embed(source = "../../../../lib/Objects/Fountain use 13.png")]
		private static const map16:Class;
		[Embed(source = "../../../../lib/Objects/Fountain use 14.png")]
		private static const map17:Class;
		
		public function WaterBucketSprite() 
		{
			var bmps:Vector.<Bitmap> = Vector.<Bitmap>([
				new map1(), new map2(), new map3(), new map4(), new map5(),
				new map6(), new map7(), new map8(), new map9(), new map10(),
				new map11(), new map12(), new map13(), new map14(), new map15(),
				new map16(), new map17()
			]);
			for each(var bmp:Bitmap in bmps)
			{
				bmp.scaleX = Tile.Size.x * 2 / bmp.width;
				bmp.scaleY = bmp.scaleX;
				bmp.smoothing = false;
			}
			super(bmps);
			Interval = 1;
			RegisterAction(0, Vector.<int>([0, 1, 2]));
			RegisterAction(1, Vector.<int>([3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]), 0);
			defaultAnimation = 0;
		}
		
	}

}