package Resources.lib.Tutorials 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author ss
	 */
	public class TutorialSlideSprite extends Image 
	{
		[Embed(source = "../../../../lib/Tutorials/Level 1 Slide.png")]
		private static const map:Class;
		public function TutorialSlideSprite() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
		}
		
	}

}