package Resources.lib.ComicPage 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Comic61 extends Image 
	{
		[Embed(source = "../../../../lib/ComicPage/6_1.png")]
		private static const map:Class;
		public function Comic61() 
		{
			super();
			addChild(new map());
			x = 36;
			y = 32;
		}
		
	}

}