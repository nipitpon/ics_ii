package Resources.lib.ComicPage 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Comic73 extends Image 
	{
		[Embed(source = "../../../../lib/ComicPage/7_3.png")]
		private static const map:Class;
		public function Comic73() 
		{
			super();
			addChild(new map());
			x = 36;
			y = 307;
		}
		
	}

}