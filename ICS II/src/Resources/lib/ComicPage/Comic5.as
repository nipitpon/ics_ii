package Resources.lib.ComicPage 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Comic5 extends Image 
	{
		[Embed(source = "../../../../lib/ComicPage/5.png")]
		private static const map:Class;
		public function Comic5() 
		{
			super();
			addChild(new map());
			x = 36;
			y = 32;
		}
		
	}

}