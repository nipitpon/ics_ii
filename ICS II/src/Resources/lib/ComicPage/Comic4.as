package Resources.lib.ComicPage 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Comic4 extends Image 
	{
		[Embed(source = "../../../../lib/ComicPage/4.png")]
		private static const map:Class;
		public function Comic4() 
		{
			super();
			addChild(new map());
			x = 36;
			y = 32;
		}
		
	}

}