package Resources.lib.ComicPage 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Comic63 extends Image 
	{
		[Embed(source = "../../../../lib/ComicPage/6_3.png")]
		private static const map:Class;
		public function Comic63() 
		{
			super();
			addChild(new map());
			x = 36;
			y = 282;
		}
		
	}

}