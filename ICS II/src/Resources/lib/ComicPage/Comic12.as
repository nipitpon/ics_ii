package Resources.lib.ComicPage 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Comic12 extends Image 
	{
		[Embed(source = "../../../../lib/ComicPage/1_2.png")]
		private static const map:Class;
		public function Comic12() 
		{
			super();
			addChild(new map());
			x = 403;
			y = 32;
		}
		
	}

}