package Resources.lib.ComicPage 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Comic72 extends Image 
	{
		[Embed(source = "../../../../lib/ComicPage/7_2.png")]
		private static const map:Class;
		public function Comic72() 
		{
			super();
			addChild(new map());
			x = 386;
			y = 32;
		}
		
	}

}