package Resources.lib.ComicPage 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Comic32 extends Image 
	{
		[Embed(source = "../../../../lib/ComicPage/3_2.png")]
		private static const map:Class;
		public function Comic32() 
		{
			super();
			addChild(new map());
			x = 36;
			y = 204;
		}
		
	}

}