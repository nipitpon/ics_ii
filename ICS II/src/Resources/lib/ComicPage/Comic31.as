package Resources.lib.ComicPage 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Comic31 extends Image 
	{
		[Embed(source = "../../../../lib/ComicPage/3_1.png")]
		private static const map:Class;
		public function Comic31() 
		{
			super();
			addChild(new map());
			x = 36;
			y = 32;
		}
		
	}

}