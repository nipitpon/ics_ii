package Resources.lib.ComicPage 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Comic62 extends Image 
	{
		[Embed(source = "../../../../lib/ComicPage/6_2.png")]
		private static const map:Class;
		public function Comic62() 
		{
			super();
			addChild(new map());
			x = 362;
			y = 32;
		}
		
	}

}