package Resources.lib.ComicPage 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class PlaneDownImage extends Image 
	{
		[Embed(source = "../../../../lib/ComicPage/PlaneDown.png")]
		private static const map:Class;
		public function PlaneDownImage() 
		{
			super(new Point(512, 384));
			addChild(new map());
		}
		
	}

}