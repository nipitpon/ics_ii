package Resources.lib.ComicPage 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Comic33 extends Image 
	{
		[Embed(source = "../../../../lib/ComicPage/3_3.png")]
		private static const map:Class;
		public function Comic33() 
		{
			super();
			addChild(new map());
			x = 36;
			y = 356;
		}
		
	}

}