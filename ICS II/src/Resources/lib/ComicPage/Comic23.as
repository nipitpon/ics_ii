package Resources.lib.ComicPage 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Comic23 extends Image 
	{
		[Embed(source = "../../../../lib/ComicPage/2_3.png")]
		private static const map:Class;
		public function Comic23() 
		{
			super();
			addChild(new map());
			x = 36;
			y = 295;
		}
		
	}

}