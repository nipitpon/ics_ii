package Resources.lib.ComicPage 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Comic13 extends Image 
	{
		[Embed(source = "../../../../lib/ComicPage/1_3.png")]
		private static const map:Class;
		public function Comic13() 
		{
			super();
			addChild(new map());
			x = 36;
			y = 295;
		}
		
	}

}