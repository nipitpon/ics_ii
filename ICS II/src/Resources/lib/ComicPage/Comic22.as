package Resources.lib.ComicPage 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Comic22 extends Image 
	{
		[Embed(source = "../../../../lib/ComicPage/2_2.png")]
		private static const map:Class;
		public function Comic22() 
		{
			super();
			addChild(new map());
			x = 425;
			y = 32;
		}
		
	}

}