package Resources.lib.Levers 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Flipbook;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class GreenLeverSprite extends Flipbook 
	{
		[Embed(source = "../../../../lib/Objects/Levers/Lever Green 1.png")]
		private static const map1:Class;
		[Embed(source = "../../../../lib/Objects/Levers/Lever Green 2.png")]
		private static const map2:Class;
		
		
		public function GreenLeverSprite() 
		{
			var bmps:Vector.<Bitmap> = Vector.<Bitmap>([new map1(), new map2()]);
			for each(var bmp:Bitmap in bmps)
			{
				bmp.scaleX = Tile.Size.x / bmp.width;
				bmp.scaleY = bmp.scaleX;
				bmp.smoothing = false;
			}
			super(bmps);
			Interval = 2;
			RegisterAction(0, Vector.<int>([0]));
			RegisterAction(1, Vector.<int>([1]));
			defaultAnimation = 0;
		}
		
	}

}