package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SwitchOnSound extends MySound 
	{
		[Embed(source = "../../../../../lib/Sound/Switch On.mp3")]
		private static const map:Class;
		public function SwitchOnSound() 
		{
			super(new map(), 55);
			
		}
		
	}

}