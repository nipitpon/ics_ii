package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FireBottleOpen extends MySound 
	{
		[Embed(source="../../../../../lib/Sound/Fire Potion.mp3")]
		private static const map:Class;
		public function FireBottleOpen() 
		{
			super(new map(), 55);
			
		}
		
	}

}