package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	/**
	 * ...
	 * @author ss
	 */
	public class SlideSound extends MySound
	{
		[Embed(source = "../../../../../lib/Sound/Slide.mp3")]
		private static const map:Class;
		public function SlideSound() 
		{
			super(new map(), 55);
		}
		
	}

}