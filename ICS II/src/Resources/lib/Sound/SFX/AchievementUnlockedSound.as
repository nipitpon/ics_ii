package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class AchievementUnlockedSound extends MySound 
	{
		[Embed(source="../../../../../lib/Sound/Achievement Sound.mp3")]
		private static const map:Class;
		public function AchievementUnlockedSound() 
		{
			super(new map(), 55);
			
		}
		
	}

}