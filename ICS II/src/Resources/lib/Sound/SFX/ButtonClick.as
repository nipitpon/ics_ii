package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ButtonClick extends MySound 
	{
		[Embed(source = "../../../../../lib/Sound/Click.mp3")]
		private static const map:Class;
		public function ButtonClick() 
		{
			super(new map(), 55);
			
		}
		
	}

}