package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FireExhaustedSound extends MySound 
	{
		[Embed(source="../../../../../lib/Sound/Fire Exhausted.mp3")]
		private static const map:Class;
		public function FireExhaustedSound() 
		{
			super(new map(), 55);
			
		}
		
	}

}