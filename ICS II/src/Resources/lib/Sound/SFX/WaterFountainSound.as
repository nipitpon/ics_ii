package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class WaterFountainSound extends MySound 
	{
		[Embed(source = "../../../../../lib/Sound/WaterFountain.mp3")]
		private static const map:Class;
		public function WaterFountainSound() 
		{
			super(new map(), 55);
			
		}
		public override function get PauseWithGame():Boolean { return true; }
	}

}