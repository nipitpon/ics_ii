package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author ss
	 */
	public class ClockTickingSound extends MySound 
	{
		[Embed(source = "../../../../../lib/Sound/Clock Ticking.mp3")]
		private static const map:Class;
		public function ClockTickingSound() 
		{
			super(new map(), 55);
			
		}
		
	}

}