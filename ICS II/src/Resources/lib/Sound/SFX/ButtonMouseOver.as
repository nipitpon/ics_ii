package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ButtonMouseOver extends MySound 
	{
		[Embed(source="../../../../../lib/Sound/Mouse Over.mp3")]
		private static const map:Class;
		public function ButtonMouseOver() 
		{
			super(new map(), 55);
			
		}
		
	}

}