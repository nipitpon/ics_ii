package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SwitchOffSound extends MySound 
	{
		[Embed(source = "../../../../../lib/Sound/Switch Off.mp3")]
		private static const map:Class;
		public function SwitchOffSound() 
		{
			super(new map(), 55);
			
		}
		
	}

}