package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FireIgniteSound extends MySound 
	{
		[Embed(source="../../../../../lib/Sound/Fire Ignite.mp3")]
		private static const map:Class;
		public function FireIgniteSound() 
		{
			super(new map(), 55);
			
		}
		
	}

}