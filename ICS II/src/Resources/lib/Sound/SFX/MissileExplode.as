package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class MissileExplode extends MySound 
	{
		[Embed(source = "../../../../../lib/Sound/Explosion2.mp3")]
		private static const map:Class;
		public function MissileExplode() 
		{
			super(new map(), 55);
			
		}
		public override function get PauseWithGame():Boolean { return true; }
		
	}

}