package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class WaterBottleOpen extends MySound 
	{
		[Embed(source="../../../../../lib/Sound/Water Potion.mp3")]
		private static const map:Class;
		public function WaterBottleOpen() 
		{
			super(new map(), 55);
			
		}
		public override function get PauseWithGame():Boolean { return true; }
	}

}