package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FireBurning extends MySound 
	{
		[Embed(source = "../../../../../lib/Sound/FireBurning.mp3")]
		private static const map:Class;
		public function FireBurning() 
		{
			super(new map(), 55);
			
		}
		public override function get PauseWithGame():Boolean { return true; }
	}

}