package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author ss
	 */
	public class ParticleBeamSound extends MySound 
	{
		[Embed(source="../../../../../lib/Sound/ParticleBeamSound.mp3")]
		private static const map:Class;
		public function ParticleBeamSound() 
		{
			super(new map(), 55);	
		}	
	}
}