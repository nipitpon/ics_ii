package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author ss
	 */
	public class WarpOutSound extends MySound 
	{
		[Embed(source = "../../../../../lib/Sound/Warp.mp3")]
		private static const map:Class;
		public function WarpOutSound() 
		{
			super(new map(), 55);
			
		}
		
	}

}