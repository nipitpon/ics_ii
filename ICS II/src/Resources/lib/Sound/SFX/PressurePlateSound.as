package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class PressurePlateSound extends MySound 
	{
		[Embed(source="../../../../../lib/Sound/Pressure Plate.mp3")]
		private static const map:Class;
		public function PressurePlateSound() 
		{
			super(new map(), 55);
			
		}
		
	}

}