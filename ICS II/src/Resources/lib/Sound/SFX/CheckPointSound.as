package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author ss
	 */
	public class CheckPointSound extends MySound 
	{
		[Embed(source = "../../../../../lib/Sound/Beam Charge.mp3")]
		private static const map:Class;
		public function CheckPointSound() 
		{
			super(new map(), 55);
			
		}
		
	}

}