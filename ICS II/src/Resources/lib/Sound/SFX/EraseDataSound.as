package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author ss
	 */
	public class EraseDataSound extends MySound 
	{
		[Embed(source = "../../../../../lib/Sound/Erase Data.mp3")]
		private static const map:Class;
		public function EraseDataSound() 
		{
			super(new map(), 55);
			
		}
		
	}

}