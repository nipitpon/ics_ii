package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FireStartingSound extends MySound 
	{
		[Embed(source = "../../../../../lib/Sound/FireStarting.mp3")]
		private static const map:Class;
		public function FireStartingSound() 
		{
			super(new map(), 55);
			
		}
		public override function get PauseWithGame():Boolean { return true; }
	}

}