package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class TreasureCollectedSound extends MySound 
	{
		[Embed(source="../../../../../lib/Sound/Treasure.mp3")]
		private static const map:Class;
		public function TreasureCollectedSound() 
		{
			super(new map(), 55);
			
		}
		
	}

}