package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SpikeSound extends MySound 
	{
		[Embed(source="../../../../../lib/Sound/Spikes.mp3")]
		private static const map:Class;
		public function SpikeSound() 
		{
			super(new map(), 55);
			
		}
		
	}

}