package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author ss
	 */
	public class BatySound extends MySound 
	{
		[Embed(source = "../../../../../lib/Sound/Bat Burning.mp3")]
		private static const map:Class;
		public function BatySound() 
		{
			super(new map(), 55);
			
		}
		
	}

}