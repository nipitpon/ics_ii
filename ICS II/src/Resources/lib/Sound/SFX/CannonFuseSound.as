package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author ss
	 */
	public class CannonFuseSound extends MySound 
	{
		[Embed(source = "../../../../../lib/Sound/Fuse Burn.mp3")]
		private static const map:Class;
		public function CannonFuseSound() 
		{
			super(new map(), 55);
			
		}
		
	}

}