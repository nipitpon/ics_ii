package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	/**
	 * ...
	 * @author ss
	 */
	public class CannonFireSound extends MySound
	{
		[Embed(source = "../../../../../lib/Sound/Cannon Fire.mp3")]
		private static const map:Class;
		public function CannonFireSound() 
		{
			super(new map(), 55);
		}
		
	}

}