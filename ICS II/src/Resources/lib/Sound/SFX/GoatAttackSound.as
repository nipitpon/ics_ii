package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class GoatAttackSound extends MySound 
	{
		[Embed(source="../../../../../lib/Sound/Gecko V2.mp3")]
		private static const map:Class;
		public function GoatAttackSound() 
		{
			super(new map(), 55);
			
		}
		
	}

}