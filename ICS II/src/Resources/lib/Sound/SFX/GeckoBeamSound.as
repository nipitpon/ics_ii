package Resources.lib.Sound.SFX 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author ss
	 */
	public class GeckoBeamSound extends MySound 
	{
		[Embed(source="../../../../../lib/Sound/Gecko Beam.mp3")]
		private static const map:Class;
		public function GeckoBeamSound() 
		{
			super(new map(), 55);
			
		}
		
	}

}