package Resources.lib.Sound.BGM 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class NightLevelBGM extends MySound 
	{
		[Embed(source="../../../../../lib/Sound/I.C.S. 2 Night Track 1.mp3")]
		private static const map:Class;
		public function NightLevelBGM() 
		{
			super(new map(), 55);
			
		}
		
	}

}