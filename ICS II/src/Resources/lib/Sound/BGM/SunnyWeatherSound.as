package Resources.lib.Sound.BGM 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SunnyWeatherSound extends MySound 
	{
		private static var played:Boolean = false;
		[Embed(source = "../../../../../lib/Sound/I.C.S. 2 Track 2 V2.mp3")]
		private static const map:Class;
		public function SunnyWeatherSound() 
		{
			super(new map(), 55);
			
		}
		public override function Play(loop:int = 0, fadeInDuration:int = 0, custom_start:int = -1):void
		{
			//ChangeStartOffset(Utility.Random(55, 60000));
			if (!played)
			{
				played = true;
				super.Play(loop, fadeInDuration, custom_start);
			}
			else
			{
				super.Play(loop, fadeInDuration, Utility.Random(55, 90000));
			}
		}
	}

}