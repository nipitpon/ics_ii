package Resources.lib.Sound.BGM 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class RainSound extends MySound 
	{
		[Embed(source = "../../../../../lib/Sound/Rain.mp3")]
		private static const map:Class;
		public function RainSound() 
		{
			super(new map(), 55);
		}
	}

}