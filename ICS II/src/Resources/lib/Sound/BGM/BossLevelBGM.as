package Resources.lib.Sound.BGM 
{
	import _Base.MySound;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class BossLevelBGM extends MySound 
	{
		[Embed(source="../../../../../lib/Sound/I.C.S. 2 Boss Track.mp3")]
		private static const map:Class;
		public function BossLevelBGM() 
		{
			super(new map(), 55);
		}
		
	}

}