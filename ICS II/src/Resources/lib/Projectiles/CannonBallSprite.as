package Resources.lib.Projectiles 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Flipbook;
	import Resources.Image;
	import Resources.SpriteSheetAnimation;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class CannonBallSprite extends SpriteSheetAnimation 
	{
		[Embed(source = "../../../../lib/Objects/Fire cannonball.png")]
		private static const map:Class;
		
		public function CannonBallSprite() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			
			super(bmp, 64, 64, new Point(32, 32));
			Interval = 1;
			RegisterAction(0, Vector.<int>([5, 4, 3, 2, 1, 0]));
			defaultAnimation = 0;
		}
		
	}

}