package Resources.lib.Projectiles 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Flipbook;
	import Resources.Image;
	import Resources.SpriteSheetAnimation;
	import Resources.SpriteSheetBook;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class RocketSprite extends SpriteSheetBook
	{
		[Embed(source = "../../../../lib/Objects/Fireball.png")]
		private static const map:Class;
		[Embed(source = "../../../../lib/Objects/Fireball FadeOut.png")]
		private static const fade_out:Class;
		
		public function RocketSprite() 
		{
			var bmps:Vector.<Bitmap> = Vector.<Bitmap>([new map(), new fade_out()]);
			for each(var bmp:Bitmap in bmps)
			{
				bmp.scaleX = 2;
				bmp.scaleY = 2;
				
			}
			super(bmps, 32, 32, new Point(16, 16));
			Interval = 1;
			RegisterAction(0, Vector.<int>([0, 1, 2, 3]));
			RegisterAction(1, Vector.<int>([7, 6, 5, 4]));
			defaultAnimation = 0;
		}
		
	}

}