package Resources.lib.Projectiles 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.SpriteSheetAnimation;
	import Resources.SpriteSheetBook;
	
	/**
	 * ...
	 * @author ss
	 */
	public class WaterBallSprite extends SpriteSheetBook 
	{
		
		[Embed(source = "../../../../lib/Objects/Waterball.png")]
		private static const map:Class;
		[Embed(source = "../../../../lib/Objects/Waterball FadeOut.png")]
		private static const fade_out:Class;
		public function WaterBallSprite() 
		{
			var bmps:Vector.<Bitmap> = Vector.<Bitmap>([new map(), new fade_out()]);
			for each(var bmp:Bitmap in bmps)
			{
				bmp.scaleX = 2;
				bmp.scaleY = 2;
				
			}
			super(bmps, 32, 32, new Point(16, 16));
			Interval = 1;
			RegisterAction(0, Vector.<int>([0, 1, 2, 3]));
			RegisterAction(1, Vector.<int>([7, 6, 5, 4]));
			defaultAnimation = 0;
		}
		
	}

}