package Resources.lib 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.SpriteSheetBook;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ParticleBeamSprite extends SpriteSheetBook 
	{
		[Embed(source = "../../../lib/Objects/Particle Beam/Laser 96x270-strip.png")]
		private static const idle:Class;
		[Embed(source = "../../../lib/Objects/Particle Beam/Laser Appear 96x270-strip.png")]
		private static const activate:Class;
		public function ParticleBeamSprite() 
		{
			var bmps:Vector.<Bitmap> = Vector.<Bitmap>([new activate(), new idle()]);
			for each(var bmp:Bitmap in bmps)
			{
				bmp.scaleX = 2;
				bmp.scaleY = 2;
			}
			super(bmps, 192, 540, null);
			Interval = 1;
			RegisterAction(0, Vector.<int>([12, 13, 14, 15, 16]));
			RegisterAction(1, Vector.<int>([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]));
			RegisterAction(2, Vector.<int>([11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]));
			defaultAnimation = 1;
		}
		
	}

}