package Resources.lib 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Flipbook;
	
	/**
	 * ...
	 * @author ss
	 */
	public class DynamicFlipbook extends Flipbook 
	{
		
		public function DynamicFlipbook(images:Vector.<Bitmap>, animations:Vector.<Vector.<int>>, interval:int, scalePoint:Point=null) 
		{
			super(images, scalePoint);
			Interval = interval;
			for (var i:int = 0; i < animations.length; i++)
			{
				RegisterAction(i, animations[i]);
			}
			defaultAnimation = 0;
		}
		
	}

}