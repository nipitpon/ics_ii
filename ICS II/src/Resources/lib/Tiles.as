package Resources.lib 
{
	import flash.automation.KeyboardAutomationAction;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	/**
	 * ...
	 * @author ss
	 */
	public final class Tiles 
	{
		[Embed(source = "../../../lib/Tiles/Tileset #1.png")]
		private static const tile1:Class;
		[Embed(source = "../../../lib/Tiles/Interactables.png")]
		private static const interactables:Class;
		public static const key:Object = { 
			"../../../../../Dropbox/ICS II/Tiles/Tileset #1.png" : tile1,
			"../../../../../Dropbox/ICS II/Tiles/Interactables.png" : interactables,
			"../Tiles/Tileset #1.png" : tile1,
			"../Tiles/Interactables.png": interactables,
			"C:/Users/Miroko/Dropbox/ICS II/Tiles/Tileset #1.png": tile1,
			"C:/Users/Miroko/Dropbox/ICS II/Tiles/Interactables.png": interactables
		};
		public static const data:Object = {
			
		};
		
		public function Tiles() 
		{
			
		}
		public static function GetBitmapData(path:String):BitmapData
		{
			if (key[path] == null) return null;
			else if (data[path] == null)
			{
				data[path] = (new key[path]() as Bitmap).bitmapData;
			}
			return data[path] as BitmapData;
		}
	}

}