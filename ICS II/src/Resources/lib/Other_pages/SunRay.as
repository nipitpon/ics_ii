package Resources.lib.Other_pages 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SunRay extends Image 
	{
		[Embed(source = "../../../../lib/Other pages/Sunray.png")]
		private static const map:Class;
		public function SunRay() 
		{
			super();
			addChild(new map());
		}
		
	}

}