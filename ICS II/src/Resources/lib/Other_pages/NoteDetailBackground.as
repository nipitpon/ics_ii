package Resources.lib.Other_pages 
{
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class NoteDetailBackground extends Image 
	{
		[Embed(source = "../../../../lib/GUI/Paper.png")]
		private static const map:Class;
		public function NoteDetailBackground() 
		{
			super();
			addChild(new map());
		}
	}
}