package Resources.lib.Other_pages 
{
	import flash.display.Bitmap;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import Resources.Image;
	import Resources.Resource;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SelectLevelButtonBackground extends Image 
	{
		[Embed(source="../../../../lib/Other pages/Unlocked Slot.png")]
		private static const empty:Class;
		[Embed(source="../../../../lib/Other pages/Locked Slot.png")]
		private static const locked:Class;
		
		private var empty_bmp:RenderableObject;
		private var locked_bmp:RenderableObject;
		public function SelectLevelButtonBackground(level:int) 
		{
			super();
			this.empty_bmp = new RenderableObject();
			var obj:Bitmap = this.empty_bmp.addChild(new SelectLevelButtonBackground.empty()) as Bitmap;
			obj.scaleX = 2;
			obj.scaleY = 2;
			obj.smoothing = false;
			obj.transform.colorTransform = new ColorTransform(0, 0, 0, 1, 172, 176, 161);
			this.empty_bmp.addChild(Resource.GetSelectLevelButtonText(level.toString()));
			addChild(this.empty_bmp);
			this.locked_bmp = new RenderableObject();
			obj = this.locked_bmp.addChild(new SelectLevelButtonBackground.locked()) as Bitmap;
			obj.scaleX = 2;
			obj.scaleY = 2;
			addChild(this.locked_bmp);
			this.locked_bmp.visible = false;
			this.locked_bmp.addChild(Resource.GetSelectLevelButtonText(level.toString()));
			this.locked_bmp.transform.colorTransform = new ColorTransform(0.5, 0.5, 0.5, 0.5);
		}
		public function set Enabled(val:Boolean):void
		{
			this.locked_bmp.visible = !val;
			this.empty_bmp.visible = val;
		}
		public function get Enabled():Boolean { return this.empty_bmp.visible; }
	}

}