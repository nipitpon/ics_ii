package Resources.lib.Other_pages 
{
	import _Base.MyTextField;
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ClearDataNoButtonImage extends Image 
	{
		public function ClearDataNoButtonImage() 
		{
			super();
			var text:MyTextField = new MyTextField(40, 0xffffff, false);
			text.text = "NO";
			addChild(text);
			
		}
		
	}

}