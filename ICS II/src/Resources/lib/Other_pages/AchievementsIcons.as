package Resources.lib.Other_pages 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Flipbook;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class AchievementsIcons extends Flipbook 
	{
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/1 Finish level 5.png")]
		private static const map1:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/2 Finish level 10.png")]
		private static const map2:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/3 Finish level 15.png")]
		private static const map3:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/4 Finish level 20.png")]
		private static const map4:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/5 Finish level 25.png")]
		private static const map5:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/6 Finish level 30.png")]
		private static const map6:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/7 Finish level 35.png")]
		private static const map7:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/8 Finish level 40.png")]
		private static const map8:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/9 Finish level 45.png")]
		private static const map9:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/10 Finish level 50.png")]
		private static const map10:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/11 Play 15 minutes.png")]
		private static const map11:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/12 Play 30 minutes.png")]
		private static const map12:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/13 Play 45 minutes.png")]
		private static const map13:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/14 Play 60 minutes.png")]
		private static const map14:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/15 Burn 5 monster.png")]
		private static const map15:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/16 Burn 20 monster.png")]
		private static const map16:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/17 Burn 50 monster.png")]
		private static const map17:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/18 Die 1 time.png")]
		private static const map18:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/19 Die 10 times.png")]
		private static const map19:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/20 Die 50 times.png")]
		private static const map20:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/21 Die 10 times in a single level.png")]
		private static const map21:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/22 Die from spikes while on fire.png")]
		private static const map22:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/23 Collect 10 treasure chests.png")]
		private static const map23:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/24 Collect 20 treasure chests.png")]
		private static const map24:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/25 Collect 30 treasure chests.png")]
		private static const map25:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/26 Collect 40 treasure chests.png")]
		private static const map26:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/27 Collect 50 treasure chests.png")]
		private static const map27:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/28 Finish the special level.png")]
		private static const map28:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/29 Visit credits page.png")]
		private static const map29:Class;
		[Embed(source="../../../../lib/Trophies and achievements/Achievements/30 Visit the SPONSOR.png")]
		private static const map30:Class;
		
		private static const map_list:Vector.<Class> = Vector.<Class>([
			map1, map2, map3, map4, map5, map6, map7, map8, map9, map10,
			map11, map12, map13, map14, map15, map16, map17, map18, map19, map20,
			map21, map22, map23, map24, map25, map26, map27, map28, map29, map30
		]);
		public function AchievementsIcons() 
		{
			var bmps:Vector.<Bitmap> = new Vector.<Bitmap>();
			var i:int = 0;
			for (i = 0; i < map_list.length; i++)
			{
				var bmp:Bitmap = new map_list[i]();
				bmp.scaleX = 2;
				bmp.scaleY = 2;
				bmps.push(bmp);
			}
			super(bmps);
			Interval = 5;
			for (i = 0; i < map_list.length; i++)
				RegisterAction(i, Vector.<int>([i]));
			defaultAnimation = 0;
		}
		
	}

}