package Resources.lib.Other_pages 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Sponsor40x40 extends Image 
	{
		[Embed(source = "../../../../lib/Other pages/Sponsor40x40.png")]
		private static const map:Class;
		public function Sponsor40x40() 
		{
			super();
			addChild(new map());
		}
		
	}

}