package Resources.lib.Other_pages 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class PreloaderGoImage extends Image 
	{
		[Embed(source = "../../../../lib/Other pages/Preloader/PlayButton.png")]
		private static const map:Class;
		public function PreloaderGoImage() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
		}
		
	}

}