package Resources.lib.Other_pages 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class AchievementsBackgroundImage extends Image 
	{
		[Embed(source="../../../../lib/Trophies and achievements/Achievements Layout.png")]
		private static const map:Class;
		public function AchievementsBackgroundImage() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
		}
		
	}

}