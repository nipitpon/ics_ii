package Resources.lib.Other_pages 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author ...
	 */
	public class SubmitScoreSprite extends Image 
	{
		[Embed(source = "../../../../lib/Other pages/Submit Score Button.png")]
		private static const map:Class;
		public function SubmitScoreSprite() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
		}
		
	}

}