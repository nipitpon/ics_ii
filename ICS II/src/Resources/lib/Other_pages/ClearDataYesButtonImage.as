package Resources.lib.Other_pages 
{
	import _Base.MyTextField;
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ClearDataYesButtonImage extends Image 
	{
		public function ClearDataYesButtonImage() 
		{
			super();
			var text:MyTextField = new MyTextField(40, 0xff0000, false);
			text.text = "YES";
			addChild(text);
		}
		
	}

}