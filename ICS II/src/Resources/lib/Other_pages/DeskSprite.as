package Resources.lib.Other_pages 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	import Resources.SpriteSheetAnimation;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class DeskSprite extends SpriteSheetAnimation
	{
		[Embed(source="../../../../lib/Other pages/Start.png")]
		private static const map:Class;
		public function DeskSprite() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			super(bmp, 64, 64, new Point(32, 32));
			Interval = 3;
			RegisterAction(0, Vector.<int>([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]));
			defaultAnimation = 0;
		}
		
	}

}