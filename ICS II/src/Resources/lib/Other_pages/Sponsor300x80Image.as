package Resources.lib.Other_pages 
{
	import _Base.MyTextField;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import Resources.Image;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Sponsor300x80Image extends Image 
	{
		public function Sponsor300x80Image() 
		{
			super();
			var text:MyTextField = new MyTextField();
			text.defaultTextFormat = new TextFormat("Pixel Font", 40, 0xff0000, true, false, false, null, null, TextFormatAlign.CENTER);
			
				text.width = 300;
				text.height = 80;
				text.text = "SPONSOR.COM";
			text.y = (80 - text.textHeight) / 2;
			addChild(text);
		}
		
	}

}