package Resources.lib.Other_pages 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class AchievementCloseImage extends Image 
	{
		[Embed(source = "../../../../lib/Other pages/Go back arrow.png")]
		private static const map:Class;
		public function AchievementCloseImage() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
		}
		
	}

}