package Resources.lib.Other_pages 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class PreloaderLoadBar extends Image 
	{
		[Embed(source = "../../../../lib/Other pages/Preloader/Loading Bar.png")]
		private static const map:Class;
		public function PreloaderLoadBar() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			super();
			addChild(bmp);
		}
		
	}

}