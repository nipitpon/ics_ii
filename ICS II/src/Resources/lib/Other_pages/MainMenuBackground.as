package Resources.lib.Other_pages 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class MainMenuBackground extends Image 
	{
		[Embed(source="../../../../lib/Default Background.png")]
		private static const map:Class;
		public function MainMenuBackground() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 3;
			bmp.scaleY = 3;
			bmp.smoothing = false;
			addChild(bmp);
		}
		
	}

}