package Resources.lib.Other_pages 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author ss
	 */
	public class Finish1BackgroundSprite extends Image 
	{
		[Embed(source = "../../../../lib/Other pages/FinishScreen1.png")]
		private static const map:Class;
		public function Finish1BackgroundSprite() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
		}
		
	}

}