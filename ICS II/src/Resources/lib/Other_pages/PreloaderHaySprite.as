package Resources.lib.Other_pages 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import HardCode.Levels.StartsAtNight;
	import Resources.Flipbook;
	import Resources.Image;
	
	/**
	 * ...
	 * @author ss
	 */
	public class PreloaderHaySprite extends Flipbook 
	{
		[Embed(source = "../../../../lib/Other pages/Preloader/Hay Sprite.png")]
		private static const map:Class;
		[Embed(source = "../../../../lib/Other pages/Preloader/Hay Start.png")]
		private static const start:Class;
		[Embed(source = "../../../../lib/Other pages/Preloader/Hay End.png")]
		private static const end:Class;
		public function PreloaderHaySprite() 
		{
			var bmps:Vector.<Bitmap> = Vector.<Bitmap>([new start(), new map(), new end()]);
			for each(var bmp:Bitmap in bmps)
			{
				bmp.scaleX = 2;
				bmp.scaleY = 2;
			}
			super(bmps);
			Interval = 1;
			RegisterAction(0, Vector.<int>([0]));
			RegisterAction(1, Vector.<int>([1]));
			RegisterAction(2, Vector.<int>([2]));
			defaultAnimation = 1;
		}
		
	}

}