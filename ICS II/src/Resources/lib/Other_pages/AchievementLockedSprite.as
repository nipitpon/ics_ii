package Resources.lib.Other_pages 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author ss
	 */
	public class AchievementLockedSprite extends Image 
	{
		[Embed(source = "../../../../lib/Trophies and achievements/Locked Icon.png")]
		private static const map:Class;
		public function AchievementLockedSprite() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
		}
		
	}

}