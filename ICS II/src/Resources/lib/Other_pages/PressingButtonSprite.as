package Resources.lib.Other_pages 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.SpriteSheetAnimation;
	
	/**
	 * ...
	 * @author ss
	 */
	public class PressingButtonSprite extends SpriteSheetAnimation 
	{
		[Embed(source = "../../../../lib/GUI/Interaction animation.png")]
		private static const map:Class;
		public function PressingButtonSprite() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			super(bmp, 38, 38, null);
			Interval = 1;
			RegisterAction(0, Vector.<int>([0, 1, 2, 3, 4, 5]));
			defaultAnimation = 0;
		}
		
	}

}