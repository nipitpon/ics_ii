package Resources.lib 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	import System.Interfaces.INegateAmbient;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class RocketLauncherSprite extends Image implements INegateAmbient
	{
		[Embed(source = "../../../lib/Objects/Fireball Dispenser 4-way.png")]
		private static const map:Class;
		public function RocketLauncherSprite() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = Tile.Size.x / bmp.width;
			bmp.scaleY = bmp.scaleX;
			addChild(bmp);
		}
		
	}

}