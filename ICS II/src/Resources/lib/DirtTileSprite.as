package Resources.lib 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import Resources.Image;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class DirtTileSprite extends Image 
	{
		
		[Embed(source = "../../../lib/Tile_Background 1.png")]
		private static const map:Class;
		private static var main_instance:Bitmap = null;
		public function DirtTileSprite(position:uint) 
		{
			super();
			if (main_instance == null) main_instance = new map();
			var _x:int = (position % 3) * 16;
			var _y:int = Math.floor(position / 3) * 16;
			var mat:Matrix = new Matrix();
			mat.translate(_x, _y);
			var dat:BitmapData = new BitmapData(16, 16, true, 0);
			dat.copyPixels(main_instance.bitmapData, new Rectangle(_x, _y, 16, 16), new Point());
			var bmp:Bitmap = new Bitmap(dat);
			
			bmp.scaleX = Tile.Size.x/16.0;
			bmp.scaleY = Tile.Size.y / 16.0;
			bmp.smoothing = false;
			addChild(bmp);
		}
		
	}

}