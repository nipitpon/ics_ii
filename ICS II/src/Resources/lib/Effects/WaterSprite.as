package Resources.lib.Effects 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Flipbook;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class WaterSprite extends Flipbook 
	{
		[Embed(source = "../../../../lib/Effects/Splash 1.png")]
		private static const map1:Class;
		[Embed(source = "../../../../lib/Effects/Splash 2.png")]
		private static const map2:Class;
		[Embed(source = "../../../../lib/Effects/Splash 3.png")]
		private static const map3:Class;
		[Embed(source = "../../../../lib/Effects/Splash 4.png")]
		private static const map4:Class;
		
		public function WaterSprite() 
		{
			var bmps:Vector.<Bitmap> = Vector.<Bitmap>([
				new map1(), new map2(), new map3(), new map4()
			]);
			for each(var bmp:Bitmap in bmps)
			{
				bmp.scaleX = (Tile.Size.x / bmp.width) * 2;
				bmp.scaleY = bmp.scaleX;
				bmp.smoothing = false;
			}
			super(bmps);
			Interval = 1;
			RegisterAction(0, Vector.<int>([0, 1, 2, 3]));
			defaultAnimation = 0;
		}
		
	}

}