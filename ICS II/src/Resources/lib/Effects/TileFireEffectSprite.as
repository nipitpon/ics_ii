package Resources.lib.Effects 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.SpriteSheetBook;
	import System.Interfaces.INegateAmbient;
	
	/**
	 * ...
	 * @author ss
	 */
	public class TileFireEffectSprite extends SpriteSheetBook implements INegateAmbient
	{
		[Embed(source = "../../../../lib/Effects/Tile Fire.png")]
		private static const idle:Class;
		[Embed(source = "../../../../lib/Effects/Tile Fire Out.png")]
		private static const end:Class;
		public function TileFireEffectSprite() 
		{
			var bmps:Vector.<Bitmap> = Vector.<Bitmap>([
				new idle(), new end()
			]);
			for each(var bmp:Bitmap in bmps)
			{
				bmp.scaleX = 2;
				bmp.scaleY = 2;
				bmp.smoothing = false;
			}
			super(bmps, 64, 64, null);
			Interval = 1;
			RegisterAction(0, Vector.<int>([0, 1, 2, 3]));
			RegisterAction(1, Vector.<int>([4, 5, 6, 7]));
			defaultAnimation = 0;
		}
		
	}

}