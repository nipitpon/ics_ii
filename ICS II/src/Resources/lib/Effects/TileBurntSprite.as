package Resources.lib.Effects 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.SpriteSheetAnimation;
	
	/**
	 * ...
	 * @author ss
	 */
	public class TileBurntSprite extends SpriteSheetAnimation 
	{
		[Embed(source = "../../../../lib/Effects/Tile burning.png")]
		private static const map:Class;
		public function TileBurntSprite() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			super(bmp, 32, 32, null);
			Interval = 1;
			RegisterAction(0, Vector.<int>([0, 1, 2, 3, 4]));
			defaultAnimation = 0;
		}
		
	}

}