package Resources.lib.Effects 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class AshSprite extends Image 
	{
		
		public function AshSprite(size:int) 
		{
			super();
			var bmp:Bitmap = new Bitmap(new BitmapData(size, size, false, 0xff370C1F));
			addChild(bmp);
		}
		
	}

}