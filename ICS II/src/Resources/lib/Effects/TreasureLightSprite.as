package Resources.lib.Effects 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author ss
	 */
	public class TreasureLightSprite extends Image 
	{
		[Embed(source = "../../../../lib/Effects/Treasure Light.png")]
		private static const map:Class;
		public function TreasureLightSprite() 
		{
			super(new Point(33, 33));
			var bmp:Bitmap = new map();
			addChild(bmp);
			bmp.scaleX = 2;
			bmp.scaleY = 2;
		}
		
	}

}