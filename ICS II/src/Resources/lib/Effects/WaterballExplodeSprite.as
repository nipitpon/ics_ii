package Resources.lib.Effects 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.SpriteSheetAnimation;
	
	/**
	 * ...
	 * @author ss
	 */
	public class WaterballExplodeSprite extends SpriteSheetAnimation 
	{
		[Embed(source = "../../../../lib/Effects/Waterball wall hit.png")]
		private static const map:Class;
		
		public function WaterballExplodeSprite() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			super(bmp, 32, 32, new Point(16, 16));
			Interval = 1;
			RegisterAction(0, Vector.<int>([3, 2, 1, 0]));
			defaultAnimation = 0;
		}
		
	}

}