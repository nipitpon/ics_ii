package Resources.lib.Effects 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Flipbook;
	import Resources.SpriteSheetAnimation;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FireParticleSprite extends SpriteSheetAnimation // Flipbook 
	{
		[Embed(source = "../../../../lib/Effects/Fire Particles.png")]
		private static const map:Class;
		public function FireParticleSprite() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			
			super(bmp, 32, 32, new Point(16, 16));
			Interval = 1;
			RegisterAction(0, Vector.<int>([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]));
			defaultAnimation = 0;
		}
		
	}

}