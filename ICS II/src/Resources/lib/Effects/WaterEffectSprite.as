package Resources.lib.Effects 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Flipbook;
	import Resources.SpriteSheetAnimation;
	import Resources.SpriteSheetBook;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class WaterEffectSprite extends SpriteSheetBook 
	{
		[Embed(source = "../../../../lib/Effects/Water Particles.png")]
		private static const map:Class;
		[Embed(source = "../../../../lib/Effects/Water Floor animation.png")]
		private static const floor:Class;
		public function WaterEffectSprite() 
		{
			var bmps:Vector.<Bitmap> = Vector.<Bitmap>([new map(), new floor()]);
			for each(var bmp:Bitmap in bmps)
			{
				bmp.scaleX = 2;
				bmp.scaleY = 2;
			}
			super(bmps, 32, 32, new Point(16, 16));
			Interval = 1;
			RegisterAction(0, Vector.<int>([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]));
			RegisterAction(1, Vector.<int>([15, 16, 17, 18, 19, 20]));
			defaultAnimation = 0;
		}
		
	}

}