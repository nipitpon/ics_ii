package Resources.lib.Effects 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.SpriteSheetAnimation;
	
	/**
	 * ...
	 * @author ss
	 */
	public class CannonBallExplosionSprite extends SpriteSheetAnimation 
	{
		[Embed(source = "../../../../lib/Effects/Explosion.png")]
		private static const map:Class;
		public function CannonBallExplosionSprite() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			super(bmp, 64, 64, new Point(32, 32));
			Interval = 1;
			RegisterAction(0, Vector.<int>([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]));
			defaultAnimation = 0;
		}
		
	}

}