package Resources.lib.Effects 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Flipbook;
	import Resources.SpriteSheetAnimation;
	
	/**
	 * ...
	 * @author ...
	 */
	public class JumpingDustSprite extends SpriteSheetAnimation 
	{
		[Embed(source = "../../../../lib/Effects/Jumping.png")]
		private static const map:Class;
		
		public function JumpingDustSprite() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			super(bmp, 64, 64, new Point(32, 32));
			Interval = 0;
			RegisterAction(0, Vector.<int>([0, 1, 2, 3, 4]));
			defaultAnimation = 0;
		}
		
	}

}