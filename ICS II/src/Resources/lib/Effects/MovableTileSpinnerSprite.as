package Resources.lib.Effects 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.SpriteSheetAnimation;
	
	/**
	 * ...
	 * @author ss
	 */
	public class MovableTileSpinnerSprite extends SpriteSheetAnimation 
	{
		[Embed(source = "../../../../lib/Effects/Moving Platform Weel.png")]
		private static const map:Class;
		public function MovableTileSpinnerSprite() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			super(bmp, 32, 32, new Point(16, 16));
			Interval = 1;
			RegisterAction(0, Vector.<int>([0]));
			RegisterAction(1, Vector.<int>([0, 1, 2, 3, 4, 5, 6, 7]));
			defaultAnimation = 0;
		}
		
	}

}