package Resources.lib.Effects 
{
	import flash.display.Bitmap;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import Resources.Flipbook;
	import Resources.SpriteSheetBook;
	import System.Interfaces.INegateAmbient;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FireSprite extends SpriteSheetBook implements INegateAmbient
	{
		[Embed(source = "../../../../lib/Effects/Fire.png")]
		private static const map1:Class;
		[Embed(source = "../../../../lib/Effects/Fire Out.png")]
		private static const map2:Class;
		
		
		public function FireSprite() 
		{
			var bmps:Vector.<Bitmap> = Vector.<Bitmap>([
				new map1(), new map2()
			]);
			for each(var bmp:Bitmap in bmps)
			{
				bmp.scaleX = 2;
				bmp.scaleY = 2;
				bmp.smoothing = false;
			}
			super(bmps, 128, 128, null);
			Interval = 1;
			RegisterAction(0, Vector.<int>([0, 1, 2, 3, 4, 5]));
			RegisterAction(1, Vector.<int>([6, 7, 8, 9, 10, 11]));
			defaultAnimation = 0;
		}
		
	}

}