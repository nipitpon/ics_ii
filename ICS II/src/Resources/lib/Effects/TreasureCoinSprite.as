package Resources.lib.Effects 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author ss
	 */
	public class TreasureCoinSprite extends Image 
	{
		[Embed(source = "../../../../lib/Effects/Treasure Coin.png")]
		private static const map:Class;
		public function TreasureCoinSprite() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
		}
		
	}

}