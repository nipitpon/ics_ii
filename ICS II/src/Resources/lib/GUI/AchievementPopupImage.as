package Resources.lib.GUI 
{
	import _Base.MyTextField;
	import fl.transitions.easing.None;
	import fl.transitions.easing.Regular;
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import Resources.Image;
	import Resources.lib.Environment.TreasureSprite;
	import Resources.lib.Other_pages.AchievementsIcons;
	import System.Interfaces.IUpdatable;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class AchievementPopupImage extends Image
	{
		public static const FadedOut:String = "FadedOut";
		private static const MaxTransitionCount:int = Utility.SecondToFrame(0.4);
		private static const MaxCount:int = Utility.SecondToFrame(3);
		[Embed(source="../../../../lib/GUI/Achievement Banner.png")]
		private static const map:Class;
		private var icon:AchievementsIcons;
		private var items:Vector.<RenderableObject>;
		private var count:int;
		private var fade_in_count:int;
		private var fade_out_count:int;
		private var text:MyTextField;
		public function AchievementPopupImage() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
			icon = new AchievementsIcons();
			icon.Left = 40;
			icon.Top = 2;			
			items = new Vector.<RenderableObject>();
			for (var i:int = 1; i <= 50; i++)
			{
				var ren:RenderableObject = new TreasureSprite(Math.min(i, 31), false);
				ren.Center = new Point(54, 54);
				items.push(ren);
			}
			text = new MyTextField();
			text.defaultTextFormat = new TextFormat("Pixel Font", 30, 0xffffff, false, false, false, null, null, TextFormatAlign.CENTER);
			text.width = 176;
			text.height = 50;
			text.x = 90;
			text.y = 2;
			text.wordWrap = true;
			addChild(text);
			count = -1;
			fade_in_count = -1;
			fade_out_count = -1;
			CenterX = 360;
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			Top = 540;
		}
		public function get IsActive():Boolean { return count >= 0 || fade_in_count >= 0 || fade_out_count >= 0; }
		public function Hide():void
		{
			fade_out_count = MaxTransitionCount;
		}
		public function Show():void
		{
			fade_in_count = MaxTransitionCount;
		}
		public function ShowAchievement(id:int):void
		{
			text.text = "ACHIEVEMENT UNLOCKED!";
			ClearIcons();
			icon.SetAnimation(id);
			addChild(icon);
			Show();
		}
		public function ShowSecretItem(level_minus_one:int):void
		{
			text.text = "SECRET ITEM FOUND!";
			ClearIcons();
			addChild(items[level_minus_one]);
			Show();
		}
		private function ClearIcons():void
		{
			if (contains(icon))
				removeChild(icon);
			for each(var ren:RenderableObject in items)
			{
				if (contains(ren))
					removeChild(ren);
			}
		}
		private function onAddedToStage(e:Event):void
		{
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		private function onRemovedFromStage(e:Event):void
		{
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		private function onEnterFrame(e:Event):void
		{
			if (fade_in_count >= 0)
			{
				Top = Regular.easeOut(MaxTransitionCount - fade_in_count, 540, -54, MaxTransitionCount);
				fade_in_count--;
				if (fade_in_count < 0)
					count = MaxCount;
			}
			if (count >= 0)
			{
				count--;
				if (count < 0)
					Hide();
			}
			if (fade_out_count >= 0)
			{
				Top = Regular.easeIn(MaxTransitionCount - fade_out_count, 496, 54, MaxTransitionCount);
				fade_out_count--;
				if (fade_out_count < 0)
					dispatchEvent(new Event(FadedOut));
			}
		}
	}

}