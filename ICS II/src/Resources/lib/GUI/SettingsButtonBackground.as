package Resources.lib.GUI 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SettingsButtonBackground extends Image 
	{
		[Embed(source = "../../../../lib/GUI/Settings Button.png")]
		private static const map:Class;
		public function SettingsButtonBackground() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			bmp.smoothing = false;
			addChild(bmp);
		}
		
	}

}