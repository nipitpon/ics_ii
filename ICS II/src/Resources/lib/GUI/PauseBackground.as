package Resources.lib.GUI 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class PauseBackground extends Image 
	{
		[Embed(source="../../../../lib/Other pages/Pause Menu/Paused BG.png")]
		private static const map:Class;
		public function PauseBackground() 
		{
			super();
			var bmp:Bitmap;
			addChild(bmp = new map());
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			bmp.smoothing = false;
			//x = 255;//165 -- +90
			//y = 144;//91 -- +53
		}
		
	}

}