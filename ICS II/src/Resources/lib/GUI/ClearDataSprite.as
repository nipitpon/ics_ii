package Resources.lib.GUI 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	import Resources.SpriteSheetAnimation;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ClearDataSprite extends SpriteSheetAnimation 
	{
		[Embed(source="../../../../lib/Other pages/Erase Data ICON animation.png")]
		private static const map:Class;
		public function ClearDataSprite() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			super(bmp, 24, 24, new Point(12, 12));
			Interval = 1;
			RegisterAction(0, Vector.<int>([0]));
			RegisterAction(1, Vector.<int>([0, 1, 2, 3, 4, 5, 6, 7, 8]));
			defaultAnimation = 0;
		}
		
	}

}