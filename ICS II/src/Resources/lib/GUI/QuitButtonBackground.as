package Resources.lib.GUI 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	import Resources.SpriteSheetAnimation;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class QuitButtonBackground extends SpriteSheetAnimation 
	{
		[Embed(source="../../../../lib/Other pages/Pause Menu/Exit ICON animation 12x12-strip.png")]
		private static const map:Class;
		public function QuitButtonBackground() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			super(bmp, 24, 24, null);
			Interval = 1;
			RegisterAction(0, Vector.<int>([5]));
			RegisterAction(1, Vector.<int>([0, 1, 2, 3, 4, 5]));
			defaultAnimation = 0;
		}
	}
}