package Resources.lib.GUI 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class BurningScreenImage extends Image 
	{
		[Embed(source = "../../../../lib/GUI/Screen burn.png")]
		private static const map:Class;
		public function BurningScreenImage() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
		}
		
	}

}