package Resources.lib.GUI 
{
	import flash.display.Bitmap;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class MenuButtonImage extends Image 
	{
		[Embed(source="../../../../lib/Pause button.png")]
		private static const map:Class;
		public function MenuButtonImage() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
		}
		
	}

}