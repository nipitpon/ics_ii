package Resources.lib.GUI 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Flipbook;
	
	/**
	 * ...
	 * @author ...
	 */
	public class MoreGameButtonSprite extends Flipbook 
	{
		[Embed(source = "../../../../lib/GUI/More Games Icon.png")]
		private static const icon:Class;
		[Embed(source = "../../../../lib/GUI/More Games Icon and Text.png")]
		private static const text:Class;
		public function MoreGameButtonSprite() 
		{
			var bmps:Vector.<Bitmap> = Vector.<Bitmap>([new icon(), new text()]);
			for each(var bmp:Bitmap in bmps)
			{
				bmp.scaleX = 2;
				bmp.scaleY = 2;
			}
			super(bmps);
			Interval = 1;
			RegisterAction(0, Vector.<int>([0]));
			RegisterAction(1, Vector.<int>([1]));
			defaultAnimation = 0;
		}
		
	}

}