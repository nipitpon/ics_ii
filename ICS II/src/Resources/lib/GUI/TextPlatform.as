package Resources.lib.GUI 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	import Resources.Image;
	import Resources.Resource;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class TextPlatform extends Image 
	{
		[Embed(source = "../../../../lib/GUI/Text Platform.png")]
		private static const map:Class;
		private var cache:Dictionary;
		private var current_text:RenderableObject;
		public function TextPlatform(text:String) 
		{
			super();
			cache = new Dictionary();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
			addChild(current_text = Resource.GetPlatformText(text));
			cache[text] = current_text;
		}
		public function SetText(text:String):void
		{
			removeChild(current_text);
			if (cache[text] == null)
			{
				cache[text] = Resource.GetPlatformText(text);
			}
			current_text = cache[text];
			addChild(current_text);
		}
	}

}