package Resources.lib.GUI 
{
	import flash.display.Bitmap;
	import Resources.Image;
	/**
	 * ...
	 * @author Knight
	 */
	public class PauseButtonBackground extends Image
	{
		[Embed(source = "../../../../lib/GUI/Temperature Bar.png")]
		private static const map:Class;
		public function PauseButtonBackground() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			bmp.smoothing = false;
			addChild(bmp);
		}
		
	}

}