package Resources.lib.GUI 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ConfirmBackground extends Image 
	{
		[Embed(source = "../../../../lib/GUI/ConfirmBackground.png")]
		private static const map:Class;
		public function ConfirmBackground() 
		{
			super();
			addChild(new map());
			x = 290;
			y = 270;
		}
		
	}

}