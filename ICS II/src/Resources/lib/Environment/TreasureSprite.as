package Resources.lib.Environment 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class TreasureSprite extends Image 
	{
		[Embed(source = "../../../../lib/Secret Items/Ancient Helmet.png")]
		private static const map1:Class;
		[Embed(source = "../../../../lib/Secret Items/Ancient Vase.png")]     
		private static const map2:Class;
		[Embed(source = "../../../../lib/Secret Items/Bag of gold.png")]      
		private static const map3:Class;
		[Embed(source = "../../../../lib/Secret Items/Balloons.png")]         
		private static const map4:Class;
		[Embed(source = "../../../../lib/Secret Items/Banana.png")]           
		private static const map5:Class;
		[Embed(source = "../../../../lib/Secret Items/Black Card.png")]       
		private static const map6:Class;
		[Embed(source = "../../../../lib/Secret Items/Credit Card.png")]      
		private static const map7:Class;
		[Embed(source = "../../../../lib/Secret Items/Crown.png")]            
		private static const map8:Class;
		[Embed(source = "../../../../lib/Secret Items/Emerald.png")]          
		private static const map9:Class;
		[Embed(source = "../../../../lib/Secret Items/Flag.png")]             
		private static const map10:Class;
		[Embed(source = "../../../../lib/Secret Items/Golden Plate.png")]     
		private static const map11:Class;
		[Embed(source = "../../../../lib/Secret Items/Hammer.png")]           
		private static const map12:Class;
		[Embed(source = "../../../../lib/Secret Items/Hourglass.png")]        
		private static const map13:Class;
		[Embed(source = "../../../../lib/Secret Items/Huge ass tea cup.png")] 
		private static const map14:Class;
		[Embed(source = "../../../../lib/Secret Items/Huge Egg.png")]         
		private static const map15:Class;
		[Embed(source = "../../../../lib/Secret Items/Machete.png")]          
		private static const map16:Class;
		[Embed(source = "../../../../lib/Secret Items/Missile.png")]          
		private static const map17:Class;
		[Embed(source = "../../../../lib/Secret Items/Musket.png")]           
		private static const map18:Class;
		[Embed(source = "../../../../lib/Secret Items/Pineapple.png")]        
		private static const map19:Class;
		[Embed(source = "../../../../lib/Secret Items/Portrait.png")]         
		private static const map20:Class;
		[Embed(source = "../../../../lib/Secret Items/Present.png")]          
		private static const map21:Class;
		[Embed(source = "../../../../lib/Secret Items/Red Card.png")]         
		private static const map22:Class;
		[Embed(source = "../../../../lib/Secret Items/Ruby.png")]             
		private static const map23:Class;
		[Embed(source = "../../../../lib/Secret Items/Rum.png")]              
		private static const map24:Class;
		[Embed(source = "../../../../lib/Secret Items/Scroll.png")]           
		private static const map25:Class;
		[Embed(source = "../../../../lib/Secret Items/Shell.png")]            
		private static const map26:Class;
		[Embed(source = "../../../../lib/Secret Items/Sickle.png")]           
		private static const map27:Class;
		[Embed(source = "../../../../lib/Secret Items/Starfish.png")]         
		private static const map28:Class;
		[Embed(source = "../../../../lib/Secret Items/Statue.png")]           
		private static const map29:Class;
		[Embed(source = "../../../../lib/Secret Items/Teddy Bear.png")]       
		private static const map30:Class;
		[Embed(source = "../../../../lib/Secret Items/Secret fossil.png")]
		private static const map31:Class;
		private static const map_array:Vector.<Class> = Vector.<Class>([
			map1, map2, map3, map4, map5, map6, map8, map9, map11,
			map10, map12, map13, map14, map15, map16, map17, map19,
			map21, map22, map23, map24, map26, map25, map27, map28, map29, map30, map20, map18, map7, map31]);
		private static const rotation_info:Object = 
		{
			7: 30,
			8: 15,
			11: 45,
			13: 45,
			19: 60,
			23: -12,
			24: 25,
			28: 5,
			30: -20
		};
		public function TreasureSprite(level:int, rotate:Boolean) 
		{
			super();
			if (level < map_array.length)
			{
				var bmp:Bitmap = new map_array[level - 1]();
				bmp.scaleX = 2;
				bmp.scaleY = 2;
				if (rotation_info[level] != null && rotate)
				{
					bmp.rotation = rotation_info[level];
				}
				addChild(bmp);
			}
		}
		
	}

}