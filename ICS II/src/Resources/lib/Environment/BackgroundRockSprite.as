package Resources.lib.Environment 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author ss
	 */
	public class BackgroundRockSprite extends Image 
	{
		[Embed(source = "../../../../lib/Environment/Rocks.png")]
		private static const map:Class;
		public function BackgroundRockSprite() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
		}
		
	}

}