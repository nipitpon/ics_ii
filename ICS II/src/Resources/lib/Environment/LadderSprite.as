package Resources.lib.Environment 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class LadderSprite extends Image 
	{
		[Embed(source = "../../../../lib/Environment/Ladder.png")]
		private static const map:Class;
		public function LadderSprite(height:int) 
		{
			super();
			for (var i:int = 0; i < height; i++)
			{
				var bmp:Bitmap = new map();
				bmp.scaleX = Tile.Size.x / bmp.width;
				bmp.scaleY = bmp.scaleX;
				bmp.y = Tile.Size.y * i;
				addChild(bmp);
			}
		}
		
	}

}