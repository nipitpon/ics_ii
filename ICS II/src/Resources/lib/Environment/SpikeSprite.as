package Resources.lib.Environment 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SpikeSprite extends Image 
	{
		[Embed(source = "../../../../lib/Environment/Spikes.png")]
		private static const map:Class;
		public function SpikeSprite() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = Tile.Size.x / bmp.width;
			bmp.scaleY = bmp.scaleX;
			bmp.smoothing = false;
			addChild(bmp);
		}
		
	}

}