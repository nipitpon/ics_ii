package Resources.lib.Environment 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	import System.Interfaces.INegateAmbient;
	
	/**
	 * ...
	 * @author ss
	 */
	public class StarSprite extends Image implements INegateAmbient
	{
		[Embed(source = "../../../../lib/Environment/Stars.png")]
		private static const map:Class;
		public function StarSprite() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
		}
		
	}

}