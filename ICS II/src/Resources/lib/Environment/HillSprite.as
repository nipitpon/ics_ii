package Resources.lib.Environment 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class HillSprite extends Image 
	{
		[Embed(source="../../../../lib/Environment/Clouds and Rocks.png")]
		private static const map:Class;
		public function HillSprite() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
		}
	}
}