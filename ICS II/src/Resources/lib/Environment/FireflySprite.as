package Resources.lib.Environment 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	import System.Interfaces.INegateAmbient;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FireflySprite extends Image implements INegateAmbient
	{
		[Embed(source = "../../../../lib/Environment/FireflyWTransparent.png")]
		private static const map:Class;
		public function FireflySprite() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			super(new Point(15, 15));
			addChild(bmp);
		}
		
	}

}