package Resources.lib.Environment 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Vine1 extends Image 
	{
		[Embed(source = "../../../../lib/Garden/Vine 5.png")]
		private static const map:Class;
		public function Vine1() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			bmp.smoothing  = false;
			addChild(bmp);
		}
		
	}

}