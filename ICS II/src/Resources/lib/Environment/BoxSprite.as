package Resources.lib.Environment 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class BoxSprite extends Image 
	{
		[Embed(source = "../../../../lib/Environment/Pushable Box 1.png")]
		private static const map:Class;
		public function BoxSprite() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = Tile.Size.x / bmp.width;
			bmp.scaleY = bmp.scaleX;
			addChild(bmp);
		}
		
	}

}