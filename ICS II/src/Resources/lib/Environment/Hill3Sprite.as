package Resources.lib.Environment 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Hill3Sprite extends Image 
	{
		[Embed(source = "../../../../lib/Environment/Hill 3.png")]
		private static const map:Class;
		public function Hill3Sprite() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
		}
		
	}

}