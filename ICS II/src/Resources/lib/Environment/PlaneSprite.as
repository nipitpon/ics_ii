package Resources.lib.Environment 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.SpriteSheetAnimation;
	
	/**
	 * ...
	 * @author ss
	 */
	public class PlaneSprite extends SpriteSheetAnimation 
	{
		[Embed(source = "../../../../lib/Environment/Plane.png")]
		private static const map:Class;
		public function PlaneSprite() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			super(bmp, 256, 96, new Point(128, 48));
			Interval = 1;
			RegisterAction(0, Vector.<int>([0]));
			RegisterAction(1, Vector.<int>([0, 1, 2, 3]), 5);
			RegisterAction(2, Vector.<int>([0, 1, 2, 3]));
			defaultAnimation = 0;
		}
		
	}

}