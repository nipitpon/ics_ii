package Resources.lib.Environment 
{
	/**
	 * ...
	 * @author Knight
	 */
	public final class DecorationSprite 
	{
		[Embed(source = "../../../../lib/Garden/Bones 1.png")]
		public static const Bone1:Class;
		[Embed(source = "../../../../lib/Garden/Bones 2.png")]
		public static const Bone2:Class;
		[Embed(source = "../../../../lib/Garden/Bones 3.png")]
		public static const Bone3:Class;
		[Embed(source = "../../../../lib/Garden/Bones 4.png")]
		public static const Bone4:Class;
		[Embed(source = "../../../../lib/Garden/Bones 5.png")]
		public static const Bone5:Class;
		[Embed(source = "../../../../lib/Garden/Bones 6.png")]
		public static const Bone6:Class;
		[Embed(source = "../../../../lib/Garden/Bush.png")]
		public static const Bush:Class;
		[Embed(source = "../../../../lib/Garden/Cactus 1.png")]
		public static const Cactus1:Class;
		[Embed(source = "../../../../lib/Garden/Cactus 2.png")]
		public static const Cactus2:Class;
		[Embed(source = "../../../../lib/Garden/Cactus 3.png")]
		public static const Cactus3:Class;
		[Embed(source = "../../../../lib/Garden/Flower 1.png")]
		public static const Flower1:Class;
		[Embed(source = "../../../../lib/Garden/Flower 2.png")]
		public static const Flower2:Class;
		[Embed(source = "../../../../lib/Garden/Flower 3.png")]
		public static const Flower3:Class;
		[Embed(source = "../../../../lib/Garden/Flower 4.png")]
		public static const Flower4:Class;
		[Embed(source = "../../../../lib/Garden/Flower 5.png")]
		public static const Flower5:Class;
		[Embed(source = "../../../../lib/Garden/Flower 6.png")]
		public static const Flower6:Class;
		[Embed(source = "../../../../lib/Garden/Flower 7.png")]
		public static const Flower7:Class;
		[Embed(source = "../../../../lib/Garden/Flower 8.png")]
		public static const Flower8:Class;
		[Embed(source = "../../../../lib/Garden/Flower 9.png")]
		public static const Flower9:Class;
		[Embed(source = "../../../../lib/Garden/Grass 1.png")]
		public static const Grass1:Class;
		[Embed(source = "../../../../lib/Garden/Grass 2.png")]
		public static const Grass2:Class;
		[Embed(source = "../../../../lib/Garden/Grass 3.png")]
		public static const Grass3:Class;
		[Embed(source = "../../../../lib/Garden/Mushroom 1.png")]
		public static const Mushroom1:Class;
		[Embed(source = "../../../../lib/Garden/Mushroom 2.png")]
		public static const Mushroom2:Class;
		[Embed(source = "../../../../lib/Garden/Mushroom 3.png")]
		public static const Mushroom3:Class;
		[Embed(source = "../../../../lib/Garden/Mushroom 4.png")]
		public static const Mushroom4:Class;
		[Embed(source = "../../../../lib/Garden/Mushroom 5.png")]
		public static const Mushroom5:Class;
		[Embed(source = "../../../../lib/Garden/NotAFlower  1.png")]
		public static const NotFlower1:Class;
		[Embed(source = "../../../../lib/Garden/NotAFlower  2.png")]
		public static const NotFlower2:Class;
		[Embed(source = "../../../../lib/Garden/Palm Tree.png")]
		public static const PalmTree:Class;
		[Embed(source = "../../../../lib/Garden/Plant 1.png")]
		public static const Plant1:Class;
		[Embed(source = "../../../../lib/Garden/Plant 2.png")]
		public static const Plant2:Class;
		[Embed(source = "../../../../lib/Garden/Plant 3.png")]
		public static const Plant3:Class;
		[Embed(source = "../../../../lib/Garden/Plant 4.png")]
		public static const Plant4:Class;
		[Embed(source = "../../../../lib/Garden/Plant 5.png")]
		public static const Plant5:Class;
		[Embed(source = "../../../../lib/Garden/Plant 6.png")]
		public static const Plant6:Class;
		[Embed(source = "../../../../lib/Garden/Rock 1.png")]
		public static const Rock1:Class;
		[Embed(source = "../../../../lib/Garden/Rock 2.png")]
		public static const Rock2:Class;
		[Embed(source = "../../../../lib/Garden/Rock 3.png")]
		public static const Rock3:Class;
		[Embed(source = "../../../../lib/Garden/Rock 4.png")]
		public static const Rock4:Class;
		[Embed(source = "../../../../lib/Garden/Rock.png")]
		public static const Rock:Class;
		[Embed(source = "../../../../lib/Garden/Small cactus 1.png")]
		public static const SmallCactus1:Class;
		[Embed(source = "../../../../lib/Garden/Small cactus 2.png")]
		public static const SmallCactus2:Class;
		[Embed(source = "../../../../lib/Garden/Small cactus 3.png")]
		public static const SmallCactus3:Class;
		[Embed(source = "../../../../lib/Garden/Shit.png")]
		public static const Shit:Class;
		[Embed(source = "../../../../lib/Garden/Tree.png")]
		public static const Tree:Class;
		[Embed(source = "../../../../lib/Garden/Vase 1.png")]
		public static const Vase1:Class;
		[Embed(source = "../../../../lib/Garden/Vase 2.png")]
		public static const Vase2:Class;
		[Embed(source = "../../../../lib/Garden/Vase 3.png")]
		public static const Vase3:Class;
		[Embed(source = "../../../../lib/Garden/Vase 4.png")]
		public static const Vase4:Class;
		[Embed(source = "../../../../lib/Garden/Vase 5.png")]
		public static const Vase5:Class;
		[Embed(source = "../../../../lib/Garden/Background Grass 1.png")]
		public static const BackgroundGrass1:Class;
		[Embed(source = "../../../../lib/Garden/Background Grass 2.png")]
		public static const BackgroundGrass2:Class;
		[Embed(source = "../../../../lib/Garden/Front Grass 1.png")]
		public static const FrontGrass1:Class;
		[Embed(source = "../../../../lib/Garden/Front Grass 2.png")]
		public static const FrontGrass2:Class;
		//[Embed(source = "../../../../lib/Garden/Weed_Back.png")]
		public static const WeedBack:Class = BackgroundGrass1;
		//[Embed(source = "../../../../lib/Garden/Weed_Front.png")]
		public static const WeedFront:Class = FrontGrass1;
		[Embed(source = "../../../../lib/Garden/Vine 1.png")]
		public static const Vine1:Class;
		[Embed(source = "../../../../lib/Garden/Vine 2.png")] 
		public static const Vine2:Class;
		[Embed(source = "../../../../lib/Garden/Vine 3.png")] 
		public static const Vine3:Class;
		[Embed(source = "../../../../lib/Garden/Vine 4.png")] 
		public static const Vine4:Class;
		[Embed(source = "../../../../lib/Garden/Vine 5.png")] 
		public static const Vine5:Class;
		[Embed(source = "../../../../lib/Garden/Vine 6.png")] 
		public static const Vine6:Class;
		[Embed(source = "../../../../lib/Garden/Vine 7.png")] 
		public static const Vine7:Class;
		[Embed(source = "../../../../lib/Garden/Vine 8.png")] 
		public static const Vine8:Class;
		[Embed(source = "../../../../lib/Garden/Vine 9.png")] 
		public static const Vine9:Class;
		[Embed(source = "../../../../lib/Garden/Vine 10.png")]
		public static const Vine10:Class;
		[Embed(source = "../../../../lib/Garden/Vine 11.png")]
		public static const Vine11:Class;
		
		public function DecorationSprite() 
		{
			
		}
		
	}

}