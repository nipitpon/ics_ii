package Resources.lib.Environment.Weather 
{
	import flash.display.Bitmap;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import Resources.Image;
	import System.GameFrame;
	import System.GameSystem;
	import System.Time;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class RainSprite extends Image 
	{
		[Embed(source = "../../../../../lib/Environment/Weather/Rain.png")]
		private static const map:Class;
		private var rains:Vector.<Bitmap>;
		public function RainSprite()
		{
			super();
			rains = new Vector.<Bitmap>();
			for (var i:int = 0; i < 20; i++)
			{
				var bmp:Bitmap = new map();
				rains.push(bmp);
				addChild(bmp);
			}
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		private function onAddedToStage(e:Event):void
		{
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
			for (var i:int = 0; i < rains.length; i++)
			{
				rains[i].y = Utility.Random( -100, -600);
			}
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		}
		private function onEnterFrame(e:Event):void
		{
			if (GameSystem.IsFreezing || GameSystem.IsCompletelyIndoor) return;
			
			for (var i:int = 0; i < rains.length; i++)
			{
				if (Time.IsAdvancing)
					rains[i].y += 90;
				else
					rains[i].y += 30;
				if (rains[i].y > GameFrame.ScreenBottom)
				{
					rains[i].y = GameFrame.ScreenTop - Utility.Random(rains[i].height, 600);
					rains[i].x = Utility.Random(GameFrame.ScreenLeft, GameFrame.ScreenRight);
				}
				else
				{
					if (rains[i].x < GameFrame.ScreenLeft - 20)
						rains[i].x = Utility.Random(GameFrame.ScreenRight, GameFrame.ScreenRight + 20);
					else if (rains[i].x > GameFrame.ScreenRight + 20)
						rains[i].x = Utility.Random(GameFrame.ScreenLeft - 20, GameFrame.ScreenLeft);
				}
			}
		}
		private function onRemovedFromStage(e:Event):void
		{
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		}
		//[Embed(source = "../../../../../lib/Environment/Weather/RainSprite.png")]
		//private static const map:Class;
		//private var rains:Vector.<Bitmap>;
		//private var update_count:int;
		//public function RainSprite() 
		//{
		//	super();
		//	rains = new Vector.<Bitmap>(3);
		//	for (var i:int = 0; i < 3; i++)
		//	{
		//		var r:Bitmap = new map();
		//		rains[i] = r;
		//		rains[i].x = -600;
		//		addChild(r);
		//	}
		//	rains[0].y = -rains[0].height;
		//	rains[1].y = 0;
		//	rains[2].y = rains[1].height;
		//	
		//	addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		//	addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		//}
		//
		//private function onAddedToStage(e:Event):void
		//{
		//	addEventListener(Event.ENTER_FRAME, onEnterFrame);
		//	update_count = 0;
		//}
		//private function onRemovedFromStage(e:Event):void
		//{
		//	removeEventListener(Event.ENTER_FRAME, onEnterFrame);
		//}
		//private function onEnterFrame(e:Event):void
		//{
		//	if (GameSystem.IsFreezing || (++update_count < 2 && !Time.IsAdvancing) || GameSystem.IsCompletelyIndoor) return;
		//	
		//	update_count = 0;
		//	for each(var r:Bitmap in rains)
		//	{
		//		if(!Time.IsAdvancing)
		//			r.y += 40;
		//		else
		//			r.y += 100;
		//	}
		//	
		//	var offset:Point = parent.localToGlobal(new Point());
		//	var offsetY:Number = offset.y;
		//	var offsetX:Number = offset.x;
		//	if (rains[0].y + rains[0].height + offsetY + 200 < 0)
		//	{
		//		rains[0].y = rains[2].y + rains[2].height;
		//		rains[0].x = -offsetX - 600;
		//		var temp:Bitmap = rains[0];
		//		rains[0] = rains[1];
		//		rains[1] = rains[2];
		//		rains[2] = temp;
		//	}
		//	else if (rains[2].y + offsetY > stage.stageHeight + 200)
		//	{
		//		rains[2].y = rains[0].y - rains[2].height;
		//		rains[2].x = -offsetX - 600;
		//		var t:Bitmap = rains[2];
		//		rains[2] = rains[1];
		//		rains[1] = rains[0];
		//		rains[0] = t;
		//	}
		//}
	}

}