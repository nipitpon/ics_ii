package Resources 
{
	import _Base.MySprite;
	import flash.display.InteractiveObject;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import flash.utils.Dictionary;
	import Resources.lib.Effects.WaterSprite;
	import System.GameSystem;
	import System.Time;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Animation extends RenderableObject 
	{
		public static const FinishAnimation:String = "FinishAnimation";
		
		private var currentAction:ActionSet;
		private var action:Vector.<ActionSet>;
		private var freezing:Boolean;
		private var interval:int;
		private var intervalCount:int;
		protected var defaultAnimation:int;
		private var defaultInterval:int;
		
		public function Animation(scaleCenter:Point = null) 
		{
			super(scaleCenter);
			currentAction = new ActionSet(0, Vector.<int>([0]), 1);
			action = new Vector.<ActionSet>();
			defaultInterval = 1;
			interval = 1;
			intervalCount = 0;
			freezing = false;
		}
		public function SetAnimation(name:int):void
		{
			var toUse:ActionSet = null;
			for each(var ac:ActionSet in action)
			{
				if (ac.ID == name)
				{
					toUse = ac;
					break;
				}
			}
			
			if (toUse == null)
				throw new Error("Cannot find animation " + name);
				
			setCurrentAnimation(toUse);
		}
		public function ResetAnimation():void
		{
			currentAction.Reset();
		}
		public function SetCurrentAnimationPosition(position:int):void
		{
			currentAction.SetPosition(position);
		}
		public function get UpdateWhenAdvancingTime():Boolean { return true; }
		public function HasAnimation(id:int):Boolean
		{
			for each(var action:ActionSet in this.action)
			{
				if (action.ID == id) return true;
			}
			return false;
		}
		public override function Update():void
		{
			super.Update();
			if (freezing) return;
			
			if (intervalCount < interval)
			{
				if (Time.IsAdvancing)
					intervalCount += 3;
				else
					intervalCount++;
				return;
			}
			intervalCount = 0;
			AnimateOneFrame();
			if (Time.IsAdvancing && interval <= 1) for (var i:int = 0; i < 2; i++)
				AnimateOneFrame();
		}
		public function set Freeze(val:Boolean):void
		{
			freezing = val;
		}
		public function get DefaultAnimation():int { return defaultAnimation; }
		public function get Freeze():Boolean { return freezing; }
		public function get CurrentAnimation():int { return currentAction.ID; }
		public function set Interval(num:int):void { defaultInterval = num; }
		public function get Interval():int { return defaultInterval; }
		
		protected function AnimateOneFrame():void
		{
			currentAction.AnimateOneFrame();
		}
		protected function get AnimationImagePosition():uint { return currentAction.positions[currentAction.CurrentPosition]; }
		protected function get IsNewFrame():Boolean { return intervalCount == 0; }
		protected function AdjustActionSpeed(id:int, newSpeed:int):void
		{
			var act:ActionSet = null;
			for each(var a:ActionSet in action)
			{
				if (a.ID == id)
				{
					act = a;
					break;
				}
			}
			if (act != null)
				act.Interval = newSpeed;
		}
		protected function ChangeAction(id:int, arg:Vector.<int>, interval:int = -1):void
		{
			if (interval < 0) 
				interval = Interval;
			var toEdit:ActionSet = null;
			for each(var actionset:ActionSet in this.action)
			{
				if (actionset.ID == id)
				{
					toEdit = actionset;
					break;
				}
			}
			if (toEdit == null)
				throw new Error("Invalid ID " + id);
			
			toEdit.positions = arg;
			toEdit.SetPosition(0);
		}
		protected function RegisterAction(id:int, arg:Vector.<int>, interval:int = -1):uint
		{
			if (interval < 0) 
				interval = Interval;
			this.action.push(new ActionSet(id, arg, interval));
			if (this.action.length == 1)
				setCurrentAnimation(this.action[0]);
			return this.action.length - 1;
		}
		protected function setCurrentAnimation(action:ActionSet):void
		{
			//if (action == currentAction) return;
			
			if (currentAction != null)
			{
				currentAction.removeEventListener(Animation.FinishAnimation, onFinishAnimation);
				currentAction.removeEventListener(AnimationEvent.Progress, onFinishAnimation);
			}
			currentAction = action;
			
			currentAction.Reset();
			interval = currentAction.Interval;
			intervalCount = 0;
			currentAction.addEventListener(Animation.FinishAnimation, onFinishAnimation);
			currentAction.addEventListener(AnimationEvent.Progress, onFinishAnimation);
		}
		protected function get CurrentAction():ActionSet { return currentAction; }
		private function onFinishAnimation(e:Event):void
		{
			dispatchEvent(e);
		}
		
		private static var animation_list:Vector.<AnimationInfo> = new Vector.<AnimationInfo>();
		public static function Scale(obj:MySprite, begin:Number, end:Number, length:int, onFinish:Function):void
		{
			var inf:AnimationInfo = new AnimationInfo();
			inf.object = obj;
			inf.type = AnimationInfo.Scale;
			inf.count = 0;
			inf.length = length;
			inf.begin = begin;
			inf.end = end;
			animation_list.push(inf);
			obj.addEventListener(Event.ENTER_FRAME, onScale);
			obj.addEventListener(FinishAnimation, onFinish);
		}
		private static function onScale(e:Event):void
		{
			if (GameSystem.IsFreezing) return;
			var inf:AnimationInfo = null;
			var i:int;
			for (i = animation_list.length - 1; i >= 0 && inf == null; i--)
			{
				if (animation_list[i].object == e.currentTarget && animation_list[i].type == AnimationInfo.Scale)
					inf = animation_list[i];
			}
			
			if (inf == null)
				throw new Error("Cannot find animation for " + e.currentTarget);
				
			inf.count++;
			var interpo:Number = inf.count / inf.length;
			inf.object.scaleX = Utility.Interpolate(inf.end, inf.begin, interpo);
			inf.object.scaleY = inf.object.scaleX;
			if (inf.count == inf.length)
			{
				inf.object.dispatchEvent(new Event(FinishAnimation));
				inf.object.removeEventListener(Event.ENTER_FRAME, onScale);
				for (i = animation_list.length - 1; i >= 0; i--)
				{
					if (inf == animation_list[i])
					{
						animation_list.splice(i, 1);
						break;
					}
				}
			}
		}
	}

}