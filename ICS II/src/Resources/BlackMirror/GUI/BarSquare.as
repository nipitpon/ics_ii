package Resources.BlackMirror.GUI 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class BarSquare extends Image 
	{
		[Embed(source = "../../../../lib/Volume Button.png")]
		private static const map:Class;
		public function BarSquare() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			bmp.smoothing = false;
			addChild(bmp);
		}
		
	}

}