package Resources.BlackMirror.GUI 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class InventoryButtonImage extends Image 
	{
		[Embed(source = "../../../../lib/Inventory_button.png")]
		private static const map:Class;
		public function InventoryButtonImage() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 3;
			bmp.scaleY = 3;
			bmp.smoothing = false;
			addChild(bmp);
		}
		
	}

}