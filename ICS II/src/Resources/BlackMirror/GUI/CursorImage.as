package Resources.BlackMirror.GUI 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Flipbook;
	import System.GameFrame;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class CursorImage extends Flipbook 
	{
		[Embed(source = "../../../../lib/Cursor.png")]
		private static const idle:Class;
		
		public function CursorImage() 
		{
			var bmps:Vector.<Bitmap> = Vector.<Bitmap>([new idle()]);
			for each(var bmp:Bitmap in bmps)
			{
				bmp.scaleX = 2;
				bmp.scaleY = 2;
				bmp.smoothing = false;
			}
			super(bmps);
			Interval = 2;
			RegisterAction(0, Vector.<int>([0]));
			RegisterAction(1, Vector.<int>([0]));
			defaultAnimation = 0;
			//filters = [GameFrame.objectShadow];
		}
		
	}

}