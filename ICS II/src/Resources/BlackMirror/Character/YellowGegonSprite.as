package Resources.BlackMirror.Character 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.SpriteSheetAnimation;
	import Resources.SpriteSheetBook;
	import System.MC.Characters.Character;
	import System.MC.Characters.Monster;
	
	/**
	 * ...
	 * @author ss
	 */
	public class YellowGegonSprite extends SpriteSheetBook 
	{
		[Embed(source = "../../../../lib/Characters/Monsters/Yellow Gegon Walking.png")]
		private static const idle:Class;
		[Embed(source = "../../../../lib/Characters/Monsters/Gegon Burning alive.png")]
		private static const die:Class;
		[Embed(source = "../../../../lib/Characters/Monsters/Gegon Skeleton Collapse.png")]
		private static const die2:Class;
		public function YellowGegonSprite() 
		{
			var bmps:Vector.<Bitmap> = Vector.<Bitmap>([new idle(), new die(), new die2()]);
			for each(var bmp:Bitmap in bmps)
			{
				bmp.scaleX = 2;
				bmp.scaleY = 2;
			}
			super(bmps, 64, 64, new Point(32, 32));
			Interval = 2;
			RegisterAction(Character.Idle, Vector.<int>([0]));
			RegisterAction(Character.Move, Vector.<int>([0, 1, 2, 3]));
			RegisterAction(Monster.Die, Vector.<int>([4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]));
			defaultAnimation = Character.Idle;
		}
		
	}

}