package Resources.BlackMirror.Character 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.SpriteSheetAnimation;
	import System.MC.Characters.Character;
	import System.MC.Characters.Monster;
	
	/**
	 * ...
	 * @author ss
	 */
	public class BatySprite extends SpriteSheetAnimation 
	{
		[Embed(source = "../../../../lib/Characters/Monsters/Baty.png")]
		private static const map:Class;
		public function BatySprite() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			super(bmp, 64, 64, new Point(32, 32));
			Interval = 1;
			RegisterAction(Character.Idle, Vector.<int>([0]));
			RegisterAction(Character.Move, Vector.<int>([0, 1, 2, 3, 4, 5]));
			RegisterAction(Monster.Die, Vector.<int>([0, 1, 2, 3, 4, 5]));
			defaultAnimation = Character.Idle;
		}
		
	}

}