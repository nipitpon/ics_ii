package Resources.BlackMirror.Character 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Flipbook;
	import Resources.SpriteSheetAnimation;
	import Resources.SpriteSheetBook;
	import System.GameFrame;
	import System.MC.Characters.Character;
	import System.MC.Characters.Monster;
	import System.MC.Characters.Player;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class PlayerSprite extends SpriteSheetBook 
	{
		[Embed(source = "../../../../lib/Characters/Player/Player idle.png")]
		private static const idle:Class;
		[Embed(source = "../../../../lib/Characters/Player/Burning alive.png")]
		private static const die1:Class;
		[Embed(source = "../../../../lib/Characters/Player/Skeleton Collapse.png")]
		private static const die2:Class;
		[Embed(source = "../../../../lib/Characters/Player/Player running.png")]
		private static const move:Class;
		[Embed(source="../../../../lib/Characters/Player/Jumping.png")]
		private static const jump:Class;
		[Embed(source="../../../../lib/Characters/Player/Falling.png")]
		private static const fall:Class;
		[Embed(source="../../../../lib/Characters/Player/Pushing.png")]
		private static const pushing:Class;
		[Embed(source = "../../../../lib/Characters/Player/Climbing Up.png")]
		private static const climb_up:Class;
		[Embed(source = "../../../../lib/Characters/Player/Climbing Down.png")]
		private static const climb_down:Class;
		[Embed(source = "../../../../lib/Characters/Player/Dying.png")]
		private static const angle:Class;
		[Embed(source = "../../../../lib/Characters/Player/Climb Left.png")]
		private static const climb_left:Class;
		[Embed(source = "../../../../lib/Characters/Player/Climb Right.png")]
		private static const climb_right:Class;
		[Embed(source = "../../../../lib/Characters/Player/Midair.png")]
		private static const midair:Class;
		[Embed(source = "../../../../lib/Characters/Player/Crouch.png")]
		private static const crouch:Class;
		[Embed(source = "../../../../lib/Characters/Player/Sliding.png")]
		private static const slide:Class;
		[Embed(source = "../../../../lib/Characters/Player/Sleeping Animation.png")]
		private static const sleep:Class;
		
		private static const array:Vector.<Class> = Vector.<Class>([
			idle, 
			die1, die2, move,
			fall, jump, pushing, climb_up, angle, climb_left, climb_right,
			midair, crouch, climb_down, slide, sleep
		]);
		public function PlayerSprite() 
		{
			var bmps:Vector.<Bitmap> = new Vector.<Bitmap>();
			for each(var c:Class in array)
			{
				var bmp:Bitmap = new c();
				bmp.smoothing = false;
				bmp.scaleX = 2;
				bmp.scaleY = bmp.scaleX;
				bmps.push(bmp);
			}
			super(bmps, 64, 64, new Point(32, 64));
			Interval = 0;
			RegisterAction(Character.Idle, Vector.<int>([0, 1, 2, 3, 4, 5]), 2);
			RegisterAction(Character.Move, Vector.<int>([18, 19, 20, 21, 22, 23, 24, 25]), 1);
			RegisterAction(Player.Fall, Vector.<int>([26, 27, 28, 29]), 1);
			RegisterAction(Player.Jump, Vector.<int>([30, 31]), 1);
			RegisterAction(Monster.Die, Vector.<int>([6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 17]), 1);
			RegisterAction(Player.Box, Vector.<int>([32, 33, 34, 35, 36, 37]), 1);
			RegisterAction(Player.ClimbUpLadder, Vector.<int>([38, 39, 40, 41, 42, 43, 44, 45]), 1);
			RegisterAction(Player.DieAngle, Vector.<int>([46, 47, 48, 49, 50, 51, 52, 53, 54, 99]), 1);
			RegisterAction(Player.ClimbLookLeft, Vector.<int>([55]), 1);
			RegisterAction(Player.ClimbLookRight, Vector.<int>([56]), 1);
			RegisterAction(Player.MidAir, Vector.<int>([57]), 1);
			RegisterAction(Player.Prone, Vector.<int>([58, 59, 60, 61]), 1);
			RegisterAction(Player.ClimbDownLadder, Vector.<int>([62, 63, 64, 65]), 1);
			RegisterAction(Player.Slide, Vector.<int>([66, 67, 68, 69]), 1);
			RegisterAction(Player.Sleep, Vector.<int>([70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85]), 4);
			defaultAnimation = Character.Idle;
		}
	}

}