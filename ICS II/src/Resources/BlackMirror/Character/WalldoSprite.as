package Resources.BlackMirror.Character 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.SpriteSheetAnimation;
	import System.MC.Characters.Character;
	
	/**
	 * ...
	 * @author ss
	 */
	public class WalldoSprite extends SpriteSheetAnimation 
	{
		[Embed(source = "../../../../lib/Characters/Monsters/Walldo Walking.png")]
		private static const map:Class;
		public function WalldoSprite() 
		{
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			super(bmp, 64, 64, new Point(32, 32));
			Interval = 3;
			RegisterAction(Character.Idle, Vector.<int>([0]));
			RegisterAction(Character.Move, Vector.<int>([0, 1, 2, 3, 4]));
			defaultAnimation = Character.Idle;
			
		}
		
	}

}