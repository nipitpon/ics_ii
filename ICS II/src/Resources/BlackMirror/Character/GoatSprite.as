package Resources.BlackMirror.Character 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import Resources.Flipbook;
	import System.MC.Characters.Character;
	import System.MC.Characters.Monster;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class GoatSprite extends Flipbook 
	{
		[Embed(source = "../../../../lib/Characters/Goat/Goat Idle.png")]
		private static const map1:Class;
		[Embed(source = "../../../../lib/Characters/Goat/Goat Walk 1.png")]
		private static const map2:Class;
		[Embed(source = "../../../../lib/Characters/Goat/Goat Walk 2.png")]
		private static const map3:Class;
		[Embed(source = "../../../../lib/Characters/Goat/Goat Walk 3.png")]
		private static const map4:Class;
		[Embed(source = "../../../../lib/Characters/Goat/Goat Walk 4.png")]
		private static const map5:Class;
		[Embed(source = "../../../../lib/Characters/Goat/Goat Walk 5.png")]
		private static const map6:Class;
		[Embed(source = "../../../../lib/Characters/Goat/Goat Walk 6.png")]
		private static const map7:Class;
		[Embed(source = "../../../../lib/Characters/Goat/Goat Death 1.png")]
		private static const map8:Class;
		[Embed(source = "../../../../lib/Characters/Goat/Goat Death 2.png")]
		private static const map9:Class;
		[Embed(source = "../../../../lib/Characters/Goat/Goat Death 3.png")]
		private static const map10:Class;
		[Embed(source = "../../../../lib/Characters/Goat/Goat Death 4.png")]
		private static const map11:Class;
		[Embed(source = "../../../../lib/Characters/Goat/Goat Death 5.png")]
		private static const map12:Class;
		[Embed(source = "../../../../lib/Characters/Goat/Goat Death 6.png")]
		private static const map13:Class;
		
		public function GoatSprite() 
		{
			var bmps:Vector.<Bitmap> = Vector.<Bitmap>([
				new map1(), new map2(), new map3(), new map4(), 
				new map5(), new map6(), new map7(), new map8(),
				new map9(), new map10(), new map11(), new map12(),
				new map13()
			]);
			for each(var bmp:Bitmap in bmps)
			{ 
				bmp.scaleX = Tile.Size.x * 2 / bmp.width;
				bmp.scaleY = bmp.scaleX;
				bmp.smoothing = false;
			}
			super(bmps, new Point(Tile.Size.x, Tile.Size.y));
			Interval = 3;
			RegisterAction(Character.Idle, Vector.<int>([0]));
			RegisterAction(Character.Move, Vector.<int>([1, 2, 3, 4, 5, 6]));
			RegisterAction(Monster.Die, Vector.<int>([7, 8, 9, 10, 11, 12]));
			defaultAnimation = Character.Idle;
		}
		
	}

}