package Resources.BlackMirror.Level.Level1 
{
	import flash.display.Bitmap;
	import flash.display.Shape;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import Resources.BlackMirror.Character.PlayerSprite;
	import Resources.Image;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class CloseBackground1 extends Image 
	{
		//[Embed(source = "../../../../../lib/Background.png")]
		[Embed(source="../../../../../lib/Environment/Sky.png")]
		private static const map:Class;
		public function CloseBackground1() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.smoothing = false;
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			addChild(bmp);
		}
		
	}

}