package Resources 
{
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.geom.Point;
	import System.Interfaces.IAnimatable;
	import System.V.RenderableObject;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author Knight
	 */
	public class SpriteSheetAnimation extends Animation implements IAnimatable
	{
		private var bitmap:Bitmap;
		
		public function SpriteSheetAnimation(sprite:Bitmap, width:Number, height:Number, scaleCenter:Point) 
		{
			super(scaleCenter);
			bitmap = sprite;
			scrollRect = new Rectangle(0, 0, width, height);
			Initialize();
		}
		public function Initialize():void
		{
			addChild(bitmap);
		}
		
		public override function set height(val:Number):void
		{
			var rect:Rectangle = scrollRect;
			rect.height = val;
			scrollRect = rect;
		}
		public override function set width(val:Number):void
		{
			var rect:Rectangle = scrollRect;
			rect.width = val;
			scrollRect = rect;
		}
		
		public override function get width():Number { return scrollRect.width; }
		public override function get height():Number { return scrollRect.height; }
		public override function Update():void
		{
			super.Update();
			
			if (Freeze || !IsNewFrame) return;
			
			var rect:Rectangle = scrollRect;
			var num:uint = AnimationImagePosition;
			var i:uint = uint(width) * ((num) % (bitmap.width / width));
			var j:uint = uint(height) * Math.floor(num / (bitmap.width / width));
			rect.x = i;
			rect.y = j;
			scrollRect = rect;
		}
		
		protected override function setCurrentAnimation(action:ActionSet):void
		{
			super.setCurrentAnimation(action);
			var pos:uint = AnimationImagePosition;
			var rect:Rectangle = scrollRect;
			rect.y = height * Math.floor(pos / (bitmap.width / width));
			rect.x = width * ((pos % (bitmap.width / width)));
			scrollRect = rect;
		}
	}
}