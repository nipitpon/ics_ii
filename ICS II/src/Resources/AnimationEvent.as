package Resources 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class AnimationEvent extends Event 
	{
		public static const Progress:String = "Progress";
		private var position:int;
		public function AnimationEvent(type:String, position:int, bubbles:Boolean = false, cancelable:Boolean = false) 
		{
			super(type, bubbles, cancelable);
			this.position = position;
		}
		public function get Position():int { return position; }
		public override function clone():Event 
		{
			return new AnimationEvent(type, position, bubbles, cancelable);
		}
	}

}