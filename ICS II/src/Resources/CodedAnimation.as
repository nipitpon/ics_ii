package Resources 
{
	import flash.geom.Point;
	import flash.utils.Dictionary;
	import System.Interfaces.IAnimatable;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class CodedAnimation extends Animation implements IAnimatable 
	{
		private var image:RenderableObject;
		private var func:Dictionary;
		public function CodedAnimation(image:RenderableObject, func:Dictionary, scaleCenter:Point=null) 
		{
			super(scaleCenter);
			this.image = image;
			addChild(image);
		}
		public override function Update():void
		{
			super.Update();
			
			if (Freeze || !IsNewFrame) return;
			
			var num:uint = AnimationImagePosition;
			if (func[CurrentAction.ID] != null)
				func[CurrentAction.ID](image, num);
		}
		
		protected override function get IsNewFrame():Boolean { return CurrentAction.positions.length > 1 && super.IsNewFrame; }
		protected override function setCurrentAnimation(action:ActionSet):void
		{
			super.setCurrentAnimation(action);
			var pos:uint = AnimationImagePosition;
			if (func[action.ID] != null) 
				func[action.ID](image, pos);
		}
	}

}