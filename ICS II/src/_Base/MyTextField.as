package _Base 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class MyTextField extends TextField 
	{
		public function MyTextField(size:int = 40, color:int = 0xffffff, center:Boolean = true) 
		{
			selectable = false;
			embedFonts = true;
			
			//autoSize = TextFieldAutoSize.LEFT;
			defaultTextFormat = new TextFormat("Pixel Font", size, color, true, null, null, null, null,
				center ? TextFormatAlign.CENTER : TextFormatAlign.LEFT);
		}
		public function GetBitmap():Bitmap
		{
			var dat:BitmapData = new BitmapData(width, height, true, 0);
			dat.draw(this);
			return new Bitmap(dat);
		}
	}

}