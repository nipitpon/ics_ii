package _Base 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.ProgressEvent;
	import flash.events.SampleDataEvent;
	import flash.events.TimerEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.utils.Timer;
	import System.Interfaces.ISound;
	/**
	 * ...
	 * @author Knight
	 */
	public class MySound extends EventDispatcher implements ISound
	{
		public static const SoundCompleted:String = "SoundCompleted";
		private var sound:Sound;
		private var channel:SoundChannel;
		private var start_offset:int;
		private var volume:int;
		private var fade_out_length:int;
		private var fade_in_length:int;
		private var pause_position:int;
		private var loop:int;
		public function MySound(sound:Sound, startOffset:int = 0) 
		{
			if (sound == null) throw new ArgumentError();
			
			this.sound = sound;
			start_offset = startOffset;
			volume = 100;
			loop = 0;
		}
		public function get PauseWithGame():Boolean { return false; }
		public function get IsPausing():Boolean { return pause_position >= 0; }
		public function Pause():void
		{
			if (channel == null) return;
			
			pause_position = channel.position;
			channel.stop();
			channel.removeEventListener(Event.SOUND_COMPLETE, onSoundCompleted);
		}
		public function Resume():void
		{
			if (!IsPausing) return;
			
			channel = sound.play(pause_position, 1, new SoundTransform(volume / 100.0));
			pause_position = -1;
			if (channel != null)
			{
				channel.addEventListener(Event.SOUND_COMPLETE, onFinishFirstUniqueOffsetLoop);
			}
		}
		public function Play(loop:int = 0, fadeInDuration:int = 0, custom_start:int = -1):void
		{
			if (IsPlaying) Stop();
			
			var start_offset:int = this.start_offset;
			
			if (custom_start >= 0) start_offset = custom_start;
			
			pause_position = -1;
			this.loop = loop;
			if (fadeInDuration == 0)
			{
				channel = sound.play(start_offset, custom_start >= 0 ? 1 : loop, new SoundTransform(volume / 100.0));
			}
			else
			{
				channel = sound.play(start_offset, custom_start >= 0 ? 1 : loop, new SoundTransform(0));
				fade_in_length = fadeInDuration;
				if (channel != null)
				{
					var timer:Timer = new Timer(10);
					timer.addEventListener(TimerEvent.TIMER, onFadingIn);
					timer.start();
				}
			}
			if (channel != null)
			{
				if (custom_start >= 0)
				{
					channel.addEventListener(Event.SOUND_COMPLETE, onFinishFirstUniqueOffsetLoop);
				}
				else
				{
					channel.addEventListener(Event.SOUND_COMPLETE, onSoundCompleted);
				}
			}
		}
		public function Stop(fadeOutDuration:int = 0):void
		{
			if (channel == null) return;
			
			pause_position = -1;
			if(fadeOutDuration == 0)
			{
				channel.removeEventListener(Event.SOUND_COMPLETE, onSoundCompleted);
				channel.removeEventListener(Event.SOUND_COMPLETE, onFinishFirstUniqueOffsetLoop);
				channel.stop();
				channel = null;
				dispatchEvent(new Event(SoundCompleted));
			}
			else
			{
				fade_out_length = fadeOutDuration;
				var timer:Timer = new Timer(10);
				timer.addEventListener(TimerEvent.TIMER, onFadingOut);
				timer.start();
			}
		}
		public function set Volume(val:int):void
		{
			if (val < 0 || val > 100) throw new ArgumentError();
			
			volume = val;
			if (channel != null)
			{
				var sTran:SoundTransform = channel.soundTransform;
				sTran.volume = volume / 100.0;
				channel.soundTransform = sTran;
			}
		}
		public function get Volume():int { return volume; }
		public function get IsPlaying():Boolean { return channel != null; }
		protected function ChangeStartOffset(new_offset:int):void
		{
			start_offset = new_offset;
		}
		private function onFinishFirstUniqueOffsetLoop(e:Event):void
		{
			channel = sound.play(start_offset, loop - 1, new SoundTransform(volume / 100.0));
			if(channel != null)
				channel.addEventListener(Event.SOUND_COMPLETE, onSoundCompleted);
		}
		private function onSoundCompleted(e:Event):void
		{
			if (channel != null)
			{
				channel.removeEventListener(Event.SOUND_COMPLETE, onSoundCompleted);
				channel = null;
			}
			dispatchEvent(new Event(SoundCompleted));
		}
		private function onFadingIn(e:TimerEvent):void
		{
			var trans:SoundTransform = channel.soundTransform;
			var timer:Timer = e.currentTarget as Timer;
			trans.volume = (volume * ((timer.currentCount * timer.delay) / fade_in_length)) / 100;
			channel.soundTransform = trans;
			if (timer.currentCount * timer.delay >= fade_in_length)
			{
				timer.removeEventListener(TimerEvent.TIMER, onFadingOut);
				timer.stop();
			}
		}
		private function onFadingOut(e:TimerEvent):void
		{
			if (channel != null)
			{
				var trans:SoundTransform = channel.soundTransform;
				var timer:Timer = e.currentTarget as Timer;
				trans.volume = (volume * ((fade_out_length - (timer.currentCount * timer.delay)) / fade_out_length)) / 100;
				channel.soundTransform = trans;
				if (timer.currentCount * timer.delay >= fade_out_length)
				{
					timer.removeEventListener(TimerEvent.TIMER, onFadingOut);
					timer.stop();
					channel.removeEventListener(Event.SOUND_COMPLETE, onSoundCompleted);
					channel.stop();
					channel = null;
					dispatchEvent(new Event(SoundCompleted));
				}
			}
			else
			{
				(e.currentTarget as Timer).removeEventListener(TimerEvent.TIMER, onFadingOut);
				dispatchEvent(new Event(SoundCompleted));
			}
		}
	}
}