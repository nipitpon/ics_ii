package _Base 
{
	import adobe.utils.CustomActions;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import System.Interfaces.ISound;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SoundBurst extends EventDispatcher implements ISound 
	{
		private var sound_playing:Vector.<Boolean>;
		private var sound_list:Vector.<ISound>;
		private var delay:Vector.<int>;
		private var timer:Timer;
		private var fadeInDuration:int;
		public function SoundBurst(sound_list:Vector.<ISound>, delay:Vector.<int>) 
		{
			if (sound_list == null || delay == null || sound_list.length != delay.length || sound_list.length == 0)
				throw new ArgumentError();
			
			sound_playing = new Vector.<Boolean>();
			this.sound_list = sound_list;
			this.delay = delay;
			for (var i:int = 0; i < sound_list.length; i++)
			{
				sound_playing.push(false);
			}
			timer = new Timer(10);
			timer.addEventListener(TimerEvent.TIMER, timer_tick);
		}
		public function get PauseWithGame():Boolean { return false; }
		public function Play(loop:int = 0, fadeInDuration:int = 0):void 
		{
			if (timer.running) Stop();
			
			this.fadeInDuration = fadeInDuration;
			timer.reset();
			timer.start();
			for (var i:int = 0; i < sound_playing.length; i++)
			{	
				sound_playing[i] = true;
			}
		}
		
		public function Stop(fadeDuration:int = 0):void 
		{
			timer.stop();
			dispatchEvent(new Event(MySound.SoundCompleted));
		}
		
		public function set Volume(value:int):void 
		{
			for each(var s:ISound in sound_list)
				s.Volume = value;
		}
		
		public function get Volume():int 
		{
			return sound_list[0].Volume;
		}
		
		public function get IsPlaying():Boolean 
		{
			for each(var s:Boolean in sound_playing)
				if (s) return true;
				
			return false;
		}
		private function timer_tick(e:Event):void
		{
			var pos:int = timer.currentCount * timer.delay;
			for (var i:int = 0; i < sound_list.length; i++)
			{
				if (pos >= delay[i] && !sound_list[i].IsPlaying && sound_playing[i])
				{
					sound_list[i].Play(0, fadeInDuration);
					sound_list[i].addEventListener(MySound.SoundCompleted, onSoundCompleted);
				}
			}
		}
		private function onSoundCompleted(e:Event):void
		{
			var found:Boolean = false;
			for (var i:int = 0; i < sound_list.length; i++)
			{
				if (sound_list[i] == e.currentTarget)
				{
					sound_list[i].removeEventListener(MySound.SoundCompleted, onSoundCompleted);
					sound_playing[i] = false;
				}
				if (sound_playing[i]) found = true;
			}
			if (!found) Stop(); 
		}
	}
}