package
{
	
	import _Base.MyTextField;
	import fl.transitions.easing.Strong;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextFieldAutoSize;
	import System.V.RenderableObject;
	import System.ControlCenter;
	
	/**
	* ...
	* @author Knight
	*/
	public class Fader extends RenderableObject
	{
		public static const FaderFadedOut:String = "FadedOut";
		public static const FaderFadedIn:String = "FadedIn";
		private static const MaxCount:Number = Utility.FramePerSecond * 1;
		private static const ColumnMaxCount:Number = Utility.FramePerSecond * 0.3;
		private var shapes:Vector.<Vector.<Bitmap>>;
		private var count:int;
		private var load_text:MyTextField;
		private var current_dat:BitmapData;
		private var shape_ren:RenderableObject;
		public function Fader() 
		{
			super();
			shape_ren = new RenderableObject();
			shapes = new Vector.<Vector.<Bitmap>>();
			load_text = new MyTextField(30);
			load_text.text = "LOADING INTENSIFIES...";
			load_text.x = 700 - load_text.width;
			load_text.y = 500;
			var bitmap:BitmapData = new BitmapData(400, 280, false, 0xff1c030c);
			var mat:Matrix = new Matrix();
			mat.scale(2, 2);
			for (var i:int = 0; i < 12; i++)
			{
				shapes.push(new Vector.<Bitmap>());
				for (var j:int = 0; j < 16; j++)
				{
					var shape:Bitmap = new Bitmap();
					shape.x = j * 50;
					shape.y = i * 50;
					var shape_dat:BitmapData = new BitmapData(50, 50);
					shape_dat.draw(bitmap, mat, null, null);
					shape.bitmapData = shape_dat;
					shapes[shapes.length - 1].push(shape);
					shape_ren.addChild(shape);
				}
			}
			load_text.autoSize = TextFieldAutoSize.LEFT;
			addChild(shape_ren);
			count = -1;
		}
		public function get IsFading():Boolean { return count >= 0; }
		public function FadeOut():void
		{
			count = MaxCount;
			for each(var sh_arr:Vector.<Bitmap> in shapes) for each(var shape:Bitmap in sh_arr)
			{
				shape.scaleX = 1;
				shape.scaleY = 1;
			}
			addEventListener(Event.ENTER_FRAME, onEnterFrame_FadeOut);
			if (contains(load_text))
				removeChild(load_text);
		}
		public function FadeIn(bitmap:BitmapData = null, text_color:int = 0xffffff, text:String = ""):void
		{
			var shape:Bitmap;
			if (hasEventListener(Event.ENTER_FRAME))
			{
				removeEventListener(Event.ENTER_FRAME, onEnterFrame_FadeOut);
				removeEventListener(Event.ENTER_FRAME, onEnterFrame_FadeIn);
			}	
			if (contains(load_text))
				removeChild(load_text);
			if (bitmap == null)
			{
				bitmap = new BitmapData(400, 280, false, 0xff1c030c);
			}
			var mat:Matrix = new Matrix();
			mat.scale(2, 2);
			if (current_dat != bitmap)
			{
				for (var i:int = 0; i < shapes.length; i++)
				{
					for (var j:int = 0; j < shapes[i].length; j++)
					{
						mat.identity();
						mat.translate(j * -25, i * -25);
						mat.scale(2, 2);
						shape = shapes[i][j];
						var shape_dat:BitmapData = new BitmapData(50, 50);
						shape_dat.draw(bitmap, mat, null, null);
						shape.bitmapData = shape_dat;
						shape.x = j * 50;
						shape.y = i * 50;
					}
				}
				current_dat = bitmap;
			}
			load_text.textColor = text_color;
			load_text.text = text;
			load_text.x = 700 - load_text.width;
			
			count = MaxCount;
			for each(var sh_arr:Vector.<Bitmap> in shapes) for each(shape in sh_arr)
			{
				shape.scaleX = 0;
				shape.scaleY = 0;
				//shape.alpha = 0;
			}
			if(!hasEventListener(Event.ENTER_FRAME))
				addEventListener(Event.ENTER_FRAME, onEnterFrame_FadeIn);
		}
		private function onEnterFrame_FadeOut(e:Event):void
		{
			for (var i:int = 0; i < shapes.length; i++)
			{
				for (var j:int = 0; j < shapes[i].length; j++)
				{
					var cell_count:Number = Math.min(ColumnMaxCount, Math.max(count - (24 - new Point(12 - i, 16 - j).length), 0));
					var shape:Bitmap = shapes[i][j];
					shape.scaleX = Strong.easeOut(ColumnMaxCount - cell_count, 1, -1, ColumnMaxCount);
					shape.scaleY = shape.scaleX;
					shape.x = (j * 50) + (Strong.easeOut(ColumnMaxCount - cell_count, 0, 25, ColumnMaxCount));
					shape.y = (i * 50) + (Strong.easeOut(ColumnMaxCount - cell_count, 0, 25, ColumnMaxCount));
					//shape.alpha = Strong.easeOut(ColumnMaxCount - cell_count, 1, -1, ColumnMaxCount);
				}
			}
			if (--count < 0)
			{
				dispatchEvent(new Event(FaderFadedOut));
				removeEventListener(Event.ENTER_FRAME, onEnterFrame_FadeOut);
			}
		}
		private function onEnterFrame_FadeIn(e:Event):void
		{
			for (var i:int = 0; i < shapes.length; i++)
			{
				for (var j:int = 0; j < shapes[i].length; j++)
				{
					var cell_count:Number = Math.min(ColumnMaxCount, Math.max(count - (24 - new Point(12 - i, 16 - j).length), 0));
					var shape:Bitmap = shapes[i][j];
					shape.scaleX = Strong.easeOut(ColumnMaxCount - cell_count, 0, 1, ColumnMaxCount);
					shape.scaleY = shape.scaleX;
					
					shape.x = (j * 50) + (Strong.easeOut(ColumnMaxCount - cell_count, 25, -25, ColumnMaxCount));
					shape.y = (i * 50) + (Strong.easeOut(ColumnMaxCount - cell_count, 25, -25, ColumnMaxCount));
					//shape.alpha = Strong.easeOut(ColumnMaxCount - cell_count, 0, 1, ColumnMaxCount);
				}
			}
			if (--count < 0)
			{
				dispatchEvent(new Event(FaderFadedIn));
				removeEventListener(Event.ENTER_FRAME, onEnterFrame_FadeIn);
			}
			if (!contains(load_text) && count < MaxCount / 2)
			{
				addChild(load_text);
			}
		}
	}
}