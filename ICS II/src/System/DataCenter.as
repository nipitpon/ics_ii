package System 
{
	//import com.newgrounds.API;
	import flash.display.DisplayObject;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.geom.Vector3D;
	import flash.net.SharedObject;
	import flash.utils.Dictionary;
	import flash.utils.getDefinitionByName;
	import Resources.Image;
	import Resources.lib.ComicPage.*;
	import System.MC.etc.GameInfo;
	import System.MC.etc.LevelInfo;
	import System.MC.etc.PlayerInfo;
	import System.MC.etc.TimeAttackLevelInfo;
	import System.MC.FlagPosition;
	import System.V.RenderableObject;
	/**
	 * ...
	 * @author Knight
	 */
	public final class DataCenter 
	{
		[Embed(source = "../../lib/pixelated/PixelWorkFont.ttf", fontName = "Pixel Font",
			embedAsCFF="false", fontWeight="regular")]
		public static const Font:Class;
		[Embed(source = "../../lib/Other pages/Pause Menu/Flamenco-Light.ttf", fontName = "Flamenco",
			embedAsCFF="false", fontWeight="regular")]
		public static const Flamenco:Class;
		
		private static const SaveFileName:String = "Knight52_ICSII";
		public static const Language_EN:int = 0;
		
		public static const Module_Game:int = 10;
		public static const Module_Comic:int = 20;
		
		public static const Section_Skill:int = 100;
		public static const Section_Buff:int = 200;
		public static const Section_Notes:int = 300;
		public static const Section_Comic:int = 400;
		public static const Section_Item:int = 500;
		public static const Section_Values:int = 600;
		
		public static const Skill_Name:int = 0;
		public static const Skill_Description:int = 1;
		public static const Skill_Requirement:int = 2;
		
		public static const Buff_CombustionSyndrome:int = 100;
		public static const Buff_Burn:int = 110;
		public static const Buff_Rain:int = 200;
		public static const Buff_Name:int = 0;
		public static const Buff_Description:int = 1;
		public static const Buff_Icon:int = 2;
		public static const Buff_SuperBuff:int = 3;
		
		private static var info:Dictionary = new Dictionary();
		private static var currentLanguage:int = Language_EN;
		private static var flags:Vector.<int> = Vector.<int>([0, 0, 0, 0, 0]);
		
		private static var save_info:GameInfo;
		public function DataCenter() 
		{
			throw new Error("Cannot instantiate SaveFile");
		}
		
		public static function Initialize():void
		{
			info[Language_EN] = new Dictionary();
			
			info[Language_EN][Module_Game] = new Dictionary();
			
			info[Language_EN][Module_Game][Section_Buff] = new Dictionary();
			info[Language_EN][Module_Game][Section_Buff][Buff_CombustionSyndrome] = new Vector.<*>();
			info[Language_EN][Module_Game][Section_Buff][Buff_CombustionSyndrome][Buff_Name] = "Combustion Syndrome";
			info[Language_EN][Module_Game][Section_Buff][Buff_CombustionSyndrome][Buff_Description] = "BURN";
			info[Language_EN][Module_Game][Section_Buff][Buff_CombustionSyndrome][Buff_Icon] = RenderableObject;
			info[Language_EN][Module_Game][Section_Buff] = new Dictionary();
			info[Language_EN][Module_Game][Section_Buff][Buff_Burn] = new Vector.<*>();
			info[Language_EN][Module_Game][Section_Buff][Buff_Burn][Buff_Name] = "le Burn";
			info[Language_EN][Module_Game][Section_Buff][Buff_Burn][Buff_Description] = "BURN";
			info[Language_EN][Module_Game][Section_Buff][Buff_Burn][Buff_Icon] = RenderableObject;
			LoadGame();
		}
		public static function GetData(language:int, module:int, section:int, code1:int, code2:int):*
		{
			if (info[language] == null || info[language][module] == null || info[language][module][section] == null ||
				info[language][module][section][code1] == null)
				throw new ArgumentError();
			
			return info[language][module][section][code1][code2];
		}
		public static function SetValue(key:String, value:*):void
		{
			info[Language_EN][Module_Game][Section_Values][key] = value;
		}
		public static function GetValue(key:String):*
		{
			return info[Language_EN][Module_Game][Section_Values][key];
		}
		public static function GetBuffFullDescription(code:int):String
		{
			var message:String = "";
			var name:String = GetData(CurrentLanguage, Module_Game, Section_Buff, code, Buff_Name) as String;
			var desc:String = GetData(CurrentLanguage, Module_Game, Section_Buff, code, Buff_Description) as String;
			if (name != null && name != "") message += name + "\n\n";
			if (desc != null && desc != "") message += desc;
			
			return message;
		}
		public static function SetFlag(position:int, value:Boolean, save:Boolean = true):void
		{
			var frag:int = 31 - (position % 31);//So that the least value starts on the left, not right.
			var num:int = Math.floor(position / 31);
			
			if (num >= flags.length) throw new ArgumentError("num is out of range");
			
			if (value)
			{
				flags[num] |= 1 << frag;
			}
			else
			{
				var mask:int = int.MAX_VALUE - (1 << frag);
				flags[num] &= mask;
			}
			if(save)
				SaveCurrentGame(); 
		}
		public static function get IsVanillaCleared():Boolean { return GetFlag(FlagPosition.VanillaFinished); }
		public static function get HasPlayerData():Boolean
		{
			var sharedObj:SharedObject = SharedObject.getLocal(SaveFileName);
			var result:Boolean = sharedObj.data.playerHP != null && int(sharedObj.data.levelMode) == GameSystem.GameMode_Vanilla;
			sharedObj.flush();
			return result;
		}
		public static function GetFlag(position:int):Boolean
		{
			var frag:int = 31 - (position % 31);//So it the least value starts on the left, not right.
			var num:int = Math.floor(position / 31);
			
			if (num >= flags.length) throw new ArgumentError(SaveFileName);
			return (flags[num] & (1 << frag)) != 0;
		}
		public static function GetBuffIcons(language:int, code:int):DisplayObject
		{
			var c:Class = GetData(language, Module_Game, Section_Buff, code, Buff_Icon);
			return new c();
		}
		public static function FrameMove():void
		{
			save_info.time++;
		}
		public static function UnlockAchievement(id:int):void
		{
			save_info.achievements_unlocked[id] = true;
			SaveCurrentGame();
		}
		public static function IsAchievementUnlocked(id:int):Boolean
		{
			return save_info.achievements_unlocked[id];
		}
		public static function get CurrentTimeTick():uint { return save_info.time; }
		public static function set CurrentLanguage(val:int):void { currentLanguage = val; }
		public static function get CurrentLanguage():int { return currentLanguage; }
		public static function get SurvivalWave():int
		{
			var sharedObj:SharedObject = SharedObject.getLocal(SaveFileName);
			var level:int = int(sharedObj.data.survivalWave);
			sharedObj.flush();
			
			if (isNaN(level)) return 0;
			else return level;
		}
		public static function get TimeAttackRecord():int
		{
			var sharedObj:SharedObject = SharedObject.getLocal(SaveFileName);
			var level:int = int(sharedObj.data.timeAtkRecord);
			sharedObj.flush();
			
			if (isNaN(level)) return -1;
			else return level;
		}
		public static function CollectSecretItem(level:int):void
		{
			save_info.items[level - 1] = true;
			SaveCurrentGame();
		}
		public static function UseFirePotion():void
		{
			save_info.fire_potion_used++;
			SaveCurrentGame();
		}
		public static function SetLevelDeath(level:int, number:int):void
		{
			save_info.deaths[level - 1] = number;
			SaveCurrentGame();
		}
		public static function SetLevelTime(level:int, time:int):void
		{
			if (save_info.level_time[level - 1] < 0 || time < save_info.level_time[level - 1])
				save_info.level_time[level - 1] = time;
			SaveCurrentGame();
		}
		public static function CalculateScore():int
		{
			//var score:int = Utility.SecondToFrame(4*3600);
			var score:int = 0;
			for (var i:int = 1; i <= 50; i++)
			{
				var s:int = DataCenter.GetLevelTime(i);
				if (s > 0)
				{
					score += Math.max(Utility.SecondToFrame(300) - s, 0);
				}
				else
				{
					trace("NO TIME RECORD AT LEVEL " + i);
				}
			}
			if (DataCenter.GetLevelTime(55) > 0)
			{
				score += Math.max(0, Utility.SecondToFrame(680) - DataCenter.GetLevelTime(55));
			}
			return score;
		}
		public static function GetLevelTime(level:int):int { return save_info.level_time[level - 1]; }
		public static function get FirePotionUsed():int { return save_info.fire_potion_used; }
		public static function GetTotalDeathByLevel(level:int):int { return save_info.total_level_death[level - 1]; }
		public static function AddTotalDeath(level:int, reason:int):void
		{
			save_info.total_death++;
			save_info.total_level_death[level - 1]++;
			if (reason != save_info.last_death_reason)
			{
				save_info.last_death_reason = reason;
				save_info.death_streak = 1;
			}
			else
			{
				save_info.death_streak++;
			}
		}
		public static function get DeathStreak():int { return save_info.death_streak; }
		public static function get LastDeathReason():int { return save_info.last_death_reason; }
		public static function get TotalDeath():int { return save_info.total_death; }
		public static function GetDeathCount(level:int):int { return save_info.deaths[level - 1]; }
		public static function SecretItemCollected(level:int):Boolean { return save_info.items[level - 1]; }
		public static function CompareAndSaveTimeAttackRecord(val:int):void
		{
			var sharedObj:SharedObject = SharedObject.getLocal(SaveFileName);
			var level:int = int(sharedObj.data.timeAtkRecord);
			
			if (level > 0) val = Math.min(val, level);
			sharedObj.data.timeAtkRecord = val;
			sharedObj.flush();
		}
		public static function CompareAndSaveSurvivalWave(val:int):void
		{
			var sharedObj:SharedObject = SharedObject.getLocal(SaveFileName);
			var level:int = int(sharedObj.data.survivalWave);
			
			if (!isNaN(level)) val = Math.max(val, level);
			sharedObj.data.survivalWave = val;
			sharedObj.flush();
		}
		public static function Flush():void
		{
			
		}
		public static function BurnGoat():void
		{
			save_info.total_goat_burn++;
		}
		public static function get GoatBurnt():int { return save_info.total_goat_burn; }
		public static function ClearSaveData(clearFlag:Boolean):void
		{
			var sharedObj:SharedObject = SharedObject.getLocal(SaveFileName);
			sharedObj.clear();
			sharedObj.data.sfxVolume = SoundCenter.SFXVolume;
			sharedObj.data.bgmVolume = SoundCenter.BGMVolume;
			sharedObj.flush();
			GameSystem.SetPlayerInfo(null);
			GameSystem.SetLevelInfo(new LevelInfo());
			LoadGame();
			//API.clearLocalMedals();
		}
		public static function LoadGame():void
		{
			var sharedObj:SharedObject = SharedObject.getLocal(SaveFileName);
			
			save_info = new GameInfo();
			if (sharedObj.data.gameInfo != null)
			{
				try
				{
					save_info.time = uint(sharedObj.data.gameInfo.time);
					save_info.items = Vector.<Boolean>(sharedObj.data.gameInfo.items);
					save_info.deaths = Vector.<int>(sharedObj.data.gameInfo.deaths);
					save_info.achievements_unlocked = Vector.<Boolean>(sharedObj.data.gameInfo.achievements_unlocked);
					save_info.total_death = int(sharedObj.data.gameInfo.total_death);
					save_info.total_level_death = Vector.<int>(sharedObj.data.gameInfo.max_deaths);
					save_info.last_death_reason = int(sharedObj.data.gameInfo.last_death_reason);
					save_info.death_streak = int(sharedObj.data.gameInfo.death_streak);
					save_info.fire_potion_used = int(sharedObj.data.gameInfo.fire_potion_used);
					if(sharedObj.data.gameInfo.level_time != null)
						save_info.level_time = Vector.<int>(sharedObj.data.gameInfo.level_time);
				}
				catch (e:Error)
				{
					save_info = new GameInfo();
				}
			}
			if (sharedObj.data.bgmVolume != null) SoundCenter.BGMVolume = int(sharedObj.data.bgmVolume);
			if (sharedObj.data.sfxVolume != null) SoundCenter.SFXVolume = int(sharedObj.data.sfxVolume);
			
			sharedObj.flush();
		}
		public static function SaveSettings(bgmVolume:int, sfxVolume:int):void
		{
			var sharedObj:SharedObject = SharedObject.getLocal(SaveFileName);
			sharedObj.data.sfxVolume = sfxVolume;
			sharedObj.data.bgmVolume = bgmVolume;
			sharedObj.flush();
		}
		public static function SaveCurrentGame(updatePlayerPosition:Boolean = false, updateLevelInfo:Boolean = false, flagOnly:Boolean = false):Boolean
		{
			var sharedObj:SharedObject = SharedObject.getLocal(SaveFileName);
			sharedObj.data.gameInfo = new Object();
			sharedObj.data.gameInfo.time = save_info.time;
			sharedObj.data.gameInfo.items = save_info.items;
			sharedObj.data.gameInfo.deaths = save_info.deaths;
			sharedObj.data.gameInfo.achievements_unlocked = save_info.achievements_unlocked;
			sharedObj.data.gameInfo.total_death = save_info.total_death;
			sharedObj.data.gameInfo.max_deaths = save_info.total_level_death;
			sharedObj.data.gameInfo.death_streak = save_info.death_streak;
			sharedObj.data.gameInfo.last_death_reason = save_info.last_death_reason;
			sharedObj.data.gameInfo.fire_potion_used = save_info.fire_potion_used;
			sharedObj.data.gameInfo.level_time = save_info.level_time;
			sharedObj.flush();
			return true;
		}
	}
}