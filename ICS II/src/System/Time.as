package System 
{
	import Resources.lib.Sound.BGM.NightLevelBGM;
	import Resources.lib.Sound.BGM.SkyLevelBGM;
	import Resources.lib.Sound.BGM.SunnyWeatherSound;
	/**
	 * ...
	 * @author Knight
	 */
	public class Time 
	{
		private static var tick:int = 0;
		/*
		 * DON'T SET ANY AMBIENT'S COLOR SLOT TO ZERO!!!!
		 * the value will be used as a divider for IIgnoreAmbient objects.
		 * If the divider is zero, result will be infinity or -infinity.
		 * Leads to very unsatisfying results.
		 * */
		public static const Ambient_Morning:int = 0x916A4E;
		public static const Ambient_Day:int = 0xffffff;
		public static const Ambient_Evening:int = 0x916A4E;
		public static const Ambient_Night:int = 0x696972;
		public static const Sky_Day:int = 0xf2ffff;// 0xabe2ff;
		public static const Sky_Morning:int = 0x0c3b96;// 0xffcf5c;
		public static const Sky_Evening:int = 0x0c3b96;// 0xffcf5c;
		public static const Sky_Night:int = 0x231133;// 0x000000;
		public static const TimeAdvanceMultiplier:int = 100;
		public static const SecondPerFrame:int = 4;
		private static var targetHour:int = -1;
		public function Time() 
		{
			throw new Error("Cannot instantiate time. It is static class");
		}
		public static function MoveOneFrame():void
		{
			if (targetHour >= 0)
			{
				for (var i:int = 0; i < TimeAdvanceMultiplier; i++)
				{
					tick += SecondPerFrame;
					if (Hour == targetHour)
					{
						StopAdvance();
						break;
					}
				}
			}
			else
				tick += SecondPerFrame;
			if (tick > 86399) tick -= 86399;
		}
		public static function get IsDayTime():Boolean { return Hour >= 6 && Hour < 18; }
		public static function get IsAdvancing():Boolean { return targetHour >= 0; }
		public static function get TargetHour():int { return targetHour; }
		public static function AdvanceTo(h:int):void
		{
			targetHour = h;
			ControlCenter.Stop();
			if (GameSystem.CurrentLevel.OverrideBGM == null)
			{
				if (targetHour < 18 && targetHour > 6)
					SoundCenter.PlayBGM(SunnyWeatherSound);
				else
					SoundCenter.PlayBGM(NightLevelBGM);
			}
		}
		public static function StopAdvance():void
		{
			targetHour = -1;
			ControlCenter.Start();
		}
		public static function SetTime(hour:uint, minute:uint, second:uint):void
		{
			if (hour > 23 || minute > 59 || second > 59)
				throw new ArgumentError();
			
			tick = hour * 3600;
			tick += minute * 60;
			tick += second;
		}
		public static function get Hour():int { return Math.floor(tick / 3600); }
		public static function get Minute():int
		{
			var m:int = tick % 3600;
			return Math.floor(m / 60);
		}
		public static function get Tick():int { return tick; }
		public static function get Second():int { return tick % 60; }
		public static function get SkyColor():int
		{
			var hour:int = Hour;
			var weight:Number;
			var red:int;
			var green:int;
			var blue:int
			if (hour >= 9 && hour <= 15) return Sky_Day;
			else if (hour >= 20 || hour <= 4) return Sky_Night;
			else if (hour > 4 && hour <= 6)//Night -> Morning
			{
				weight = ((tick) - 18000) / Number(7199);
				red = Utility.Interpolate((Sky_Morning & 0xff0000) >> 16, (Sky_Night & 0xff0000) >> 16, weight);
				green = Utility.Interpolate((Sky_Morning & 0x00ff00) >> 8, (Sky_Night & 0x00ff00) >> 8, weight);
				blue = Utility.Interpolate((Sky_Morning & 0x0000ff), (Sky_Night & 0x0000ff), weight);
			}
			else if (hour >= 18 && hour < 20)//Evening -> Night
			{
				weight = ((tick) - 64800) / Number(7199);
				red = Utility.Interpolate((Sky_Night & 0xff0000) >> 16, (Sky_Evening & 0xff0000) >> 16, weight);
				green = Utility.Interpolate((Sky_Night & 0x00ff00) >> 8, (Sky_Evening & 0x00ff00) >> 8, weight);
				blue = Utility.Interpolate((Sky_Night & 0x0000ff), (Sky_Evening & 0x0000ff), weight);
			}
			else if (hour > 15 && hour < 18)//Day -> Evening
			{
				weight = ((tick) - 57600) / Number(7199);
				red = Utility.Interpolate((Sky_Evening & 0xff0000) >> 16, (Sky_Day & 0xff0000) >> 16, weight);
				green = Utility.Interpolate((Sky_Evening & 0x00ff00) >> 8, (Sky_Day & 0x00ff00) >> 8, weight);
				blue = Utility.Interpolate((Sky_Evening & 0x0000ff), (Sky_Day & 0x0000ff), weight);
			}
			else if (hour > 6 && hour < 9)//Morning -> Day
			{
				weight = ((tick) - 25200) / Number(7199);
				red = Utility.Interpolate((Sky_Day & 0xff0000) >> 16, (Sky_Morning & 0xff0000) >> 16, weight);
				green = Utility.Interpolate((Sky_Day & 0x00ff00) >> 8, (Sky_Morning & 0x00ff00) >> 8, weight);
				blue = Utility.Interpolate((Sky_Day & 0x0000ff), (Sky_Morning & 0x0000ff), weight);
			}
			return (red << 16) | (green << 8) | blue;
		}
		public static function get NightMaskVisibility():Number
		{
			var hour:int = Hour;
			var weight:Number;
			if (hour > 6 && hour < 18) weight = 0;
			else if (hour >= 20 || hour <= 4) weight = 1;
			else if (hour >= 18 && hour < 20)
			{
				weight = ((tick) - 64800) / Number(7199);
			}
			else if (hour > 4 && hour <= 6)
			{
				weight = 1 - ((tick) - 18000) / Number(7199);
			}
			return weight;
		}
		public static function get Ambient():int
		{
			var hour:int = Hour;
			var weight:Number;
			var red:int;
			var green:int;
			var blue:int
			if (hour >= 9 && hour <= 15) return Ambient_Day;
			else if (hour >= 20 || hour <= 4) return Ambient_Night;
			else if (hour > 4 && hour <= 6)//Night -> Morning
			{
				weight = ((tick) - 18000) / Number(7199);
				red = Utility.Interpolate((Ambient_Morning & 0xff0000) >> 16, (Ambient_Night & 0xff0000) >> 16, weight);
				green = Utility.Interpolate((Ambient_Morning & 0x00ff00) >> 8, (Ambient_Night & 0x00ff00) >> 8, weight);
				blue = Utility.Interpolate((Ambient_Morning & 0x0000ff), (Ambient_Night & 0x0000ff), weight);
			}
			else if (hour >= 18 && hour < 20)//Evening -> Night
			{
				weight = ((tick) - 64800) / Number(7199);
				red = Utility.Interpolate((Ambient_Night & 0xff0000) >> 16, (Ambient_Evening & 0xff0000) >> 16, weight);
				green = Utility.Interpolate((Ambient_Night & 0x00ff00) >> 8, (Ambient_Evening & 0x00ff00) >> 8, weight);
				blue = Utility.Interpolate((Ambient_Night & 0x0000ff), (Ambient_Evening & 0x0000ff), weight);
			}
			else if (hour > 15 && hour < 18)//Day -> Evening
			{
				weight = ((tick) - 57600) / Number(7199);
				red = Utility.Interpolate((Ambient_Evening & 0xff0000) >> 16, (Ambient_Day & 0xff0000) >> 16, weight);
				green = Utility.Interpolate((Ambient_Evening & 0x00ff00) >> 8, (Ambient_Day & 0x00ff00) >> 8, weight);
				blue = Utility.Interpolate((Ambient_Evening & 0x0000ff), (Ambient_Day & 0x0000ff), weight);
			}
			else if (hour > 6 && hour < 9)//Morning -> Day
			{
				weight = ((tick) - 25200) / Number(7199);
				red = Utility.Interpolate((Ambient_Day & 0xff0000) >> 16, (Ambient_Morning & 0xff0000) >> 16, weight);
				green = Utility.Interpolate((Ambient_Day & 0x00ff00) >> 8, (Ambient_Morning & 0x00ff00) >> 8, weight);
				blue = Utility.Interpolate((Ambient_Day & 0x0000ff), (Ambient_Morning & 0x0000ff), weight);
			}
			return (red << 16) | (green << 8) | blue;
		}
	}
}