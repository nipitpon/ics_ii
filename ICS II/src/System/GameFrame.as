package System 
{
	import com.spilgames.gameapi.GameAPI;
	import fl.transitions.easing.None;
	import fl.transitions.easing.Regular;
	import fl.transitions.easing.Strong;
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.BlurFilter;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.engine.RenderingMode;
	import HardCode.Tiles.BurnableTiles;
	import Resources.lib.Environment.MoonSprite;
	import Resources.lib.Environment.StarSprite;
	import Resources.lib.Environment.SunSprite;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.INegateAmbient;
	import System.Interfaces.IInteractable;
	import System.Interfaces.IItem;
	import System.Interfaces.ILaunchable;
	import System.Interfaces.IMVC;
	import System.Interfaces.IOverlay;
	import System.Interfaces.IStatusHolder;
	import System.Interfaces.IUpdatable;
	import System.MC.Characters.Character;
	import System.MC.Characters.Monster;
	import System.MC.Characters.Player;
	import System.MC.Effects.Effect;
	import System.MC.etc.ZoomedInSceneInfo;
	import System.MC.Flags.EventTrigger;
	import System.MC.Items.Item;
	import System.MC.Ladder.Ladder;
	import System.MC.Level;
	import System.MC.Overlay;
	import System.MC.PressurePlatform.PressurePlatform;
	import System.MC.Terrain;
	import System.MC.Tile;
	import System.MC.WaterBlock;
	import System.MC.Weather;
	import System.V.FloatingText;
	import System.V.MonsterHealthBar;
	import System.V.NightEffect;
	import System.V.RenderableObject;
	import System.V.Sky;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class GameFrame extends RenderableObject 
	{
		public static const CameraSpeed:Number = 5;
		public static const objectGlow:GlowFilter = new GlowFilter(0xFFFFFF, 1.0, 5.0, 5.0, 500, 1, false, false);
		public static const objectShadow:DropShadowFilter = new  DropShadowFilter(3, 45, 0x000000, 3.0, 1.0, 1.0, 1.0, 1, false, false, false);
		private var ambientAffected:RenderableObject;
		private var sky:Sky;
		private var star:StarSprite;
		private var sun:RenderableObject;
		private var moon:MoonSprite;
		private var farBackground:RenderableObject;
		private var forestBackground:RenderableObject;
		private var nearForestBackground:RenderableObject;
		private var closeBackground:RenderableObject;
		private var waterBlock:RenderableObject;
		private var floor:RenderableObject;
		private var floorOverlay:RenderableObject;
		private var floorBackground:RenderableObject;
		private var treasure:RenderableObject;
		private var ladder:RenderableObject;
		private var interactable:RenderableObject;
		private var character_background:RenderableObject;
		private var character:RenderableObject;
		private var character_foreground:RenderableObject;
		private var items:RenderableObject;
		private var effects:RenderableObject;
		private var foreground:RenderableObject;
		private var weatherEffect:RenderableObject;
		private var interactableText:RenderableObject;
		private var zoomed_in_button:RenderableObject;
		private var zoomed_in_button_under:RenderableObject;
		private var top_most:RenderableObject;
		
		private var healthBars:RenderableObject;
		private var damageNumber:RenderableObject;
		
		private var nightEffect:NightEffect;
		
		private var shakeVector:Point;
		private var shakeDuration:Number;
		private var shake_decreaseRate:Number;
		
		private var focus:Point;
		private var prevAmbient:int = 0;
		private var ignoreAmbient:Vector.<INegateAmbient>;
		private var ambient_strength:Number;
		private static var instance:GameFrame = null;
		public function GameFrame() 
		{
			instance = this;
			ambientAffected = new RenderableObject();
			sky = new Sky();
			star = new StarSprite();
			sun = new SunSprite();
			moon = new MoonSprite();
			farBackground = new RenderableObject();
			forestBackground = new RenderableObject();
			nearForestBackground = new RenderableObject();
			floorBackground = new RenderableObject();
			waterBlock = new RenderableObject();
			floor = new RenderableObject();
			floorOverlay = new RenderableObject();
			treasure = new RenderableObject();
			ladder = new RenderableObject();
			interactable = new RenderableObject();
			character_background = new RenderableObject();
			character = new RenderableObject();
			character_foreground = new RenderableObject();
			items = new RenderableObject();
			effects = new RenderableObject();
			foreground = new RenderableObject();
			interactableText = new RenderableObject();
			weatherEffect = new RenderableObject();
			focus = new Point();
			healthBars = new RenderableObject();
			damageNumber = new RenderableObject();
			nightEffect = new NightEffect();
			top_most = new RenderableObject();
			closeBackground = new RenderableObject();
			zoomed_in_button = new RenderableObject();
			zoomed_in_button_under = new RenderableObject();
			
			ignoreAmbient = Vector.<INegateAmbient>([moon]);
			Initialize();
		}
		public function Initialize():void
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddToStage);
			super.addChild(ambientAffected);
			ambientAffected.addChild(sky);
			ambientAffected.addChild(star);
			ambientAffected.addChild(sun);
			ambientAffected.addChild(moon);
			ambientAffected.addChild(farBackground);
			ambientAffected.addChild(forestBackground);
			ambientAffected.addChild(nearForestBackground);
			ambientAffected.addChild(floorBackground);
			ambientAffected.addChild(closeBackground);
			ambientAffected.addChild(interactable);
			ambientAffected.addChild(treasure);
			ambientAffected.addChild(floor);
			ambientAffected.addChild(floorOverlay);
			ambientAffected.addChild(ladder);
			ambientAffected.addChild(character_background);
			ambientAffected.addChild(character);
			ambientAffected.addChild(character_foreground);
			ambientAffected.addChild(items);
			ambientAffected.addChild(effects);
			ambientAffected.addChild(waterBlock);
			ambientAffected.addChild(foreground);
			ambientAffected.addChild(weatherEffect);
			super.addChild(interactableText);
			super.addChild(healthBars);
			super.addChild(damageNumber);
			super.addChild(nightEffect);
			super.addChild(zoomed_in_button_under);
			super.addChild(zoomed_in_button);
			super.addChild(top_most);
			
			nightEffect.alpha = 0;
			ambient_strength = 1;
			sun.x = 100;
			moon.x = 100;
		}
		public override function addChild(child:DisplayObject):DisplayObject
		{
			throw new Error("Cannot add child to GameFrame");
		}
		
		public static function SetBackground(far:RenderableObject, middle:RenderableObject, near:RenderableObject, close:RenderableObject):void
		{
			instance.ambientAffected.addChildAt(far, 4);
			instance.ambientAffected.removeChild(instance.farBackground);
			instance.ambientAffected.addChildAt(middle, 5);
			instance.ambientAffected.removeChild(instance.forestBackground);
			instance.ambientAffected.addChildAt(near, 6);
			instance.ambientAffected.removeChild(instance.nearForestBackground);
			instance.ambientAffected.addChildAt(close, 8);
			instance.ambientAffected.removeChild(instance.closeBackground);
			instance.farBackground = far;
			instance.forestBackground = middle;
			instance.nearForestBackground = near;
			instance.closeBackground = close;
		}
		public static function CenterNightEffect(x:Number, y:Number):void
		{
			if (instance.nightEffect.alpha != 0)
			{
				instance.nightEffect.x = x;
				instance.nightEffect.y = y;
			}
		}
		public static function Blur():void { instance.BlurScreen(); }
		public static function Unblur():void { instance.UnblurScreen(); }
		public static function ShowDamage(x:Number, y:Number, color:uint, number:uint):void
		{
			if (number == 0) return;
			
			instance.damageNumber.addChild(new FloatingText(x, y, color, number.toString()));
			/*
			 * Flash will render 12 top most layers only. And These numbers can stack a lot more than that
			 * So, considering background images and stuffs too, it is limited to only 8 at a time.
			 * */
			while (instance.damageNumber.numChildren > 8)
				instance.damageNumber.removeChildAt(0);
		}
		public static function ShowExclam(x:Number, y:Number, color:uint):void
		{
			var text:FloatingText = new FloatingText(x, y, color, "!", 40, true, 3);
			instance.damageNumber.addChild(text);
			while (instance.damageNumber.numChildren > 8)
				instance.damageNumber.removeChildAt(0);
		}
		public static function ShowReload(x:Number, y:Number):void
		{
			var text:FloatingText = new FloatingText(x, y, 0xff0000, "RELOAD", 20, true, 1);
			instance.damageNumber.addChild(text);
			while (instance.damageNumber.numChildren > 8)
				instance.damageNumber.removeChildAt(0);
		}
		public static function get X():Number { return instance.x; }
		public static function get Y():Number { return instance.y; }
		public static function get ScreenLeft():Number { return -instance.x; }
		public static function get ScreenRight():Number { return -instance.x + instance.stage.stageWidth; }
		public static function get ScreenBottom():Number { return -instance.y + instance.stage.stageHeight; }
		public static function get ScreenTop():Number { return -instance.y; }
		public static function AddChild(mvc:IMVC):RenderableObject
		{
			GameSystem.AddGameObject(mvc);
			
			if (mvc is Monster) instance.healthBars.addChild((mvc as Monster).HealthBar);
			if (mvc.View is INegateAmbient)
			{
				instance.ignoreAmbient.push(mvc.View as INegateAmbient);
				instance.SetAmbient(instance.prevAmbient);
			}
			
			if (mvc is Terrain) return null; 
			else if (mvc is IOverlay)
			{
				if (!(mvc as IOverlay).BottomMost)
				{
					switch((mvc as IOverlay).OverlayLevel)
					{
						case Overlay.Location_FloorBackground: return instance.floorBackground.addChild(mvc.View) as RenderableObject;
						case Overlay.Location_FloorForeground: return instance.floorOverlay.addChild(mvc.View) as RenderableObject;
						case Overlay.Location_Foreground: return instance.foreground.addChild(mvc.View) as RenderableObject;
						case Overlay.Location_Floor: return instance.floor.addChild(mvc.View) as RenderableObject;
						case Overlay.Location_TopMost: return instance.top_most.addChild(mvc.View) as RenderableObject;
						case Overlay.Location_CharacterBackground: return instance.character_background.addChild(mvc.View) as RenderableObject;
						case Overlay.Location_Treasure : return instance.treasure.addChild(mvc.View) as RenderableObject;
						case Overlay.Location_CharacterForeground : return instance.character_foreground.addChild(mvc.View) as RenderableObject;
						case Overlay.Location_WeatherEffect: return instance.weatherEffect.addChild(mvc.View) as RenderableObject;
						case Overlay.Location_Character: return instance.character.addChild(mvc.View) as RenderableObject;
						default: throw new Error("Unknown overlay object: " + (mvc as Overlay).OverlayLevel);
					}
				}
				else
				{
					switch((mvc as IOverlay).OverlayLevel)
					{
						case Overlay.Location_FloorBackground: return instance.floorBackground.addChildAt(mvc.View, 0) as RenderableObject;
						case Overlay.Location_FloorForeground: return instance.floorOverlay.addChildAt(mvc.View, 0) as RenderableObject;
						case Overlay.Location_Foreground: return instance.foreground.addChildAt(mvc.View, 0) as RenderableObject;
						case Overlay.Location_Floor: return instance.floor.addChildAt(mvc.View, 0) as RenderableObject;
						case Overlay.Location_TopMost: return instance.top_most.addChildAt(mvc.View, 0) as RenderableObject;
						case Overlay.Location_CharacterBackground: return instance.character_background.addChildAt(mvc.View, 0) as RenderableObject;
						case Overlay.Location_Treasure: return instance.treasure.addChildAt(mvc.View, 0) as RenderableObject;
						case Overlay.Location_CharacterForeground: return instance.character_foreground.addChildAt(mvc.View, 0) as RenderableObject;
						case Overlay.Location_WeatherEffect: return instance.weatherEffect.addChildAt(mvc.View, 0) as RenderableObject;
						case Overlay.Location_Character: return instance.character.addChildAt(mvc.View, 0) as RenderableObject;
						default: throw new Error("Unknown overlay object: " + (mvc as Overlay).OverlayLevel);
					}
				}
				return null;
			}
			else if (mvc is Ladder) return instance.ladder.addChild(mvc.View) as RenderableObject;
			else if (mvc is Player || mvc is ILaunchable) return instance.character.addChild(mvc.View) as RenderableObject;
			else if (mvc is Monster) return instance.character_background.addChild(mvc.View) as RenderableObject;
			else if (mvc is BurnableTiles) return instance.character.addChild(mvc.View) as RenderableObject;
			else if (mvc is Tile)
			{
				if ((mvc as Tile).BottomMost)
					return instance.floor.addChildAt(mvc.View, 0) as RenderableObject;
				else
					return instance.floor.addChild(mvc.View) as RenderableObject;
			}
			else if (mvc is IInteractable || mvc is PressurePlatform) return instance.interactable.addChild(mvc.View) as RenderableObject;
			else if (mvc is Effect || mvc is EventTrigger) return instance.effects.addChild(mvc.View) as RenderableObject;
			else if (mvc is Weather) return instance.weatherEffect.addChild(mvc.View) as RenderableObject;
			else if (mvc is Item) return instance.items.addChild(mvc.View) as RenderableObject;
			else if (mvc is WaterBlock) return instance.waterBlock.addChild(mvc.View) as RenderableObject;
			else throw new Error("Unknown object type");
		}
		public static function ContainChild(mvc:IMVC):Boolean
		{
			if (mvc is Terrain)
			{
				for each( var t:Tile in (mvc as Terrain).Tiles)
				{
					if (ContainChild(t)) return true;
				}
				return false;
			}
			else if (mvc is IOverlay)
			{
				switch((mvc as IOverlay).OverlayLevel)
				{
					case Overlay.Location_FloorBackground: return instance.floorBackground.contains(mvc.View);
					case Overlay.Location_FloorForeground: return instance.floorOverlay.contains(mvc.View);
					case Overlay.Location_Foreground: return instance.foreground.contains(mvc.View);
					case Overlay.Location_Floor : return instance.floor.contains(mvc.View);
					case Overlay.Location_TopMost : return instance.top_most.contains(mvc.View);
					case Overlay.Location_CharacterBackground: return instance.character_background.contains(mvc.View);
					case Overlay.Location_Treasure: return instance.treasure.contains(mvc.View);
					case Overlay.Location_CharacterForeground: return instance.character_foreground.contains(mvc.View);
					case Overlay.Location_WeatherEffect: return instance.weatherEffect.contains(mvc.View);
					case Overlay.Location_Character: return instance.character.contains(mvc.View);
					default: throw new Error("Unknown overlay object: " + (mvc as Overlay).OverlayLevel);
				}
			}
			else if (mvc is Ladder) return instance.ladder.contains(mvc.View);
			else if (mvc is Player || mvc is ILaunchable) return instance.character.contains(mvc.View);
			else if (mvc is Monster) return instance.character_background.contains(mvc.View);
			else if (mvc is BurnableTiles) return instance.character.contains(mvc.View);
			else if (mvc is Tile) return instance.floor.contains(mvc.View);
			else if (mvc is IInteractable || mvc is PressurePlatform) return instance.interactable.contains(mvc.View);
			else if (mvc is Effect || mvc is EventTrigger) return instance.effects.contains(mvc.View);
			else if (mvc is Weather) return instance.weatherEffect.contains(mvc.View);
			else if (mvc is WaterBlock) return instance.waterBlock.contains(mvc.View);
			else if (mvc is Item) return instance.items.contains(mvc.View);
			else throw new Error("Unknown object type");
		}
		public static function RemoveChild(mvc:IMVC):RenderableObject
		{
			GameSystem.RemoveGameObject(mvc);
			
			if (mvc.View is INegateAmbient) for (var i:int = 0; i < instance.ignoreAmbient.length; i++)
			{
				if (instance.ignoreAmbient[i] == mvc.View)
				{
					instance.ignoreAmbient.splice(i, 1);
					break;
				}
			}
			
			if (mvc is Monster) instance.healthBars.removeChild((mvc as Monster).HealthBar); 
			
			if (mvc is Terrain) return null; 
			else if (mvc is IOverlay)
			{
				switch((mvc as IOverlay).OverlayLevel)
				{
					case Overlay.Location_FloorBackground: return instance.floorBackground.removeChild(mvc.View) as RenderableObject;
					case Overlay.Location_FloorForeground: return instance.floorOverlay.removeChild(mvc.View) as RenderableObject;
					case Overlay.Location_Foreground: return instance.foreground.removeChild(mvc.View) as RenderableObject;
					case Overlay.Location_Floor : return instance.floor.removeChild(mvc.View) as RenderableObject;
					case Overlay.Location_TopMost: return instance.top_most.removeChild(mvc.View) as RenderableObject;
					case Overlay.Location_CharacterBackground: return instance.character_background.removeChild(mvc.View) as RenderableObject;
					case Overlay.Location_Treasure: return instance.treasure.removeChild(mvc.View) as RenderableObject;
					case Overlay.Location_CharacterForeground: return instance.character_foreground.removeChild(mvc.View) as RenderableObject;
					case Overlay.Location_WeatherEffect: return instance.weatherEffect.removeChild(mvc.View) as RenderableObject;
					case Overlay.Location_Character: return instance.character.removeChild(mvc.View) as RenderableObject;
					default: throw new Error("Unknown overlay object: " + (mvc as Overlay).OverlayLevel);
				}
			}
			else if (mvc is Ladder) return instance.ladder.removeChild(mvc.View) as RenderableObject;
			else if (mvc is Player || mvc is ILaunchable) return instance.character.removeChild(mvc.View) as RenderableObject;
			else if (mvc is Monster) return instance.character_background.removeChild(mvc.View) as RenderableObject;
			else if (mvc is BurnableTiles) return instance.character.removeChild(mvc.View) as RenderableObject;
			else if (mvc is Tile) return instance.floor.removeChild(mvc.View) as RenderableObject;
			else if (mvc is IInteractable || mvc is PressurePlatform) return instance.interactable.removeChild(mvc.View) as RenderableObject;
			else if (mvc is Effect || mvc is EventTrigger) return instance.effects.removeChild(mvc.View) as RenderableObject;
			else if (mvc is Weather) return instance.weatherEffect.removeChild(mvc.View) as RenderableObject;
			else if (mvc is WaterBlock) return instance.waterBlock.removeChild(mvc.View) as RenderableObject;
			else if (mvc is Item) return instance.items.removeChild(mvc.View) as RenderableObject;
			else throw new Error("Unknown object type " + Object(mvc).constructor);
		}
		public static function Shake(vector:Point, durationFrame:uint, shake_decreaseRate:Number):void
		{
			if (instance.shakeVector != null)
			{
				if (instance.shakeVector.length > vector.length) return;
			}
			instance.shakeVector = vector;
			instance.shakeDuration = durationFrame;
			instance.shake_decreaseRate = shake_decreaseRate;
		}
		public static function SetCameraPosition(focusX:Number, focusY:Number):void
		{
			instance.focus.x = focusX;
			instance.x = instance.stage.stageWidth / 2 - focusX;
			instance.focus.y = focusY;
			instance.y = instance.stage.stageHeight / 2 - focusY;
		}
		public static function UpdateCamera(focusX:Number, focusY:Number):void
		{
			var distanceX:Number = focusX - instance.focus.x;
			var distanceY:Number = focusY - instance.focus.y;
			if (Math.abs(distanceX) > CameraSpeed)
			{
				instance.x -= distanceX / 3;
				instance.focus.x += distanceX / 3;
			}
			else if (focusX != instance.focus.x)
			{
				instance.focus.x = focusX;
				instance.x = instance.stage.stageWidth / 2 - focusX;
			}
			
			if (Math.abs(distanceY) > CameraSpeed / 10)
			{
				instance.focus.y += distanceY / 10;
				instance.y = instance.stage.stageHeight / 2 - instance.focus.y;
			}
			else if (focusY != instance.focus.y)
			{
				instance.focus.y = focusY;
				instance.y = instance.stage.stageHeight / 2 - focusY;
			}
		}
		public static function get AmbientStrength():Number { return instance.ambient_strength; }
		public static function get FocusY():Number { return instance.focus.y; }
		public static function get FocusX():Number { return instance.focus.x; }
		public static function get SkyLayer():Sky { return instance.sky; }
		public static function get FarBackground():RenderableObject { return instance.farBackground; }
		public static function set AmbientStrength(val:Number):void 
		{ 
			instance.ambient_strength = val; 
			instance.SetAmbient(instance.prevAmbient);
		}
		public static function ToScreenPosition(point:Point):Point
		{
			if (point == null) throw new ArgumentError();
			
			point.x += instance.x;
			point.y += instance.y;
			return point;
		}
		public static function Flash(startRed:uint, startGreen:uint, startBlue:uint, length:uint):void
		{
			instance.Flash(startRed, startGreen, startBlue, length);
		}
		public static function NegateAmbient():void
		{
			instance.SetAmbient(instance.prevAmbient);
		}
		private function SetAmbient(ambient:int):void
		{
			prevAmbient = ambient;
			var w:Weather = GameSystem.CurrentWeather;
			var red:Number = ((ambient & 0xff0000) >> 16) / 255.0;
			var green:Number = ((ambient & 0x00ff00) >> 8) / 255.0;
			var blue:Number = ((ambient & 0x0000ff) / 255.0);
			
			red *= ambient_strength;
			green *= ambient_strength;
			blue *= ambient_strength;
			
			var cTransform:ColorTransform = ambientAffected.transform.colorTransform;
			cTransform.redMultiplier = red;
			cTransform.greenMultiplier = green;
			cTransform.blueMultiplier = blue;
			ambientAffected.transform.colorTransform = cTransform;
			
			var avg:Number = (red + green + blue) / 3;
			cTransform = forestBackground.transform.colorTransform;
			cTransform.redMultiplier = avg;
			cTransform.greenMultiplier = avg;
			cTransform.blueMultiplier = avg;
			forestBackground.transform.colorTransform = cTransform;
			
			cTransform = nearForestBackground.transform.colorTransform;
			cTransform.redMultiplier = Math.min(avg * 2, 1);
			cTransform.greenMultiplier = Math.min(avg * 2, 1);
			cTransform.blueMultiplier = Math.min(avg * 2, 1);
			nearForestBackground.transform.colorTransform = cTransform;
			
			for each(var i:INegateAmbient in ignoreAmbient)
			{
				i.NegateAmbient(red, green, blue);
			}
			
			cTransform = effects.transform.colorTransform;
			cTransform.redMultiplier = 1 / red;
			cTransform.greenMultiplier = 1 / green;
			cTransform.blueMultiplier = 1 / blue;
			effects.transform.colorTransform = cTransform;
		}
		private var overlay:Bitmap;
		private function BlurScreen():void
		{
			//instance.filters = [new BlurFilter(20, 20, 1)]; 
			var data:BitmapData = new BitmapData(stage.stageWidth + 20, stage.stageWidth + 20);
			var matrix:Matrix = new Matrix();
			matrix.translate(X + 10, Y + 10);
			data.draw(instance, matrix, null, null, new Rectangle(-10, -10, stage.stageWidth + 20, stage.stageHeight + 20));
			if (overlay == null)
			{
				overlay = new Bitmap(data);
				overlay.filters = [new BlurFilter(10, 10, BitmapFilterQuality.HIGH)];
			}
			else
			{
				overlay.bitmapData = data;
			}
			overlay.x = -X - 10;
			overlay.y = -Y - 10;
			super.addChild(overlay);
		}
		private function UnblurScreen():void
		{
			if(instance.overlay != null && instance.contains(instance.overlay))
				instance.removeChild(instance.overlay);
		}
		private function UpdateSunAndMoon():void
		{
			var tick:int = Time.Tick;
			var hour:int = Time.Hour;
			var sun_y:Number = -y + 540;
			var moon_y:Number = -y + 540;
			if (hour >= 6 && hour < 12) sun_y = Regular.easeOut(tick - (6 * 3600), -y + 540, -390, 6 * 3600); 
			else if (hour >= 12 && hour < 18) sun_y = Regular.easeIn(tick - (12 * 3600), -y + 150, 390, 6 * 3600);
			else if (hour >= 18) moon_y = Regular.easeOut(tick - (18 * 3600), -y + 540, -390, 6 * 3600);
			else if (hour < 6) moon_y = Regular.easeIn(tick, -y + 150, 390, 6 * 3600);
			
			sun.x = -x + 300;
			sun.y = sun_y;
			moon.x = -x + 300;
			moon.y = moon_y;
			if (hour >= 19 && hour <= 22) star.alpha = Utility.Interpolate(1, 0, (tick - 68400) / 14400);
			else if (hour <= 6 && hour >= 3) star.alpha = Utility.Interpolate(1, 0, (tick - 10800) / 14400);
			else if (hour > 22 || hour < 3) star.alpha = 1;
			else star.alpha = 0;
			star.x = -x;
			star.y = -y;
		}
		private function onEnterFrame(e:Event):void
		{
			Update();
			GameSystem.FrameMoves();
			
			if (GameSystem.IsFreezing) return;
			
			var ambient:uint = Time.Ambient;
			if (prevAmbient != ambient)
			{
				SetAmbient(ambient);
			}
			UpdateSunAndMoon();
			sky.Update();
			farBackground.Update();
			nearForestBackground.Update();
			forestBackground.Update();
			var nightMaskAlpha:Number = Time.NightMaskVisibility;
			if (Math.abs(nightMaskAlpha - nightEffect.alpha) > 0.025)
				nightEffect.alpha = nightMaskAlpha;
			if (shakeVector != null)
			{
				var centerX:Number = -(focus.x - stage.stageWidth / 2);
				var centerY:Number = -(focus.y - stage.stageHeight / 2);
				
				if (shakeDuration == 0)
				{
					shakeVector = null;
					x = centerX;
					y = centerY;
					shake_decreaseRate = 0;
				}
				else
				{
					if (x > centerX) x = centerX - shakeVector.x;
					else x = centerX + shakeVector.x;
					if (y > centerY) y = centerY - shakeVector.y;
					else y = centerY + shakeVector.y;
					shakeVector.x *= shake_decreaseRate;
					shakeVector.y *= shake_decreaseRate;
					shakeDuration--;
				}
			}
			sky.x = -x;
			sky.y = -y;
			farBackground.x = -x / 1.5;
			//farBackground.y = -y / 1.5;
			forestBackground.x = -x / 2;
			//forestBackground.y = -y / 2;
			nearForestBackground.x = -x / 4;
			//nearForestBackground.y = -y * 2 / 5;
			
		}
		private function onAddToStage(e:Event):void
		{
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
			removeEventListener(Event.ADDED_TO_STAGE, onAddToStage);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoveFromStage);
			focus.x = x + stage.stageWidth / 2;
			focus.y = y + stage.stageHeight / 2;
			GameSystem.Initialize(stage);
			if(GameSystem.CurrentLevel.ShowPauseMenu)
				addEventListener(Event.DEACTIVATE, onDeactivated);
				
			SetAmbient(Time.Ambient);
		}
		private function onRemoveFromStage(e:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemoveFromStage);
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			addEventListener(Event.ADDED_TO_STAGE, onAddToStage);
			focus.x = 0;
			focus.y = 0;
			x = 0;
			y = 0;
			sky.x = 0;
			sky.y = 0;
			farBackground.x = 0;
			forestBackground.x = 0;
			forestBackground.y = 0;
			nearForestBackground.x = 0;
			nearForestBackground.y = 0;
			if(GameSystem.CurrentLevel.ShowPauseMenu)
				removeEventListener(Event.DEACTIVATE, onDeactivated);
			GameSystem.Flush();
			var toClear:Vector.<RenderableObject> = Vector.<RenderableObject>([character, items, effects, floor, treasure, character_foreground, 
				interactable, healthBars, floorOverlay, damageNumber, foreground, character_background, ladder, weatherEffect, floorBackground]);
			for each(var ren:RenderableObject in toClear)
				ren.removeChildren();
			
			ignoreAmbient = Vector.<INegateAmbient>([moon]);
		}
		private function onDeactivated(e:Event):void
		{
			if (!GUI.IsShowingPopup && !GamePage.IsFadingOut)
			{
				GUI.ShowPauseMenu();
			}
		}
	}
}