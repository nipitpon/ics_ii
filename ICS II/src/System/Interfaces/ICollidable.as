package System.Interfaces 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import System.MC.Collision.CollisionBox;
	
	/**
	 * ...
	 * @author ...
	 */
	public interface ICollidable extends IMVC
	{
		function get Collision():CollisionBox;
		function MoveCollisionBoxByOffset(x:int, y:int):void;
		function Intersects(box:CollisionBox, movement:Point):ICollidable;
		function IntersectsLine(x1:Number, x2:Number, y1:Number, y2:Number, movement:Point):ICollidable;
		function IntersectsPoint(point:Point, movement:Point):ICollidable;
		function TakenHit(obj:ICollidable, movement:Point):void;
		function Hit(obj:ICollidable, movement:Point):void;
		function CollideWith(c:*):Boolean;
	}
}