package System.Interfaces 
{
	import flash.events.IEventDispatcher;
	import System.MC.States.CharacterState_Idle;
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface ISkill extends IEventDispatcher
	{
		function Activate():void;
		function Deactivate():void;
		function get UsingAnimation():int;
		function get MPRequired():uint;
		function get AmmoRequired():uint;
		function get Range():uint;
		function get IsDeactivated():Boolean;
		function get IsPassive():Boolean;
		function get HasAnimation():Boolean;
		function get AllowMovement():Boolean;
	}
}