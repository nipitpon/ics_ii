package System.Interfaces 
{
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface INegateAmbient 
	{
		function NegateAmbient(red:Number, green:Number, blue:Number):void;
	}
	
}