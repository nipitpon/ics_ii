package System.Interfaces 
{
	import flash.geom.Point;
	import System.MC.Collision.CollisionBox;
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface IItemCollector extends IStatusHolder, IMovable
	{
		function IntersectsCollectionRadius(box:CollisionBox, movement:Point):Boolean;
		function get CollectionCenter():Point;
	}
	
}