package System.Interfaces 
{
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface IHaveCooldown extends ISkill
	{
		function CountCooldown():void;
		function get Cooldown():uint;
		function get CurrentCount():uint;
		function get IsHavingCooldown():Boolean;
	}
	
}