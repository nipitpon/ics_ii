package System.Interfaces 
{
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface IDescriptor 
	{
		function SetText(text:String):void;
		function ClearText():void;
	}
	
}