package System.Interfaces 
{
	import flash.geom.Point;
	import System.MC.Collision.CollisionBox;
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface IInteractor extends IStatusHolder, IMovable
	{
		function IntersectsInteractionRadius(box:CollisionBox, movement:Point):Boolean;
		function get InteractionCenter():Point;
		function get IsInteractable():Boolean;
	}
	
}