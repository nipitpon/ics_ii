package System.Interfaces 
{
	
	/**
	 * ...
	 * @author ss
	 */
	public interface IBurnable extends ICollidable
	{
		function get IsBurning():Boolean;
		function Burn():void;
	}
	
}