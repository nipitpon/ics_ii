package System.Interfaces 
{
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface IMovable extends IMVC
	{
		function CalculateMovement():Point;
		function get DefyGravity():Boolean;
		function Push(x:Number, y:Number):void;
		function get ForceX():Number;
		function get ForceY():Number;
		function PushWithDecay(x:Number, y:Number, decayRate:Number):void;
		function ClearAllForce():void;
		function ClearForceX():void;
		function ClearForceY():void;
	}
}