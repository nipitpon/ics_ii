package System.Interfaces 
{
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface IReceiveControl 
	{
		function KeyPress(id:int):void;
		function KeyRelease(id:int):void;
		function MousePress(x:Number, y:Number):void;
		function MouseMove(x:Number, y:Number):void;
		function MouseRelease(x:Number, y:Number):void;
		function get IsReceivingControl():Boolean;
	}
}