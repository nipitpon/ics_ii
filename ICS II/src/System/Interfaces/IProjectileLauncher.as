package System.Interfaces 
{
	import flash.geom.Point;
	import System.MC.etc.ForceStruct;
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface IProjectileLauncher 
	{
		function get ReleasePoint():Point;
		function get ProjectileHoming():Boolean;
		function set ProjectileHoming(val:Boolean):void;
		function ReleaseProjectile(type:Class, angleOffset:Number, pushBack:ForceStruct):Boolean;
	}
}