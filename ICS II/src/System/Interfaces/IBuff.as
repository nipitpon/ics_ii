package System.Interfaces 
{
	import flash.events.IEventDispatcher;
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface IBuff extends IEventDispatcher, IUpdatable
	{
		function Activate():void;
		function Deactivate():void;
		function Reset():void;
		function get Duration():uint;
		function get DurationCount():uint;
		function get Owner():IBuffHolder;
		function get Code():int;
	}
	
}