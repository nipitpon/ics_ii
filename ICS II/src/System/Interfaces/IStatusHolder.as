package System.Interfaces 
{
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface IStatusHolder extends ICollidable
	{
		function get HP():Number;
		function get MaxHP():Number;
		function get ATK():Number;
		function get DEF():Number;
		function get VIT():Number;
		function get MaxMP():Number;
		function get MP():Number;
		function get IsAlive():Boolean;
		function CalculateDamage(base:Number):uint;
		function TakeDamage(damage:uint, movement:Point, crit:Boolean, position:Point = null):uint;
		function Heal(val:uint):void;
		function IncreaseMP(val:int):void;
		function get IsVulnerable():Boolean;
		function get Stompable():Boolean;
		function get Side():Class;
		function AddAdditionalATK(val:int):void;
		function AddAdditionalDEF(val:int):void;
		function AddAdditionalVIT(val:int):void;
		function get AdditionalATK():int;
		function get AdditionalDEF():int;
		function get AdditionalVIT():int;
		function get IsInvincible():Boolean;
	}
}