package System.Interfaces 
{
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface IOverlay extends IMVC
	{
		
		function set BottomMost(val:Boolean):void;
		function get BottomMost():Boolean;
		function get OverlayLevel():int;
	}
	
}