package System.Interfaces 
{
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface IBuffHolder extends IMVC
	{
		function AddBuff(buff:Class):IBuff;
		function RemoveBuff(buff:Class):IBuff;
	}
	
}