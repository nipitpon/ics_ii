package System.Interfaces 
{
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface IItem extends ICollidable, IUpdatable
	{
		function GetCollected(collector:IItemCollector):void;
		function get Collectable():Boolean;
	}
	
}