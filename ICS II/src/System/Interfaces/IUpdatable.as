package System.Interfaces 
{
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface IUpdatable
	{
		function Update():void;
		function get UpdateWhenAdvancingTime():Boolean;
	}
	
}