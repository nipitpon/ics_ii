package System.Interfaces 
{
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface INamable
	{
		function get Name():String;
	}
	
}