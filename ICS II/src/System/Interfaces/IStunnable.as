package System.Interfaces 
{
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface IStunnable 
	{
		function Stun(val:uint):void;
		function get IsStunned():Boolean;
		function get IsStunnable():Boolean;
		function Unstun():void;
	}
	
}