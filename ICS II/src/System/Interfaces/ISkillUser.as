package System.Interfaces 
{
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface ISkillUser 
	{
		function PassRequirement(skill:ISkill):Boolean;
		function UseSkill(skill:ISkill):void;
	}
	
}