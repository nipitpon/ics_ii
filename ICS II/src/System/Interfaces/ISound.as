package System.Interfaces 
{
	import flash.events.IEventDispatcher;
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface ISound extends IEventDispatcher
	{
		function Play(loop:int = 0, fadeInDuration:int = 0, start_location:int = -1):void;
		function Stop(fadeDuration:int = 0):void;
		function set Volume(val:int):void;
		function get Volume():int;
		function get IsPlaying():Boolean;
		function get PauseWithGame():Boolean;
		function Pause():void;
		function Resume():void;
		function get IsPausing():Boolean;
	}
}