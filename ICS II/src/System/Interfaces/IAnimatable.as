package System.Interfaces 
{
	import flash.display.IBitmapDrawable;
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface IAnimatable extends IBitmapDrawable, IUpdatable
	{
		function SetAnimation(id:int):void;
		function get CurrentAnimation():int;
		function set Freeze(val:Boolean):void;
		function get Freeze():Boolean;
		function get DefaultAnimation():int;
		function ResetAnimation():void;
		function HasAnimation(id:int):Boolean;
	}
}