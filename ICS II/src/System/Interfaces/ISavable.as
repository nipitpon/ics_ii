package System.Interfaces 
{
	
	/**
	 * ...
	 * @author ss
	 */
	public interface ISavable
	{
		function GetInfo():Object;
		function SetInfo(info:Object):void;
		function set ID(val:int):void;
		function get ID():int;
	}
}