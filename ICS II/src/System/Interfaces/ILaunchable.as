package System.Interfaces 
{
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface ILaunchable extends IMovable, ITurnable, ICollidable
	{
		function get Speed():Number;
		function get Range():Number;
		function LaunchAt(x:Number, y:Number):void;
		function SetTarget(target:ICollidable):void;
		function get Solidity():int;
	}
	
}