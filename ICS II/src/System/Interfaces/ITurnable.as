package System.Interfaces 
{
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface ITurnable 
	{
		function TurnLeft():void;
		function TurnRight():void;
		function get IsTurningLeft():Boolean;
		function get IsTurningRight():Boolean;
	}
	
}