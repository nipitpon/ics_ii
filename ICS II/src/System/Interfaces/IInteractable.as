package System.Interfaces 
{
	import System.MC.Collision.CollisionBox;
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface IInteractable extends ICollidable
	{
		function get IsInteractable():Boolean;
		function GetInteracted(interactor:IInteractor):void;
		function get Caption():String;
		function Highlight(stay_on_forever:Boolean):void;
		function Unhighlight():void;
	}
	
}