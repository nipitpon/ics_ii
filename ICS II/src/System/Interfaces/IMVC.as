package System.Interfaces 
{
	import flash.events.IEventDispatcher;
	import flash.geom.Point;
	import System.MC.Collision.CollisionBox;
	import System.MC.Effects.Effect;
	import System.MC.ModelViewController;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public interface IMVC extends IEventDispatcher 
	{
		function set Center(point:Point):void;
		function set Left(val:Number):void;
		function set Right(val:Number):void;
		function set Top(val:Number):void;
		function set Bottom(val:Number):void;
		
		function get Center():Point;
		function get Top():Number;
		function get Bottom():Number;
		function get Left():Number;
		function get Right():Number;
		function get View():RenderableObject;
		function get Active():Boolean;
		function get CenterX():Number;
		function get CenterY():Number;
		function MoveByOffset(x:int, y:int):void;
	}
}