package System 
{
	/**
	 * ...
	 * @author Knight
	 */
	public class ExecutionCode 
	{
		private static const Caller_GameSystem:int = 0x10;
		public static const Skill_StartTestLevel:int = 0x00 | Caller_GameSystem;
		public static const Skill_Level1:int = 0x01 | Caller_GameSystem;
		public static const Skill_Level2:int = 0x03 | Caller_GameSystem;
		public static const Skill_StartSurvival:int = 0x02 | Caller_GameSystem;
		public static const Skill_StartTimeAttack:int = 0x04 | Caller_GameSystem;
		
		public function ExecutionCode() 
		{
			throw new Error("Cannot instantiate ExecutionCode");
		}
		
	}

}