package System 
{
	//import com.newgrounds.//API;
	import Resources.lib.Sound.SFX.AchievementUnlockedSound;
	import System.MC.etc.DeathReason;
	/**
	 * ...
	 * @author Knight
	 */
	public final class Achievements 
	{
		public function Achievements() 
		{
			
		}
		public static function UseFirePotion():void
		{
			//if (DataCenter.FirePotionUsed == Counts[25] && !DataCenter.IsAchievementUnlocked(25))
			//{
			//	DataCenter.UnlockAchievement(25);
			//	GamePage.ShowAchievement(25);
			//	SoundCenter.PlaySFX(AchievementUnlockedSound);
			//}
		}
		public static function VisitSponsor():void
		{
			if (!DataCenter.IsAchievementUnlocked(29))
			{
				DataCenter.UnlockAchievement(29);
				GamePage.ShowAchievement(29);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Visit sponsor page");
			}
		}
		public static function VisitCreditPage():void
		{
			if (!DataCenter.IsAchievementUnlocked(28))
			{
				DataCenter.UnlockAchievement(28);
				GamePage.ShowAchievement(28);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Visit credit page");
			}
		}
		public static function CollectOneSecretItem():void
		{
			var count:int = 0;
			for (var i:int = 1; i < 51; i++)
			{
				if (DataCenter.SecretItemCollected(i))
					count++;
			}
			if (count == 10 && !DataCenter.IsAchievementUnlocked(22))
			{
				DataCenter.UnlockAchievement(22);
				GamePage.ShowAchievement(22);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Collect 10 treasure chests");
			}
			else if (count == 20 && !DataCenter.IsAchievementUnlocked(23))
			{
				DataCenter.UnlockAchievement(23);
				GamePage.ShowAchievement(23);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Collect 20 treasure chests");
			}
			else if (count == 30 && !DataCenter.IsAchievementUnlocked(24))
			{
				DataCenter.UnlockAchievement(24);
				GamePage.ShowAchievement(24);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Collect 30 treasure chests");
			}
			else if (count == 40 && !DataCenter.IsAchievementUnlocked(25))
			{
				DataCenter.UnlockAchievement(25);
				GamePage.ShowAchievement(25);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Collect 40 treasure chests");
			}
			else if (count == 50 && !DataCenter.IsAchievementUnlocked(26))
			{
				DataCenter.UnlockAchievement(26);
				GamePage.ShowAchievement(26);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Collect every treasure chests");
			}
		}
		public static function BurnGoat():void
		{
			if (DataCenter.GoatBurnt >= 5 && !DataCenter.IsAchievementUnlocked(14))
			{
				DataCenter.UnlockAchievement(14);
				GamePage.ShowAchievement(14);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Burn 5 monsters");
			}
			else if (DataCenter.GoatBurnt >= 20 && !DataCenter.IsAchievementUnlocked(15))
			{ 
				DataCenter.UnlockAchievement(15);
				GamePage.ShowAchievement(15);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Burn 20 monsters");
			}
			else if (DataCenter.GoatBurnt >= 50 && !DataCenter.IsAchievementUnlocked(16))
			{
				DataCenter.UnlockAchievement(16);
				GamePage.ShowAchievement(16);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Burn 50 monsters");
			}
		}
		public static function PassLevel():void
		{
			var count:int = 0;
			var last_10_die:Boolean = false;
			for (var i:int = 1; i <= 50; i++)
			{
				if (DataCenter.GetDeathCount(i) >= 0)
				{
					count++;
				}
				if (i >= 20 && DataCenter.GetDeathCount(i) != 0)
				{
					last_10_die = true;
				}
			}
			if (count >= 5 && !DataCenter.IsAchievementUnlocked(0))
			{
				DataCenter.UnlockAchievement(0);
				GamePage.ShowAchievement(0);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Finish Level 5");
			}
			if (count >= 10 && !DataCenter.IsAchievementUnlocked(1))
			{
				DataCenter.UnlockAchievement(1);
				GamePage.ShowAchievement(1);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Finish Level 10");
			}
			if (count >= 15 && !DataCenter.IsAchievementUnlocked(2))
			{
				DataCenter.UnlockAchievement(2);
				GamePage.ShowAchievement(2);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Finish Level 15");
			}
			if (count >= 20 && !DataCenter.IsAchievementUnlocked(3))
			{
				DataCenter.UnlockAchievement(3);
				GamePage.ShowAchievement(3);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Finish Level 20");
			}
			if (count >= 25 && !DataCenter.IsAchievementUnlocked(4))
			{
				DataCenter.UnlockAchievement(4);
				GamePage.ShowAchievement(4);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Finish Level 25");
			}
			if (count >= 30 && !DataCenter.IsAchievementUnlocked(5))
			{
				DataCenter.UnlockAchievement(5);
				GamePage.ShowAchievement(5);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Finish Level 30");
			}
			if (count >= 35 && !DataCenter.IsAchievementUnlocked(6))
			{
				DataCenter.UnlockAchievement(6);
				GamePage.ShowAchievement(6);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Finish Level 35");
			}
			if (count >= 40 && !DataCenter.IsAchievementUnlocked(7))
			{
				DataCenter.UnlockAchievement(7);
				GamePage.ShowAchievement(7);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Finish Level 40");
			}
			if (count >= 45 && !DataCenter.IsAchievementUnlocked(8))
			{
				DataCenter.UnlockAchievement(8);
				GamePage.ShowAchievement(8);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Finish Level 45");
			}
			if (count >= 50 && !DataCenter.IsAchievementUnlocked(9))
			{
				DataCenter.UnlockAchievement(9);
				GamePage.ShowAchievement(9);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Finish Level 50");
			}
			
			if (GameSystem.CurrentGameMode == 55 && !DataCenter.IsAchievementUnlocked(27))
			{
				DataCenter.UnlockAchievement(27);
				GamePage.ShowAchievement(27);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Finish special level");
			}
		}
		public static function PlayerDie():void
		{
			if (DataCenter.TotalDeath == 1 && !DataCenter.IsAchievementUnlocked(17))
			{
				DataCenter.UnlockAchievement(17);
				GamePage.ShowAchievement(17);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Die once");
			}
			else if (DataCenter.TotalDeath == 10 && !DataCenter.IsAchievementUnlocked(18))
			{
				DataCenter.UnlockAchievement(18);
				GamePage.ShowAchievement(18);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Die 10 times");
			}
			else if (DataCenter.TotalDeath == 50 && !DataCenter.IsAchievementUnlocked(19))
			{
				DataCenter.UnlockAchievement(19);
				GamePage.ShowAchievement(19);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Die 50 times");
			}
			
			if (GameSystem.DeathStreak >= 10 && !DataCenter.IsAchievementUnlocked(20))
			{
				DataCenter.UnlockAchievement(20);
				GamePage.ShowAchievement(20);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Die 10 times in a single level");
			}
			
			if (DataCenter.LastDeathReason == DeathReason.Spike && GameSystem.PlayerInstance.IsBurning && !DataCenter.IsAchievementUnlocked(21))
			{
				DataCenter.UnlockAchievement(21);
				GamePage.ShowAchievement(21);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Die on spike while burning");
			}
		}
		public static function FrameMove():void
		{
			DataCenter.FrameMove();
			if (DataCenter.CurrentTimeTick == Utility.SecondToFrame(60 * 15) && !DataCenter.IsAchievementUnlocked(10))
			{
				DataCenter.UnlockAchievement(10);
				GamePage.ShowAchievement(10);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Play 15 minutes");
			}
			else if (DataCenter.CurrentTimeTick == Utility.SecondToFrame(60 * 30) && !DataCenter.IsAchievementUnlocked(11))
			{
				DataCenter.UnlockAchievement(11);
				GamePage.ShowAchievement(11);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Play 30 minutes");
			}
			else if (DataCenter.CurrentTimeTick == Utility.SecondToFrame(60 * 45) && !DataCenter.IsAchievementUnlocked(12))
			{
				DataCenter.UnlockAchievement(12);
				GamePage.ShowAchievement(12);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Play 45 minutes");
			}
			else if (DataCenter.CurrentTimeTick == Utility.SecondToFrame(60 * 60) && !DataCenter.IsAchievementUnlocked(13))
			{
				DataCenter.UnlockAchievement(13);
				GamePage.ShowAchievement(13);
				SoundCenter.PlaySFX(AchievementUnlockedSound);
				//API.unlockMedal("Play 60 minutes");
			}
		}
		private static function SubmitAward(tag:String):void
		{
		}
	}
}
