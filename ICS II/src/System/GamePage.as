package System 
{
	import _Base.MySprite;
	import fl.transitions.easing.None;
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.text.TextField;
	import HardCode.GUI.Sponsor300x80;
	import HardCode.Pages.Achievements.AchievementPage;
	import HardCode.Pages.ClearData.ClearDataPage;
	import HardCode.Pages.CreditsPage.CreditPage;
	import HardCode.Pages.Game.GameOver.GameOver;
	import HardCode.Pages.LevelSelection.LevelSelectionPage;
	import HardCode.Pages.MainMenuSettings.MainSettings;
	import HardCode.Pages.SettingsPage.GameSettings;
	import Resources.lib.GUI.AchievementPopupImage;
	import System.Interfaces.IReceiveControl;
	import System.MC.FlagPosition;
	/**
	 * ...
	 * @author Knight
	 */
	public class GamePage extends Pages implements IReceiveControl
	{
		private var gameFrame:GameFrame;
		private var gui:GUI;
		private var main_settings:MainSettings;
		private var settings:GameSettings;
		private static var instance:GamePage;
		private var gameOver:GameOver;
		private var fade_layer:Shape;
		private var clear_data:ClearDataPage;
		private var unfreeze_when_close:Boolean;
		private var achievements:AchievementPage;
		private var select_level:LevelSelectionPage;
		private var credit:CreditPage;
		private var ach_popup:AchievementPopupImage;
		private var sponsor:Sponsor300x80;
		private var popup_queue:Array;
		public function GamePage() 
		{
			instance = this;
			gameFrame = new GameFrame();
			popup_queue = new Array();
			gui = new GUI();
			settings = new GameSettings(this, false);
			main_settings = new MainSettings(this);
			select_level = new LevelSelectionPage();
			achievements = new AchievementPage();
			credit = new CreditPage();
			gameOver = new GameOver();
			fade_layer = new Shape();
			ach_popup = new AchievementPopupImage();
			sponsor = new Sponsor300x80();
			clear_data = new ClearDataPage();
			ach_popup.addEventListener(AchievementPopupImage.FadedOut, onPopupFadedOut);
			
			Initialize();
		}
		public override function get IsShowingPopup():Boolean 
		{ 
			return settings.visible || gameOver.visible || contains(select_level) || contains(credit) || contains(achievements) ||
				IsShowingMainSettings || contains(clear_data); 
		}
		public function get IsShowingMainSettings():Boolean { return contains(main_settings); }
		public static function AddTop(obj:MySprite):void
		{
			instance.addChild(obj);
		}		
		public static function Remove(obj:MySprite):void
		{
			if(instance.contains(obj))
				instance.removeChild(obj);
		}
		public override function get IsReceivingControl():Boolean { return true; }
		public override function KeyPress(code:int):void
		{
			if (code == ControlCenter.Pause)
			{
				if (!IsShowingPopup && GameSystem.CurrentLevel.ShowPauseMenu)
				{
					GUI.ShowPauseMenu();
				}
				else if (IsShowingPopup)
				{
					if (settings.visible && settings.alpha == 1)
						CloseSettingsPopup();
					if (contains(select_level) && select_level.alpha == 1)
						CloseLevelSelection();
					if (contains(credit) && credit.alpha == 1)
						CloseCredit();
					if (contains(achievements) && achievements.alpha == 1)
						CloseAchievements();
					if (contains(main_settings) && main_settings.alpha == 1)
						CloseMainSettingsPopup(true);
					if (contains(clear_data) && clear_data.alpha == 1)
						CloseClearData(true);
				}
			}
		}
		public override function Start():void
		{
			super.Start();
			if (contains(select_level))
				CloseLevelSelection();
		}
		public override function KeyRelease(code:int):void
		{}
		public override function MousePress(x:Number, y:Number):void
		{}
		public override function MouseRelease(x:Number, y:Number):void
		{}
		public override function MouseMove(x:Number, y:Number):void
		{}
		public override function addChild(obj:DisplayObject):DisplayObject
		{
			var result:DisplayObject = super.addChild(obj);
			if (ach_popup.IsActive)
				swapChildren(obj, ach_popup);
			return result;
		}
		public static function get IsFadingOut():Boolean { return instance.fade_layer.visible; }
		public static function ShowGameOver(dam:uint, position:Point):void
		{
			instance.fade_layer.visible = false;
			instance.gameOver.Show(dam, position);
		}
		public static function FinishVanilla():void
		{
			DataCenter.SetFlag(FlagPosition.VanillaFinished, true);
			DataCenter.ClearSaveData(false);
			instance.FadeOut(instance.onVanillaFinished);
		}
		public static function HideGameOver():void
		{
			instance.gameOver.Hide();
		}
		public static function OpenCredit(freezeGame:Boolean):void
		{
			Achievements.VisitCreditPage();
			instance.unfreeze_when_close = freezeGame;
			if (freezeGame)
				GameSystem.Freeze();
			instance.credit.alpha = 0;
			instance.addChild(instance.credit);
			instance.swapChildren(instance.credit, instance.sponsor.View);
			var tween:Tween = new Tween(instance.credit, "alpha", None.easeNone, 0, 1, Utility.SecondToFrame(0.2), false);
			tween.start();
			instance.sponsor.View.visible = false;
		}
		public static function CloseCredit():void
		{
			var tween:Tween = new Tween(instance.credit, "alpha", None.easeNone, 1, 0, Utility.SecondToFrame(0.2), false);
			tween.start();
			tween.addEventListener(TweenEvent.MOTION_FINISH, function(e:Event):void
			{
				instance.removeChild(instance.credit);
				instance.stage.focus = instance;
				instance.sponsor.View.visible = true;
			});
			if (instance.unfreeze_when_close)
				GameSystem.Unfreeze();
		}
		public static function OpenClearData():void
		{
			instance.select_level.alpha = 0;
			instance.addChild(instance.clear_data);
			instance.swapChildren(instance.clear_data, instance.sponsor.View);
			var tween:Tween = new Tween(instance.clear_data, "alpha", None.easeNone, 0, 1, Utility.SecondToFrame(0.2), false);
			tween.start();
		}
		public static function CloseClearData(unfreeze:Boolean):void
		{
			var tween:Tween = new Tween(instance.clear_data, "alpha", None.easeNone, 1, 0, Utility.SecondToFrame(0.2), false);
			tween.start();
			tween.addEventListener(TweenEvent.MOTION_FINISH, function(e:Event):void
			{
				instance.removeChild(instance.clear_data);
				instance.stage.focus = instance;
			});
			if(unfreeze)
				GameSystem.Unfreeze();
		}
		public static function OpenLevelSelection(freezeGame:Boolean):void
		{
			instance.unfreeze_when_close = freezeGame;
			if (freezeGame)
				GameSystem.Freeze();
			instance.select_level.alpha = 0;
			instance.addChild(instance.select_level);
			instance.swapChildren(instance.select_level, instance.sponsor.View);
			var tween:Tween = new Tween(instance.select_level, "alpha", None.easeNone, 0, 1, Utility.SecondToFrame(0.2), false);
			tween.start();
		}
		public static function CloseLevelSelection():void
		{
			var tween:Tween = new Tween(instance.select_level, "alpha", None.easeNone, 1, 0, Utility.SecondToFrame(0.2), false);
			tween.start();
			tween.addEventListener(TweenEvent.MOTION_FINISH, function(e:Event):void
			{
				if(instance.contains(instance.select_level))
					instance.removeChild(instance.select_level);
				instance.stage.focus = instance;
			});
			if (instance.unfreeze_when_close)
				GameSystem.Unfreeze();
		}
		public static function OpenAchievements(freezeGame:Boolean):void
		{
			instance.unfreeze_when_close = freezeGame;
			if (freezeGame)
				GameSystem.Freeze();
			instance.achievements.alpha = 0;
			instance.addChild(instance.achievements);
			instance.swapChildren(instance.achievements, instance.sponsor.View);
			var tween:Tween = new Tween(instance.achievements, "alpha", None.easeNone, 0, 1, Utility.SecondToFrame(0.2), false);
			tween.start();
			instance.sponsor.View.visible = false;
		}
		public static function CloseAchievements():void
		{
			var tween:Tween = new Tween(instance.achievements, "alpha", None.easeNone, 1, 0, Utility.SecondToFrame(0.2), false);
			tween.start();
			tween.addEventListener(TweenEvent.MOTION_FINISH, function(e:Event):void
			{
				instance.removeChild(instance.achievements);
				instance.stage.focus = instance;
				instance.sponsor.View.visible = true;
			});
			if (instance.unfreeze_when_close)
				GameSystem.Unfreeze();
		}
		public static function ShowAchievement(id:int):void
		{
			if (instance.ach_popup.IsActive)
				instance.popup_queue.push([instance.ach_popup.ShowAchievement, id]);
			else
				instance.ach_popup.ShowAchievement(id);
		}
		public static function ShowSecretItem(level_minus_one:int):void
		{
			if (instance.ach_popup.IsActive)
				instance.popup_queue.push([instance.ach_popup.ShowSecretItem, level_minus_one]);
			else
				instance.ach_popup.ShowSecretItem(level_minus_one);
		}
		public static function OpenMainSettingsPopup(freezeGame:Boolean):void
		{
			instance.unfreeze_when_close = freezeGame;
			if (freezeGame)
				GameSystem.Freeze();
			instance.addChild(instance.main_settings);
			instance.swapChildren(instance.main_settings, instance.sponsor.View);
			var tween:Tween = new Tween(instance.main_settings, "alpha", None.easeNone, 0, 1, Utility.SecondToFrame(0.2), false);
			tween.start();
		}
		public static function CloseMainSettingsPopup(unfreeze:Boolean):void
		{
			var tween:Tween = new Tween(instance.main_settings, "alpha", None.easeNone, 1, 0, Utility.SecondToFrame(0.2), false);
			tween.start();
			tween.addEventListener(TweenEvent.MOTION_FINISH, function(e:Event):void
			{
				instance.removeChild(instance.main_settings);
				instance.stage.focus = instance;
			});
			DataCenter.SaveSettings(SoundCenter.BGMVolume, SoundCenter.SFXVolume);
			if (instance.unfreeze_when_close && unfreeze)
				GameSystem.Unfreeze();
		}
		public static function OpenSettingsPopup(freezeGame:Boolean = false):void
		{
			instance.unfreeze_when_close = freezeGame;
			if (freezeGame)
				GameSystem.Freeze();
			instance.settings.visible = true;
			var tween:Tween = new Tween(instance.settings, "alpha", None.easeNone, 0, 1, Utility.SecondToFrame(0.2), false);
			tween.start();
		}
		public static function CloseSettingsPopup():void
		{
			var tween:Tween = new Tween(instance.settings, "alpha", None.easeNone, 1, 0, Utility.SecondToFrame(0.2), false);
			tween.start();
			tween.addEventListener(TweenEvent.MOTION_FINISH, instance.onSettingsFadedOut);
			DataCenter.SaveSettings(SoundCenter.BGMVolume, SoundCenter.SFXVolume);
			if (instance.unfreeze_when_close)
				GameSystem.Unfreeze();
		}
		public static function ShowPauseMenu():void
		{
			instance.sponsor.CenterX = 360;
			instance.sponsor.Bottom = 520;
		}
		public static function HidePauseMenu():void
		{
			instance.sponsor.Right = 720;
			instance.sponsor.Top = 0;
			
		}
		private function FadeOut(func:Function):void
		{
			fade_layer.visible = true;
			fade_layer.alpha = 0;
			var tween:Tween = new Tween(fade_layer, "alpha", None.easeNone, 0, 1, Utility.SecondToFrame(5));
			tween.addEventListener(TweenEvent.MOTION_FINISH, func);
			tween.start();
		}
		private function onVanillaFinished(e:Event):void
		{
			(e.currentTarget as Tween).removeEventListener(TweenEvent.MOTION_FINISH, onVanillaFinished);
			Main.ChangeToFinishVanilla();
		}
		protected override function onAddedToStage(e:Event):void
		{
			super.onAddedToStage(e);
			fade_layer.graphics.clear();
			fade_layer.visible = false;
			fade_layer.graphics.beginFill(0xffffff);
			fade_layer.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
			//DataCenter.LoadGame();
			ControlCenter.AddGameObject(sponsor);
		}
		protected override function onRemovedFromStage(e:Event):void
		{
			super.onRemovedFromStage(e);
			gameOver.Hide();
			ControlCenter.RemoveGameObject(sponsor);
		}
		private function onPopupFadedOut(e:Event):void
		{
			if (popup_queue.length > 0)
			{
				var arr:Array = popup_queue.shift() as Array;
				arr[0](arr[1]);
			}
		}
		private function Initialize():void
		{
			addChild(gameFrame);
			addChild(gui);
			addChild(settings);
			addChild(gameOver);
			addChild(fade_layer);
			addChild(ach_popup);
			addChild(sponsor.View);
			settings.visible = false;
			sponsor.Right = 720;
			sponsor.Top = 0;
		}
		private function onSettingsFadedOut(e:Event):void
		{
			settings.visible = false;
			main_settings.visible = false;
			stage.focus = this;
		}
		public static function get IsShowingMainSettings():Boolean { return instance.IsShowingMainSettings; }
		public static function get IsGameOver():Boolean { return instance.gameOver.visible; }
		public static function get IsShowingPopup():Boolean { return instance.IsShowingPopup; }
	}
}