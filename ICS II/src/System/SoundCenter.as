package System 
{
	import adobe.utils.CustomActions;
	import _Base.MySound;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.utils.Dictionary;
	import mx.resources.IResourceBundle;
	import System.Interfaces.ISound;
	/**
	 * ...
	 * @author Knight
	 */
	public final class SoundCenter 
	{
		private static var sfx:Dictionary;
		private static var bgm:ISound;
		private static var bgm_volume:int = 100;
		private static var sfx_volume:int = 100;
		private static var bgm_mute:Boolean = false;
		private static var sfx_mute:Boolean = false;
		private static var sfx_instance:Vector.<ISound>;
		private static var sfx_instance_multiplier:Dictionary = new Dictionary();
		public function SoundCenter() 
		{
			throw new Error("Cannot instantiate SoundCenter");
		}
		public static function Initialize():void
		{
			sfx = new Dictionary();
			sfx_instance = new Vector.<ISound>();
		}
		public static function Flush():void
		{
			for each(var s:Class in sfx)
			{
				(sfx[s] as ISound).Stop();
			}
		}
		public static function StopBGM():void
		{
			if (bgm == null) return;
			
			bgm.Stop(500);
			bgm = null;
		}
		public static function PlaySFXFromInstance(instance:ISound, volumeMultiplier:Number = 1, loop:int = 0):void
		{
			if (instance == null) throw new ArgumentError();
			
			instance.Volume = (sfx_mute ? 0 : sfx_volume) * volumeMultiplier;
			instance.Play(loop);
			instance.addEventListener(MySound.SoundCompleted, onInstanceSoundCompleted);
			sfx_instance.push(instance);
		}
		public static function SetSoundVolumeMultiplier(sound:ISound, multiplier:Number):ISound
		{
			sound.Volume = (sfx_mute ? 0 : sfx_volume) * multiplier;
			sfx_instance_multiplier[sound] = multiplier;
			return sound;
		}
		public static function PlayBGM(c:Class):void
		{
			if (c == Object(bgm).constructor) return;
			
			if (bgm != null) StopBGM();
			
			bgm = new c();
			bgm.Volume = (bgm_mute ? 0 : bgm_volume);
			bgm.Play(9999999, 500);
		}
		public static function PlaySFX(c:Class, volumeMultiplier:Number = 1, loop:int = 0):void
		{
			if (sfx[c] == null) sfx[c] = new c();
			
			(sfx[c] as ISound).Volume = (sfx_mute ? 0 : sfx_volume) * volumeMultiplier;
			(sfx[c] as ISound).Play(loop);
		}
		public static function PauseAllSFX():void
		{
			var sound:ISound = null;
			for each(sound in sfx_instance)
				sound.Pause();
			for each(sound in sfx)
				sound.Pause();
		}
		public static function ResumeAllSFX():void
		{
			var sound:ISound = null;
			for each(sound in sfx_instance)
			{
				if(sound.IsPausing)
					sound.Resume();
			}
			for each(sound in sfx)
			{
				if(sound.IsPausing)
					sound.Resume();
			}
		}
		public static function set BGMVolume(val:int):void 
		{
			bgm_volume = val; 
			if (bgm != null && !bgm_mute)
			{
				bgm.Volume = bgm_volume;
			}
		}
		public static function MuteBGM():void 
		{
			bgm_mute = true; 
			if (bgm != null)
			{
				bgm.Volume = 0;
			}
		}
		public static function UnmuteBGM():void
		{
			bgm_mute = false;
			if (bgm != null)
			{
				bgm.Volume = bgm_volume;
			}
		}
		public static function get IsMutingSFX():Boolean { return sfx_mute; }
		public static function get IsMutingBGM():Boolean { return bgm_mute; }
		public static function MuteSFX():void
		{
			sfx_mute = true;
			for each(var c:ISound in sfx)
			{
				c.Volume = 0;
			}
			for each(var ins:ISound in sfx_instance)
			{
				ins.Volume = 0;
			}
		}
		public static function UnmuteSFX():void
		{
			sfx_mute = false;
			for each(var c:ISound in sfx)
			{
				c.Volume = sfx_volume;
			}
			for each(var ins:ISound in sfx_instance)
			{
				if(sfx_instance_multiplier[ins] == null)
					ins.Volume = sfx_volume;
				else
					ins.Volume = sfx_volume * sfx_instance_multiplier[ins];
			}
		}
		public static function set SFXVolume(val:int):void { sfx_volume = val; }
		public static function get BGMVolume():int { return bgm_volume; }
		public static function get SFXVolume():int { return sfx_volume; }
		private static function onInstanceSoundCompleted(e:Event):void
		{
			for (var i:int = 0; i < sfx_instance.length; i++)
			{
				if (sfx_instance[i] == e.currentTarget)
				{
					sfx_instance[i].removeEventListener(MySound.SoundCompleted, onInstanceSoundCompleted);
					
					sfx_instance_multiplier[sfx_instance[i]] = null;
					sfx_instance.splice(i, 1);
					break;
				}
			}
		}
	}

}