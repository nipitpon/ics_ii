package System 
{
	import _Base.MyTextField;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import HardCode.GUI.Sponsor300x80;
	import mx.core.IRepeaterClient;
	import System.Interfaces.IReceiveControl;
	import System.Interfaces.IUpdatable;
	import System.MC.Buttons.Button;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Pages extends RenderableObject implements IReceiveControl
	{
		public static const Page_Start:String = "Page_Start";
		public static const Page_Stop:String = "Page_Stop";
		private var receiveControl:Vector.<IReceiveControl>;
		private var buttons:Vector.<Button>;
		public function Pages() 
		{
			buttons = new Vector.<Button>();
			receiveControl = new Vector.<IReceiveControl>();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		public function Start():void
		{ 
			Button.ClearFocus();
			//ControlCenter.ReleaseAllKey();
			dispatchEvent(new Event(Page_Start));
			
			if (false && CONFIG::release && this.loaderInfo.url.indexOf("knight52.somee.com") < 0) 
			{
				var txt:MyTextField = new MyTextField(40, 0xff0000);
				txt.text = "Stolen property!!!";
				txt.x = Utility.Random(0, 600);
				txt.y = Utility.Random(0, 500);
				txt.width = 800;
				addChild(txt);
			}
		}
		
		public function KeyPress(id:int):void 
		{ 
			var current_focus:Button = null;
			for each(var button:Button in buttons)
			{
				if (button.HasFocus)
				{
					current_focus = button;
					break;
				}
			}
			if (current_focus == null)
			{
				Button.SetFocus(GetTopLeftMostButton());
			}
			else
			{
				var new_focus:Button = null;
				switch(id)
				{
					case ControlCenter.SelectLeftButton: new_focus = GetLeftButton(current_focus); break;
					case ControlCenter.SelectUpperButton: new_focus = GetUpButton(current_focus); break;
					case ControlCenter.SelectRightButton: new_focus = GetRightButton(current_focus); break;
					case ControlCenter.SelectLowerButton: new_focus = GetDownButton(current_focus); break;
				}
				if (new_focus == null)
					new_focus = current_focus;
				Button.SetFocus(new_focus);
			}
		}
		public function KeyRelease(id:int):void {}
		public function MousePress(x:Number, y:Number):void {}
		public function MouseMove(x:Number, y:Number):void {}
		public function MouseRelease(x:Number, y:Number):void {}
		public function get IsReceivingControl():Boolean { return true; }
		public function Stop():void 
		{ 
			dispatchEvent(new Event(Page_Stop)); 
		}
		public function get IsShowingPopup():Boolean { return false; }
		protected function AddControlReceiver(ctrl:IReceiveControl):void
		{
			if (ctrl == null)
				throw new ArgumentError();
			receiveControl.push(ctrl);
			if (ctrl is Button && !(ctrl is Sponsor300x80))
				buttons.push(ctrl as Button);
		}
		protected function RemoveControlReceiver(ctrl:IReceiveControl):void
		{
			if (ctrl == null)
				throw new ArgumentError();
			if(receiveControl.indexOf(ctrl) >= 0)
				receiveControl.splice(receiveControl.indexOf(ctrl), 1);
			
			if (ctrl is Button && !(ctrl is Sponsor300x80) && buttons.indexOf(ctrl) >= 0)
				buttons.splice(buttons.indexOf(ctrl), 1);
				
			ControlCenter.RemoveGameObject(ctrl);
		}
		protected function onAddedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			for each(var ctrl:IReceiveControl in receiveControl)
				ControlCenter.AddGameObject(ctrl);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			stage.focus = stage;
			ControlCenter.AddGameObject(this);
		}
		protected function onRemovedFromStage(e:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			for each(var ctrl:IReceiveControl in receiveControl)
				ControlCenter.RemoveGameObject(ctrl);
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			Button.ClearFocus();
			ControlCenter.RemoveGameObject(this);
		}
		private function GetLeftButton(current_button:Button):Button
		{
			var outgoing:Button = null;
			var min_length:Number = Infinity;
			for each(var button:Button in buttons)
			{
				if (button == current_button || !button.Enabled) continue;
				
				var dif:Point = new Point(current_button.Collision.CenterX - button.Collision.Right, current_button.Collision.CenterY - button.Collision.CenterY);
				if (current_button.Collision.Top < button.Collision.Bottom && current_button.Collision.Bottom > button.Collision.Top)
					dif.y = 0;
				if (dif.x > 0 && dif.length < min_length)
				{
					min_length = dif.length;
					outgoing = button;
				}
			}
			return outgoing;
		}
		private function GetUpButton(current_button:Button):Button
		{
			var outgoing:Button = null;
			var min_length:Number = Infinity;
			var up_vector:Point = new Point(0, -1);
			for each(var button:Button in buttons)
			{
				if (button == current_button || !button.Enabled) continue;
				
				var dif:Point = new Point(current_button.Collision.CenterX - button.Collision.CenterX, current_button.Collision.CenterY - button.Collision.Bottom);
				var angle:Number = 1 - (Math.abs(Utility.AngleBetweenVector(up_vector, dif)) / 360.0);
				if (current_button.Collision.Left < button.Collision.Right && current_button.Collision.Right > button.Collision.Left)
					dif.x = 0;
				if (dif.y > 0 && dif.length < min_length)
				{
					min_length = dif.length;
					outgoing = button;
				}
			}
			return outgoing;
		}
		private function GetDownButton(current_button:Button):Button
		{
			var outgoing:Button = null;
			var min_length:Number = Infinity;
			for each(var button:Button in buttons)
			{
				if (button == current_button || !button.Enabled) continue;
				
				var dif:Point = new Point(current_button.Collision.CenterX - button.Collision.CenterX, current_button.Collision.CenterY - button.Collision.Top);
				
				if (current_button.Collision.Left < button.Collision.Right && current_button.Collision.Right > button.Collision.Left)
					dif.x = 0;
				if (dif.y < 0 && dif.length < min_length)
				{
					min_length = dif.length;
					outgoing = button;
				}
			}
			return outgoing;
		}
		private function GetRightButton(current_button:Button):Button
		{
			var outgoing:Button = null;
			var min_length:Number = Infinity;
			for each(var button:Button in buttons)
			{
				if (button == current_button || !button.Enabled) continue;
				
				var dif:Point = new Point(current_button.Collision.CenterX - button.Collision.Left, current_button.Collision.CenterY - button.Collision.CenterY);
				if (current_button.Collision.Top < button.Collision.Bottom && current_button.Collision.Bottom > button.Collision.Top)
					dif.y = 0;
				if (dif.x < 0 && dif.length < min_length)
				{
					min_length = dif.length;
					outgoing = button;
				}
			}
			return outgoing;
		}
		private function GetTopLeftMostButton():Button
		{
			var outgoing:Button = null;
			var min_length:Number = Infinity;
			var point:Point = new Point();
			for each(var button:Button in buttons)
			{
				if (!button.Enabled) continue;
				
				point.x = button.Collision.Left;
				point.y = button.Collision.Top;
				if (point.length < min_length)
				{
					outgoing = button;
					min_length = point.length;
				}
			}
			return outgoing;
		}
		private function onEnterFrame(e:Event):void
		{
			for each(var but:Button in buttons)
			{
				if (but is IUpdatable)
					(but as IUpdatable).Update();
			}
		}
	}

}