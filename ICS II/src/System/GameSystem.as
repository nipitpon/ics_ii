package System 
{
	import _Base.MyTextField;
	//import com.newgrounds.API;
	import com.spilgames.gameapi.GameAPI;
	import flash.display.Stage;
	import flash.events.DRMAuthenticationCompleteEvent;
	import flash.events.Event;
	import flash.geom.Point;
	import HardCode.BlackMirror.Level.*;
	import HardCode.GUI.MonsterMarker;
	import HardCode.Levels.*;
	import HardCode.Projectiles.Rocket;
	import HardCode.Skill_StartLevel1;
	import HardCode.Tiles.Cannon;
	import HardCode.Weather.Weather_Sunny;
	import Resources.CodedAnimation;
	import System.MC.Characters.Character;
	import System.MC.Characters.Monster;
	import System.MC.Collision.PlayerBound;
	import System.Interfaces.IMVC;
	import System.MC.*;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionSphere;
	import System.MC.etc.LevelInfo;
	import System.MC.etc.PlayerInfo;
	import System.MC.etc.ZoomedInSceneInfo;
	import System.MC.Flags.EventTrigger;
	import System.MC.Interactables.InteractableObject;
	import System.MC.Items.Item;
	import System.MC.Ladder.Ladder;
	import System.MC.Projectiles.AttackTargetProjectile;
	import System.V.ForestBackground;
	import System.V.InteractionText;
	import System.V.RenderableObject;
	import System.Interfaces.*;
	
	/**
	 * ...
	 * @author Knight
	 */
	public final class GameSystem 
	{
		public static const Gravity:Number = 3;
		public static const MaxGravity:Number = Tile.Size.y * 1;
		public static const UpdateRange:int = 120000;
		public static const GameMode_Vanilla:int = 0;
		public static const GameMode_Survival:int = 1;
		public static const GameMode_TimeAttack:int = 2;
		
		private static var death_count:int;
		private static var level_time:int;
		private static var current_mode:int;
		private static var namable:Vector.<INamable>;
		private static var updatable:Vector.<IUpdatable> = new Vector.<IUpdatable>();
		private static var collidable:Vector.<ICollidable>;
		private static var movable:Vector.<IMovable>;
		private static var itemCollector:Vector.<IItemCollector>;
		private static var item:Vector.<IItem>;
		private static var interactable:Vector.<IInteractable>;
		private static var interactor:Vector.<IInteractor>;
		private static var statusHolder:Vector.<IStatusHolder>;
		private static var launchables:Vector.<ILaunchable>;
		private static var ladders:Vector.<Ladder>;
		private static var waterBlocks:Vector.<WaterBlock>;
		private static var terrain:Terrain;
		private static var player:Player;
		private static var stage:Stage;
		private static var freeze:Boolean = false;
		private static var currentWeather:Weather;
		private static var weathers:Vector.<Weather>;
		private static var weather_count:uint = 0;
		private static var weather_enable:Boolean = false;
		private static var lock_left:Number = 0;
		private static var lock_right:Number = 0;
		private static var leftBound:PlayerBound;
		private static var rightBound:PlayerBound;
		private static var current_level:Level;
		private static var player_info:PlayerInfo = null;
		private static var level_info:LevelInfo = null;
		private static var fighting_boss:Boolean = false;
		private static var boss:Monster = null;
		private static var override_focus_x:Number = NaN;
		private static var override_focus_y:Number = NaN;
		
		private static var leftBound_Center:Point;
		private static var rightBound_Center:Point;
		public function GameSystem() 
		{
			throw new Error("Cannot instantiate GameSystem");
		}
		
		public static function Initialize(stage:Stage):void
		{
			namable = new Vector.<INamable>();
			collidable = new Vector.<ICollidable>();
			movable = new Vector.<IMovable>();
			item = new Vector.<IItem>();
			itemCollector = new Vector.<IItemCollector>();
			interactable = new Vector.<IInteractable>();
			interactor = new Vector.<IInteractor>();
			statusHolder = new Vector.<IStatusHolder>();
			launchables = new Vector.<ILaunchable>();
			ladders = new Vector.<Ladder>();
			currentWeather = null;
			weathers = new Vector.<Weather>();
			leftBound_Center = new Point();
			rightBound_Center = new Point();
			leftBound = new PlayerBound();
			rightBound = new PlayerBound();
			waterBlocks = new Vector.<WaterBlock>();
			GameSystem.stage = stage;
			AddGameObject(leftBound);
			AddGameObject(rightBound);
			
			StartLevel(current_mode);
			//switch(current_mode)
			//{
			//	case GameMode_Vanilla: StartLevel(); break;
			//	case GameMode_Survival: StartSurvivalLevel(); break;
			//	case GameMode_TimeAttack: StartTimeAttack(); break;
			//}
			
			//StartTestLevel();
		}
		public static function SetLevel(level:Level):void
		{
			GameFrame.AddChild(level.TerrainLayer);
			if (current_mode > 50 && current_mode != 55) GUI.SetLevelText( -1);
			else GUI.SetLevelText(current_mode);
			//for each(var point:SpawnPoint in level.SpawnPoints) AddGameObject(point); 
			//for each(var obj:IInteractable in level.Interactables) GameFrame.AddChild(obj); 
			//for each(var overlay:Overlay in level.Overlays) GameFrame.AddChild(overlay);
			//for each(var flag:EventTrigger in level.Flags) GameFrame.AddChild(flag);
			//
			(level.FarBG as RenderableObject).Top = level.TerrainLayer.Bottom - 540;
			(level.MiddleBG as RenderableObject).Top = level.TerrainLayer.Bottom - 540;
			(level.NearBG as RenderableObject).Top = level.TerrainLayer.Bottom - 540;
			(level.CloseBG as RenderableObject).Top = level.TerrainLayer.Bottom - 540;
			GameFrame.SetBackground(level.FarBG, level.MiddleBG, level.NearBG, level.CloseBG);
			current_level = level;
			UnlockCamera();
			weathers.push(new Weather_Sunny());
			level.Start();
			SetWeather(weathers[0]);
			if (level.OverrideBGM != null)
				SoundCenter.PlayBGM(level.OverrideBGM);
		}
		public static function EnableWeatherShuffle():void
		{
			weather_enable = true;
		}
		public static function SetGameMode(mode:int):void
		{
			if (current_mode != mode)
			{
				death_count = 0;
				GUI.SetDeathCount(death_count);
				Level.ClearObjectInfo();
			}
			level_time = 0;
			current_mode = mode;
		}
		public static function get DeathStreak():int { return death_count; }
		public static function get CurrentGameMode():int { return current_mode; }
		public static function BurnGoat():void
		{
			DataCenter.BurnGoat();
			Achievements.BurnGoat();
		}
		public static function FinishLevel():void
		{
			if (current_mode > 0 && current_mode <= 50)
			{
				DataCenter.SetLevelDeath(current_mode, death_count);
			}
			if (current_mode > 0 && current_mode <= 55)
			{
				DataCenter.SetLevelTime(current_mode, level_time);
				GameAPI.getInstance().Score.submit(DataCenter.CalculateScore());
				//API.postScore("How fast can you complete each levels", DataCenter.CalculateScore());
			}
			Achievements.PassLevel();
		}
		public static function PlayerDieOnce(reason:int):void
		{
			death_count++;
			GUI.SetDeathCount(death_count);
			DataCenter.AddTotalDeath(current_mode, reason);
			Achievements.PlayerDie();
		}
		public static function AddMarkedMonster(mon:Monster):void
		{
			var marker:MonsterMarker = new MonsterMarker(mon);
			GameFrame.AddChild(marker);
		}
		public static function get CurrentLevel():Level { return current_level; }
		public static function AddGameObject(mvc:*):void
		{
			if (mvc is IUpdatable) updatable.push(mvc as IUpdatable);
			if (mvc is INamable) namable.push(mvc as INamable);
			if (mvc is ILaunchable) launchables.push(mvc as ILaunchable);
			if (mvc is IAnimatable)
			{
				var animate:IAnimatable = mvc as IAnimatable;
				animate.SetAnimation(animate.DefaultAnimation);
			}
			if (mvc is IMVC && (mvc as IMVC).View is IUpdatable)
			{
				updatable.push((mvc as IMVC).View as IUpdatable);
			}
			if (mvc is WaterBlock) waterBlocks.push(mvc as WaterBlock);
			if (mvc is Ladder) ladders.push(mvc as Ladder);
			if (mvc is ICollidable) collidable.push(mvc as ICollidable);
			if (mvc is IMovable) movable.push(mvc as IMovable);
			if (mvc is IInteractable) interactable.push(mvc as IInteractable);
			if (mvc is IInteractor) interactor.push(mvc as IInteractor);
			if (mvc is IItem) item.push(mvc as IItem);
			if (mvc is IItemCollector) itemCollector.push(mvc as IItemCollector);
			if (mvc is Player)
			{
				player = mvc as Player;
				current_level.CheckTile(int(player.CenterX / Tile.Size.x));
				var focus:Point = ScreenFocus;
				GameFrame.SetCameraPosition(focus.x, focus.y);
				GUI.OnPlayerAdded();
			}
			if (mvc is Terrain) terrain = mvc as Terrain;
			if (mvc is IStatusHolder) statusHolder.push(mvc as IStatusHolder);
			
			if (mvc is IReceiveControl)
				ControlCenter.AddGameObject(mvc as IReceiveControl);
		}
		public static function ContainsGameObject(obj:*):Boolean
		{
			var classes:Vector.<Class> = Vector.<Class>([IUpdatable, INamable, ICollidable,
				IMovable, IInteractable, IInteractor, IItem, IItemCollector, IStatusHolder,
				ILaunchable, Ladder, WaterBlock]);
			var vecs:Array = ([updatable, namable, collidable, movable, interactable, interactor,
				item, itemCollector, statusHolder, launchables, ladders, waterBlocks]);
			for (var i:int = 0; i < classes.length; i++)
			{
				if (obj is classes[i]) for each(var a:* in vecs[i])
				{
					if (a == obj) return true;
				}
			}
			
			return obj == player || obj == terrain;
		}
		public static function FloorBelowPoint(point:Point):Number
		{
			return terrain.FloorBelowPoint(point);
		}
		public static function StartBossBattle(boss:Monster):void
		{
			fighting_boss = true;
			SetBoss(boss);
		}
		public static function StopBossBattle():void
		{
			fighting_boss = false;
			
			SoundCenter.StopBGM(); 
			if (!HasLevelBGM && !IsGameOver)
			{
				SoundCenter.PlayBGM(CurrentWeather.BackgroundMusic);
			}
			UnsetBoss();
		}
		public static function StartRaining():void
		{
			SetWeather(weathers[1]);
			weather_count = 0;
		}
		public static function FocusAt(x:Number, y:Number):void
		{
			override_focus_x = x;
			override_focus_y = y;
		}
		public static function get HasLevelBGM():Boolean { return CurrentLevel.OverrideBGM != null; }
		public static function get IsHavingBossBattle():Boolean { return fighting_boss; }
		public static function SetWeather(weather:Weather):void
		{
			if (weather == currentWeather) return;
			
			if (currentWeather != null)
			{
				currentWeather.Stop();
			}
			
			currentWeather = weather;
			
			weather.Start();
		}
		public static function RemoveGameObject(mvc:*):void
		{
			var i:int = 0;
			if (mvc is Ladder) for (i = 0; i < ladders.length; i++)
			{
				if (ladders[i] == mvc)
				{
					ladders.splice(i, 1);
					break;
				}
			}
			if (mvc is WaterBlock) for (i = 0; i < waterBlocks.length; i++)
			{
				if (waterBlocks[i] == mvc)
				{
					waterBlocks.splice(i, 1);
					break;
				}
			}
			if (mvc is INamable) for (i = 0; i < namable.length; i++)
			{
				if (namable[i] == mvc)
				{
					namable.splice(i, 1);
					break;
				}
			}
			if (mvc is ILaunchable) for (i = 0; i < launchables.length; i++)
			{
				if (launchables[i] == mvc)
				{
					launchables.splice(i, 1);
					break;
				}
			}
			if (mvc is IMVC && (mvc as IMVC).View is IUpdatable)
			{
				var v:IUpdatable = (mvc as IMVC).View as IUpdatable;
				for (i = 0; i < updatable.length; i++)
				{
					if (updatable[i] == v)
					{
						updatable.splice(i, 1);
						break;
					}
				}
			}
			if (mvc is IUpdatable) for (i = 0; i < updatable.length; i++)
			{
				if (updatable[i] == mvc)
				{
					updatable.splice(i, 1);
					break;
				}
			}
			if (mvc is ICollidable) for (i = 0; i < collidable.length; i++)
			{
				if (collidable[i] == mvc)
				{
					collidable.splice(i, 1);
					break;
				}
			}
			if (mvc is IMovable) for (i = 0; i < movable.length; i++)
			{
				if (movable[i] == mvc)
				{
					movable.splice(i, 1);
					break;
				}
			}
			if (mvc is IItem) for (i = 0; i < item.length; i++)
			{
				if (item[i] == mvc)
				{
					item.splice(i, 1);
					break;
				}
			}
			if (mvc is IItemCollector) for (i = 0; i < itemCollector.length; i++)
			{
				if (itemCollector[i] == mvc)
				{
					itemCollector.splice(i, 1);
					break;
				}
			}
			if (mvc is IInteractable) for (i = 0; i < interactable.length; i++)
			{
				if (interactable[i] == mvc)
				{
					interactable.splice(i, 1);
					break;
				}
			}
			if (mvc is IInteractor) for (i = 0; i < interactor.length; i++)
			{
				if (interactor[i] == mvc)
				{
					interactor.splice(i, 1);
					break;
				}
			}
			if (mvc is IStatusHolder) for (i = 0; i < statusHolder.length; i++)
			{
				if (statusHolder[i] == mvc)
				{
					statusHolder.splice(i, 1);
					break;
				}
			}
			if (mvc is Player) player = null;
			if (mvc is Terrain) terrain = null;
			
			if(mvc is IReceiveControl)
				ControlCenter.RemoveGameObject(mvc as IReceiveControl);
		}
		public static function GetIntersectedWaterBlock(collision:CollisionBox):WaterBlock
		{
			var point:Point = new Point(0, 1);
			for each(var waterBlock:WaterBlock in waterBlocks)
			{
				if (waterBlock.Intersects(collision, point) != null)
				{
					waterBlock.Intersects(collision, point);
					return waterBlock;
				}
			}
			return null;
		}
		public static function GetIntersectedLadder(p:Point):Ladder
		{
			var point:Point = new Point(0, 1);
			for each(var ladder:Ladder in ladders)
			{
				if (ladder.IntersectsPoint(p, point))
					return ladder;
			}
			return null;
		}
		public static function GetIntersectedStatusHolderWithLine(x1:Number, x2:Number, y1:Number, y2:Number,
			aliveOnly:Boolean, vulnerableOnly:Boolean, sideExclude:Class):Vector.<IStatusHolder>
		{
			var outgoing:Vector.<IStatusHolder> = new Vector.<IStatusHolder>();
			var point:Point = new Point(0, 1);
			for each(var collide:IStatusHolder in statusHolder)
			{
				if ((sideExclude == Object || collide.Side != sideExclude)&&
					(!aliveOnly || collide.IsAlive) && 
					(!vulnerableOnly || collide.IsVulnerable) && 
					collide.IntersectsLine(x1, x2, y1, y2, point))
					outgoing.push(collide);
			}
			return outgoing;
		}
		public static function GetIntersectedCollidable(collisionBox:CollisionBox, movement:Point = null):Vector.<ICollidable>
		{
			var outgoing:Vector.<ICollidable> = new Vector.<ICollidable>();
			if(movement == null)
				movement = new Point(0, 1);
			//When dropping test, it needs to have some motion so the
			//collisionbox begin to calculate.
			for each(var collide:ICollidable in collidable)
			{
				if (collide.Intersects(collisionBox, movement))
					outgoing.push(collide);
			}
			return outgoing;
		}
		public static function GetIntersectedLaunchable(collisionBox:CollisionBox):Vector.<ILaunchable>
		{
			var outgoing:Vector.<ILaunchable> = new Vector.<ILaunchable>();
			var point:Point = new Point();
			for each(var launchable:ILaunchable in launchables)
			{
				if (launchable.Intersects(collisionBox, point))
				{
					outgoing.push(launchable);
				}
			}
			return outgoing;
		}
		public static function GetIntersectedStatusHolder(collisionBox:CollisionBox, aliveOnly:Boolean, vulnerableOnly:Boolean, sideExclude:Class):Vector.<IStatusHolder>
		{
			var outgoing:Vector.<IStatusHolder> = new Vector.<IStatusHolder>();
			var point:Point = new Point();
			var c:Class = null;
			for each(var collide:IStatusHolder in statusHolder)
			{
				if ((sideExclude == Object || collide.Side != sideExclude)&&
					(!aliveOnly || collide.IsAlive) && 
					(!vulnerableOnly || collide.IsVulnerable) && 
					collide.Intersects(collisionBox, point))
					outgoing.push(collide);
			}
			return outgoing;
		}
		public static function get IsSunny():Boolean { return currentWeather == weathers[0]; }
		public static function get IsGameOver():Boolean { return GamePage.IsGameOver; }
		public static function GetCollidables():Vector.<ICollidable> { return collidable; }
		public static function GetObjectByName(name:String):INamable
		{
			for (var i:int = 0; i < namable.length; i++)
			{
				if (namable[i].Name == name) return namable[i];
			}
			//There are cases that namable objects are so far that it's not in the map right now.
			//So we have to search in the level too.
			if (current_level != null) return current_level.GetObjectByName(name);
			else return null;
		}
		public static function GetObjectByNames(name:String):Vector.<INamable>
		{
			var outgoing:Vector.<INamable> = new Vector.<INamable>();
			for each(var n:INamable in namable)
			{
				if (n.Name == name) outgoing.push(n);
			}
			for each(var n2:INamable in current_level.GetObjectByNames(name))
			{
				var found:Boolean = false;
				for (var i:int = 0; i < outgoing.length && !found; i++)
				{
					if (outgoing[i] == n2)
						found = true;
				}
				if(!found)
					outgoing.push(n2);
			}
			return outgoing;
		}
		public static function Interacts():void
		{
			if (GamePage.IsShowingPopup || GUI.IsShowingPauseMenu) return;
			
			var point:Point = new Point();
			for each(var inter:IInteractor in interactor)
			{
				if (!inter.IsInteractable) continue;
				
				var interCenter:Point = inter.Center;
				var closest:IInteractable = null;
				var length:Number = 99999;
				for each(var interact:IInteractable in interactable)
				{
					var interactCenter:Point = interact.Center;
					var new_length:Number = 0;
					if (inter.IntersectsInteractionRadius(interact.Collision, point) &&
						interact.IsInteractable && (new_length = Point.distance(interCenter, interactCenter)) < length)
					{
						closest = interact;
						length = new_length;
					}
				}
				if (closest != null)
				{
					closest.GetInteracted(inter);
				}
			}
		}
		public static function FrameMoves():void
		{
			Achievements.FrameMove();
			if (freeze || IsGameOver) return;
			
			if (weather_enable)
			{
				weather_count += Time.SecondPerFrame;
				
				if (weather_count >= 7200)
				{
					ShuffleWeather();
					weather_count = 0;
				}
			}
			level_time++;
			
			if(!CurrentLevel.FreezeTime)
				Time.MoveOneFrame();
			
			for each(var update:IUpdatable in updatable)
			{
				if (update.UpdateWhenAdvancingTime || !Time.IsAdvancing)
					update.Update();
			}
			
			var point:Point = new Point();
			for each(var move:IMovable in movable)
			{
				if (Time.IsAdvancing && (move is IUpdatable) && !(move as IUpdatable).UpdateWhenAdvancingTime) continue;
				
				if (!move.DefyGravity && move.ForceY < MaxGravity)
					move.Push(0, Gravity);
				var movement:Point = move.CalculateMovement();
				
				if (movement.x == 0 && movement.y == 0) continue;
				
				if (move is ICollidable)
				{
					var cur:ICollidable = move as ICollidable;
					if (!move.DefyGravity)
					{
						if (movement.x > 0 && cur.Collision.Right + movement.x > GameSystem.TerrainEnd)
							movement.x = GameSystem.TerrainEnd - cur.Collision.Right;
						else if (movement.x < 0 && cur.Collision.Left + movement.x < GameSystem.TerrainStart)
							movement.x = GameSystem.TerrainStart - cur.Collision.Left;						
					}
					for (var i:int = collidable.length - 1; i >= 0; i--)//for each(var collide:ICollidable in collidable)
					{
						var collide:ICollidable = collidable[i];
						if (collide == move || collide.Collision == null || 
							(!collide.CollideWith(move) && !cur.CollideWith(collide))) continue;
							
						if (Math.abs(collide.Collision.CenterX - cur.Collision.CenterX) >
							collide.Collision.Width + cur.Collision.Width) continue;
						
						var hit:ICollidable = collide.Intersects(cur.Collision, movement);
						if (hit != null)
						{
							cur.Hit(hit, movement);
							var x:Number = movement.x;
							var y:Number = movement.y;
							if (cur.Active && hit.Active)
							{
								hit.TakenHit(cur, movement);
							}
						}
						if (!cur.Active) break;
					}
				}
				
				if (!move.Active) continue;
				
				if (move is IInteractor)
				{
					var inter:IInteractor = move as IInteractor;
					
					var interCenter:Point = inter.Center;
					var closest:IInteractable = null;
					var length:Number = 99999;
					for each(var interact:IInteractable in interactable)
					{
						var interactCenter:Point = interact.Center;
						var new_length:Number = 0;
						if (inter.IntersectsInteractionRadius(interact.Collision, movement) &&
							interact.IsInteractable && (new_length = Point.distance(interCenter, interactCenter)) < length)
						{
							if (closest != null)
								closest.Unhighlight();
							closest = interact;
							length = new_length;
						}
						else
						{
							interact.Unhighlight();
						}
					}
					if (closest != null)
					{
						closest.Highlight(false);
						//GUI.ShowInteractionText("X)\n" + closest.Caption);
					}
					else
					{
						//GUI.HideInteractionText();
					}
				}
				move.MoveByOffset(movement.x, movement.y);
			}
			
			if(GameSystem.item.length > 0) for each(var item:IItem in GameSystem.item)
			{
				for each(var collector:IItemCollector in itemCollector)
				{
					if (item.Collectable && collector.IntersectsCollectionRadius(item.Collision, point))
					{
						item.GetCollected(collector);
					}
				}
			}
			
			var newFocus:Point = ScreenFocus;
			
			GameFrame.CenterNightEffect(player.CenterX, player.CenterY);
			leftBound_Center.x = newFocus.x - (ScreenWidth / 2) - (leftBound.Collision.Width / 2);
			rightBound_Center.x = newFocus.x + (ScreenWidth / 2) + (rightBound.Collision.Width / 2);
			leftBound_Center.y = newFocus.y;
			rightBound_Center.y = newFocus.y;
			
			GameFrame.UpdateCamera(newFocus.x, newFocus.y);
			leftBound.Center = leftBound_Center;
			rightBound.Center = rightBound_Center;
			
			current_level.CheckTile(int(player.CenterX / Tile.Size.x));
		}
		public static function get IsCompletelyIndoor():Boolean 
		{ return current_level.IsIndoor(rightBound_Center) && current_level.IsIndoor(leftBound_Center); }
		public static function SaveGame(playerPos:Boolean, levelInfo:Boolean):void
		{
			DataCenter.SaveCurrentGame(playerPos, levelInfo);
		}
		public static function LockCamera(center:Number, width:Number = 0):void
		{
			if (width == 0) width = GameSystem.ScreenWidth / 2;
			
			if (center != 0)
			{
				lock_left = center - width;
				lock_right = center + width;
			}
			else
			{
				lock_left = 0;
				lock_right = 0;
			}
		}
		public static function UnlockCamera():void
		{
			LockCamera(0);
		}
		public static function CanAdvanceTime():Boolean
		{
			if (player == null) return false;
			for each(var stat:IStatusHolder in statusHolder)
			{
				if (stat.Side != player.Side && Math.abs(stat.CenterX - player.CenterX) < 500)
					return false;
			}
			return true;
		}
		public static function SetPlayerInfo(pInf:PlayerInfo):void
		{
			player_info = pInf;
		}
		public static function SetLevelInfo(lInf:LevelInfo):void
		{
			level_info = lInf;
		}
		public static function GetLevelInfo():LevelInfo
		{
			if (current_level == null) throw new Error("No level is set");
			else return (level_info = current_level.GetInfo());
		}
		public static function GetStoredLevelInfo():LevelInfo
		{
			return level_info;
		}
		public static function IsBetweenPlayerBounds(box:CollisionBox):Boolean
		{
			return box.Right <= rightBound.Collision.Left && box.Left >= leftBound.Collision.Right;
		}
		public static function get CurrentWeather():Weather { return currentWeather; }
		public static function get ScreenWidth():int { return stage.stageWidth; }
		public static function get ScreenHeight():int { return stage.stageHeight; }
		public static function get LeftBound():Number { return ScreenFocus.x - ScreenWidth / 2; }
		public static function get RightBound():Number { return ScreenFocus.x + ScreenWidth / 2; }
		public static function get ScreenFocus():Point
		{			
			var newFocus:Point = player.Center;
			
			//if (player.IsTurningLeft) newFocus.x -= 100;
			//else newFocus.x += 100;
			
			if (isNaN(override_focus_x))
			{
				if (newFocus.x < TerrainStart + stage.stageWidth / 2) newFocus.x = TerrainStart + stage.stageWidth / 2;
				if (newFocus.x > TerrainEnd - stage.stageWidth / 2) newFocus.x = TerrainEnd - stage.stageWidth / 2;
				
				if(lock_left != 0)
				{
					if (newFocus.x < lock_left + ScreenWidth / 2)
						newFocus.x = lock_left + ScreenWidth / 2;
					else if (newFocus.x > lock_right - ScreenWidth / 2)
						newFocus.x = lock_right - ScreenWidth / 2;
				}
			}
			else
			{
				newFocus.x = override_focus_x;
			}
			
			if (isNaN(override_focus_y))
			{
				if (newFocus.y > TerrainBottom - stage.stageHeight / 2) newFocus.y = TerrainBottom - (stage.stageHeight / 2);
				else if (!player.IsProning && !player.IsSliding) newFocus.y -= 50;
				else if (player.IsJumping) newFocus.y = (GameFrame.FocusY + newFocus.y) / 2;
				if (newFocus.y < TerrainTop + stage.stageHeight / 2) newFocus.y = TerrainTop + (stage.stageHeight / 2);
			}
			else
			{
				newFocus.y = override_focus_y;
			}
			
			return newFocus;
		}
		public static function Freeze():void 
		{
			freeze = true;
			GUI.ShowFreezeLayer();
			if (player.IsMoving) player.StopMoving(true);
			GameFrame.Blur();
		}
		public static function Unfreeze():void
		{
			freeze = false;
			GUI.HideFreezeLayer();
			GameFrame.Unblur();
		}
		public static function get PlayerInstance():Player { return player; }
		public static function get IsFreezing():Boolean { return freeze; }
		public static function IsIndoor(position:Point):Boolean { return CurrentLevel.IsIndoor(position); }
		public static function Flush():void
		{
			UnsetBoss();
			RemoveLevel(current_level);
			RemoveGameObject(leftBound);
			RemoveGameObject(rightBound);
			var arr:Array = [namable, updatable, collidable, movable, item, itemCollector,
				interactable, statusHolder, weathers, interactor, launchables];
			for each(var a:* in arr)
			{
				for each(var obj:* in a)
				{
					if (obj is IMVC && GameFrame.ContainChild(obj as IMVC))
						GameFrame.RemoveChild(obj as IMVC);
					else if (obj is IReceiveControl)
						ControlCenter.RemoveGameObject(obj as IReceiveControl);
				}
			}
			namable = new Vector.<INamable>();
			updatable = new Vector.<IUpdatable>();
			collidable = new Vector.<ICollidable>();
			movable = new Vector.<IMovable>();
			item = new Vector.<IItem>();
			itemCollector = new Vector.<IItemCollector>();
			interactable = new Vector.<IInteractable>();
			statusHolder = new Vector.<IStatusHolder>();
			weathers = new Vector.<Weather>();
			interactor = new Vector.<IInteractor>();
			launchables = new Vector.<ILaunchable>();
			weather_count = 0;
			weather_enable = false;
			currentWeather = null;
			freeze = false;
			fighting_boss = false;
		}
		public static function get TerrainStart():Number { return terrain.Collision.Left; }
		public static function get TerrainEnd():Number { return terrain.Collision.Right; }
		public static function get TerrainTop():Number { return terrain.Collision.Top; }
		public static function get TerrainBottom():Number { return terrain.Collision.Bottom; }
		
		private static function ShuffleWeather():void
		{
			var new_weather:Weather = weathers[Utility.Random(0, weathers.length)];
			
			if(new_weather != CurrentWeather)
				SetWeather(new_weather);
		}
		private static function StartTestLevel():void
		{
			Overseer.Execute(ExecutionCode.Skill_StartTestLevel);
		}
		private static function StartLevel(code:int):void
		{
			switch(code)
			{
				case 1: SetLevel(new IntroLevel1(2)); break;
				case 2: SetLevel(new IntroToSunlightLevel(3)); break;
				case 3: SetLevel(new IntroToSpike(4)); break;
				case 4: SetLevel(new SpikeJumping(5)); break;
				case 5: SetLevel(new IntroToLadder(6)); break;
				case 6: SetLevel(new IntroToWaterPotion(7)); break;
				case 7: SetLevel(new IntroToSwitch(8)); break;
				case 8: SetLevel(new IntroToBurnable(9)); break;
				case 9: SetLevel(new LongSprint(10)); break;
				case 10: SetLevel(new ParticleBeamLevel(11)); break;
				case 11: SetLevel(new IntroToLaunchers(12)); break;
				case 12: SetLevel(new LauncherRedDoorWaterPotion(13)); break;
				case 13: SetLevel(new Swamp(14)); break;
				case 14: SetLevel(new TreasureIsland(15)); break;
				case 15: SetLevel(new IntroToMonsters(16)); break;
				case 16: SetLevel(new MonsterRun(17)); break;
				case 17: SetLevel(new IntroToBumpers(18)); break;
				case 18: SetLevel(new FireBallBurnHay(19)); break;
				case 19: SetLevel(new JumpToTheTop(20)); break;
				case 20: SetLevel(new GeckoBoss(21)); break;
				case 21: SetLevel(new AdvancedSlide(22)); break;
				case 22: SetLevel(new WaterfallCave(23)); break;
				case 23: SetLevel(new IntroToBox(24)); break;
				case 24: SetLevel(new MoveBoxToBlockLauncher(25)); break;
				case 25: SetLevel(new IntroToBed(26)); break;
				case 26: SetLevel(new StartsAtNight(27)); break;
				case 27: SetLevel(new WindMill(28)); break;
				case 28: SetLevel(new MatchStick(29)); break;
				case 29: SetLevel(new Lvl29(30)); break;
				case 30: SetLevel(new MovingTileBeam(31)); break;
				case 31: SetLevel(new EntranceToRuin(32)); break;
				case 32: SetLevel(new RuinMainHall(33)); break;
				case 33: SetLevel(new RuinCorridor(34)); break;
				case 34: SetLevel(new PathwayToThroneRoom(35)); break;
				case 35: SetLevel(new IntroToWalldo(36)); break;
				case 36: SetLevel(new RuinAttic(37)); break;
				case 37: SetLevel(new RuinRooftop(38)); break;
				case 38: SetLevel(new RuinAttic2(39)); break;
				case 39: SetLevel(new RuinBackdoor(40)); break;
				case 40: SetLevel(new RuinGarden(41)); break;
				case 41: SetLevel(new CliffBase(42)); break;
				case 42: SetLevel(new CliffUp(43)); break;
				case 43: SetLevel(new CliffCave(44)); break;
				case 44: SetLevel(new CliffFlat(45)); break;
				case 45: SetLevel(new IntroToCannon(46)); break;
				case 46: SetLevel(new CliffShooter(47)); break;
				case 47: SetLevel(new RaceAgainstCannon(48)); break;
				case 48: SetLevel(new AirStripWall(49)); break;
				case 49: SetLevel(new TheBarn(50)); break;
				case 50: SetLevel(new SkyLevel( -1)); break;
				case 55: SetLevel(new FireBallMonstrosityLevel( -2)); break;
				case 60: SetLevel(new FireBallMonstrosityLevel(14)); break;
				case 80: SetLevel(new MainMenuLevel()); break;
				case 102: SetLevel(new DynamicLevel()); break;
				default: throw new Error("Unknown level: " + code);
			}
		}
		private static function StartSurvivalLevel():void
		{
			Overseer.Execute(ExecutionCode.Skill_StartSurvival);
		}
		private static function StartTimeAttack():void
		{
			Overseer.Execute(ExecutionCode.Skill_StartTimeAttack);
		}
		private static function boss_TakeDamage(e:Event):void
		{
			GUI.UpdateBossHP(boss.HP, boss.MaxHP);
			if (!boss.IsAlive)
				UnsetBoss();
		}
		private static function RemoveLevel(level:Level):void
		{
			GameFrame.RemoveChild(level.TerrainLayer);
			level.Stop();
		}
		private static function SetBoss(boss:Monster):void
		{
			if (GameSystem.boss != null)
			{
				GameSystem.boss.removeEventListener(Character.CharacterTakeDamage, boss_TakeDamage);
				GUI.HideBossHP();
			}
			GameSystem.boss = boss;
			if (boss != null)
			{
				GameSystem.boss.addEventListener(Character.CharacterTakeDamage, boss_TakeDamage);
				GUI.ShowBossHP();
				GUI.UpdateBossHP(boss.HP, boss.MaxHP);
			}
		}
		private static function UnsetBoss():void
		{
			SetBoss(null);
		}
	}
}