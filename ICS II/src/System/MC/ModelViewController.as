package System.MC 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import System.GameFrame;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IMVC;
	import System.Interfaces.IUpdatable;
	import System.MC.Effects.Effect;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ModelViewController extends EventDispatcher implements IMVC
	{
		public static const Added:String = "Added";
		public static const Removed:String = "Removed";
		private var view:RenderableObject;
		private var attaching_effects:Vector.<ModelViewController>;
		private var id:int;
		public function ModelViewController(view:RenderableObject) 
		{
			this.view = view;
			attaching_effects = new Vector.<ModelViewController>();
			if (view is IAnimatable)
			{
				var anim:IAnimatable = view as IAnimatable;
				anim.SetAnimation(anim.DefaultAnimation);
			}
			view.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		public function DetachEffect(effect:ModelViewController):void
		{
			for (var i:int = 0; i < attaching_effects.length; i++)
			{
				if (attaching_effects[i] == effect)
				{
					attaching_effects.splice(i, 1)[0].removeEventListener(Removed, onEffectRemoved);
					break;
				}
			}
		}
		public function AttachEffect(effect:ModelViewController):void
		{
			attaching_effects.push(effect);
			effect.addEventListener(Removed, onEffectRemoved);
		}
		public function set ID(val:int):void { id = val; }
		public function get ID():int { return id; }
		public function set Center(point:Point):void { View.Center = point; }
		public function set Left(val:Number):void { View.Left = val; }		
		public function set Right(val:Number):void { View.Right = val; }
		public function set Top(val:Number):void { View.Top = val; }
		public function set Bottom(val:Number):void { View.Bottom = val; }
		public function set CenterY(val:Number):void { View.CenterY = val; }
		public function MoveByOffset(x:int, y:int):void
		{
			View.MoveByOffset(x, y);
			for each(var effect:ModelViewController in attaching_effects)
			{
				effect.MoveByOffset(x, y);
			}
		}
		public function set CenterX(val:Number):void { View.CenterX = val; }
		public function get Active():Boolean { return View.stage != null; }
		public function get Center():Point { return View.Center; }
		public function get CenterX():Number { return View.CenterX; }
		public function get CenterY():Number { return View.CenterY; }
		public function get Left():Number { return View.Left; }
		public function get Right():Number { return View.Right; }
		public function get Top():Number { return View.Top; }
		public function get Bottom():Number { return View.Bottom; }
		public function get View():RenderableObject { return view; }
		private function onAddedToStage(e:Event):void
		{
			dispatchEvent(new Event(Added));
			View.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			View.addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		}
		private function onEffectRemoved(e:Event):void
		{
			for (var i:int = 0; i < attaching_effects.length; i++)
			{
				if (attaching_effects[i] == e.currentTarget)
				{
					attaching_effects.splice(i, 1)[0].removeEventListener(Removed, onEffectRemoved);
					break;
				}
			}
		}
		private function onRemovedFromStage(e:Event):void
		{
			dispatchEvent(new Event(Removed));
			View.removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			View.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
	}

}