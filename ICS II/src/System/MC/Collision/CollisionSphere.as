package System.MC.Collision 
{
	import flash.display.InteractiveObject;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author ...
	 */
	public class CollisionSphere extends CollisionBox 
	{		
		private var center:Point;
		private var radius:Number;
		public function CollisionSphere(center:Point, radius:Number, intersectDirection:uint = uint.MAX_VALUE) 
		{
			if (intersectDirection == uint.MAX_VALUE)
				intersectDirection = CollisionBox.IntersectDirection_All;
			super(intersectDirection);
			if (center == null)
				throw new ArgumentError();
			this.center = center;
			this.radius = radius;
		}
		public function IntersectX(y:Number):Vector.<Number>
		{
			var outgoing:Vector.<Number> = new Vector.<Number>();
			if (center.y + radius < y || center.y - radius > y)
				return outgoing;
				
			outgoing.push(-(Math.sqrt((radius ^ 2) - ((y - center.y) ^ 2))) + center.x);
			outgoing.push((Math.sqrt((radius ^ 2) - ((y - center.y) ^ 2))) + center.x);
			return outgoing;
		}
		public function IntersectY(x:Number):Vector.<Number>
		{
			var outgoing:Vector.<Number> = new Vector.<Number>();
			if (center.x + radius < x || center.x - radius > x) 
				return outgoing;
				
			outgoing.push( -(Math.sqrt((radius ^ 2) - ((x - center.x) ^ 2))) + center.y);
			outgoing.push((Math.sqrt((radius ^ 2) - ((x - center.x) ^ 2))) + center.y);
			return outgoing;
		}
		public override function MoveByOffset(x:int, y:int):void
		{
			center.x += x;
			center.y += y;
		}
		public override function set Center(val:Point):void 
		{ 
			if (val == null)
				throw new ArgumentError();
			center = val; 
		}
		public override function CreateBoundingBox():Rectangle 
		{ 
			return new Rectangle(center.x - radius, center.y - radius, radius * 2, radius * 2); 
		}
		public override function get Center():Point { return new Point(center.x, center.y); }
		public override function get Top():Number { return center.y - radius; }
		public override function get Bottom():Number { return center.y + radius; }
		public override function get Left():Number { return center.x - radius; }
		public override function get Right():Number { return center.x + radius; }
		public override function get Width():Number { return radius * 2; }
		public override function get Height():Number { return radius * 2; }
		public override function get CenterX():Number { return center.x; }
		public override function get CenterY():Number { return center.y; }
		public override function set CenterX(val:Number):void { center.x = val; }
		public override function set CenterY(val:Number):void { center.y = val; }
		public override function set Left(val:Number):void { center.x = val + radius; }
		public override function set Right(val:Number):void { center.x = val - radius; }
		public override function set Bottom(val:Number):void { center.y = val - radius; }
		public override function set Top(val:Number):void { center.y = val + radius; }
		public function get Radius():Number { return radius; }
		public override function Adjust(offsetX:int, offsetY:int, newWidth:uint, newHeight:uint):void 
		{ 
			if (newWidth != newHeight)
				throw new ArgumentError("CollisionSphere must have width and height equally");
			
			center.x += offsetX;
			center.y += offsetY;
			radius = newWidth;
		}
		public override function IntersectsLine(x1:Number, x2:Number, y1:Number, y2:Number, movement:Point):Boolean
		{
			if (!IntersectsFromEveryDirection &&
				!(IntersectsFromLeft && movement.x < 0) &&
				!(IntersectsFromRight && movement.x > 0) &&
				!(IntersectsFromTop && movement.y < 0) &&
				!(IntersectsFromBottom && movement.y > 0)) return false;
				
			var m1:Number = (y2 - y1) / (x2 - x1);
			var m2:Number = -(1 / m1);
			if (!isNaN(m1) && isFinite(m1) && !isNaN(m2) && isFinite(m2))
			{
				var c1:Number = y1 - (m1 * x1);
				
				var c2:Number = CenterY - (m2 * CenterX);
				
				var x:Number = (c1 - c2) / (m2 - m1);
				var y:Number = ((m2 * c1) - (m1 * c2)) / (m2 - m1);
				
				return ((Left < x && Right > x) && (Top < y && Bottom > y)) &&
					((x1 > x && x2 < x) || (x1 < x && x2 > x)) && 
					((y1 > y && y2 < y) || (y1 < y && y2 > y)) && 
					Point.distance(center, new Point(x, y)) <= radius;
			}
			else
			{
				if (isNaN(m1) || !isFinite(m1))
					return ((y2 > CenterY && y1 < CenterY) || (y2 < CenterY && y1 > CenterY)) &&
						Point.distance(center, new Point(x2, CenterY)) <= radius;
				else if (isNaN(m2) || !isFinite(m2))
					return ((x2 > CenterX && x1 < CenterX) || (x2 < CenterX && x1 > CenterX)) && 
						Point.distance(center, new Point(CenterX, y2)) <= radius;
				else 
					throw new Error("Invalid condition: m1 = " + m1 + ", m2 = " + m2);
			}
		}
		public override function IntersectsPoint(point:Point, movement:Point):Boolean 
		{
			if (point == null)
				throw new ArgumentError();
				
			if (IntersectsFromEveryDirection ||
				(IntersectsFromTop && movement.y < 0) ||
				(IntersectsFromBottom && movement.y > 0) ||
				(IntersectsFromLeft && movement.x < 0) ||
				(IntersectsFromRight && movement.x > 0))
				return Point.distance(point, this.center) <= radius;
			else return false;
		}
		public override function IntersectsCircle(center:Point, radius:Number, movement:Point):Boolean 
		{
			if (center == null || movement == null)
				throw new ArgumentError();
				
			if (IntersectsFromEveryDirection ||
				(IntersectsFromTop && movement.y < 0) ||
				(IntersectsFromBottom && movement.y > 0) ||
				(IntersectsFromLeft && movement.x < 0) ||
				(IntersectsFromRight && movement.x > 0))
				return Point.distance(center, this.center) <= radius + this.radius;
			else return false;
		}
		public override function IntersectsRect(rectangle:Rectangle, movement:Point):Boolean
		{
			if (rectangle == null || movement == null)
				throw new ArgumentError();
				
			if (IntersectsFromEveryDirection ||
				(IntersectsFromTop && movement.y < 0) ||
				(IntersectsFromBottom && movement.y > 0) ||
				(IntersectsFromLeft && movement.x < 0) ||
				(IntersectsFromRight && movement.x > 0))
			{
				if (rectangle.containsPoint(center)) return true;
				
				var rectTopLeft:Point = rectangle.topLeft;
				var rectTopRight:Point = new Point(rectangle.right, rectangle.top);
				var rectBottomLeft:Point = new Point(rectangle.left, rectangle.bottom);
				var rectBottomRight:Point = rectangle.bottomRight;
				
				if (Point.distance(rectTopLeft, center) < radius || Point.distance(rectTopRight, center) < radius ||
					Point.distance(rectBottomLeft, center) < radius || Point.distance(rectBottomRight, center) < radius)
					return true;
				
				var cirE:Point = new Point(center.x + radius, center.y);
				var cirN:Point = new Point(center.x, center.y - radius);
				var cirS:Point = new Point(center.x, center.y + radius);
				var cirW:Point = new Point(center.x - radius, center.y);
				
				if (rectangle.containsPoint(cirE) || rectangle.containsPoint(cirW) ||
					rectangle.containsPoint(cirN) || rectangle.containsPoint(cirS))
					return true;
				return false;
			}
			else return false;
		}
	}

}