package System.MC.Collision 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import System.MC.Tile;
	/**
	 * ...
	 * @author Knight
	 */
	public class CollisionTile extends CollisionBox 
	{
		private var collision_info:Vector.<Vector.<int>>;
		private var offset:Point;
		private var height_offset:Number;
		public function CollisionTile(collision_info:Vector.<Vector.<int>>, direction:uint = uint.MAX_VALUE, height_offset:Number = 0)
		{
			super(direction);
			this.collision_info = collision_info;
			offset = new Point();
			this.height_offset = height_offset;
		}
		public override function get Top():Number { return offset.y; }
		public override function get Bottom():Number { return offset.y + (collision_info.length * Tile.Size.y); }
		public override function get Left():Number { return offset.x; }
		public override function get Right():Number { return offset.x + Width; }
		public override function get Width():Number { return collision_info[0].length * Tile.Size.x; }
		public override function get Height():Number { return collision_info.length * Tile.Size.y; }
		public override function set Width(val:Number):void { throw new Error("Not supported"); }
		public override function set Height(val:Number):void { throw new Error("Not supported"); }
		public override function set Right(val:Number):void { Left = val - Width; }
		public override function set Left(val:Number):void { offset.x = val; }
		public override function set Top(val:Number):void { offset.y = val; }
		public override function set Bottom(val:Number):void { Top = val - Height; }
		public override function set Center(val:Point):void 
		{
			CenterX = val.x;
			offset.y = val.y - Height / 2;
		}
		public override function set CenterX(val:Number):void { offset.x = val - Width / 2; } 
		public override function set CenterY(val:Number):void { offset.y = val - Height / 2; }
		public override function GetTopAtPos(left:Number, right:Number, y:Number):Number 
		{
			var x_tile_start:Number = Math.max(Math.floor((left - Left) / Tile.Size.x), 0);
			var x_tile_end:Number = Math.min(Math.floor((right - 1 - Left) / Tile.Size.x), collision_info[0].length - 1);
			var y_tile:Number = Math.max(Math.floor((y - Top) / Tile.Size.y), 0);
			if (x_tile_end >= 0 && x_tile_start < collision_info[0].length)
			{
				for (var i:int = y_tile; i < collision_info.length; i++)
				{
					for (var j:int = x_tile_start; j <= x_tile_end; j++)
					{
						if (collision_info[i][j] > 0) 
							return (Top + i * Tile.Size.y) - height_offset;
					}
				}
			}
			return NaN;
		}
		public override function GetBottomAtPos(left:Number, right:Number, y:Number):Number 
		{
			var x_tile_start:Number = Math.max(Math.floor((left - Left) / Tile.Size.x), 0);
			var x_tile_end:Number = Math.min(Math.floor((right - Left) / Tile.Size.x), collision_info[0].length);
			var y_tile:Number = Math.min(Math.floor((y - Top) / Tile.Size.y), collision_info.length);
			if (x_tile_end >= 0 && x_tile_start < collision_info[0].length)
			{
				for (var i:int = collision_info.length - 1; i >= y_tile; i--)
				{
					for (var j:int = x_tile_start; j <= x_tile_end; j++)
					{
						if (collision_info[i][j] > 0) 
							return Top + (i + 1) * Tile.Size.y;
					}
				}
			}
			return NaN;
		}
		public override function GetLeftAtPos(x:Number, top:Number, bottom:Number):Number 
		{ 
			var x_tile:Number = Math.max(Math.floor((x - Left) / Tile.Size.x), 0);
			var y_tile_start:Number = Math.max(Math.floor((top + 1 - Top) / Tile.Size.y), 0);
			var y_tile_end:Number = Math.min(Math.floor((bottom - 1 - Top) / Tile.Size.y), collision_info.length);
			if (y_tile_end >= 0 && y_tile_start < collision_info.length)
			{
				for (var i:int = x_tile; i < collision_info[0].length; i++)
				{
					for (var j:int = y_tile_start; j <= y_tile_end && j <= collision_info.length - 1; j++)
					{
						if (collision_info[j][i] > 0)
							return Left + (i) * Tile.Size.x;
					}
				}
			}
			return NaN;
		}
		public override function GetRightAtPos(x:Number, top:Number, bottom:Number):Number
		{
			var x_tile:Number = Math.min(Math.floor((x - Left) / Tile.Size.x), collision_info[0].length - 1);
			var y_tile_start:Number = Math.max(Math.floor((top + 1 - Top) / Tile.Size.y), 0);
			var y_tile_end:Number = Math.min(Math.floor((bottom - 1 - Top) / Tile.Size.y), collision_info.length);
			if (y_tile_end >= 0 && y_tile_start < collision_info.length)
			{
				for (var i:int = x_tile; i >= 0; i--)
				{
					for (var j:int = y_tile_start; j <= y_tile_end; j++)
					{
						if (collision_info[j][i] > 0) 
							return Left + (i + 1) * Tile.Size.x;
					}
				}
			}
			return NaN;
		}
		public override function Adjust(offsetX:int, offsetY:int, newWidth:uint, newHeight:uint):void { throw new Error("Not supported"); }
		public override function CreateBoundingBox():Rectangle { return new Rectangle(offset.x, offset.y, Width, Height); }
		public override function IntersectsPoint(point:Point, movement:Point):Boolean 
		{
			if (!IntersectsFromEveryDirection && ((movement == null) ||
				!(IntersectsFromLeft && movement.x < 0) &&
				!(IntersectsFromRight && movement.x > 0) &&
				!(IntersectsFromTop && movement.y < 0) &&
				!(IntersectsFromBottom && movement.y > 0))) return false;
				
			var x_tile:Number = Math.floor((point.x - Left) / Tile.Size.x);
			var y_tile:Number = Math.floor((point.y - Top) / Tile.Size.y);
			return y_tile >= 0 && x_tile >= 0 && 
				y_tile < collision_info.length && 
				x_tile < collision_info[y_tile].length && 
				collision_info[y_tile][x_tile] > 0 &&
				(point.y - Top) - (y_tile * Tile.Size.y) >= height_offset;
		}		
		public override function IntersectsCircle(center:Point, radius:Number, movement:Point):Boolean { throw new Error("Not implemented"); }
		public override function IntersectsRect(rectangle:Rectangle, movement:Point):Boolean 
		{ 
			if (!IntersectsFromEveryDirection &&
				!(IntersectsFromLeft && movement.x < 0) &&
				!(IntersectsFromRight && movement.x > 0) &&
				!(IntersectsFromTop && movement.y < 0) &&
				!(IntersectsFromBottom && movement.y > 0)) return false;
				
			var result:int = 0;
			var left:int = Math.floor((rectangle.left - Left) / Tile.Size.x);
			var right:int = Math.ceil((rectangle.right - Left) / Tile.Size.x);
			var top:int = Math.floor((rectangle.top - Top) / Tile.Size.y);
			var bottom:int = Math.ceil((rectangle.bottom - Top) / Tile.Size.y);
			if ((rectangle.bottom - Top) - ((bottom - 1) * Tile.Size.y) < height_offset)
				bottom--;
			if (right >= 0 && left < collision_info[0].length && top < collision_info.length && bottom >= 0)
			{
				for (var i:int = Math.max(0, top); i < Math.min(bottom, collision_info.length); i++)
					for (var j:int = Math.max(0, left); j < Math.min(right, collision_info[i].length); j++)
				{
					if (collision_info[i][j] >= 0) return true;
				}
			}
			return false;
		}
		public override function IntersectsLine(x1:Number, x2:Number, y1:Number, y2:Number, movement:Point):Boolean 
		{ 
			return false;
		}
		public override function MoveByOffset(x:int, y:int):void 
		{ 
			offset.x += x; 
			offset.y += y; 
		}
		
	}

}