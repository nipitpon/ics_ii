package System.MC.Collision 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author Knight
	 */
	public class CollisionSet extends CollisionBox 
	{
		private var child:Vector.<CollisionBox>;
		private var bigBox:Rectangle;
		public function CollisionSet() 
		{
			super(IntersectDirection_All);
			child = new Vector.<CollisionBox>();
			bigBox = new Rectangle();
		}
		public function AddChild(child:CollisionBox):void
		{
			this.child.push(child);
			RefreshBigBox();
		}
		public override function CreateBoundingBox():Rectangle
		{
			return new Rectangle(bigBox.x, bigBox.y, bigBox.width, bigBox.height);
		}
		public override function MoveByOffset(x:int, y:int):void
		{				
			for each(var box:CollisionBox in child)
				box.MoveByOffset(x, y);
				
			bigBox.x += x;
			bigBox.y += y;
		}
		public override function set Center(val:Point):void
		{
			if (val == null)
				throw new ArgumentError();
			
			var c:Point = Center;
			c.x -= val.x;
			c.y -= val.y;
			MoveByOffset(c.x, c.y);
		}
		public override function set Top(val:Number):void { MoveByOffset(0, val - bigBox.top ); }
		public override function set Left(val:Number):void { MoveByOffset(val - bigBox.left, 0); }
		public override function set Right(val:Number):void { MoveByOffset(val - bigBox.right, 0); }
		public override function set Bottom(val:Number):void { MoveByOffset(0, val - bigBox.bottom); }
		public override function get Bottom():Number { return bigBox.bottom; }
		public override function get Right():Number { return bigBox.right; }
		public override function get Left():Number { return bigBox.left; }
		public override function get Top():Number { return bigBox.top; }
		public override function get Center():Point
		{
			return new Point(bigBox.x + bigBox.width / 2, bigBox.y + bigBox.height / 2);
		}
		public override function IntersectsPoint(point:Point, movement:Point):Boolean 
		{
			if (point == null || movement == null)
				throw new ArgumentError();
				
			for each(var box:CollisionBox in child)
			{
				if (box.IntersectsPoint(point, movement)) return true;
			}
			return false;
		}
		public override function IntersectsCircle(center:Point, radius:Number, movement:Point):Boolean 
		{
			if (center == null || movement == null)
				throw new ArgumentError();
				
			for each(var box:CollisionBox in child)
			{
				if (box.IntersectsCircle(center, radius, movement)) return true;
			}
			return false;
		}
		public override function IntersectsRect(rectangle:Rectangle, movement:Point):Boolean
		{
			if (rectangle == null || movement == null)
				throw new ArgumentError();
			
			for each(var box:CollisionBox in child)
			{
				if (box.IntersectsRect(rectangle, movement)) return true;
			}
			return false;
		}
		
		private function RefreshBigBox():void
		{
			bigBox = null;
			for each(var c:CollisionBox in child)
			{
				var bound:Rectangle = c.CreateBoundingBox();
				
				if (bigBox == null)
					bigBox = bound;
				else
				{
					if (bigBox.x > bound.x) bigBox.x = bound.x;
					if (bigBox.y > bound.y) bigBox.y = bound.y;
					if (bigBox.right < bound.right) bigBox.right = bound.right;
					if (bigBox.bottom < bound.bottom) bigBox.bottom = bound.bottom;
				}
			}
		}
	}

}