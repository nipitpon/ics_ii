package System.MC.Collision 
{
	import flash.geom.Point;
	import System.GameSystem;
	import System.Interfaces.ICollidable;
	import System.MC.Characters.Monster;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class PlayerBound extends CollidableObject 
	{
		public function PlayerBound() 
		{
			super(new RenderableObject(), new CollisionRectangle(0, 0, 50, 3000, CollisionBox.IntersectDirection_All));
			collide_with.push(Player, Monster);
		}
		public override function get Active():Boolean { return true; }
		public override function get Center():Point { return Collision.Center; }
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			super.TakenHit(obj, movement);
			if (obj is Player)
			{
				if (obj.CenterX > CenterX) movement.x = Collision.Right - obj.Collision.Left;
				else if (obj.CenterX < CenterX) movement.x = Collision.Left - obj.Collision.Right;
				
				var collide:Vector.<ICollidable> = GameSystem.GetIntersectedCollidable(obj.Collision, new Point(movement.x, 0));
				var found:Boolean = false;
				for each(var col:ICollidable in collide)
				{
					if (col is Tile)
					{
						found = true;
						break;
					}
				}
				if (found)
				{
					(obj as Player).DieFromSpike();
				}
			}
		}
	}

}