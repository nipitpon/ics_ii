package System.MC.Collision 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.engine.DigitWidth;
	/**
	 * ...
	 * @author ...
	 */
	public class CollisionRectangle extends CollisionBox 
	{
		private var collision:Rectangle;
		public function CollisionRectangle(x:int, y:int, width:Number, height:Number, 
			intersectDirection:uint = uint.MAX_VALUE) 
		{
			if (intersectDirection == uint.MAX_VALUE)
				intersectDirection = CollisionBox.IntersectDirection_All;
			super(intersectDirection);
			collision = new Rectangle(x, y, Math.round(width), Math.round(height));
		}
		public function set X(val:Number):void { collision.x = val; }
		public function set Y(val:Number):void { collision.y = val; }
		public override function set Width(val:Number):void { collision.width = val; }
		public override function set Height(val:Number):void { collision.height = val; }
		public override function set Center(val:Point):void
		{
			if (val == null)
				throw new ArgumentError();
				
			collision.x = val.x - collision.width / 2;
			collision.y = val.y - collision.height / 2;
		}
		public override function MoveByOffset(x:int, y:int):void
		{
			collision.x += x;
			collision.y += y;
		}
		public override function set Top(val:Number):void { Y = val; }
		public override function set Bottom(val:Number):void { Y = val - Height; }
		public override function set Left(val:Number):void { X = val; }
		public override function set Right(val:Number):void { X = val - Width; }
		public override function set CenterX(val:Number):void { X = val - Width / 2; }
		public override function set CenterY(val:Number):void { Y = val - Height / 2; }
		
		public override function get Top():Number { return Y; }
		public override function get Bottom():Number { return Y + Height; }
		public override function get Left():Number { return X; }
		public override function get Right():Number { return X + Width; }
		public function get X():Number { return collision.x; }
		public function get Y():Number { return collision.y; }
		public override function get Width():Number { return collision.width; }
		public override function get Height():Number { return collision.height; }
		public override function get CenterX():Number { return collision.left + collision.width / 2; }
		public override function get CenterY():Number { return collision.top + collision.height / 2; }
		public function get Rect():Rectangle { return collision; }
		public override function GetTopAtPos(left:Number, right:Number, y:Number):Number 
		{ 
			if (right >= collision.left && left <= collision.right) return collision.top;
			return NaN;
		}
		public override function GetBottomAtPos(left:Number, right:Number, y:Number):Number 
		{
			if (right >= collision.left && left <= collision.right) return collision.bottom;
			return NaN;
		}
		public override function GetLeftAtPos(x:Number, top:Number, bottom:Number):Number 
		{
			if (bottom >= collision.top && top <= collision.bottom) return collision.left;
			return NaN;
		}
		public override function GetRightAtPos(x:Number, top:Number, bottom:Number):Number 
		{
			if (x > collision.left && bottom >= collision.top && top <= collision.bottom) return collision.right;
			return NaN;
		}
		public override function Adjust(offsetX:int, offsetY:int, newWidth:uint, newHeight:uint):void 
		{ 
			collision.x += offsetX;
			collision.y += offsetY;
			collision.width = newWidth;
			collision.height = newHeight;
		}
		public override function CreateBoundingBox():Rectangle { return new Rectangle(collision.x, collision.y, collision.width, collision.height); }
		public override function get Center():Point
		{
			return new Point(collision.x + collision.width / 2, collision.y + collision.height / 2);
		}
		public override function IntersectsLine(x1:Number, x2:Number, y1:Number, y2:Number, movement:Point):Boolean
		{ 
			if (!IntersectsFromEveryDirection &&
				!(IntersectsFromLeft && movement.x < 0) &&
				!(IntersectsFromRight && movement.x > 0) &&
				!(IntersectsFromTop && movement.y < 0) &&
				!(IntersectsFromBottom && movement.y > 0)) return false;
				
			var m:Number = (y2 - y1) / (x2 - x1);
			
			var xRight:Number = Math.max(x2, x1);
			var xLeft:Number = Math.min(x2, x1);
			var yTop:Number = Math.min(y2, y1);
			var yBottom:Number = Math.max(y2, y1);
			if (!isNaN(m) && isFinite(m) && m != 0)
			{
				var c:Number = y1 - (m * x1);
				
				var intersect:Number = (collision.top - c) / m;
				if (intersect >= xLeft && intersect <= xRight &&
					intersect >= collision.left && intersect <= collision.right) return true;
				
				intersect = (collision.bottom - c) / m;
				if (intersect >= xLeft && intersect <= xRight &&
					intersect >= collision.left && intersect <= collision.right) return true;
				
				intersect = (m * collision.left) + c;
				if (intersect <= yBottom && intersect >= yTop &&
					intersect >= collision.top && intersect <= collision.bottom) return true;
				
				intersect = (m * collision.right) + c;
				if (intersect <= yBottom && intersect >= yTop &&
					intersect >= collision.top && intersect <= collision.bottom) return true;
				
				return false;
			}
			else
			{
				if (m == 0)
				{
					return (y1 >= collision.top && y1 <= collision.bottom) && 
						((xLeft <= collision.left && xRight >= collision.right) ||
						(collision.contains(xLeft, yTop) || collision.contains(xRight, yBottom)));
				}
				else
				{
					return (x1 >= collision.left && x1 <= collision.right) && 
						((yTop <= collision.top && yBottom >= collision.bottom) ||
						(collision.contains(x1, yTop) || collision.contains(x2, yBottom)));
				}
			}
		}
		public override function IntersectsPoint(point:Point, movement:Point):Boolean
		{
			if (!IntersectsFromEveryDirection && ((movement == null) ||
				!(IntersectsFromLeft && movement.x < 0) &&
				!(IntersectsFromRight && movement.x > 0) &&
				!(IntersectsFromTop && movement.y < 0) &&
				!(IntersectsFromBottom && movement.y > 0))) return false;
			else return collision.containsPoint(point);
		}
		public override function IntersectsCircle(center:Point, radius:Number, movement:Point):Boolean
		{
			if (movement == null || center == null || radius == 0)
				throw new ArgumentError();
				
			if (IntersectsFromEveryDirection ||
				(IntersectsFromLeft && movement.x < 0) ||
				(IntersectsFromRight && movement.x > 0) ||
				(IntersectsFromTop && movement.y < 0) ||
				(IntersectsFromBottom && movement.y > 0))
			{
				if (IntersectsPoint(center, movement)) 
					return true;
				
				var rectTopLeft:Point = collision.topLeft;
				var rectTopRight:Point = new Point(collision.right, collision.top);
				var rectBottomLeft:Point = new Point(collision.left, collision.bottom);
				var rectBottomRight:Point = collision.bottomRight;
				
				if (Point.distance(rectTopLeft, center) < radius || Point.distance(rectTopRight, center) < radius ||
					Point.distance(rectBottomLeft, center) < radius || Point.distance(rectBottomRight, center) < radius)
					return true;
				
				var cirE:Point = new Point(center.x + radius, center.y);
				var cirN:Point = new Point(center.x, center.y - radius);
				var cirS:Point = new Point(center.x, center.y + radius);
				var cirW:Point = new Point(center.x - radius, center.y);
				
				if (collision.containsPoint(cirE) || collision.containsPoint(cirW) ||
					collision.containsPoint(cirN) || collision.containsPoint(cirS))
					return true;
				return false;
			}
			else return false;
		}
		public override function IntersectsRect(rectangle:Rectangle, movement:Point):Boolean
		{
			if (movement == null || rectangle == null)
				throw new ArgumentError();
				
			if (IntersectsFromEveryDirection ||
				(IntersectsFromRight && movement.x > 0) ||
				(IntersectsFromLeft && movement.x < 0) ||
				(IntersectsFromTop && movement.y < 0) ||
				(IntersectsFromBottom && movement.y > 0))
				return  collision.intersects(rectangle);
			else return false;
		}
	}

}