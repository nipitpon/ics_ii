package System.MC.Collision 
{
	import flash.display.Shape;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IMVC;
	import System.MC.Overlay;
	import System.V.RenderableObject;
	import System.MC.ModelViewController;
	
	/**
	 * ...
	 * @author ...
	 */
	public class CollidableObject extends ModelViewController implements ICollidable
	{
		private var colision:CollisionBox = null;
		protected var collide_with:Vector.<Class>;
		//private var v_center:Shape;
		//private var c_center:Shape;
		public function CollidableObject(view:RenderableObject, collisionBox:CollisionBox) 
		{
			super(view);
			collide_with = new Vector.<Class>();
			this.colision = collisionBox;
			//v_center = new Shape();
			//v_center.graphics.lineStyle(2, 0xff0000);
			//v_center.graphics.drawCircle(0, 0, 10);
			//
			//c_center = new Shape();
			//c_center.graphics.lineStyle(2, 0x0000ff);
			//c_center.graphics.drawCircle(0, 0, 15);
			//v_center.x = view.width / 2;
			//v_center.y = view.height / 2;
			//
			//c_center.x = collisionBox.Left + collisionBox.Width/2;
			//c_center.y = collisionBox.Top + collisionBox.Height / 2;
			//view.addChild(v_center);
			//view.addChild(c_center);
		}
		public function CollideWith(c:*):Boolean
		{
			for each(var cl:Class in collide_with)
				if (c is cl) return true;
				
			return false;
		}
		public function IntersectsLine(x1:Number, x2:Number, y1:Number, y2:Number, movement:Point):ICollidable
		{
			return colision.IntersectsLine(x1, x2, y1, y2, movement) ? this : null;
		}
		public function IntersectsPoint(point:Point, movement:Point):ICollidable
		{
			if (colision.IntersectsPoint(point, movement)) return this;
			else return null;
		}
		public function Intersects(collisionBox:CollisionBox, movement:Point):ICollidable
		{
			if (movement == null)
				throw new ArgumentError();
			var x1:Number = collisionBox.CenterX;
			var y1:Number = collisionBox.CenterY;
			collisionBox.MoveByOffset(movement.x, movement.y);
			var x2:Number = collisionBox.CenterX;
			var y2:Number = collisionBox.CenterY;
			var result:Boolean = false;
			if (collisionBox is CollisionSphere)
			{
				var sphere:CollisionSphere = collisionBox as CollisionSphere;
				result = colision.IntersectsCircle(sphere.Center, sphere.Radius, movement);
			}
			else if (collisionBox is CollisionRectangle)
			{
				var rect:CollisionRectangle = collisionBox as CollisionRectangle;
				result = colision.IntersectsRect(rect.Rect, movement);
			}
			if (!result && (movement.x != 0 || movement.y != 0))
			{
				result = colision.IntersectsLine(x1, x2, y1, y2, movement);
			}
			
			collisionBox.MoveByOffset(-movement.x, -movement.y);
			if (result) return this;
			else return null;
		}
		public function TakenHit(obj:ICollidable, movement:Point):void {}
		public function Hit(obj:ICollidable, movement:Point):void {}
		public function get Collision():CollisionBox { return colision; }
		public override function MoveByOffset(x:int, y:int):void
		{
			super.MoveByOffset(x, y);
			MoveCollisionBoxByOffset(x, y);
		}
		public function MoveCollisionBoxByOffset(x:int, y:int):void
		{
			colision.MoveByOffset(x, y);
		}
		public override function set CenterX(val:Number):void
		{
			super.CenterX = val;
			Collision.CenterX = val;
		}
		public override function set CenterY(val:Number):void
		{
			super.CenterY = val;
			Collision.CenterY = val;
		}
		public override function set Center(val:Point):void
		{
			var thisCenter:Point = Center;
			var colCenter:Point = colision.Center;
			var newColCenter:Point = new Point(colCenter.x - thisCenter.x + val.x, colCenter.y - thisCenter.y + val.y);
			
			super.Center = val;
			
			colision.Center = newColCenter;
		}
		public override function set Left(val:Number):void
		{
			var offset:Number = View.Left - Collision.Left;
			colision.Left = val - offset;
			super.Left = (val);
		}
		public override function set Right(val:Number):void
		{
			var offset:Number = View.Right - colision.Right;
			colision.Right = val;
			super.Right = (val + offset);
		}
		public override function set Top(val:Number):void
		{
			var offset:Number = colision.Top - View.Top;
			colision.Top = val + offset;
			super.Top = (val);
		}
		public override function set Bottom(val:Number):void
		{
			var offset:Number = colision.Bottom - View.Bottom;
			colision.Bottom = val;
			super.Bottom = (val - offset);
		}
		
		protected function SetCollisionBox(box:CollisionBox):void
		{
			colision = box;
		}
	}

}