package System.MC.Collision 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author ...
	 */
	public class CollisionBox
	{
		public static const IntersectDirection_Left:uint = 0x8;
		public static const IntersectDirection_Right:uint = 0x4;
		public static const IntersectDirection_Top:uint = 0x2;
		public static const IntersectDirection_Bottom:uint = 0x1;
		public static const IntersectDirection_All:uint = IntersectDirection_Bottom | IntersectDirection_Left
					| IntersectDirection_Right | IntersectDirection_Top;
		private var intersectDirection:uint;
		/*
		 * bit 0x 1 1 1 1
		 * 1. left       sent parameter is moving leftward
		 * 2. right      sent parameter is moving rightward
		 * 3. top        sent parameter is moving upward
		 * 4. bottom     sent parameter is moving downward;
		 * */
		public function CollisionBox(intersectDirection:uint) 
		{
			this.intersectDirection = intersectDirection;
		}
		public function get IntersectsFromTop():Boolean { return (intersectDirection & IntersectDirection_Top) != 0;}
		public function get IntersectsFromBottom():Boolean { return (intersectDirection & IntersectDirection_Bottom) != 0; }
		public function get IntersectsFromLeft():Boolean { return (intersectDirection & IntersectDirection_Left) != 0; }
		public function get IntersectsFromRight():Boolean { return (intersectDirection & IntersectDirection_Right) != 0; }
		public function get IntersectsFromEveryDirection():Boolean { return (intersectDirection & IntersectDirection_All) == IntersectDirection_All; }
		public function get CenterX():Number { return (Left + Right) / 2; }
		public function get CenterY():Number { return (Top + Bottom) / 2; }
		public function get Center():Point { return new Point(CenterX, CenterY); }
		public function get Top():Number { throw new Error("Not implemented"); }
		public function get Bottom():Number { throw new Error("Not implemented"); }
		public function get Left():Number { throw new Error("Not implemented"); }
		public function get Right():Number { throw new Error("Not implemented"); }
		public function get Width():Number { throw new Error("Not implemented"); }
		public function get Height():Number { throw new Error("Not implemented"); }
		public function set Width(val:Number):void { throw new Error("Not implemented"); }
		public function set Height(val:Number):void { throw new Error("Not implemented"); }
		public function set Right(val:Number):void { throw new Error("Not implemented"); }
		public function set Left(val:Number):void { throw new Error("Not implemented"); }
		public function set Top(val:Number):void { throw new Error("Not implemented"); }
		public function set Bottom(val:Number):void { throw new Error("Not implemented"); }
		public function set Center(val:Point):void { throw new Error("Not implemented"); }
		public function set CenterX(val:Number):void { throw new Error("Not implemented"); } 
		public function set CenterY(val:Number):void { throw new Error("Not implemented"); }
		public function Adjust(offsetX:int, offsetY:int, newWidth:uint, newHeight:uint):void { throw new Error("Not implemented"); }
		public function CreateBoundingBox():Rectangle { throw new Error("Not implemented"); }
		public function IntersectsPoint(point:Point, movement:Point):Boolean { throw new Error("Not implemented"); }
		public function IntersectsCircle(center:Point, radius:Number, movement:Point):Boolean { throw new Error("Not implemented"); }
		public function IntersectsRect(rectangle:Rectangle, movement:Point):Boolean { throw new Error("Not implemented"); }
		public function IntersectsLine(x1:Number, x2:Number, y1:Number, y2:Number, movement:Point):Boolean { throw new Error("Not implemented"); }
		public function MoveByOffset(x:int, y:int):void { throw new Error("Not implemented"); }
		public function GetTopAtPos(left:Number, right:Number, y:Number):Number { throw new Error("Not implemented"); }
		public function GetBottomAtPos(left:Number, right:Number, y:Number):Number { throw new Error("Not implemented"); }
		public function GetLeftAtPos(x:Number, top:Number, bottom:Number):Number { throw new Error("Not implemented"); }
		public function GetRightAtPos(x:Number, top:Number, bottom:Number):Number { throw new Error("Not implemented"); }
		public static function Distance(box1:CollisionBox, box2:CollisionBox, x:Boolean, y:Boolean):Number
		{
			if (x && y)
			{
				var distX:Number = Distance(box1, box2, true, false);
				var distY:Number = Distance(box1, box2, false, true);
				return new Point(distX, distY).length;
			}
			else if (x)
			{
				return Math.abs(box1.CenterX - box2.CenterX);
			}
			else if (y)
			{
				return Math.abs(box1.CenterY - box2.CenterY);
			}
			return Infinity;
		}
		public static function HitLocation(box:CollisionBox, pointStart:Point, pointEnd:Point):Vector.<Point>
		{
			if (pointStart == null || pointEnd == null || box == null) throw new ArgumentError();
			
			var m:Number = (pointStart.y - pointEnd.y) / (pointStart.x - pointEnd.x);
			var c:Number = pointStart.y - (m * pointStart.x);
			
			var outgoing:Vector.<Point> = new Vector.<Point>();
			if (box is CollisionSphere)
			{
				var sphere:CollisionSphere = box as CollisionSphere;
				var sphereCenter:Point = sphere.Center;
				
				var h:Number = sphereCenter.x;
				var k:Number = sphereCenter.y;
				var r:Number = sphere.Radius;
				outgoing.push(pointEnd);
			}
			else if(box is CollisionRectangle)
			{
				var rect:CollisionRectangle = box as CollisionRectangle;
				
				var yLeft:Number = m * rect.Left + c;
				if ((yLeft >= pointStart.y && yLeft <= pointEnd.y) ||
					(yLeft <= pointStart.y && yLeft >= pointEnd.y))
				{
					outgoing.push(new Point(rect.Left, yLeft));
				}
				
				var yRight:Number = m * rect.Right + c;
				if ((yRight >= pointStart.y && yRight <= pointEnd.y) ||
					(yRight <= pointStart.y && yRight >= pointEnd.y))
				{
					outgoing.push(new Point(rect.Right, yRight));
				}
				
				var xTop:Number = (rect.Top - c) / m;
				if ((xTop >= pointStart.x && xTop <= pointEnd.x) ||
					(xTop <= pointStart.x && xTop >= pointEnd.x))
				{
					outgoing.push(new Point(xTop, rect.Top));
				}
				
				var xBottom:Number = (rect.Bottom - c) / m;
				if ((xBottom >= pointStart.x && xBottom <= pointEnd.x) ||
					(xBottom <= pointStart.x && xBottom >= pointEnd.x))
				{
					outgoing.push(new Point(xBottom, rect.Bottom));
				}
			}
			return outgoing;
		}
	}
}