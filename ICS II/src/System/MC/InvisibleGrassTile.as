package System.MC 
{
	import Resources.Resource;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class InvisibleGrassTile extends Tile 
	{
		
		public function InvisibleGrassTile(edgeLeft:Boolean, edgeRight:Boolean, blockFromBottom:Boolean = true, length:int = 1) 
		{
			var code:int = CollisionBox.IntersectDirection_All;
			if (!blockFromBottom)
				code = CollisionBox.IntersectDirection_Bottom;
			super(true, new CollisionRectangle(0, Tile.Size.y / 10, Tile.Size.x * length, Tile.Size.y * 0.2, code));
			Initialize(edgeLeft, edgeRight, length);
		}
		
		private function Initialize(edgeLeft:Boolean, edgeRight:Boolean, length:int):void
		{
			for (var i:int = 0; i < length; i++)
			{
				var surface:RenderableObject = Resource.GetImage(Utility.Random(2, 5));
				surface.x = Tile.Size.x * i;
				View.addChild(surface);
			}
			if (edgeLeft)
			{
				var eLeft:RenderableObject = Resource.GetImage(0);
				eLeft.x++;
				View.addChild(eLeft);
			}
			if (edgeRight)
			{
				var eRight:RenderableObject = Resource.GetImage(1);
				eRight.Left = (Tile.Size.x * (length - 1)) - 1;
				View.addChild(eRight);
			}
		}
	}

}