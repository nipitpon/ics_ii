package System.MC 
{
	import Resources.Resource;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class CaveRoof extends Tile 
	{
		
		public function CaveRoof(edgeLeft:Boolean, edgeRight:Boolean, edgeBottom:Boolean, edgeTop:Boolean, collisionBox:CollisionBox = null, length:int = 1, height:int = 1) 
		{
			super(false, collisionBox, length, height); 
			Initialize(edgeLeft, edgeRight, edgeBottom, edgeTop, length, height);
		}
		private function Initialize(edgeLeft:Boolean, edgeRight:Boolean, edgeBottom:Boolean, edgeTop:Boolean, length:int, height:int):void
		{
			var i:int = 0;
			for (i = 0; i < length; i++)
			{
				for (var j:int = 0; j < height; j++)
				{
					var obj:RenderableObject = Resource.GetImage(Utility.Random(17, 21));
					obj.x = Size.x * i;
					obj.y = Size.y * j;
					View.addChild(obj);
				}
			}
			if (edgeTop)
			{
				for (i = 0; i < length; i++)
				{
					var surface:RenderableObject = Resource.GetImage(Utility.Random(2, 6));
					surface.x = i * Size.x;
					View.addChild(surface);
				}
			}
			if (edgeLeft)
			{
				var left:RenderableObject = null;
				for (i = 0; i < height; i++)
				{
					left = Resource.GetImage(25);
					left.y = Size.y * i;
					View.addChild(left);
				}
				if (edgeTop)
				{
					left = Resource.GetImage(0);
					left.x += 2;
					View.addChild(left);
				}
			}
			if (edgeRight)
			{
				
				var right:RenderableObject = null;
				for (i = 0; i < height; i++)
				{	
					right = Resource.GetImage(26);
					right.Left = (Tile.Size.x * (length - 1)) - 1;
					right.y = Size.y * i;
					View.addChild(right);
				}
				if (edgeTop)
				{
					right = Resource.GetImage(1);
					right.x = (Tile.Size.x * (length - 1)) - 3;
					View.addChild(right);
				}
			}
			if (edgeBottom)
			{
				for (i = 0; i < length; i++)
				{
					var bottom:RenderableObject = Resource.GetImage(Utility.Random(21, 25));
					bottom.x = Size.x * i;
					bottom.y = Size.y * (height - 1);
					View.addChild(bottom);
				}
			}
		}
	}

}