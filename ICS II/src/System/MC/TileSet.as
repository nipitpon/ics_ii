package System.MC 
{
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import System.V.RenderableObject;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IMVC;
	import System.MC.Collision.CollisionSet;
	import System.MC.Collision.CollisionBox;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class TileSet extends EventDispatcher implements IMVC, ICollidable 
	{
		private var tile:Vector.<Tile>;
		private var collisionSet:CollisionSet;
		private var owner:IMVC;
		public function TileSet(owner:IMVC) 
		{
			tile = new Vector.<Tile>();
			collisionSet = new CollisionSet();
			this.owner = owner;
		}
		public function AddTile(tile:Tile):void
		{
			this.tile.push(tile);
			collisionSet.AddChild(tile.Collision);
		}
		public function get Active():Boolean { return true; }
		public function get Owner():IMVC { return owner; }
		public function get Length():int { return tile.length; }
		public function get Collision():CollisionBox { return collisionSet; }
		public function MoveCollisionBoxByOffset(x:int, y:int):void { throw new Error("Not supported"); }
		public function CollideWith(c:*):Boolean
		{
			for each(var child:Tile in tile)
				if (child.CollideWith(c)) return true;
			return false;
		}
		public function MoveByOffset(x:int, y:int):void
		{
			for each(var t:Tile in tile)
				t.MoveByOffset(x, y);
			collisionSet.MoveByOffset(x, y);
		}
		public function Contains(tile:Tile):Boolean
		{
			for each(var t:Tile in this.tile)
			{
				if (t == tile) return true;
			}
			return false;
		}
		public function get AllowDrop():Boolean
		{
			for each(var t:Tile in this.tile)
			{
				if (!t.AllowDrop) return false;
			}
			return true;
		}
		public function GetTileAtIndex(index:uint):Tile 
		{
			if (index >= tile.length)
				throw new ArgumentError();
		
			return tile[index];
		}
		public function IntersectsLine(x1:Number, x2:Number, y1:Number, y2:Number, movement:Point):ICollidable
		{
			var outgoing:TileSet = new TileSet(this);
			for each(var child:Tile in tile)
			{
				if (child.IntersectsLine(x1, x2, y1, y2, movement)) outgoing.AddTile(child);
			}
			if (outgoing.Length == 0) return null;
			else return outgoing;
		}
		public function IntersectsPoint(point:Point, movement:Point):ICollidable
		{
			var outgoing:TileSet = new TileSet(this);
			for each(var child:Tile in tile)
			{
				if (child.IntersectsPoint(point, movement)) outgoing.AddTile(child);
			}
			if (outgoing.Length == 0) return null;
			else return outgoing;
		}
		public function Intersects(box:CollisionBox, movement:Point):ICollidable
		{
			var outgoing:TileSet = new TileSet(this);
			for each(var child:Tile in tile)
			{
				if (child.Intersects(box, movement)) outgoing.AddTile(child);
			}
			if (outgoing.Length == 0) return null;
			else return outgoing;
		}
		public function TakenHit(obj:ICollidable, movement:Point):void
		{
			for each(var child:Tile in tile)
				child.TakenHit(obj, movement);
		}
		public function Hit(obj:ICollidable, movement:Point):void
		{
			for each(var child:Tile in tile)
				child.Hit(obj, movement);
		}
		public function get CenterX():Number { throw new Error("Not supported"); }
		public function get CenterY():Number { throw new Error("Not supported"); }
		public function set Center(point:Point):void { throw new Error("Not supported");}
		public function get Center():Point { throw new Error("Not supported"); }
		public function get View():RenderableObject { throw new Error("Not supported"); }
		public function set Left(val:Number):void{ throw new Error("Not supported"); }
		public function set Right(val:Number):void{ throw new Error("Not supported"); }
		public function set Top(val:Number):void{ throw new Error("Not supported"); }
		public function set Bottom(val:Number):void{ throw new Error("Not supported"); }
		public function get Top():Number{ throw new Error("Not supported"); }
		public function get Bottom():Number{ throw new Error("Not supported"); }
		public function get Left():Number{ throw new Error("Not supported"); }
		public function get Right():Number{ throw new Error("Not supported"); }
	}

}