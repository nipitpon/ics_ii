package System.MC 
{
	import flash.events.Event;
	import flash.geom.Point;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.ISavable;
	import System.Interfaces.IStatusHolder;
	import System.Interfaces.IUpdatable;
	import System.MC.Characters.Monster;
	import System.Time;
	/**
	 * ...
	 * @author Knight
	 */
	public class SpawnPoint implements IUpdatable, ISavable
	{
		private var location:Point;
		private var spawns:Vector.<Class>;
		private var night:Vector.<Class>;
		private var delay:int;
		private var count:int;
		private var list:Vector.<IStatusHolder>;
		private var night_list:Vector.<IStatusHolder>;
		private var spawnCount:int;
		private var id:int;
		private var killed:int;
		public function SpawnPoint(location:Point, delay:int, spawnCount:int = int.MAX_VALUE)
		{
			this.location = location;
			this.delay = delay;
			list = new Vector.<IStatusHolder>();
			count = delay;
			this.spawnCount = spawnCount;
			night_list = new Vector.<IStatusHolder>();
			count = 1;//So that it spawns right away and not go to below zero(it will if set to 0).
			killed = 0;
		}
		public function SetInfo(obj:Object):void
		{
			spawnCount -= obj["killed"];
			killed = obj["killed"];
		}
		public function set ID(val:int):void { id = val; }
		public function get ID():int { return id; }
		public function GetInfo():Object { return { "killed": killed }; }
		public function SetSpawns(spawns:Vector.<Class>, night:Vector.<Class>):void
		{
			this.spawns = spawns;
			this.night = night;
		}
		public function get RemainingMonsterWave():int { return spawnCount + list.length; }
		public function get SpawnLocation():Point { return location; }
		public function get UpdateWhenAdvancingTime():Boolean { return false; }
		public function Update():void
		{
			if (spawnCount == 0) GameSystem.RemoveGameObject(this);
			else
			{
				count--;
				if (count < 0) count = delay;
				if (count == 0 && list.length == 0)
				{
					{
						Spawn(spawns, list);
						spawnCount--;
						count = delay;
					}
				}
				if (count == delay / 2 && night_list.length == 0 && !Time.IsDayTime)
				{
					{	
						Spawn(night, night_list);
						spawnCount--;
						count = delay;
					}
				}
			}
		}
		private function Spawn(list:Vector.<Class>, cache:Vector.<IStatusHolder>):void
		{
			for each(var i:Class in list)
			{
				var mon:IStatusHolder = new i();
				GameFrame.AddChild(mon);
				mon.Center = location;
				mon.addEventListener(ModelViewController.Removed, onMonRemoved);
				cache.push(mon);
			}
		}
		protected function onMonRemoved(e:Event):void
		{
			var mon:IStatusHolder = e.currentTarget as IStatusHolder;
			mon.removeEventListener(ModelViewController.Removed, onMonRemoved);
			for (var i:int = 0; i < list.length; i++)
			{
				if (list[i] == e.currentTarget)
				{
					list.splice(i, 1);
				}
			}
			for (var j:int = 0; j < night_list.length; j++)
			{
				if (night_list[j] == e.currentTarget)
				{
					night_list.splice(i, 1);
				}
			}
			killed++;
		}
	}

}