package System.MC.Buffs 
{
	import System.Interfaces.IBuffHolder;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ToggleBuff extends Buff 
	{
		
		public function ToggleBuff(owner:IBuffHolder, code:int) 
		{
			super(owner, uint.MAX_VALUE, code);
		}
		public override function Update():void
		{
			
		}
	}
}