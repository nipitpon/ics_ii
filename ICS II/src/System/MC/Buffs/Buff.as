package System.MC.Buffs 
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import System.Interfaces.IBuff;
	import System.Interfaces.IBuffHolder;
	import System.Interfaces.IStatusHolder;
	import System.Time;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Buff extends EventDispatcher implements IBuff 
	{
		private var owner:IBuffHolder;
		private var duration:uint;
		private var count:uint;
		private var code:int;
		public function Buff(owner:IBuffHolder, duration:uint, code:int) 
		{
			super();
			this.owner = owner;
			this.duration = duration;
			this.code = code;
		}
		public function get UpdateWhenAdvancingTime():Boolean { return true; }
		public function Update():void
		{
			if (Time.IsAdvancing)
				count = Math.min(uint(count - Time.TimeAdvanceMultiplier), 0);
			else
				count--;
			if (count == 0)
				Deactivate();
		}
		public function Activate():void
		{
			count = duration;
		}
		public function Deactivate():void
		{
			owner.RemoveBuff(Object(this).constructor);
		}
		public function Reset():void
		{
			count = duration;
		}
		public function get Code():int { return code; }
		public function get Duration():uint { return duration }
		public function get DurationCount():uint { return count; }
		public function get Owner():IBuffHolder { return owner; }
	}
}