package System.MC 
{
	import flash.geom.Point;
	import System.GameSystem;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IMovable;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Collision.CollisionSphere;
	import System.MC.etc.ForceStruct;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class MovableObject extends CollidableObject implements IMovable
	{
		protected var speed:Number;
		protected var force:Point;
		private var decayForce:Vector.<ForceStruct>;
		public function MovableObject(view:RenderableObject, collisionBox:CollisionBox) 
		{
			super(view, collisionBox);
			decayForce = new Vector.<ForceStruct>();
			force = new Point();
		}
		public function Update():void
		{				
			if (DefyGravity) Decay();
			
			//Number rounding errors usually get 123534352857E-15 or something. 
			//Obviously, that's way more than 20k which no combined force can ever reached.
			//So we use this number to detect number rounding error, and elimitate it.
			if (force.x > 20000 || Math.abs(force.x) < 0.1) force.x = 0;
			if (force.y > 20000 || Math.abs(force.y) < 0.1) force.y = 0;
		}
		public function get UpdateWhenAdvancingTime():Boolean { return false; }
		public function CalculateMovement():Point { return new Point(force.x, force.y); }
		public function ClearAllForce():void
		{
			force.x = 0;
			force.y = 0;
			while (decayForce.length > 0)
				decayForce.pop();
		}
		public function ClearForceX():void
		{
			force.x = 0;
			for each(var f:ForceStruct in decayForce)
				f.x = 0;
		}
		public function ClearForceY():void
		{
			force.y = 0;
			for each(var f:ForceStruct in decayForce)
				f.y = 0;
		}
		public function get DefyGravity():Boolean { return true; }
		public function get ForceX():Number { return force.x; }
		public function get ForceY():Number { return force.y; }
		public function Push(x:Number, y:Number):void
		{
			force.x += x;
			force.y += y;
		}
		public function PushWithDecay(x:Number, y:Number, decayRate:Number):void
		{
			decayForce.push(new ForceStruct(x, y, decayRate));
			Push(x, y);
		}
		
		protected function Decay():void
		{
			for (var i:int = 0; i < decayForce.length; i++)
			{
				var dRate:ForceStruct = decayForce[i];
				force.x -= dRate.x;
				force.y -= dRate.y;
				
				dRate.x = int(dRate.x * dRate.decayRate * 100) / 100;
				dRate.y = int(dRate.y * dRate.decayRate * 100) / 100;
				if (Math.abs(dRate.x) < 1)
					dRate.x = 0;
				force.x = int((force.x + dRate.x) * 100) / 100;
				
				if (Math.abs(dRate.y) < 1)
					dRate.y = 0;
				force.y = int((force.y + dRate.y) * 100) / 100;
				
				if (dRate.x == 0 && dRate.y == 0)
				{
					decayForce.splice(i, 1);
					i--;
				}
			}
			
		}
	}

}