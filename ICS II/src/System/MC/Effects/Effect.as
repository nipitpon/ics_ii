package System.MC.Effects 
{
	import flash.geom.Point;
	import System.Interfaces.IUpdatable;
	import System.MC.ModelViewController;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Effect extends ModelViewController
	{
		
		public function Effect(view:RenderableObject) 
		{
			super(view);
		}
		public function MoveToLocationByScalingPoint(point:Point):void
		{
			View.MoveToLocationByScalingPoint(point);
		}
	}

}