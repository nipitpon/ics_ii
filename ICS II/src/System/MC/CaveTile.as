package System.MC 
{
	import Resources.Resource;
	import System.MC.Collision.CollisionBox;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class CaveTile extends Tile 
	{
		
		public function CaveTile(floorCode:int, edgeLeft:Boolean, edgeRight:Boolean, allowDrop:Boolean, customBox:CollisionBox = null, customMainTile:int = -1) 
		{
			super(allowDrop, customBox);
			Initialize(floorCode, edgeLeft, edgeRight, customMainTile);
		}
		
		private function Initialize(floorCode:int, edgeLeft:Boolean, edgeRight:Boolean, customMainTile:int):void
		{
			if (customMainTile < 0)
			{
				if (edgeLeft || edgeRight)
					View.addChild(Resource.GetImage(Utility.Random(17, 21)));
				else
					View.addChild(Resource.GetImage(Utility.Random(6, 10)));
			}
			else
			{
				View.addChild(Resource.GetImage(customMainTile));
			}
			
			if (floorCode >= 0)
			{
				var surface:RenderableObject = Resource.GetImage(floorCode);
				View.addChild(surface);
			}
			if (edgeLeft)
			{
				var eLeft:RenderableObject = Resource.GetImage(25);
				View.addChild(eLeft);
			}
			if (edgeRight)
			{
				var eRight:RenderableObject = Resource.GetImage(26);
				View.addChild(eRight);
			}
		}
	}

}