package System.MC 
{
	import System.Interfaces.ITurnable;
	import System.MC.Collision.CollisionBox;
	import System.V.RenderableObject;
	/**
	 * ...
	 * @author Knight
	 */
	public class TurnableObject extends MovableObject implements ITurnable
	{
		
		public function TurnableObject(view:RenderableObject, collisionBox:CollisionBox) 
		{
			super(view, collisionBox);
		}
		public function TurnLeft():void
		{
			View.scaleX = -1;
		}
		public function TurnRight():void
		{
			View.scaleX = 1;
		}
		public function get IsTurningLeft():Boolean { return View.scaleX < 0; }
		public function get IsTurningRight():Boolean { return View.scaleX > 0; }
	}

}