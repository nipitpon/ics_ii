package System.MC 
{
	import adobe.utils.CustomActions;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import flash.utils.Dictionary;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IMovable;
	import System.Interfaces.IMVC;
	import System.Interfaces.INamable;
	import System.V.RenderableObject;
	import System.MC.Collision.CollisionSet;
	import System.MC.Collision.CollisionBox;
	/**
	 * ...
	 * @author Knight
	 */
	public class Terrain extends EventDispatcher implements IMVC
	{
		private var view:RenderableObject;
		private var tiles:Vector.<Tile>;
		private var namables:Vector.<INamable>;
		private var collision:CollisionSet;
		private var tile_index:Dictionary;
		private var last_player_tile_x:int;
		public function Terrain() 
		{
			namables = new Vector.<INamable>();
			view = new RenderableObject();
			tiles = new Vector.<Tile>();
			collision = new CollisionSet();
			tile_index = new Dictionary();
			last_player_tile_x = int.MAX_VALUE;
		}
		
		public function SetTileAt(x:Number, y:Number, tile:Tile):void
		{
			//tile.Center = new Point((0.5 + x) * Tile.Size.x, (0.5 + y) * Tile.Size.y);
			tile.MoveByOffset(x * Tile.Size.x, y * Tile.Size.y);
			
			view.addChild(tile.View);
			tiles.push(tile);
			collision.AddChild(tile.Collision);
			var len:int = Math.max(tile.Collision.Width / Tile.Size.x, 1);
			for (var i:int = x; i < x + len; i++)
			{
				if (tile_index[int(i)] == null)
					tile_index[int(i)] = new Vector.<Tile>();
					
				tile_index[int(i)].push(tile);
			}
			if (tile is INamable)
				namables.push(tile);
		}
		public function MoveCollisionBoxByOffset(x:int, y:int):void
		{
			collision.MoveByOffset(x, y);
		}
		public function MoveByOffset(x:int, y:int):void
		{
			view.MoveByOffset(x, y);
			MoveCollisionBoxByOffset(x, y);
		}
		public function RemoveAllTile():void
		{
			for each(var t:Tile in tiles)
			{
				if (GameFrame.ContainChild(t))
					GameFrame.RemoveChild(t);
			}
		}
		public function CheckTile(playerTileX:int):void
		{
			if (playerTileX == last_player_tile_x) return;
			
			var start:int = playerTileX - Math.ceil(GameSystem.UpdateRange / Tile.Size.x) - 2;
			var end:int = playerTileX + Math.ceil(GameSystem.UpdateRange / Tile.Size.x) + 2;
			var toRemove:Vector.<int> = new Vector.<int>();
			var range:int = 0;
			if (playerTileX < last_player_tile_x)
			{
				range = last_player_tile_x - playerTileX;
				var oldEnd:int = last_player_tile_x + Math.ceil(GameSystem.UpdateRange / Tile.Size.x) + 2;
				for (var a:int = oldEnd - range; a <= oldEnd; a++)
				{
					toRemove.push(a);
				}
			}
			else if (playerTileX > last_player_tile_x)
			{
				range = playerTileX - last_player_tile_x;
				var oldStart:int = last_player_tile_x - Math.ceil(GameSystem.UpdateRange / Tile.Size.x) - 2;
				for (var b:int = oldStart; b <= oldStart + range; b++)
				{
					toRemove.push(b);
				}
			}
			for (var i:int = start ; i < end; i++)
			{
				if (tile_index[i] != null)
				{
					for each(var tile:Tile in tile_index[i])
					{
						if (!GameFrame.ContainChild(tile))
						{
							GameFrame.AddChild(tile);
						}
					}
				}
			}
			for each(var re:int in toRemove)
			{
				if (tile_index[re] != null) for each(var t:Tile in tile_index[re])
				{
					if (!t.IsInIndexRange(start, end) && GameFrame.ContainChild(t)) 
						GameFrame.RemoveChild(t);
				}
			}
		}
		public function GetObjectByName(name:String):INamable
		{
			for each(var nam:INamable in namables)
			{
				if (nam.Name == name) return nam;
			}
			return null;
		}
		public function GetObjectByNames(name:String):Vector.<INamable>
		{
			var outgoing:Vector.<INamable> = new Vector.<INamable>();
			for each(var nam:INamable in namables)
			{
				if (nam.Name == nam) outgoing.push(nam);
			}
			return outgoing;
		}
		public function get Active():Boolean { return true; }
		public function get View():RenderableObject { return view; }
		public function get Collision():CollisionBox { return collision; }
		public function get Center():Point { return collision.Center; }
		public function get CenterX():Number { return collision.CenterX; }
		public function get CenterY():Number { return collision.CenterY; }
		public function get Tiles():Vector.<Tile> { return tiles; }
		public function set Center(val:Point):void 
		{ 
			collision.Center = val; 
			View.Center = val;
		}
		public function set Left(val:Number):void
		{
			var offset:Number = View.Left - collision.Left;
			collision.Left = val+offset;
			View.Left = val;
		}
		public function set Right(val:Number):void
		{
			var offset:Number = View.Right - collision.Right;
			collision.Right = val+offset;
			View.Right = val;
		}
		public function set Top(val:Number):void
		{
			var offset:Number = View.Top - collision.Top;
			collision.Top = val+offset;
			View.Top = val;
		}
		public function set Bottom(val:Number):void
		{
			var offset:Number = View.Bottom - collision.Bottom;
			collision.Bottom = val+offset;
			View.Bottom = val;
		}
		public function get Left():Number { return Collision.Left; }
		public function get Right():Number { return Collision.Right; }
		public function get Top():Number { return Collision.Top; }
		public function get Bottom():Number { return Collision.Bottom; }
		public function IntersectsLine(x1:Number, x2:Number, y1:Number, y2:Number, movement:Point):ICollidable
		{
			if (movement == null)
				throw new ArgumentError();
				
			var outgoing:TileSet = new TileSet(this);
			for each( var tile:Tile in tiles)
			{
				if (Math.abs(x1 - tile.CenterX) > GameSystem.UpdateRange ||
					Math.abs(x2 - tile.CenterX) > GameSystem.UpdateRange) continue;
					
				if (tile.IntersectsLine(x1, x2, y1, y2, movement) != null) outgoing.AddTile(tile);
			}
			if (outgoing.Length == 0) return null;
			else return outgoing;
		}
		public function FloorBelowPoint(point:Point):Number
		{
			var current:Number = Infinity;
			var mostMinus:Number = -Infinity;
			for each(var tile:Tile in tiles)
			{
				if (tile.Collision.Left > point.x || tile.Collision.Right < point.x) continue;
				
				var top_result:Number = tile.Collision.GetTopAtPos(point.x, point.x, point.y);
				var result:Number = top_result - point.y ;
				if (result > 0 && result < current)
					current = top_result;
				else if (result <= 0 && result > mostMinus)
					mostMinus = top_result;
			}
			if(isFinite(current)) return current;
			else return mostMinus;
		}
		public function Intersects(box:CollisionBox, movement:Point):ICollidable 
		{ 
			if (box == null || movement == null)
				throw new ArgumentError();
				
			var outgoing:TileSet = new TileSet(this);
			for each( var tile:Tile in tiles)
			{
				if (tile.Intersects(box, movement) != null) outgoing.AddTile(tile);
			}
			if (outgoing.Length == 0) return null;
			else return outgoing;
		}
		public function Hit(obj:ICollidable, movement:Point):void
		{}
		public function TakenHit(obj:ICollidable, movement:Point):void
		{}
	}
}