package System.MC.Flags 
{
	import flash.events.Event;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import Resources.lib.FlagSprite;
	import System.DataCenter;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.ICollidable;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.ModelViewController;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class EventTrigger extends CollidableObject 
	{
		private var flagPosition:int;
		private var flagValue:Boolean;
		public function EventTrigger(flagPosition:int, flagValue:Boolean) 
		{
			super(new FlagSprite(), new CollisionRectangle(0, 0, 200, 200));
			this.flagPosition = flagPosition;
			this.flagValue = flagValue;
			collide_with.push(Player);
			addEventListener(ModelViewController.Removed, onRemoved); 
		}
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			super.TakenHit(obj, movement);
			Trigger(obj);
		}
		public function get IsFlagTriggered():Boolean { return DataCenter.GetFlag(flagPosition) == flagValue; }
		protected function Trigger(obj:ICollidable):void
		{
		}
		protected function RemoveTrigger():void
		{
			if(flagPosition >= 0)
				DataCenter.SetFlag(flagPosition, flagValue);
			GameFrame.RemoveChild(this);
		}
		private function onRemoved(e:Event):void
		{
			removeEventListener(ModelViewController.Removed, onRemoved);
		}
	}
}