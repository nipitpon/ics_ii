package System.MC.Flags 
{
	import flash.events.Event;
	import flash.geom.Point;
	import Resources.lib.Sound.SFX.WarHornSound;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IStatusHolder;
	import System.Interfaces.IUpdatable;
	import System.MC.Characters.Monster;
	import System.MC.Characters.Player;
	import System.MC.ModelViewController;
	import System.SoundCenter;
	/**
	 * ...
	 * @author Knight
	 */
	public class HordeTrigger extends EventTrigger implements IUpdatable
	{
		private var left:Vector.<Class>;
		private var right:Vector.<Class>;
		private var summon:Vector.<Monster>;
		private var start:Boolean;
		private var count:uint;
		private var startLeft:Point;
		private var startRight:Point;
		private var screenCenter:Number;
		private var lock_width:Number;
		public function HordeTrigger(flagPosition:int, screenCenter:Number, left:Vector.<Class>, right:Vector.<Class>,
			startLeft:Point, startRight:Point, width:Number = 400) 
		{
			super(flagPosition, true);
			if (left == null || right == null || screenCenter == 0)
				throw new ArgumentError();
			start = false;
			this.left = left;
			this.right = right;
			summon = new Vector.<Monster>();
			this.startLeft = startLeft;
			this.startRight = startRight;
			this.screenCenter = screenCenter;
			this.lock_width = width;
		}
		public function Update():void
		{
			if (!start || (left.length == 0 && right.length == 0)) return;
			
			if (count++ == Utility.SecondToFrame(1))
			{
				count = 0;
				if (left.length > 0)
				{
					var monLeft:Monster = new (left.shift())();
					monLeft.Center = startLeft;
					monLeft.TurnRight();
					monLeft.Invoke(GameSystem.PlayerInstance);
					monLeft.addEventListener(ModelViewController.Removed, monster_Removed);
					monLeft.BoundByPlayerBound = true;
					summon.push(monLeft);
					GameSystem.AddMarkedMonster(monLeft);
					GameFrame.AddChild(monLeft);
				}
				if (right.length > 0)
				{
					var monRight:Monster = new (right.shift())();
					monRight.Center = startRight;
					monRight.TurnLeft();
					monRight.Invoke(GameSystem.PlayerInstance);
					monRight.BoundByPlayerBound = true;
					monRight.addEventListener(ModelViewController.Removed, monster_Removed);
					summon.push(monRight);
					GameSystem.AddMarkedMonster(monRight);
					GameFrame.AddChild(monRight);
				}
			}
		}
		public function get UpdateWhenAdvancingTime():Boolean { return false; }
		protected override function Trigger(obj:ICollidable):void
		{
			super.Trigger(obj);
			if (obj is Player && !start)
			{
				start = true;
				SoundCenter.PlaySFXFromInstance(new WarHornSound());
				GameSystem.LockCamera(screenCenter, lock_width);
			}
		}
		protected function Deactivate():void
		{
			if(GameFrame.ContainChild(this))
				RemoveTrigger();
			GameSystem.UnlockCamera();			
		}
		private function monster_Removed(e:Event):void
		{
			(e.currentTarget as Monster).removeEventListener(ModelViewController.Removed, monster_Removed);
			for (var i:int = 0; i < summon.length; i++)
			{
				if (summon[i] == e.currentTarget)
				{
					summon.splice(i, 1);
					break;
				}
			}
			if (summon.length == 0)
			{
				Deactivate();
			}
		}
	}

}