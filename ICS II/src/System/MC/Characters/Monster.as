package System.MC.Characters 
{
	import flash.display.GraphicsGradientFill;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import HardCode.Buffs.Burn;
	import Resources.lib.Sound.SFX.GoatAttackSound;
	import Resources.lib.Sound.SFX.GoatBurnSound;
	import System.GameFrame;
	import System.GameSystem;
	import System.GUI;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IBurnable;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IHaveCooldown;
	import System.Interfaces.IMovable;
	import System.Interfaces.ISkill;
	import System.Interfaces.IStatusHolder;
	import System.MC.Collision.CollisionBox;
	import System.MC.Items.Item;
	import System.MC.Items.ItemSet;
	import System.MC.Projectiles.Projectile;
	import System.MC.States.MonsterState_Die;
	import System.MC.States.MonsterState_DieFromStomp;
	import System.MC.States.MonsterState_Idle;
	import System.MC.States.MonsterState_Move;
	import System.MC.Tile;
	import System.SoundCenter;
	import System.Time;
	import System.V.MonsterHealthBar;
	import System.V.RenderableObject;
	/**
	 * ...
	 * @author Knight
	 */
	public class Monster extends Character
	{
		public static const Die:uint = 40;
		public static const DieFromStomp:uint = 41;
		private var ko:uint;
		private var progressBar:MonsterHealthBar;
		private var itemSet:Vector.<ItemSet>;
		private var target:IStatusHolder;
		private var skills:Vector.<ISkill>;
		private var hit_player_bound:Boolean;
		private var show_hp:Boolean;
		private var hit_count:int;
		private var standing_platform:Tile;
		public function Monster(view:RenderableObject, collisionBox:CollisionBox, atk:uint, def:uint, vit:uint,
			ko:uint, mp:uint, speed:Number, itemSet:Vector.<ItemSet>)
		{
			if (GameSystem.PlayerInstance != null)
			{
				var addap:int = GameSystem.PlayerInstance.APLevel;
				ko += addap;
				for (addap; addap > 0; addap--)
				{
					var result:int = Utility.Random(0, 99);
					if (result > 66) atk++;
					else if (result > 33) def++;
					else vit++;
				}
			}
			super(view, collisionBox, atk, def, vit, mp, speed);
			hit_player_bound = false;
			show_hp = true;
			this.skills = new Vector.<ISkill>();
			this.ko = ko;
			this.itemSet = itemSet;
			progressBar = new MonsterHealthBar(collisionBox.Right - Collision.Left, 10);
			progressBar.x = collisionBox.Left;
			progressBar.y = collisionBox.Top - progressBar.height - 30;
			progressBar.Min = 0;
			progressBar.Max = MaxHP;
			progressBar.CurrentValue = HP;
			progressBar.visible = false;
			states[Die] = new MonsterState_Die(this);
			states[Idle] = new MonsterState_Idle(this);
			states[Move] = new MonsterState_Move(this);
			states[DieFromStomp] = new MonsterState_DieFromStomp(this);
			target = null;
			damageNumberColor = 0xffffff;
			SetState(Idle);
			collide_with = Vector.<Class>([Tile, Player, Projectile]);
			hit_count = 0;
			standing_platform = null;
		}
		
		public override function Update():void
		{
			super.Update();
			
			if (target != null && !target.Active) target = null;
			for each(var s:ISkill in skills)
			{
				if (s is IHaveCooldown)
				{
					(s as IHaveCooldown).CountCooldown();
				}
			}
			if (hit_count > 0) hit_count--;
		}
		public override function get UpdateWhenAdvancingTime():Boolean { return true; }
		public override function CalculateMovement():Point 
		{
			var result:Point = super.CalculateMovement();
			if (Time.IsAdvancing)
			{
				result.x *= 2;
				result.y *= 2;
			}
			return result;
		}
		public override function get DefyGravity():Boolean { return super.DefyGravity || CurrentState == states[Die]; }
		public override function CalculateDamage(mul:Number):uint 
		{ return 4 * (ATK + atk_add) * mul * (Utility.Random(60, 100) / 100.0); }
		public function get Skills():Vector.<ISkill> { return skills; }
		public function get BoundByPlayerBound():Boolean { return hit_player_bound; }
		public function set BoundByPlayerBound(val:Boolean):void { hit_player_bound = val; }
		public function GetAppropriateSkill():ISkill
		{
			if (target == null || Skills.length == 0) return null;
			
			var distance:Number = Point.distance(Center, Target.Center);
			var toUse:ISkill = null;
			for each(var s:ISkill in Skills)
			{
				if (s.Range > distance * 1.2 && (!(s is IHaveCooldown) || !(s as IHaveCooldown).IsHavingCooldown))
				{
					if (toUse == null || toUse.Range > s.Range)
					{
						toUse = s;
					}
				}
			}
			return toUse;
		}
		public override function Heal(val:uint):void
		{
			super.Heal(val);
			progressBar.CurrentValue = HP;
		}
		public override function Stun(sec:uint):void
		{
			if(!BoundByPlayerBound || GameSystem.IsBetweenPlayerBounds(Collision))
				super.Stun(sec);
		}
		public override function TakeDamage(damage:uint, movement:Point, crit:Boolean, position:Point = null):uint
		{
			if (!IsAlive) return 0;
			
			var dam:uint = super.TakeDamage(damage, movement, crit, position);
			progressBar.CurrentValue = HP;
			if (HP == 0) DeclareDead(damage, position);
			if (CurrentState == states[Die]) return dam;
			
			if (dam > ko && CanKO)
			{
				GetKO(movement);
			}
			if (!progressBar.visible) progressBar.visible = show_hp;
			
			return dam;
		}
		public function TakeDamageFromStomp(dam:uint):uint
		{
			var outgoing:uint = super.TakeDamage(dam, null, false, null);
			if (!progressBar.visible) progressBar.visible = show_hp;
			progressBar.CurrentValue = HP;
			if (HP == 0)
			{
				DeclareDeadFromStomp();
			}
			else View.Flash(255, 255, 255, 5);
			return outgoing;
		}
		public override function MoveByOffset(x:int, y:int):void
		{
			super.MoveByOffset(x, y);
			progressBar.x += x;
			progressBar.y += y;
			if (standing_platform != null)
			{
				if (IsMovingLeft && standing_platform.Collision.Left > Left)
					BeginMoveRight(true);
				else if (IsMovingRight && standing_platform.Collision.Right < Right)
					BeginMoveLeft(true);
			}
		}
		public override function set Center(val:Point):void
		{
			super.Center = val;
			progressBar.x = Collision.Left;
			progressBar.y = Collision.Top - progressBar.height - 30;
	 	}
		public override function get Side():Class { return Monster; }
		public function get HealthBar():MonsterHealthBar { return progressBar; }
		public override function get IsBurning():Boolean { return HasBuff(HardCode.Buffs.Burn); }
		public override function Hit(obj:ICollidable, movement:Point):void
		{
			if (movement.y > 0 && standing_platform == null && obj is Tile)
			{
				standing_platform = obj as Tile;
			}
			if (standing_platform != null && obj is Tile)
			{
				var left:Number = movement.x < 0 ? Collision.Left : (Collision.Right - 3);
				var right:Number = movement.x < 0 ? (Collision.Left + 3) : Collision.Right;
				var top:Number = Math.abs((obj as Tile).Collision.GetTopAtPos(left + movement.x, right + movement.x, Collision.CenterY) - Collision.Bottom);
				var bottom:Number = Math.abs((obj as Tile).Collision.GetTopAtPos(left + movement.x, right + movement.x, Collision.CenterY) - Collision.Bottom);
				if (IsMovingLeft && movement.x < 0 && (isNaN(top) || top > 20))
					BeginMoveRight(true);
				else if (IsMovingRight && movement.x > 0 && (isNaN(bottom) || bottom > 20))
				{
					BeginMoveLeft(true);
				}
			}
			
			super.Hit(obj, movement);
		}
		public function Invoke(target:IStatusHolder):void
		{
			if(target.Side != Side)
				this.target = target;
		}
		public function Burn():void
		{
			if (!HasBuff(HardCode.Buffs.Burn) && !IsDying)
			{
				AddBuff(HardCode.Buffs.Burn);
				GameSystem.BurnGoat();
				UpdateSpeed(Speed * 3);
			}
		}
		public function SmashByBox():void
		{
			DeclareDead(2, new Point());
		}
		public function get Invoked():Boolean { return target != null; }
		public function get Target():IStatusHolder { return target; }
		protected function UpdateSpeed(new_speed:Number):void
		{
			if (IsMovingLeft)
				Push(Speed, 0);
			else if (IsMovingRight)
				Push( -Speed, 0);
			
			speed = new_speed;
			
			if (IsMovingLeft)
				Push( -Speed, 0);
			else if (IsMovingRight)
				Push(Speed, 0);
		}
		protected function AddSkill(skill:ISkill):void
		{
			skills.push(skill);
		}
		protected function ClearSkill():void
		{
			skills = new Vector.<ISkill>();
		}
		protected function ContainsSkill(skill:ISkill):Boolean
		{
			for each(var s:ISkill in skills)
			{
				if (s == skill) return true;
			}
			return false;
		}
		protected override function DeclareDeadFromStomp():void
		{
			super.DeclareDeadFromStomp();
			progressBar.visible = false;
			DropItems();
			SetState(DieFromStomp);
			RemoveBuff(HardCode.Buffs.Burn).Deactivate();
		}
		protected function set ShowHP(val:Boolean):void { show_hp = val; }
		protected function get ShowHP():Boolean { return show_hp; }
		protected override function DeclareDead(dam:uint, position:Point):void
		{
			super.DeclareDead(dam, position);
			progressBar.visible = false;
			DropItems();
			SetState(Die);
			if(HasBuff(HardCode.Buffs.Burn))
				RemoveBuff(HardCode.Buffs.Burn).Deactivate();
		}
		private function DropItems():void
		{
			var center:Point = Collision.Center;
			for each(var item:ItemSet in itemSet)
			{
				var vec:Vector.<Class> = item.DropItemFromSet();
				for each(var c:Class in vec)
				{
					var toDrop:Item = new c(Utility.Random( -5, 5), -Utility.Random(10, 30));
					toDrop.Center = center;
					GameFrame.AddChild(toDrop);
				}
			}
		}
	}
}