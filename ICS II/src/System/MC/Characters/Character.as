package System.MC.Characters 
{
	import adobe.utils.CustomActions;
	import flash.display.InteractiveObject;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	import HardCode.Tiles.BoxTile;
	import HardCode.Tiles.SpikeTile;
	import System.GameFrame;
	import System.GUI;
	import System.Interfaces.IBuff;
	import System.Interfaces.IBuffHolder;
	import System.Interfaces.ILaunchable;
	import System.Interfaces.IProjectileLauncher;
	import System.Interfaces.ISkill;
	import System.Interfaces.ISkillUser;
	import System.Interfaces.IStatusHolder;
	import System.Interfaces.IStunnable;
	import System.MC.*;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IMovable;
	import System.Interfaces.IMVC;
	import System.Interfaces.IUpdatable;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Collision.CollisionSphere;
	import System.MC.Collision.PlayerBound;
	import System.MC.etc.CharacterInfo;
	import System.MC.etc.ForceStruct;
	import System.MC.Projectiles.Projectile;
	import System.MC.States.CharacterState_Idle;
	import System.MC.States.CharacterState_KO;
	import System.MC.States.CharacterState_Skill;
	import System.V.RenderableObject;
	import System.MC.TileSet;
	import System.GameSystem;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Character extends TurnableObject implements IAnimatable, IStatusHolder, ISkillUser, IBuffHolder
	{
		public static const Idle:uint = 0;
		public static const KO:uint = 1;
		public static const Move:uint = 2;
		public static const Skill1:int = 4;
		public static const Skill2:int = 5;
		
		public static const CharacterTakeDamage:String = "Character_TakeDamage";
		
		protected var status:int;
		protected var currentState:CharacterState_Idle;
		protected var states:Dictionary;
		protected var atk:uint;
		protected var atk_add:int;
		protected var def:uint;
		protected var def_add:int;
		protected var vit:uint;
		protected var vit_add:int;
		protected var mp:uint;
		protected var maxmp:uint;
		protected var damageNumberColor:uint;
		
		private var hp:uint;
		private var movingLeft:Boolean;
		private var movingRight:Boolean;
		private var hpMul:Number;
		private var buffs:Vector.<IBuff>;
		private var stun:Boolean;
		private var stun_duration:uint;
		private var stun_count:uint;
		private var projectileHoming:Boolean = false;
		
		public function Character(view:RenderableObject, collisionBox:CollisionBox,
			atk:uint, def:uint, vit:uint, mp:uint, speed:Number) 
		{
			super(view, collisionBox);
			collide_with = Vector.<Class>([IStatusHolder, Tile, TileSet, Projectile]);
			if (!(view is IAnimatable))
				throw new Error("Character's View must be IAnimatable. C'mon, don't be a lazy ass and make some animation");
			states = new Dictionary();
			states[Idle] = new CharacterState_Idle(this);
			states[KO] = new CharacterState_KO(this);
			states[Skill1] = new CharacterState_Skill(this);
			buffs = new Vector.<IBuff>();
			atk_add = 0;
			def_add = 0;
			vit_add = 0;
			this.atk = atk;
			this.def = def;
			this.vit = vit;
			this.maxmp = mp;
			this.mp = maxmp;
			hpMul = 1;
			movingLeft = false;
			movingRight = false;
			hp = MaxHP;
			this.speed = speed;
			stun = false;
			SetState(Idle);
			addEventListener(Removed, onRemoved);
		}
		public function ResetAnimation():void
		{
			(View as IAnimatable).ResetAnimation();
		}
		public function HasAnimation(id:int):Boolean { return (View as IAnimatable).HasAnimation(id); }
		public function CalculateDamage(multiplier:Number):uint { throw new Error("function not implemented"); }
		public function Stun(seconds:uint):void
		{
			if (!IsAlive) return;
			
			stun_duration = Math.max(stun_count, Utility.SecondToFrame(seconds));
			stun_count = stun_duration;
			if (!IsStunned)
			{
				stun = true;
				SetState(Idle);
			}
		}
		public function Unstun():void
		{
			if (!IsStunned) return;
			
			stun = false;
		}
		public function TakeDamage(damage:uint, movement:Point, crit:Boolean, position:Point = null):uint
		{
			if (!IsAlive || !IsVulnerable) return 0;
			
			damage = damage * Math.max((0.5 + (100 - (DEF + def_add)) / 200), 0.01);
			//CAN'T CALL DECLAREDEAD HERE!!!!
			//This function is also called from TakeDamageFromStomp.
			//If call DeclareDead() here. Action when dying from normal attack will trigger too.
			if (hp > damage)
				hp -= damage;
			else hp = 0;
			
			dispatchEvent(new Event(CharacterTakeDamage));
			//var damageNumberColor:Number = this.damageNumberColor;
			//if (crit) damageNumberColor = 0x6200c1;
			//if (position != null)
			//	GameFrame.ShowDamage(position.x, position.y, damageNumberColor, damage);
			//else
			//	GameFrame.ShowDamage(Collision.CenterX, Collision.CenterY, damageNumberColor, damage);
			return damage;
		}
		public function get IsBurning():Boolean { throw new Error("Not Implemented"); }
		public function IncreaseMP(val:int):void
		{
			if (!IsAlive) return;
			
			if (mp + val > MaxMP)
				mp = MaxMP;
			else
				mp += val;
			
			
			GameFrame.ShowDamage(CenterX, CenterY, 0x4444ff, val);
			View.Flash(0, 0, 255, 10);
		}
		public function Heal(val:uint):void
		{
			if (!IsAlive) return;
			
			if (hp + val > MaxHP)
				hp = MaxHP;
			else
				hp += val;
			
			
			GameFrame.ShowDamage(CenterX, CenterY, 0x00ff00, val);
			View.Flash(0, 255, 0, 10);
		}
		public override function Update():void
		{
			super.Update();
			for each(var buff:IBuff in buffs)
				buff.Update();
			if (IsStunned)
			{
				stun_count--;
				if (stun_count == 0)
				{
					Unstun();
				}
			}			
			if (CurrentState != null)
				CurrentState.OnUpdate();
		}
		public function AddAdditionalATK(val:int):void 
		{ 
			atk_add += val; 
			if (MP > MaxMP)
				mp = MaxMP;
		}
		public function AddAdditionalDEF(val:int):void { def_add += val; }
		public function AddAdditionalVIT(val:int):void 
		{ 
			vit_add += val; 
			if (HP > MaxHP)
				hp = MaxHP;
		}
		public function get IsStunnable():Boolean { return true; }	
		public function get Speed():Number { return speed; }
		public function get IsVulnerable():Boolean { return true; }
		public override function get DefyGravity():Boolean { return false; }
		public function get CurrentState():CharacterState_Idle { return currentState; }
		public function SetAnimation(name:int):void { (View as IAnimatable).SetAnimation(name); }
		public function set Freeze(val:Boolean):void { (View as IAnimatable).Freeze = val; }
		public function set CurrentState(state:CharacterState_Idle):void
		{
			if (state == null) throw new ArgumentError();
			
			if (state == currentState) return;
			
			if (currentState != null) currentState.OnDeactivated();
			currentState = state;
			state.OnActivating();
		}
		public function get IsStunned():Boolean { return stun; }
		public function get Stompable():Boolean { return true; }
		public function get Freeze():Boolean { return (View as IAnimatable).Freeze; }
		public function get DefaultAnimation():int { return (View as IAnimatable).DefaultAnimation; }
		public function get CurrentAnimation():int { return (View as IAnimatable).CurrentAnimation; }
		public function get HP():Number { return hp; }
		public function get MaxHP():Number { return (20 * (VIT + vit_add))* hpMul; }
		public function get HPMultiplier():Number { return hpMul; }
		public function get ATK():Number { return atk; }
		public function get DEF():Number { return def; }
		public function get VIT():Number { return vit; }
		public function get AdditionalATK():int { return atk_add; }
		public function get AdditionalDEF():int { return def_add; }
		public function get AdditionalVIT():int { return vit_add; }
		public function get IsAlive():Boolean { return HP > 0; }
		public function get MaxMP():Number { return DEF + VIT + vit_add + def_add; }
		public function get MP():Number { return mp; }
		public function set HPMultiplier(val:Number):void
		{
			hpMul = val;
			if (HP > MaxHP)
				hp = MaxHP;
		}
		public function PassRequirement(skill:ISkill):Boolean
		{
			if (skill == null) throw new ArgumentError();
			return skill.MPRequired <= MP;
		}
		public function UseSkill(skill:ISkill):void
		{
			if (skill == null) throw new ArgumentError();
			
			if (!PassRequirement(skill)) return;
			
			var skillState:CharacterState_Skill = states[Skill1] as CharacterState_Skill;
			
			mp -= skill.MPRequired;
			
			skillState.SetSkill(skill);
			SetState(Skill1);
		}
		public function GetBuffCooldownState(buff:IBuff):Number
		{
			for each(var b:IBuff in buffs)
			{
				if (b == buff)
				{
					return b.DurationCount / Number(b.Duration);
				}
			}
			throw new Error("No buff " + buff + " on character " + this);
		}
		public function get IsDying():Boolean { return CurrentAnimation == Monster.Die; }
		public function BeginMoveLeft(setToMove:Boolean):void
		{
			if (IsMovingLeft) return;
			
			movingLeft = true;
			movingRight = false;
			TurnLeft();
			ClearForceX();
			Push( -Speed, 0);
			if(setToMove)
				SetState(Move);
		}
		public function BeginMoveRight(setToMove:Boolean):void
		{
			if (IsMovingRight) return;
			
			movingRight = true;
			movingLeft = false;
			TurnRight();
			ClearForceX();
			Push(Speed, 0);
			if(setToMove)
				SetState(Move);

		}
		public function GetBuff(buff:Class):IBuff
		{
			for each(var b:IBuff in buffs)
			{
				if (b is buff) return b;
			}
			return null;
		}
		public function HasBuff(buff:Class):Boolean
		{
			for each(var b:IBuff in buffs)
			{
				if (b is buff) return true;
			}
			return false;
		}
		public function AddBuff(buff:Class):IBuff
		{
			for each(var buf:IBuff in buffs)
			{
				if (buf is buff)
				{
					buf.Deactivate();
					break;
				}
			}
			
			var b:IBuff = new buff(this);
			b.Activate();
			buffs.push(b);
			return b;
		}
		public function RemoveBuff(buff:Class):IBuff
		{
			for (var i:int = 0; i < buffs.length; i++)
			{
				if (buffs[i] is buff)
				{
					return buffs.splice(i, 1)[0];
				}
			}
			return null;
		}
		public function StopMoving(setToIdle:Boolean):void
		{
			StopMovingLeft(setToIdle);
			StopMovingRight(setToIdle);
		}
		public function StopMovingRight(setToIdle:Boolean):void
		{
			if (!IsMovingRight) return;
			
			movingRight = false;
			Push( -Speed, 0);
			if (setToIdle)
			{
				SetState(Idle);
			}
		}
		public function StopMovingLeft(setToIdle:Boolean):void
		{
			if (!IsMovingLeft) return;
			
			movingLeft = false;
			Push(Speed, 0);
			if (setToIdle)
			{
				SetState(Idle);
			}
		}
		public override function Hit(obj:ICollidable, movement:Point):void
		{
			if (!IsAlive) return;
			
			if (obj is TileSet)
			{
				HitTileSet(obj as TileSet, movement);
				Decay();
			}
			else if (obj is Tile && !(obj is SpikeTile))
			{
				HitTile(obj as Tile, movement);
				Decay();
			}
		}
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			if (!IsAlive) return;
			
			super.TakenHit(obj, movement);
			if (obj is Tile)
			{
				MoveByOffset(movement.x, movement.y);
			}
		}
		public function SetState(id:int):void
		{
			if (id >= states.length) throw new ArgumentError();
			
			if (states[id] == null) id = Idle;
			
			CurrentState = states[id];
		}
		public function get Side():Class { return Character; }
		public function get IsMoving():Boolean { return IsMovingLeft || IsMovingRight; }
		public function get IsMovingLeft():Boolean { return movingLeft; }
		public function get IsMovingRight():Boolean { return movingRight; }
		public function get CanKO():Boolean { return true; }
		public function get ProjectileHoming():Boolean { return projectileHoming; }
		public function get IsInvincible():Boolean { return false; }
		
		public function set ProjectileHoming(val:Boolean):void { projectileHoming = val; }
		public function GetKO(movement:Point):void
		{
			SetState(Idle);
			if (movement.x < 0 && !IsTurningRight) TurnRight();
			else if (movement.x > 0 && !IsTurningLeft) TurnLeft();
			SetState(KO);
		}
		public function ReleaseProjectile(type:Class, angleOffset:Number, force:ForceStruct):Boolean
		{
			if (!(this is IProjectileLauncher))
				throw new Error("Invalid method call: ReleaseProjectile on non-IProjectileLauncher object");
				
			var projectile:ILaunchable = new type(this);
			var releasePoint:Point = (this as IProjectileLauncher).ReleasePoint;
			if (projectileHoming)
			{
				var vec:Vector.<ICollidable> = GameSystem.GetCollidables(); 
				var myC:Point = Center;
				var maxDis:Number = 99999;
				var dis:Number = 0;
				var target:IStatusHolder = null;
				for each(var c:ICollidable in vec)
				{
					if (c == this) continue;
					
					if (c is IStatusHolder && (c as IStatusHolder).Side != Side)
					{
						var cCenter:Point = c.Center;
						if ((myC.x < cCenter.x && IsTurningRight) || (myC.x > cCenter.x && IsTurningLeft))
						{
							dis = Point.distance(myC, c.Center);
							if (dis < maxDis && dis < GameSystem.UpdateRange)
							{
								maxDis = dis;
								target = c as IStatusHolder;
							}
						}
					}
				}
				
				if (target != null)
					projectile.SetTarget(target);
			}
			projectile.Center = releasePoint;
			
			if (IsTurningLeft)
			{
				projectile.View.rotation = 180 - angleOffset;
				if(force != null)
					PushWithDecay(-force.x, force.y, force.decayRate);
			}
			else
			{
				projectile.View.rotation = angleOffset;
				if(force != null)
					PushWithDecay(force.x, force.y, force.decayRate);
			}
			projectile.LaunchAt(releasePoint.x, releasePoint.y);
			GameFrame.AddChild(projectile);
			return true;
		}
		protected function DeclareDeadFromStomp():void
		{
			hp = 0;
			Unstun();
		}
		protected function DeclareDead(dam:uint, position:Point ):void
		{
			hp = 0;
			Unstun();
		}
		protected function SetHP(val:int):void
		{
			hp = val;
		}
		protected function get CurrentSkill():ISkill 
		{
			if (CurrentState != states[Skill1]) return null; 
			else
			{
				return (CurrentState as CharacterState_Skill).CurrentSkill;
			}
		}
		private function onRemoved(e:Event):void
		{
			removeEventListener(Removed, onRemoved);
			for each(var buff:IBuff in buffs)
			{
				RemoveBuff((buff as Object).constructor).Deactivate();
			}
		}
		protected function HitTile(t:Tile, movement:Point):void
		{
			var hit_x:Boolean = movement.x != 0 && t.Intersects(Collision, new Point(movement.x, t.ForceY != 0 ? movement.y : 0));
			var hit_y:Boolean = movement.y != 0 && t.Intersects(Collision, new Point(0, movement.y));
			var cBox:CollisionBox = t.Collision;
			if (hit_y)
			{
				var result:Number = cBox.GetTopAtPos(Collision.Left, Collision.Right, Collision.CenterY + movement.y);
				if (movement.y > 0 && result >= Collision.Bottom - 5)
				{
					force.y = 0;
					movement.y = Math.min(movement.y, result - Collision.Bottom);
				}
				else if (movement.y < 0)
				{
					force.y = 0;
					movement.y = Math.max(movement.y, Math.min(cBox.GetBottomAtPos(Collision.Left, Collision.Right, Collision.CenterY + movement.y) - Collision.Top, 0));
				}
			}
			if (hit_x)
			{
				if (movement.x > 0)
					movement.x = Math.min(cBox.GetLeftAtPos(Collision.CenterX + movement.x, Collision.Top + movement.y, Collision.Bottom) - Collision.Right, movement.x);
				else if (movement.x < 0)
					movement.x = Math.max(movement.x, cBox.GetRightAtPos(Collision.CenterX + movement.x, Collision.Top + movement.y, Collision.Bottom) - Collision.Left + 1);
			}
			
			if (!hit_x && !hit_y)
			{
				if (movement.y > 0)
				{
					var col:Vector.<ICollidable> = GameSystem.GetIntersectedCollidable(Collision, new Point(movement.x));
					var found:Boolean = false;
					
					for each(var c:ICollidable in col)
					{
						if (CollideWith(c) && c is Tile)
						{
							found = true;
							break;
						}
					}
					
					if(!found)
						movement.y = 0;
				}
			}
		}
		protected function HitTileSet(s:TileSet, movement:Point):void
		{
			for (var i:uint = 0; i < s.Length; i++)
			{
				var t:Tile = s.GetTileAtIndex(i);
				HitTile(t, movement);
			}
		}
	}
}