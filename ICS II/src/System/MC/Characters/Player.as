package System.MC.Characters 
{
	import _Base.MyTextField;
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import flash.utils.Dictionary;
	import HardCode.Buffs.CombustionSyndrome;
	import HardCode.Effects.LandingDustEffect;
	import HardCode.Tiles.BoxTile;
	import Resources.Animation;
	import Resources.BlackMirror.Character.PlayerSprite;
	import Resources.lib.Sound.SFX.ButtonClick;
	import Resources.lib.Sound.SFX.ButtonMouseOver;
	import Resources.lib.Sound.SFX.FireExhaustedSound;
	import System.DataCenter;
	import System.GamePage;
	import System.GameSystem;
	import System.GUI;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IBuff;
	import System.Interfaces.IBurnable;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IHaveCooldown;
	import System.Interfaces.IInteractor;
	import System.Interfaces.IItemCollector;
	import System.Interfaces.ILaunchable;
	import System.Interfaces.IMovable;
	import System.Interfaces.IProjectileLauncher;
	import System.Interfaces.IReceiveControl;
	import System.Interfaces.ISavable;
	import System.Interfaces.ISkill;
	import System.Interfaces.IStatusHolder;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Collision.CollisionBox;
	import System.ControlCenter;
	import System.MC.Collision.CollisionSphere;
	import System.MC.Collision.PlayerBound;
	import System.MC.etc.CharacterInfo;
	import System.MC.etc.DeathReason;
	import System.MC.etc.ForceStruct;
	import System.MC.etc.PlayerInfo;
	import System.MC.Ladder.Ladder;
	import System.MC.Level;
	import System.MC.ModelViewController;
	import System.MC.Overlay;
	import System.MC.Projectiles.AttackTargetProjectile;
	import System.MC.Skills.PassiveSkill;
	import System.MC.Skills.ToggleSkill;
	import System.MC.States.CharacterState_Idle;
	import System.MC.States.PlayerState_ClimbLadder;
	import System.MC.States.PlayerState_Die;
	import System.MC.States.PlayerState_DieFromMonster;
	import System.MC.States.PlayerState_Fall;
	import System.MC.States.PlayerState_Idle;
	import System.MC.States.PlayerState_Jump;
	import System.MC.States.PlayerState_Move;
	import System.MC.States.PlayerState_Prone;
	import System.MC.States.PlayerState_Reload;
	import System.MC.States.PlayerState_Skill;
	import System.MC.States.PlayerState_Sleep;
	import System.MC.States.PlayerState_Slide;
	import System.MC.States.PlayerState_WarpOut;
	import System.MC.Tile;
	import System.MC.TileSet;
	import System.SoundCenter;
	import System.V.MonsterHealthBar;
	import System.V.RenderableObject;
	import System.GameFrame;
	/**
	 * ...
	 * @author Knight
	 */
	public class Player extends Character implements IReceiveControl, IProjectileLauncher, IInteractor, IItemCollector, IBurnable, ISavable
	{
		public static const Jump:int = 11;
		public static const Fall:int = 12;
		public static const Prone:int = 13;
		public static const BlowGum:int = 14;
		public static const Box:int = 15;
		public static const ClimbUpLadder:int = 16;
		public static const DieAngle:int = 17;
		public static const ClimbLookLeft:int = 18;
		public static const ClimbLookRight:int = 19;
		public static const MidAir:int = 20;
		public static const ClimbDownLadder:int = 21;
		public static const Slide:int = 22;
		public static const Sleep:int = 23;
		public static const WarpOut:int = 24;
		
		public static const Auto:int = 0;
		public static const BurstFire:int = 1;
		public static const SemiAuto:int = 2;
		public static const Missile:int = 3;
		public static const Malee:int = 4;
		public static const Etc:int = 5;
		public static const Etc2:int = 6;
		
		private static var instance:Player;
		private var jumping:Boolean;
		private var skills:Vector.<Vector.<ISkill>>;
		private var ammoMax:uint;
		private var current_ammo:uint;
		private var extra_mag:uint;
		private var reload:PlayerState_Reload;
		private var skill:PlayerState_Skill;
		private var ap:uint;
		private var ap_point:uint;
		private var ap_level:uint;
		private var sp:uint;
		private var sp_point:uint;
		private var sp_level:uint;
		private var collectionSphere:CollisionSphere;
		private var vulnerable_count:uint;
		private var vulnerable_length:uint;
		private var toDrop:Vector.<Tile>;
		private var dropping:Boolean = false;
		private var skillStates:Vector.<Vector.<uint>>;
		private var currentMode:int = Auto;
		private var missile_unlock:Boolean = false;
		private var soul_bullet:Boolean = false;
		private var jam:Boolean;
		private var jam_rate:int;
		private var touching_floor:Boolean;
		private var move_shoot:Boolean;
		private var text:MyTextField;
		private var climb:PlayerState_ClimbLadder;
		private static const skillIcons:Vector.<Vector.<Class>> = Vector.< Vector.<Class> > ([ ]);
		private static const skillDesc:Vector.<Vector.<int>> = Vector.<Vector.<int>>([ ]);
		private static var shortcut:Dictionary;
		private var stance:Boolean;
		public function Player() 
		{
			instance = this;
			super(new PlayerSprite(), 
				new CollisionRectangle(21, 19, 22, 45),
				5, 5, 5, 10, 8);
			stance = false;
			jumping = false;
			move_shoot = false;
			damageNumberColor = 0xff8888;
			collectionSphere = new CollisionSphere(new Point(53.5, 89), Tile.Size.x * 2, CollisionBox.IntersectDirection_All);
			states[Idle] = new PlayerState_Idle(this);
			states[Jump] = new PlayerState_Jump(this);
			states[Fall] = new PlayerState_Fall(this);
			states[Move] = new PlayerState_Move(this);
			states[Skill1] = skill = new PlayerState_Skill(this);
			states[Prone] = new PlayerState_Prone(this);
			states[Monster.Die] = new PlayerState_Die(this);
			states[ClimbUpLadder] = climb = new PlayerState_ClimbLadder(this);
			states[DieAngle] = new PlayerState_DieFromMonster(this);
			states[Slide] = new PlayerState_Slide(this);
			states[Sleep] = new PlayerState_Sleep(this);
			states[WarpOut] = new PlayerState_WarpOut(this);
			reload = new PlayerState_Reload(this);
			shortcut = new Dictionary();
			SetState(Idle);
			sp = 0;
			text = new MyTextField();
			text.text = "haha";
			text.textColor = 0xff0000;
			//View.addChild(text);
			touching_floor = true;
			
			Initialize();
		}
		public function UseMaleeAttack():void
		{
			UseStoredSkill(Malee, 0);
		}
		public function StartSoulBullet():void { soul_bullet = true; }
		public function StopSoulBullet():void { soul_bullet = false; }
		public function get IsUsingSoulBullet():Boolean { return soul_bullet; }
		public function UseDefaultAttack():void
		{
			UseStoredSkill(currentMode, 0);
		}
		public function UseStoredSkill(mode:uint, slot:uint):void
		{
			UseSkill(skills[mode][slot]);
		}
		public override function set Center(val:Point):void
		{
			var cCen:Point = Center;
			MoveByOffset(val.x - cCen.x, val.y - cCen.y);
			collectionSphere.Center = val;
		}
		public override function set CenterX(val:Number):void
		{
			super.CenterX = val;
			collectionSphere.CenterX = val;
		}
		//public override function set CenterY(val:Number):void
		//{
		//	super.CenterY = val;
		//	collectionSphere.CenterY = val;
		//}
		public override function get Center():Point { return Collision.Center; }
		public override function get CenterX():Number { return Collision.CenterX; }
		public override function get CenterY():Number { return Collision.CenterY; }
		public override function get DefyGravity():Boolean { return currentState == states[DieAngle] || IsClimbing; }
		public function get IsWarpingOut():Boolean { return currentState == states[WarpOut]; }
		public override function get IsBurning():Boolean 
		{ 
			return HasBuff(CombustionSyndrome) && 
				(GetBuff(CombustionSyndrome) as CombustionSyndrome).IsBurning; 
		}
		public override function set HPMultiplier(val:Number):void
		{
			var percent:Number = HP / MaxHP;
			super.HPMultiplier = val;
			SetHP(MaxHP * percent);
			RefreshHP();
		}
		public override function AddAdditionalATK(val:int):void
		{
			super.AddAdditionalATK(val);
		}
		public function SetInfo(obj:Object):void
		{
			Center = obj["center"];
		}
		public function GetInfo():Object
		{
			return { "center": Center };
		}
		public function ExtinguishFire():void
		{
			var syndrome:CombustionSyndrome = GetBuff(CombustionSyndrome) as CombustionSyndrome;
			if (syndrome != null)
				syndrome.Reset();
		}
		public override function AddAdditionalVIT(val:int):void
		{
			var hppercent:Number = HP / MaxHP;
			var mppercent:Number = MP / MaxMP;
			super.AddAdditionalVIT(val);
			SetHP(hppercent * MaxHP);
			mp = mppercent * MaxMP;
			RefreshHP();
			RefreshMP();
		}
		public override function AddAdditionalDEF(val:int):void
		{
			var mppercent:Number = MP / MaxMP;
			super.AddAdditionalDEF(val);
			mp = mppercent * MaxMP;
			RefreshMP();
		}
		public override function UseSkill(skill:ISkill):void
		{
			if (skill == null) throw new ArgumentError();
			
			if (!PassRequirement(skill))
			{
				GameFrame.ShowExclam(Utility.Random(Collision.Left, Collision.Right),
					Utility.Random(Collision.Top, Collision.CenterY), 0xff0000);
				return;
			}
			
			if (skill is IHaveCooldown && (skill as IHaveCooldown).IsHavingCooldown)
			{
				if (skill != skills[Auto][0] && skill != skills[SemiAuto][0] && 
				skill != skills[BurstFire][0] && skill != skills[Missile][0])
				{
					GameFrame.ShowExclam(Utility.Random(Collision.Left, Collision.Right),
						Utility.Random(Collision.Top, Collision.CenterY), 0xff0000);
				}
				return;
			}
			
			MP -= skill.MPRequired;
			if (!skill.HasAnimation)
			{
				skill.Activate();
			}
			else
			{
				if (IsReloading)
				{
					reload.OnDeactivating();
				}
				var skillState:PlayerState_Skill = states[Skill1] as PlayerState_Skill;
				
				skillState.SetSkill(skill);
				skillState.OnActivating();
				//SetState(Skill1);
			}
		}
		public function Burn():void
		{
			var buff:IBuff = GetBuff(CombustionSyndrome);
			if(buff != null)
				(buff as CombustionSyndrome).Burn();
		}
		public override function CalculateDamage(multiplier:Number):uint
		{
			return (2 * (ATK + atk_add) + ((DEF + def_add + VIT + vit_add))) *
				multiplier * (Utility.Random(60, 100) / 100.0);
		}
		public override function PassRequirement(skill:ISkill):Boolean
		{
			if (skill == null) throw new ArgumentError();
			
			return super.PassRequirement(skill) && skill.AmmoRequired <= current_ammo;
		}
		public function IsSkillPassRequirement(skillX:int, skillY:int):Boolean
		{
			return PassRequirement(skills[skillX][skillY]);
		}
		public override function MoveByOffset(x:int, y:int):void
		{
			if (stance) x = 0;
			super.MoveByOffset(x, y);
			
			if (y > 5 && CurrentState != states[Fall] && CurrentState != states[Skill1] && CurrentState != states[Monster.Die] &&
				CurrentState != states[ClimbUpLadder] && !IsDying)
			{
				var iTemp:Number = GameSystem.FloorBelowPoint(Center);
				touching_floor = false;
				SetState(Fall);
			}
			collectionSphere.MoveByOffset(x, y);
		}
		public override function get IsDying():Boolean { return super.IsDying || CurrentState == states[DieAngle]; }
		public override function Update():void
		{
			super.Update();
			if (!IsDying)
			{
				var ladder:Ladder = GameSystem.GetIntersectedLadder(Collision.Center);
				if (ladder != null && (ControlCenter.PressingKey(ControlCenter.PlayerClimb)))
				{
					if (currentState != states[ClimbUpLadder])
					{
						if (IsMoving) StopMoving(false);
						climb.SetLadder(ladder);
						SetState(ClimbUpLadder);
					}
				}
			}
			if (vulnerable_length > 0)
			{
				if (vulnerable_count++ >= vulnerable_length)
				{
					vulnerable_length = 0;
					vulnerable_count = 0;
					View.visible = true;
				}
				else if (vulnerable_count % 2 == 0)
				{
					View.visible = !View.visible;
				}
			}
			//if (CenterY > 550)
			//	MoveByOffset(0, -CenterY);
			for each (var skill:Vector.<ISkill> in skills)
			{
				for each(var s:ISkill in skill)
				{
					if (s is IHaveCooldown)
					{
						(s as IHaveCooldown).CountCooldown();
					}
				}
			}
			text.text = CenterX + ", " + CenterY;
		}
		public override function get IsInvincible():Boolean { return vulnerable_length > 0; }
		public override function TakeDamage(num:uint, movement:Point, crit:Boolean, position:Point = null):uint
		{
			if (num == 0) return num;
			
			var prev:int = HP;
			var dam:uint = super.TakeDamage(num, movement, crit, position);
			
			if (HP == 0) DeclareDead(dam, position);
			if (dam > 0)
			{
				vulnerable_length = Utility.SecondToFrame(3);
			}
			return dam;
		}
		public override function get Side():Class { return Player; }
		public function get ExtraMagazine():uint { return extra_mag; }
		public function set ExtraMagazine(val:uint):void
		{
			extra_mag = val;
		}
		public function IntersectsInteractionRadius(box:CollisionBox, movement:Point):Boolean
		{
			return Intersects(box, movement) != null;
		}
		public function IntersectsCollectionRadius(box:CollisionBox, movement:Point):Boolean
		{
			if (box is CollisionRectangle)
			{
				var rect:CollisionRectangle = box as CollisionRectangle;
				return collectionSphere.IntersectsRect(rect.Rect, movement);
			}
			else if (box is CollisionSphere)
			{
				var sphere:CollisionSphere = box as CollisionSphere;
				return collectionSphere.IntersectsCircle(sphere.Center, sphere.Radius, movement);
			}
			else return false;
		}
		public function get IsReceivingControl():Boolean 
		{ 
			return !GameSystem.IsFreezing && !IsDying; 
		}
		public function get CollectionCenter():Point { return Collision.Center; }
		public function get AmmoMax():uint { return ammoMax; }
		public function set AmmoMax(val:uint):void
		{
			ammoMax = val;
		}
		public function DieFromBurning():void
		{
			SoundCenter.PlaySFXFromInstance(new FireExhaustedSound());
			SetState(Monster.Die);
			Die(DeathReason.Fire);
		}
		public function DieFromMonster():void
		{
			SetState(Player.DieAngle);
			Die(DeathReason.Goat);
		}
		public function DieFromSpike():void
		{
			SetState(Player.DieAngle);
			Die(DeathReason.Spike);
		}
		private function Die(reason:int):void
		{
			GameSystem.PlayerDieOnce(reason);
			var buff:IBuff = RemoveBuff(CombustionSyndrome);
			if (buff != null)
				buff.Deactivate();
			//StopMoving(false); 
			//(View as IAnimatable).SetAnimation(Monster.Die);
			//View.addEventListener(Animation.FinishAnimation, onFinishedDying);
		}
		public function GetCooldownState(x:uint, y:int):Number
		{
			if (x >= skills.length || y > skills[x].length) return 0;
			
			if (skills[x][y + 1] is IHaveCooldown)
			{
				var cd:IHaveCooldown = skills[x][y + 1] as IHaveCooldown;
				if (cd.Cooldown == 0) return 0;
				
				return Number(cd.CurrentCount) / cd.Cooldown;
			}
			else return 0;
		}
		public function UnlockMissile():void 
		{ 
			missile_unlock = true; 
		}
		public function set AllowMoveAndShoot(val:Boolean):void { move_shoot = true; }
		public function get AllowMoveAndShoot():Boolean { return move_shoot; }
		public function LockMissile():void { missile_unlock = false; }
		public function get IsMissileUnlocked():Boolean { return missile_unlock; }
		public function get IsUsingSkill():Boolean { return skill.IsActivating; }
		public function get IsSliding():Boolean { return currentState == states[Slide]; }
		public function get IsClimbing():Boolean { return CurrentState == states[ClimbUpLadder]; }
		public function KeyPress(id:int):void
		{
			(currentState as PlayerState_Idle).OnKeyPress(id);
			if (id == ControlCenter.SwitchLeft) MoveDefaultSkillLeft();
			else if (id == ControlCenter.SwitchRight) MoveDefaultSkillRight();
			
			if (!IsStunned && !IsDying)
			{
				var cState:PlayerState_Idle = currentState as PlayerState_Idle;
				var ladder:Ladder = GameSystem.GetIntersectedLadder(Collision.Center);
				if (ladder != null && (id == ControlCenter.PlayerClimb))
				{
					if (currentState != states[ClimbUpLadder])
					{
						if (IsMoving) StopMoving(false);
						climb.SetLadder(ladder);
						SetState(ClimbUpLadder);
					}
				}
				else if ((!IsUsingSkill && !IsClimbing) || (move_shoot && skill.CurrentSkill.AllowMovement))
				{
					if (!IsProning && cState.CanProne && id == ControlCenter.Drop)
					{
						CheckDropOrClimbLadder();
					}
					if (!IsProning || CanStand())
					{
						if (!IsMovingLeft && id == ControlCenter.PlayerWalkLeft && cState.CanMove)
						{
							if (IsMovingRight) StopMovingRight(false);
							BeginMoveLeft(true); 
						}
						if (!IsMovingRight && id == ControlCenter.PlayerWalkRight && cState.CanMove)
						{
							if (IsMovingLeft) StopMovingLeft(false);
							BeginMoveRight(true); 
						}
					}
					else if(IsProning)
					{
						if (id == ControlCenter.PlayerWalkLeft && IsTurningRight)
							TurnLeft();
						else if (id == ControlCenter.PlayerWalkRight && IsTurningLeft)
							TurnRight();
					}
					if (id == ControlCenter.PlayerJump && IsProning)
					{
						if (!IsSliding)
							SetState(Slide);
					}
					else if ((id == ControlCenter.PlayerJump || id == ControlCenter.PlayerClimb) && cState.CanJump && touching_floor && CanStand())
					{
						BeginJump();
					}
				}
				if (id == ControlCenter.Restart && GameSystem.CurrentLevel.ShowPauseMenu)
				{
					GameSystem.Freeze();
					Main.ChangeToGamePage();
					//Level.ClearObjectInfo();
				}
			}
		}
		public function StartReloading():void
		{
			if (Ammo == ammoMax) return;
			
			if (ExtraMagazine > 0)
			{
				Ammo = AmmoMax;
				ExtraMagazine--;
				jam = false;
			}
			else
			{
				reload.OnActivating();
			}
			
		}
		public function RefillMagazine():void
		{
			Ammo = ammoMax;
			jam = false;
		}
		public function FinishReloading(refillMag:Boolean):void
		{
			if (refillMag)
			{
				Ammo = ammoMax;
				jam = false;
			}
			currentState.OnActivating();
		}
		public function BeginJump(forceY:Number = -27):void
		{
			if (IsJumping || stance) return;
			
			SetState(Jump);
			touching_floor = false;
			Push(0, Math.min(forceY, -forceY));
		}
		public function KeyRelease(id:int):void
		{
			(currentState as PlayerState_Idle).OnKeyRelease(id);
			
			if (IsJumping) return;
			
			if (IsMovingLeft && id == ControlCenter.PlayerWalkLeft)
			{
				StopMovingLeft(true);
				
				if (ControlCenter.PressingKey(ControlCenter.Drop))
				{
					CheckDropOrClimbLadder();
				}
			}
			if (IsMovingRight && id == ControlCenter.PlayerWalkRight)
			{
				StopMovingRight(true);
				if (ControlCenter.PressingKey(ControlCenter.Drop))
					CheckDropOrClimbLadder();
			}
			if (!IsProning || CanStand())
			{
				if (id == ControlCenter.PlayerWalkLeft && ControlCenter.PressingKey(ControlCenter.PlayerWalkRight))
					BeginMoveRight(true);
				if (id == ControlCenter.PlayerWalkRight && ControlCenter.PressingKey(ControlCenter.PlayerWalkLeft))
					BeginMoveLeft(true);
			}
		}
		public function set MP(val:Number):void
		{
			mp = val;
			RefreshMP();
		}
		public function set Ammo(val:uint):void
		{
			if (soul_bullet && val < current_ammo) return;
			
			current_ammo = val;
			if (current_ammo == 0)
			{
				if(ExtraMagazine > 0)
				{
					current_ammo = ammoMax;
					ExtraMagazine--;
				}
			}
		}
		public function get Ammo():uint { return current_ammo; }
		public function get IsDropping():Boolean { return dropping; }
		public function get IsReloading():Boolean { return reload.IsReloading; }
		public override function get IsVulnerable():Boolean { return vulnerable_length == 0; }
		public function get IsInGameObject():Boolean { return true; }
		public function get SPPointRequired():uint { return uint(Math.ceil(5 * Math.log(sp_level + 2) + 5 * Math.log(sp_level + 3) + 5 * Math.log(sp_level + 4))); }
		public function get APPointRequired():uint { return uint(Math.ceil(10 * Math.log(ap_level + 2))); }
		public function get CurrentSPPoint():uint { return sp_point; }
		public function get CurrentAPPoint():uint { return ap_point; }
		public function get APAvailable():uint { return ap; }
		public function get SPAvailable():uint { return sp; }
		public function get InteractionCenter():Point { return CollectionCenter; }
		public function get JamRate():int { return jam_rate; }
		public function get IsGunJammed():Boolean { return jam; }
		public function get APLevel():int { return ap_level; }
		public function set CurrentSPPoint(val:uint):void
		{
			if (sp_level == 28) return;
			
			sp_point = val;
			while (sp_point >= SPPointRequired && sp_level < 28)
			{
				sp_point -= SPPointRequired;
				sp++;
				sp_level++;
			}
			if (sp_level == 28) sp_point = SPPointRequired;
		}
		public function set CurrentAPPoint(val:uint):void
		{
			ap_point = val;
			while (ap_point >= APPointRequired)
			{
				ap_point -= APPointRequired;
				ap++;
				ap_level++;
			}
		}
		public function set JamRate(val:int):void { jam_rate = val; }
		
		public function MousePress(x:Number, y:Number):void
		{
			
		}
		public function MouseMove(x:Number, y:Number):void
		{
			
		}
		public function MouseRelease(x:Number, y:Number):void
		{
			
		}
		public override function ReleaseProjectile(type:Class, angleOffset:Number, force:ForceStruct):Boolean
		{
			if (Utility.IsClassInheritedFrom(type, AttackTargetProjectile))//Is bullet. Not missile.
			{
				if (jam || (Ammo < AmmoMax && !GameSystem.IsIndoor(Center) && Utility.Random(0, 100) < jam_rate))
				{
					if (!jam)
					{	
						jam = true;
					}
					GameFrame.ShowReload(Utility.Random(Collision.Left, Collision.Right), Utility.Random(Collision.Top, Collision.CenterY));
					return false;
				}
				if(Ammo > 0)
					Ammo--;
				else
				{
					GameFrame.ShowReload(Utility.Random(Collision.Left, Collision.Right), Utility.Random(Collision.Top, Collision.Bottom));
					return false;
				}
			}
				
			return super.ReleaseProjectile(type, angleOffset, force);
		}
		public override function BeginMoveLeft(setToIdle:Boolean):void
		{
			if (!stance) super.BeginMoveLeft(setToIdle);
			else
			{
				TurnLeft();
				if (setToIdle)
					SetState(Idle);
			}
		}
		public override function BeginMoveRight(setToIdle:Boolean):void
		{
			if (!stance) super.BeginMoveRight(setToIdle);
			else
			{
				TurnRight();
				if (setToIdle)
					SetState(Idle);
			}
		}
		public function BeginDrop():void
		{
			dropping = true;
			toDrop = new Vector.<Tile>();
			var rect:CollisionRectangle = new CollisionRectangle(Collision.Left, Collision.Top - 100,
				Collision.Width, (Collision.Bottom - (Collision.Top - 100)) + 20);
			var collide:Vector.<ICollidable> = GameSystem.GetIntersectedCollidable(rect);
			for each(var c:ICollidable in collide)
			{
				if (c is Tile && (c as Tile).AllowDrop) toDrop.push(c);
			}
			SetState(Fall);
		}
		public function StopDrop():void
		{
			dropping = false;
			toDrop = null;
			if (!IsUsingSkill && !IsReloading && ControlCenter.PressingKey(ControlCenter.Drop))
				CheckDropOrClimbLadder();
			else
				SetState(Idle);
		}
		public function UpgradeATK():void
		{
			DecreaseAvailableAPPointByOne();
			atk++;
			RefreshHP();
			RefreshMP();
		}
		public function UpgradeDEF():void
		{
			DecreaseAvailableAPPointByOne();
			var percent:Number = MP / MaxMP;
			def++;
			mp = percent * MaxMP;
			RefreshMP();
			RefreshHP();
		}
		public function UpgradeVIT():void
		{
			DecreaseAvailableAPPointByOne();
			var percent:Number = HP / MaxHP;
			var mpPercent:Number = MP / MaxMP;
			vit++;
			SetHP(MaxHP * percent);
			mp = mpPercent * MaxMP;
			RefreshHP();
			RefreshMP();
		}
		public function get IsJumping():Boolean { return currentState == states[Jump] || currentState == states[Fall]; }
		public function get CanUseSkill():Boolean { return currentState == states[Idle] || currentState == states[Move]; }
		public function get IsFalling():Boolean { return currentState == states[Fall]; }
		public function CanStand():Boolean
		{
			var box:CollisionBox = null;
			if (IsProning)
			{
				box = new CollisionRectangle(Collision.Left, Collision.Top - 28, 22, 45);
			}
			else
			{
				box = new CollisionRectangle(Collision.Left, Collision.Top - 3, 22, 45);
			}
			var collide:Vector.<ICollidable> = GameSystem.GetIntersectedCollidable(box, null);
			var found:Boolean = false;
			for each(var c:ICollidable in collide)
			{
				if (c is Tile && (c as Tile).Collision.IntersectsFromEveryDirection)
				{
					found = true; 
					break;
				}
			}
			return !found;
		}
		public override function Hit(obj:ICollidable, movement:Point):void
		{
			var prev:Number = movement.y;
			var prevX:Number = movement.x;
			var prevForce:Number = ForceY;
			super.Hit(obj, movement);
			if (obj is Tile)
			{
				touching_floor = prev >= 0 && movement.y <= 0;
				if (IsDropping)
				{
					if (prev < 0) StopDrop();
					else
					{
						if (!(obj as Tile).AllowDrop || !ContainsInToDrop(obj as Tile))
						{
							StopDrop();
						}
						else
						{
							movement.y = prev;
							force.y = prevForce;							
						}
					}
				}
				else if ((IsJumping || IsFalling) && touching_floor && prev > 0)//prev will be zero when in mid air, about to fall.
				{
					if (obj.Collision.IntersectsFromEveryDirection || 
						obj.Collision.GetTopAtPos(Collision.Left, Collision.Right, Collision.CenterY) < Collision.Bottom - 5)
					{
						var effect:LandingDustEffect = new LandingDustEffect();
						effect.CenterX = Collision.CenterX;
						effect.Bottom = Collision.Bottom;
						GameFrame.AddChild(effect);
						ClearForceY();
					}
					if (!IsDying && IsReceivingControl && (!IsUsingSkill || (move_shoot && skill.CurrentSkill.AllowMovement)))
					{
						if (!IsProning && ControlCenter.PressingKey(ControlCenter.Drop))
						{
							CheckDropOrClimbLadder();
						}
						else if ((IsMovingLeft && ControlCenter.PressingKey(ControlCenter.PlayerWalkLeft)) ||
							(IsMovingRight && ControlCenter.PressingKey(ControlCenter.PlayerWalkRight)))
						{
							SetState(Move);
						}
						else if (!IsMovingLeft && ControlCenter.PressingKey(ControlCenter.PlayerWalkLeft))
						{
							BeginMoveLeft(true);
							if(!stance)
								SetState(Move);
						}
						else if (!IsMovingRight && ControlCenter.PressingKey(ControlCenter.PlayerWalkRight))
						{
							BeginMoveRight(true);
							if(!stance)
								SetState(Move);
						}
						else
						{
							SetState(Idle);
						}
					}
				}
				else if (IsJumping && prevForce < 0 && movement.y == 0)
				{
					if (CurrentState == states[Jump] && !touching_floor) SetState(Fall);
				}
			}
		}
		public function get IsProning():Boolean { return CurrentState == states[Prone]; }
		public function get ReleasePoint():Point
		{
			if (IsTurningRight)
			{
				if (IsProning) return new Point(142 + View.Left, 125 + View.Top);
				else return new Point(88 + View.Left, 74 + View.Top);
			}
			else
			{
				if (IsProning) return new Point(View.Right - 142, 125 + View.Top);
				else return new Point(View.Right - 88, 74 + View.Top);
			}
		}
		public function GetSkillState(x:uint, y:uint):uint
		{
			if (x >= skillStates.length || y >= skillStates[x].length) 
				throw new ArgumentError();
				
			return skillStates[x][y];
		}
		public function UpgradeSkill(x:uint, y:uint):void
		{
			if (x >= skillStates.length || y >= skillStates[x].length ) return;
			
			if (SPAvailable == 0)
				throw new ArgumentError();
			
			skillStates[x][y] = 2;
			if (y + 1 < skillStates[x].length) skillStates[x][y + 1] = 1;
			if (skills[x][y].IsPassive) skills[x][y].Activate();
			sp--;
		}
		public function set StanceMode(val:Boolean):void 
		{ 
			stance = val; 
			//It would disrupt active skill.
			if(CurrentState != states[Skill1])
				SetState(Idle);
		}
		public function get IsInteractable():Boolean { return !IsDying; }
		public function get StanceMode():Boolean { return stance; }
		public function RegisterShortcut(key:uint, skillX:uint, skillY:uint):void
		{
			shortcut[key] = skills[skillX][skillY + 1];
		}
		public function UnregisterShortcut(key:uint):void
		{
			shortcut[key] = null;
		}
		public function HasSkillWithShortcut(key:uint):Boolean
		{
			return shortcut[key] != null;
		}
		public function GetSkillAtShortcut(key:uint):ISkill
		{
			return shortcut[key] as ISkill;
		}
		public function UseSkillFromShortcut(key:uint):void
		{
			if(shortcut[key] != null)
				UseSkill(shortcut[key] as ISkill);
		}
		public static function GetSkillFullDescription(x:uint, y:int):String
		{
			if (x >= skillDesc.length || y >= skillDesc[x].length - 1 || skillDesc[x][y + 1] == -1) return null;
			
			var message:String = "<p>" + (DataCenter.GetData(DataCenter.CurrentLanguage, DataCenter.Module_Game, DataCenter.Section_Skill,
				skillDesc[x][y + 1], DataCenter.Skill_Name) as String);
			
			message += " (" + (instance.skills[x][y + 1].IsPassive ? "Passive Skill":
				((instance.skills[x][y+1] is ToggleSkill) ? "Toggle Skill" : "Active Skill")) + ")" + "</p>";
			var desc:String = DataCenter.GetData(DataCenter.CurrentLanguage, DataCenter.Module_Game, DataCenter.Section_Skill,
				skillDesc[x][y + 1], DataCenter.Skill_Description) as String;
				
			if(desc != "") message += "<br/><p>" + desc + "</p>";
			
			var req:String = DataCenter.GetData(DataCenter.CurrentLanguage, DataCenter.Module_Game, DataCenter.Section_Skill,
				skillDesc[x][y + 1], DataCenter.Skill_Requirement) as String;
			
			if (req != "")
			{
				if (instance != null && !instance.IsSkillPassRequirement(x, y + 1))
				{
					req = "<font color='#ff0000'>" + req + "</font>";
				}
				message += "<br/><p>Requirement: " + req + "</p>";
			}
			
			if (instance != null && instance.skills[x][y + 1] is IHaveCooldown)
			{
				var cd:Number = Utility.FrameToSecond((instance.skills[x][y + 1] as IHaveCooldown).Cooldown);
				if(cd > 0)
					message += "<br/><p>Cooldown : " + cd + " seconds </p>";
			}
			return message;
		}
		public function IsSkillPassive(skillX:uint, skillY:uint):Boolean 
		{
			if (skillX >= skills.length || skillY >= skills[skillX].length - 1 || skills[skillX][skillY + 1] == null) return false;
			
			return skills[skillX][skillY + 1].IsPassive;
		}
		protected override function get CurrentSkill():ISkill
		{
			if (CurrentState != states[Skill1]) return null;
			else
			{
				return (CurrentState as PlayerState_Skill).CurrentSkill;
			}
		}
		protected override function DeclareDead(damage:uint, position:Point):void
		{
			super.DeclareDead(damage, position);
			if (position == null) position = Center;
			
			position = GameFrame.ToScreenPosition(position);
			GameSystem.Freeze();
			Heal(MaxHP);
			GamePage.ShowGameOver(damage, position);
			if (GameSystem.IsHavingBossBattle)
			{
				GameSystem.StopBossBattle();
				if (GameSystem.HasLevelBGM)
					SoundCenter.StopBGM();
			}
			else SoundCenter.StopBGM();
			
		}
		private function MoveDefaultSkillLeft():void
		{
		}
		private function MoveDefaultSkillRight():void
		{
		}
		private function ContainsInToDrop(tile:Tile):Boolean
		{
			if (toDrop == null) return false;
			for each(var t:Tile in toDrop)
			{
				if (t == tile) return true;
			}
			return false;
		}
		private function DecreaseAvailableAPPointByOne():void
		{
			if (APAvailable == 0)
				throw new Error("Invalid call: upgrade status when no ap is available");
			ap--;
		}
		private function CheckDropOrClimbLadder():void
		{
			var center:Point = Center;
			center.y += Tile.Size.y;
			var collide:Vector.<ICollidable> = GameSystem.GetIntersectedCollidable(new CollisionRectangle(center.x - 1, center.y - 1, 3, 3), new Point(0, Tile.Size.y));
			var ladder:Ladder = null;
			var has_tile:Boolean = false;
			for each(var col:ICollidable in collide)
			{
				if (col is Ladder)
				{
					ladder = col as Ladder;
				}
				else if (col is Tile && (col as Tile).Collision.IntersectsFromEveryDirection)
				{
					has_tile = true;
				}
			}
			if (ladder != null && !has_tile)
			{
				Center = center;
				if (IsMoving) StopMoving(false);
				climb.SetLadder(ladder);
				SetState(ClimbUpLadder);
			}
			else
				SetState(Prone);
		}
		private function Initialize():void
		{
			extra_mag = 0;
			ammoMax = 30;
			current_ammo = ammoMax;
			sp_level = 0;
			ap_level = 0;
			jam = false;
			jam_rate = 0;
			
			skillStates = Vector.<Vector.<uint>> ([
				Vector.<uint>([2, 1, 0, 0, 0]),          //Auto
				Vector.<uint>([2, 1, 0, 0, 0]),          //BurstFire
				Vector.<uint>([2, 1, 0, 0, 0]),          //SemiAuto
				Vector.<uint>([2, 1, 0, 0, 0]),          //Missile
				Vector.<uint>([2, 1, 0, 0, 0]),          //Malee
				Vector.<uint>([0, 1, 0, 0, 0]),          //etc
				Vector.<uint>([0, 1, 0, 0, 0])           //etc2
			]);
			skills = Vector.<Vector.<ISkill>> ([]);	
			
			currentMode = Auto;
			addEventListener(ModelViewController.Removed, onRemoved);
		}
		private function onRemoved(e:Event):void
		{
			removeEventListener(ModelViewController.Removed, onRemoved);
			for (var i:int = 0; i < skills.length; i++) for (var j:int = 0; j < skills[i].length; j++)
			{
				if (skills[i][j] != null && skills[i][j].IsPassive && skillStates[i][j] == 2)
				{
					skills[i][j].Deactivate();
				}
			}
		}
		private function RefreshHP():void
		{
		}
		private function RefreshMP():void
		{
		}
	}
}