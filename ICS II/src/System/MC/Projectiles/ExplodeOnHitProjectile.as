package System.MC.Projectiles 
{
	import flash.geom.Point;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.ICollidable;
	import System.Interfaces.ILaunchable;
	import System.Interfaces.IStatusHolder;
	import System.MC.Characters.Monster;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionSphere;
	import System.MC.Tile;
	import System.MC.TileSet;
	import System.V.MonsterHealthBar;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ExplodeOnHitProjectile extends Projectile 
	{
		private var splash:Number;
		private var radius:Number;
		public function ExplodeOnHitProjectile(view:RenderableObject, collisionBox:CollisionBox, range:Number, owner:IStatusHolder,
			dmgMultiplier:Number, critical:int, splashMul:Number, radius:Number, turningSpeed:uint) 
		{
			super(view, collisionBox, range, owner, dmgMultiplier, critical, turningSpeed);
			splash = splashMul;
			this.radius = radius;
		}
		public function DetonateProjectile(hitter:ICollidable, movement:Point):void
		{
			if(GameFrame.ContainChild(this))
				GameFrame.RemoveChild(this);
			else return;
			var sphere:CollisionSphere = new CollisionSphere(Center, radius, CollisionBox.IntersectDirection_All);
			sphere.MoveByOffset(movement.x, movement.y);
			var col:Vector.<IStatusHolder> = GameSystem.GetIntersectedStatusHolder(sphere, true, true, Object);
			for each(var c:IStatusHolder in col)
			{
				if (c == hitter || c == Owner) continue;
				
				var sDam:uint = Owner.CalculateDamage(splash);
				sDam = (c as IStatusHolder).TakeDamage(sDam, movement, false);
				if (c is Monster) (c as Monster).Invoke(Owner);
			}
			var lau:Vector.<ILaunchable> = GameSystem.GetIntersectedLaunchable(sphere);
			for each(var l:ILaunchable in lau)
			{
				if (l is ExplodeOnHitProjectile && l != this && l.Solidity < Solidity)
					(l as ExplodeOnHitProjectile).DetonateProjectile(null, movement);
			}
		}
		public override function Hit(obj:ICollidable, movement:Point):void
		{
			super.Hit(obj, movement);
			if (obj is TileSet)
			{
				var found:Boolean = false;
				var tile:TileSet = obj as TileSet;
				
				for (var i:int = 0; i < tile.Length && !found; i++)
				{
					if (tile.GetTileAtIndex(i).Collision.IntersectsFromEveryDirection)
						found = true;
				}
				if (!found) return;
			}
			if (obj is Tile)
			{
				if (!obj.Collision.IntersectsFromEveryDirection)
					return;
			}
			
			if (obj is IStatusHolder)
			{
				if (!(obj as IStatusHolder).IsAlive || (obj as IStatusHolder).Side == Owner.Side ||
					(obj as IStatusHolder).IsInvincible) return;
				
				var mDam:uint = Owner.CalculateDamage(multiplier);
				mDam = (obj as IStatusHolder).TakeDamage(mDam, new Point(), false);
				if (obj is Monster)
					(obj as Monster).Invoke(Owner);
			}
			if(!(obj is ILaunchable) || (obj as ILaunchable).Solidity > Solidity)
				DetonateProjectile(obj, movement);
		}
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			super.TakenHit(obj, movement);
		}
	}

}