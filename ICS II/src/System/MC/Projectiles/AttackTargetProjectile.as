package System.MC.Projectiles 
{
	import adobe.utils.CustomActions;
	import flash.geom.Point;
	import HardCode.Levels.IntroToBurnable;
	import System.GameFrame;
	import System.Interfaces.IBurnable;
	import System.Interfaces.ICollidable;
	import System.Interfaces.ILaunchable;
	import System.Interfaces.IProjectileLauncher;
	import System.Interfaces.IStatusHolder;
	import System.MC.Characters.Monster;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Tile;
	import System.MC.TileSet;
	import System.V.MonsterHealthBar;
	import System.V.RenderableObject;
	/**
	 * ...
	 * @author Knight
	 */
	public class AttackTargetProjectile extends Projectile 
	{
		public function AttackTargetProjectile(view:RenderableObject, collisionBox:CollisionBox,
			range:Number, owner:IProjectileLauncher, dmgMultiplier:Number, critChance:uint, turningSpeed:uint) 
		{
			super(view, collisionBox, range, owner, dmgMultiplier, critChance, turningSpeed);
			collide_with.push(IBurnable);
		}
		public override function Hit(obj:ICollidable, movement:Point):void
		{
			if (obj == Owner || (obj is Tile && (Point.distance(StartingPosition, Center)) <= Tile.Size.x / 2)) return;
			var found:Boolean = false;
			if (obj is TileSet)
			{
				var tile:TileSet = obj as TileSet;
				found = false;
				for (var i:int = 0; i < tile.Length && !found; i++)
				{
					if (tile.GetTileAtIndex(i).Collision.IntersectsFromEveryDirection)
						found = true;
				}
				if(found)
					GameFrame.RemoveChild(this);
			}
			else if (obj is Tile)
			{
				if(obj.Collision.IntersectsFromEveryDirection)
					onHitTile(obj as Tile);
			}
			if (obj is IBurnable)
			{
				found = false;
				for each(var a:IStatusHolder in attacked)
				{
					if (a == obj)
					{
						found = true;
						break;
					}
				}
				
				if (!found)
					onHitTarget(obj as IBurnable);
			}
		}
		protected function onHitTile(tile:Tile):void
		{
			GameFrame.RemoveChild(this);
		}
		protected function onHitTarget(target:IBurnable):void 
		{ 
			
			attacked.push(target as IStatusHolder);
			if(GameFrame.ContainChild(this))
				GameFrame.RemoveChild(this);
		}
	}

}