package System.MC.Projectiles 
{
	import adobe.utils.CustomActions;
	import flash.events.Event;
	import flash.geom.Point;
	import Resources.Animation;
	import System.GameFrame;
	import System.GameSystem;
	import System.GUI;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.ICollidable;
	import System.Interfaces.ILaunchable;
	import System.Interfaces.IProjectileLauncher;
	import System.Interfaces.IStatusHolder;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionBox;
	import System.MC.Tile;
	import System.MC.TurnableObject;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Projectile extends TurnableObject implements ILaunchable 
	{
		private var range:Number = 0;
		private var start:Point = null;
		private var owner:IProjectileLauncher = null;
		private var target:ICollidable = null;
		private var distance:int;
		private var turningSpeed:uint;
		protected var multiplier:Number;
		protected var attacked:Vector.<IStatusHolder>;
		protected var critChance:uint;
		private var solidity:int;
		public function Projectile(view:RenderableObject, collisionBox:CollisionBox, range:Number, owner:IProjectileLauncher,
			dmgMultiplier:Number, critical:int, turningSpeed:uint) 
		{
			super(view, collisionBox);
			this.range = range;
			start = new Point();
			this.owner = owner;
			multiplier = dmgMultiplier;
			attacked = new Vector.<IStatusHolder>();
			critChance = critical;
			this.turningSpeed = turningSpeed;
			collide_with.push(IStatusHolder, Tile, Projectile);
			solidity = int.MAX_VALUE;
		}
		public function LaunchAt(x:Number, y:Number):void
		{
			start.x = x;
			start.y = y;
			Push(Math.cos(Utility.ToRadian(View.rotation)) * Speed, Math.sin(Utility.ToRadian(View.rotation)) * Speed);
			distance = 0;
		}
		public override function MoveByOffset(x:int, y:int):void
		{
			super.MoveByOffset(x, y);
			distance += (new Point(x, y)).length;
			if (distance > Range)
			{
				if (View is IAnimatable && (View as IAnimatable).HasAnimation(1))
				{
					(View as IAnimatable).SetAnimation(1);
					View.addEventListener(Animation.FinishAnimation, onFadedOut);
				}
				else
					onFadedOut(null);
			}
		}
		public override function Update():void
		{
			super.Update();
			if (target == null) return;
			
			var rot:Number = View.rotation;
			var angle:Number = 0;
			angle = Utility.ToDegree(Math.atan2(target.Collision.CenterY - CenterY, target.Collision.CenterX - CenterX));
			
			force.x = 0;
			force.y = 0;
			
			var dif:Number = Math.abs(angle - rot);
			if (dif > 180) angle += 360;
			
			if (Math.abs(angle - rot) < turningSpeed) rot = angle;
			else if (angle < rot) rot -= turningSpeed;
			else if (angle > rot) rot += turningSpeed;
			
			//{
			//	if (target.CenterY > CenterY)
			//	{
			//		if(target.CenterX > CenterX)
			//			rot += turningSpeed; 
			//		else
			//			rot -= turningSpeed;
			//	}
			//	else if (target.CenterY < CenterY)
			//	{
			//		if(target.CenterX > CenterX)
			//			rot -= turningSpeed;
			//		else
			//			rot += turningSpeed;
			//	}
			//}
			
			force.x = Math.cos(Utility.ToRadian(rot)) * Speed;
			force.y = Math.sin(Utility.ToRadian(rot)) * Speed;
			
			View.rotation = rot;
			
			if (force.x < 0)
			{
				if (target.Collision.CenterX > CenterX)
					target = null;
			}
			else if(force.x > 0)
			{
				if (target.Collision.CenterX < CenterX) 
					target = null;
			}
		}
		public function SetTarget(target:ICollidable):void
		{
			this.target = target;
		}
		public function set Solidity(val:int):void { solidity = val; }
		public function get Solidity():int { return solidity; }
		public function get Target():ICollidable { return target; }
		public function get Speed():Number { return speed; }
		public function get Range():Number { return range; }
		public function set Range(val:Number):void { range = val; }
		public function get Owner():IProjectileLauncher { return owner; }
		public override function CalculateMovement():Point
		{
			return new Point(force.x, force.y);
		}
		private function onFadedOut(e:Event):void
		{
			GameFrame.RemoveChild(this);
		}
		protected function get StartingPosition():Point { return start; }
	}

}