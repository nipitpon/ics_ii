package System.MC.Items 
{
	import fl.transitions.Tween;
	import fl.transitions.easing.Regular
	import fl.transitions.TweenEvent;
	import flash.events.Event;
	import flash.geom.Point;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IItem;
	import System.Interfaces.IItemCollector;
	import System.MC.Collision.CollisionBox;
	import System.MC.MovableObject;
	import System.MC.States.ObjectState;
	import System.MC.Tile;
	import System.MC.TileSet;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Item extends MovableObject implements IItem
	{
		public static const Bouncing:uint = 0;
		public static const Idle:uint = 1;
		public static const FlyToCollector:uint = 2;
		private var state:uint = Bouncing;
		private var viewOffset:int = 0;
		private var viewOffsetChange:int = -10;
		protected var collector:IItemCollector;
		private var tween:Tween;
		private var count:int;
		public function Item(view:RenderableObject, collisionBox:CollisionBox, pushX:Number, pushY:Number)
		{
			super(view, collisionBox);
			Push(pushX, pushY);
			if (pushY == 0)
				state = Idle;
			count = Utility.SecondToFrame(60);
			collide_with = Vector.<Class>([Tile, TileSet, IItemCollector]);
		}
		public override function get DefyGravity():Boolean { return state != Bouncing; }
		public override function Update():void
		{
			super.Update();
			//if (state == Idle && tween == null)
			//{
			//	tween = new Tween(View, "y", Regular.easeInOut, View.y, View.y + viewOffsetChange, 1, true);
			//	tween.addEventListener(TweenEvent.MOTION_FINISH, onTweenFinished);
			//}
			if (state == FlyToCollector)
			{
				Push( -ForceX, -ForceY);
				var cCenter:Point = collector.Center;
				var thisCenter:Point = Center;
				if (Point.distance(cCenter, thisCenter) < 30)
				{
					Center = cCenter;
				}
				else
				{
					var angle:Number = Math.atan2(cCenter.y - thisCenter.y, cCenter.x - thisCenter.x);
					var difX:Number = 30 * Math.cos(angle);
					var difY:Number = 30 * Math.sin(angle);
					Push(difX, difY);
				}
			}
			//else if (--count <= 0 || Math.abs(CenterX - GameSystem.PlayerInstance.CenterX) > GameSystem.UpdateRange)
			//{
			//	GameFrame.RemoveChild(this);
			//}
		}
		public function GetCollected(collector:IItemCollector):void
		{
			this.collector = collector;
			state = FlyToCollector;
			if (tween != null)
			{
				tween.removeEventListener(TweenEvent.MOTION_FINISH, onTweenFinished);
				tween.setPosition(View.y - viewOffsetChange);
				tween.stop();
			}
			View.Center = Collision.Center;
		}
		public function get Collectable():Boolean { return state == Idle; }
		public override function Hit(obj:ICollidable, movement:Point):void
		{
			super.Hit(obj, movement);
			
			if (state == Bouncing && movement.y > 0)
			{
				if (obj is TileSet)
				{
					HitTileSet(obj as TileSet, movement);
				}
				else if (obj is Tile)
				{
					HitTile(obj as Tile, movement);
				}
				if (movement.x > 0 && Collision.Right + movement.x > GameSystem.TerrainEnd)
					movement.x = GameSystem.TerrainEnd - Collision.Right;
				else if (movement.x < 0 && Collision.Left + movement.x < GameSystem.TerrainStart)
					movement.x = GameSystem.TerrainStart - Collision.Left;
			}
			else if (collector != null && obj == collector)
			{
				OnCollected();
			}
		}
		protected function OnCollected():void
		{
			GameFrame.RemoveChild(this);
		}
		private function HitTile(t:Tile, movement:Point):void
		{
			var cBox:CollisionBox = t.Collision;
			if (movement.y != 0 && t.Intersects(Collision, new Point(0, movement.y)))
			{
				force.y = 0;
				force.x = 0;
				if (movement.y > 0)
					movement.y = cBox.Top - Collision.Bottom;
				else if (movement.y < 0)
					movement.y = cBox.Bottom - Collision.Top;
				state = Idle;
				collide_with = Vector.<Class>([IItemCollector]);
			}

			if (movement.x != 0 && t.Intersects(Collision, new Point(movement.x, 0)))
			{
				force.x = 0;
				if (movement.x > 0)
					movement.x = cBox.Left - Collision.Right;
				else if (movement.x < 0)
					movement.x = cBox.Right - Collision.Left;
			}
		}
		private function HitTileSet(s:TileSet, movement:Point):void
		{
			for (var i:uint = 0; i < s.Length; i++)
			{
				var t:Tile = s.GetTileAtIndex(i);
				
				HitTile(t, movement);
			}
		}
		private function onTweenFinished(e:Event):void
		{
			viewOffsetChange *= -1;
			tween = new Tween(View, "y", Regular.easeInOut, View.y, View.y + viewOffsetChange * 2, 2, true);
			tween.addEventListener(TweenEvent.MOTION_FINISH, onTweenFinished);
		}
	}
}