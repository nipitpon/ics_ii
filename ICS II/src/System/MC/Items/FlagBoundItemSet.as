package System.MC.Items 
{
	import System.DataCenter;
	import System.GameSystem;
	/**
	 * ...
	 * @author Knight
	 */
	public class FlagBoundItemSet extends ItemSet 
	{
		private var flag:Vector.<int>;
		public function FlagBoundItemSet(itemSet:Vector.<Class>, flag:Vector.<int>) 
		{
			super(itemSet);
			if (itemSet == null || flag == null || itemSet.length != flag.length)
				throw new ArgumentError();
			this.flag = flag;
			
		}
		public override function DropItemFromSet():Vector.<Class>
		{
			var obj:Vector.<Class> = super.DropItemFromSet();
			
			var outgoing:Vector.<Class> = new Vector.<Class>();
			
			for (var i:int = 0; i < obj.length; i++)
			{
				if (!DataCenter.GetFlag(flag[i]))
					outgoing.push(obj[i]);
			}
			return outgoing;
		}
	}

}