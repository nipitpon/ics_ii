package System.MC.Items 
{
	/**
	 * ...
	 * @author Knight
	 */
	public class ChanceBoundItemSet extends ItemSet 
	{
		private var chance:Vector.<int>;
		public function ChanceBoundItemSet(itemSet:Vector.<Class>, chance:Vector.<int>) 
		{
			super(itemSet);
			this.chance = chance;
			if (chance == null || itemSet.length != chance.length)
				throw new ArgumentError();
		}
		public override function DropItemFromSet():Vector.<Class>
		{
			var incoming:Vector.<Class> = super.DropItemFromSet();
			var outgoing:Vector.<Class> = new Vector.<Class>();
			for (var i:int = 0; i < incoming.length; i++)
			{
				if (Utility.Random(0, 100) < chance[i])
				{
					outgoing.push(incoming[i]);
				}
			}
			return outgoing;
		}
		
	}

}