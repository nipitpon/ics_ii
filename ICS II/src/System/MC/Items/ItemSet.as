package System.MC.Items 
{
	/**
	 * ...
	 * @author Knight
	 */
	public class ItemSet 
	{
		private var itemSet:Vector.<Class>;
		public function ItemSet(itemSet:Vector.<Class>) 
		{
			this.itemSet = itemSet;
		}
		public function DropItemFromSet():Vector.<Class> { return itemSet; }
	}

}