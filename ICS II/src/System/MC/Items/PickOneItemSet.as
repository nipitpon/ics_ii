package System.MC.Items 
{
	/**
	 * ...
	 * @author Knight
	 */
	public class PickOneItemSet extends ItemSet 
	{
		private var chance:Vector.<int>;
		public function PickOneItemSet(itemSet:Vector.<Class>, chance:Vector.<int>) 
		{
			super(itemSet);
			this.chance = chance;
			
		}
		public override function DropItemFromSet():Vector.<Class>
		{
			var items:Vector.<Class> = super.DropItemFromSet();
			var rand:int = Utility.Random(0, 100);
			var runner:int = 0;
			for (var i:int = 0; i < items.length; i++)
			{
				if (rand < runner + chance[i])
					return Vector.<Class>([items[i]]);
				else runner += chance[i];
			}
			return Vector.<Class>([]);
		}
	}
}