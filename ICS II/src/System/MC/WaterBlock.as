package System.MC 
{
	import System.Interfaces.IUpdatable;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Collision.CollisionTile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class WaterBlock extends CollidableObject implements IUpdatable
	{
		
		public function WaterBlock(length:* = 1) 
		{
			var col:CollisionBox = null;
			var ren:RenderableObject = new RenderableObject();
			if(length is Vector.<Vector.<int>>)
			{
				col = new CollisionTile(length as Vector.<Vector.<int>>, uint.MAX_VALUE, 10);
			}
			else
			{
				col = new CollisionRectangle(0, 0, 32, 32);
			}
			super(ren, col);
		}
		public function Update():void
		{
			View.Update();
		}
		public function get UpdateWhenAdvancingTime():Boolean { return true; }
	}

}