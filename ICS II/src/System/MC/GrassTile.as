package System.MC 
{
	import flash.display.Bitmap;
	import Resources.Image;
	import Resources.lib.GrassTileSprite;
	import Resources.Resource;
	import System.MC.Collision.CollisionBox;
	import System.V.RenderableObject;
	/**
	 * ...
	 * @author Knight
	 */
	public class GrassTile extends Tile
	{
		public function GrassTile(customBox:CollisionBox = null, matrix:Vector.<Vector.<int>> = null) 
		{
			super(false, customBox, matrix[0].length, matrix.length);
			Initialize(matrix);
		}
		
		private function Initialize(matrix:Vector.<Vector.<int>>):void
		{
			var i:int = 0, j:int = 0;
			for (i = 0; i < matrix.length; i++)
			{
				for (j = 0; j < matrix[i].length; j++)
				{
					var img:RenderableObject = new GrassTileSprite(matrix[i][j]);
					img.x = Size.x * j;
					img.y = Size.y * i;
					View.addChild(img);
				}
			}
			
		}
	}

}