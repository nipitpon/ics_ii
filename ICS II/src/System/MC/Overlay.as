package System.MC 
{
	import System.Interfaces.IOverlay;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Overlay extends ModelViewController implements IOverlay
	{
		public static const Location_FloorBackground:int = 0;
		public static const Location_FloorForeground:int = 1;
		public static const Location_Foreground:int = 2;
		public static const Location_Floor:int = 3;
		public static const Location_TopMost:int = 4;
		public static const Location_CharacterBackground:int = 5;
		public static const Location_Treasure:int = 6;
		public static const Location_CharacterForeground:int = 7;
		public static const Location_WeatherEffect:int = 8;
		public static const Location_Character:int = 9;
		private var location:int;
		private var bottom_most:Boolean;
		public function Overlay(view:RenderableObject, location:int )
		{
			super(view);
			this.location = location;
			bottom_most = false;
		}
		public function set BottomMost(val:Boolean):void { bottom_most = val; }
		public function get BottomMost():Boolean { return bottom_most; }
		public function set OverlayLevel(val:int):void { location = val; }
		public function get OverlayLevel():int { return location; }
	}

}