package System.MC.Ladder 
{
	import Resources.lib.Environment.LadderSprite;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Collision.CollisionTile;
	import System.MC.Tile;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Ladder extends CollidableObject 
	{
		public function Ladder(height:* = 1) 
		{
			var box:CollisionBox;
			var ren:RenderableObject;
			if (height is int)
			{
				box = new CollisionRectangle(0, 0, Tile.Size.x, Tile.Size.y * height)
				ren = new LadderSprite(height)
			}
			else
			{
				box = new CollisionTile(height as Vector.<Vector.<int>>);
				ren = new RenderableObject();
			}
			super(ren, box);
		}
	}
}