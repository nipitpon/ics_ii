package System.MC.PressurePlatform 
{
	import flash.geom.Point;
	import HardCode.Tiles.DoorTile;
	import Resources.lib.Sound.SFX.PressurePlateSound;
	import System.GameSystem;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.ICollidable;
	import System.Interfaces.INamable;
	import System.Interfaces.ISavable;
	import System.Interfaces.IUpdatable;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.Tile;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class PressurePlatform extends CollidableObject implements IUpdatable, ISavable
	{
		private var door_name:String;
		private var presser:ICollidable;
		public function PressurePlatform(view:RenderableObject, door_name:String) 
		{
			super(view, new CollisionRectangle(0, Tile.Size.y / 2, Tile.Size.x, Tile.Size.y / 2));
			collide_with.push(Player, Tile);
			this.door_name = door_name;
		}
		public function Update():void
		{
			if (presser != null && Intersects(presser.Collision, new Point(0, 1)) == null)
			{
				presser = null;
				Deactivate();
			}
		}
		public function SetInfo(obj:Object):void
		{
			(View as IAnimatable).SetAnimation(obj["animation"]);
		}
		public function GetInfo():Object { return { "animation": (View as IAnimatable).CurrentAnimation }; }
		public function get UpdateWhenAdvancingTime():Boolean { return false; }
		public function Activate():void
		{
			(View as IAnimatable).SetAnimation(1);
			SoundCenter.PlaySFXFromInstance(new PressurePlateSound());
			var doors:Vector.<INamable> = GameSystem.GetObjectByNames(door_name);
			for each(var door:INamable in doors)
			{
				(door as DoorTile).Activate();
			}
		}
		public function Deactivate():void
		{
			(View as IAnimatable).SetAnimation(0);
			SoundCenter.PlaySFXFromInstance(new PressurePlateSound());
			var doors:Vector.<INamable> = GameSystem.GetObjectByNames(door_name);
			for each(var door:INamable in doors)
			{
				(door as DoorTile).Activate();
			}
		}
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			super.TakenHit(obj, movement);
			if (presser == null && (View as IAnimatable).CurrentAnimation == 0)
			{
				Activate();
				presser = obj;
			}
		}
	}
}