package System.MC.Skills 
{
	import System.ControlCenter;
	import System.GameSystem;
	import System.Interfaces.IReceiveControl;
	import System.Interfaces.IUpdatable;
	import System.MC.Characters.Character;
	/**
	 * ...
	 * @author Knight
	 */
	public class KeyBoundSkill extends Skill implements IUpdatable
	{
		private var key:int;
		public function KeyBoundSkill(owner:Character, key:int, ammo:uint, mp:uint, range:uint, cooldown:uint) 
		{
			this.key = key;
			super(owner, ammo, mp, range, cooldown);
		}
		public override function Activate():void
		{
			super.Activate();
			GameSystem.AddGameObject(this);
			//ControlCenter.AddGameObject(this);
			//if (!ControlCenter.PressingKey(key))
			//	Deactivate();
		}
		public function get UpdateWhenAdvancingTime():Boolean { return false; }
		public function Update():void
		{
			if (!ControlCenter.PressingKey(key))
				Deactivate();
		}
		public override function Deactivate():void
		{
			super.Deactivate();
			//ControlCenter.RemoveGameObject(this);
			GameSystem.RemoveGameObject(this);
		}
		public function KeyRelease(code:int):void 
		{ 
			//if (key == code)
			//{
			//	Deactivate();
			//}
		}
		public function KeyPress(code:int):void { }
		public function get IsInGameObject():Boolean { return true; }
		public function MousePress(x:Number, y:Number):void { }
		public function MouseRelease(x:Number, y:Number):void { }
		public function MouseMove(x:Number, y:Number):void { }
	}
}