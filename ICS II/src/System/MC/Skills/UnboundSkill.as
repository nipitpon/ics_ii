package System.MC.Skills 
{
	import System.GameSystem;
	import System.Interfaces.IUpdatable;
	import System.MC.Characters.Character;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class UnboundSkill extends Skill implements IUpdatable
	{
		public function UnboundSkill(owner:Character, ammo:uint, mp:uint, range:uint, cooldown:uint) 
		{
			super(owner, ammo, mp, range, cooldown);
		}
		public function get UpdateWhenAdvancingTime():Boolean { return false; }
		public function Update():void { }
		public override function Activate():void
		{
			super.Activate();
			GameSystem.AddGameObject(this);
		}
		public override function Deactivate():void
		{
			super.Deactivate();
			GameSystem.RemoveGameObject(this);
		}
	}

}