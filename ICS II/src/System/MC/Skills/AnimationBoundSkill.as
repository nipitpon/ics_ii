package System.MC.Skills 
{
	import flash.events.Event;
	import Resources.Animation;
	import System.Interfaces.IAnimatable;
	import System.MC.Characters.Character;
	/**
	 * ...
	 * @author Knight
	 */
	public class AnimationBoundSkill extends Skill 
	{
		public function AnimationBoundSkill(owner:Character, ammo:uint, mp:uint, range:uint, cooldown:uint) 
		{
			super(owner, ammo, mp, range, cooldown);
		}
		
		public override function Activate():void
		{
			super.Activate();
			Owner.View.addEventListener(Animation.FinishAnimation, onFinishAnimation);
		}
		public override function Deactivate():void
		{
			Owner.View.removeEventListener(Animation.FinishAnimation, onFinishAnimation);
			super.Deactivate();
		}
		
		protected function onFinishAnimation(e:Event):void
		{
			Deactivate();
		}
	}

}