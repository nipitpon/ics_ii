package System.MC.Skills 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import System.GameFrame;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IHaveCooldown;
	import System.Interfaces.ISkill;
	import System.Interfaces.IUpdatable;
	import System.MC.Characters.Character;
	import System.MC.Characters.Player;
	import System.Time;
	/**
	 * ...
	 * @author Knight
	 */
	public class Skill extends EventDispatcher implements ISkill, IHaveCooldown
	{	
		public static const Deactivating:String = "Skill_Deactivating";
		private var owner:Character;
		private var animation:int;
		private var ammoRequired:uint;
		private var mpRequired:uint;
		private var range:uint;
		protected var deactivated:Boolean;
		protected var cooldown:uint;
		protected var cooldown_count:uint;
		public function Skill(owner:Character, ammo:uint, mp:uint, range:uint, cooldown:uint) 
		{
			this.owner = owner;
			ammoRequired = ammo;
			mpRequired = mp;
			this.range = range;
			deactivated = true;
			this.cooldown = cooldown;
			cooldown_count = 0;
		}
		public function CountCooldown():void
		{
			if (cooldown_count > 0)
			{
				if (Time.IsAdvancing)
					cooldown_count = Math.min(uint(cooldown_count - Time.TimeAdvanceMultiplier), 0);
				else
					cooldown_count--;
			}
		}
		public function get AllowMovement():Boolean { return false; }
		public function get Cooldown():uint { return cooldown; }
		public function get HasAnimation():Boolean { return true; }
		public function get CurrentCount():uint { return cooldown_count; }
		public function get IsHavingCooldown():Boolean { return cooldown_count != 0; }
		public function get Owner():Character { return owner; }
		public function get Range():uint { return range; }
		public function Activate():void 
		{ 
			deactivated = false;
			if (HasAnimation)
			{
				(Owner.View as IAnimatable).SetAnimation(UsingAnimation);
			}
			cooldown_count = cooldown;
		}
		public function Deactivate():void 
		{ 
			deactivated = true;
			dispatchEvent(new Event(Deactivating));
		}
		public function get IsDeactivated():Boolean { return deactivated; }
		public function get UsingAnimation():int { return animation; }
		public function set UsingAnimation(val:int):void { animation = val; }
		public function get AmmoRequired():uint { return ammoRequired; }
		public function get MPRequired():uint { return mpRequired; }
		public function get IsPassive():Boolean { return false; }
	}
}