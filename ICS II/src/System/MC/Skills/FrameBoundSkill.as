package System.MC.Skills 
{
	import fl.events.ProLoaderRSLPreloaderSandboxEvent;
	import System.GameSystem;
	import System.Interfaces.IUpdatable;
	import System.MC.Characters.Character;
	import System.Time;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FrameBoundSkill extends Skill implements IUpdatable
	{
		private var frame:uint;
		private var frame_count:uint;
		public function FrameBoundSkill(owner:Character, ammo:uint, mp:uint, range:uint, frame:uint, cooldown:uint) 
		{
			super(owner, ammo, mp, range, cooldown);
			this.frame = frame;
		}
		public function get UpdateWhenAdvancingTime():Boolean { return true; }
		public function Update():void
		{
			if (frame_count >= frame)
				Trigger();
			else
			{
				if (Time.IsAdvancing)
					frame_count += Time.TimeAdvanceMultiplier;
				else 
					frame_count++;
			}
		}
		public override function Activate():void
		{
			super.Activate();
			frame_count = 0;
			GameSystem.AddGameObject(this);
		}
		public override function Deactivate():void
		{
			super.Deactivate();
			GameSystem.RemoveGameObject(this);
		}
		protected function Trigger():void
		{
			Deactivate();
		}
		protected function get CurrentFrameCount():uint { return frame_count; }
	}

}