package System.MC.Skills 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import System.Interfaces.ISkill;
	import System.MC.Characters.Character;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class PassiveSkill extends EventDispatcher implements ISkill 
	{
		private var owner:Character;
		public function PassiveSkill(owner:Character) 
		{
			this.owner = owner;
		}
		public function get AllowMovement():Boolean { return true; }
		public function get UsingAnimation():int { return 0;}
		public function get MPRequired():uint { return 0; }
		public function get AmmoRequired():uint { return 0; }
		public function get Range():uint { return 0; }
		public function get HasAnimation():Boolean { return false; }
		public function get IsDeactivated():Boolean { return false; }
		public function Activate():void { }
		public function Deactivate():void { }
		public function get IsPassive():Boolean { return true; }
	}

}