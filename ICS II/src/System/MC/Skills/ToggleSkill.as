package System.MC.Skills 
{
	import flash.events.Event;
	import System.Interfaces.IAnimatable;
	import System.MC.Characters.Character;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ToggleSkill extends Skill
	{
		public function ToggleSkill(owner:Character, ammo:uint, mp:uint, range:uint, cooldown:uint) 
		{
			super(owner, ammo, mp, range, cooldown);
		}
		public function get IsActivating():Boolean { return !IsDeactivated; }
		public override function get UsingAnimation():int { return (Owner.View as IAnimatable).CurrentAnimation; }
		public override function Activate():void
		{
			if (!IsActivating)
			{
				cooldown_count = 0;
				deactivated = false;
			}
			else
			{
				cooldown_count = cooldown;
				deactivated = true;
			}
			dispatchEvent(new Event(Deactivating));
			Owner.View.Flash(255, 255, 255, 10);
		}
	}

}