package System.MC.Skills 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import System.Interfaces.IHaveCooldown;
	import System.Interfaces.ISkill;
	import System.MC.Skills.Skill;
	import System.Time;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SkillSet extends EventDispatcher implements ISkill, IHaveCooldown
	{
		private var skillSet:Vector.<ISkill>;
		private var position:int = 0;
		private var deactivated:Boolean;
		protected var cooldown:uint;
		protected var cooldown_count:uint;
		public function SkillSet(cooldown:uint, ...arg) 
		{
			if (arg.length == 0) throw new Error("No subskill");
			
			skillSet = new Vector.<ISkill>();
			for each(var s:* in arg)
			{
				if (!(s is ISkill))
				{
					throw new Error("Cannot insert non-skill class into SkillSet");
				}
				skillSet.push(s as ISkill);
			}
			deactivated = true;
			this.cooldown = cooldown;
			cooldown_count = 0;
		}
		public function CountCooldown():void
		{
			if (cooldown_count > 0)
			{
				if(Time.IsAdvancing)
					cooldown_count = Math.min(cooldown - Time.TimeAdvanceMultiplier, 0);
				else 
					cooldown_count--;
			}
		}
		public function get AllowMovement():Boolean { return false; }
		public function get HasAnimation():Boolean { return true; }
		public function get Cooldown():uint { return cooldown; }
		public function get CurrentCount():uint { return cooldown_count; }
		public function get IsHavingCooldown():Boolean { return cooldown_count != 0; }
		public function Activate():void
		{
			if (skillSet.length == 0) 
				throw new Error("SkillSet empty");
			
			deactivated = false;
			position = 0;
			var skill:ISkill = skillSet[position];
			cooldown_count = cooldown;
			skill.addEventListener(Skill.Deactivating, onSkillDeactivating);
			skill.Activate();
		}
		public function Deactivate():void 
		{ 
			deactivated = true; 
			if (position < skillSet.length)
			{
				if(skillSet[position].hasEventListener(Skill.Deactivating))
					skillSet[position].removeEventListener(Skill.Deactivating, onSkillDeactivating);
				skillSet[position].Deactivate();
			}
			dispatchEvent(new Event(Skill.Deactivating));
		}
		public function get UsingAnimation():int { return 0; }
		public function get IsPassive():Boolean { return false; }
		private function onSkillDeactivating(e:Event):void
		{
			(e.currentTarget as ISkill).removeEventListener(Skill.Deactivating, onSkillDeactivating);
			position++;
			if (position >= skillSet.length)
			{
				dispatchEvent(new Event(Skill.Deactivating));
			}
			else
			{
				var skill:ISkill = skillSet[position];
				skill.addEventListener(Skill.Deactivating, onSkillDeactivating);
				skill.Activate();
			}
		}
		public function get Range():uint
		{
			var max:uint = 0;
			for each(var s:ISkill in skillSet)
			{
				max = Math.max(s.Range, max);
			}
			return max;
		}
		public function get AmmoRequired():uint
		{
			var outgoing:uint = 0;
			for each(var skill:ISkill in skillSet)
			{
				outgoing += skill.AmmoRequired;
			}
			return outgoing;
		}
		public function get MPRequired():uint
		{
			var outgoing:uint = 0;
			for each(var skill:ISkill in skillSet)
			{
				outgoing += skill.MPRequired;
			}
			return outgoing;
		}
		public function get IsDeactivated():Boolean { return deactivated; }
	}

}