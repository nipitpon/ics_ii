package System.MC.Skills 
{
	import flash.geom.Point;
	import System.GameSystem;
	import System.Interfaces.IUpdatable;
	import System.MC.Characters.Character;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ForceBoundSkill extends Skill implements IUpdatable
	{
		private var checkForceX:Boolean = false;
		private var checkForceY:Boolean = false;
		public function ForceBoundSkill(owner:Character, ammo:uint, mp:uint, range:uint, cooldown:uint) 
		{
			super(owner, ammo, mp, range, cooldown);
		}
		public function get UpdateWhenAdvancingTime():Boolean { return true; }
		public function Update():void
		{
			if ((!checkForceX || Owner.ForceX == 0) &&
				(!checkForceY || Owner.ForceY == 0))
				Deactivate();
		}
		public override function Activate():void
		{
			super.Activate();
			GameSystem.AddGameObject(this);
		}
		public override function Deactivate():void
		{
			super.Deactivate();
			GameSystem.RemoveGameObject(this);
		}
		protected function CheckForceX(check:Boolean):void
		{
			checkForceX = check;
		}
		protected function CheckForceY(check:Boolean):void
		{
			checkForceY = check;
		}
	}

}