package System.MC 
{
	import _Base.MySound;
	import adobe.utils.CustomActions;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.InteractiveObject;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	import HardCode.Effects.WaterParticle;
	import HardCode.Goat.Baty;
	import HardCode.Goat.Goat;
	import HardCode.Goat.GoatSpawnPoint;
	import HardCode.Goat.GreenGegon;
	import HardCode.Goat.Walldo;
	import HardCode.Goat.YellowGegon;
	import HardCode.NPC.Bed;
	import HardCode.NPC.Bumper;
	import HardCode.NPC.Desk;
	import HardCode.NPC.Documents;
	import HardCode.NPC.FirePotion;
	import HardCode.NPC.Lever;
	import HardCode.NPC.Toolboard;
	import HardCode.NPC.TreasureChest;
	import HardCode.NPC.TrophyShelf;
	import HardCode.NPC.WarpSpot;
	import HardCode.NPC.WaterBottle;
	import HardCode.Overlays.CheckPoint;
	import HardCode.Overlays.Decoration;
	import HardCode.Overlays.SpinnableTile;
	import HardCode.Projectiles.Rocket;
	import HardCode.Projectiles.WaterBall;
	import HardCode.Tiles.BoxTile;
	import HardCode.Tiles.BurnableTiles;
	import HardCode.Tiles.Cannon;
	import HardCode.Tiles.DoorTile;
	import HardCode.Tiles.RocketLauncherTile;
	import HardCode.Tiles.SpikeTile;
	import HardCode.Tiles.StaticTile;
	import Resources.Animation;
	import Resources.Flipbook;
	import Resources.Image;
	import Resources.lib.DynamicFlipbook;
	import Resources.lib.Environment.BackgroundRockSprite;
	import Resources.lib.Environment.BackgroundTreeSprite;
	import Resources.lib.Environment.DecorationSprite;
	import Resources.lib.Environment.HillSprite;
	import Resources.lib.Environment.SunSprite;
	import Resources.lib.Sound.BGM.NightLevelBGM;
	import Resources.lib.Sound.BGM.SunnyWeatherSound;
	import Resources.lib.Tiles;
	import Resources.Resource;
	import System.ControlCenter;
	import System.DataCenter;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IInteractable;
	import System.Interfaces.IMVC;
	import System.Interfaces.INamable;
	import System.Interfaces.IOverlay;
	import System.Interfaces.ISavable;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionTile;
	import System.MC.etc.LevelInfo;
	import System.MC.Flags.EventTrigger;
	import System.MC.Ladder.Ladder;
	import System.MC.PressurePlatform.PressurePlatform;
	import System.MC.SpawnPoint;
	import System.SoundCenter;
	import System.Time;
	import System.V.FarBackground;
	import System.V.ForestBackground;
	import System.V.RenderableObject;
	/**
	 * ...
	 * @author Knight
	 */
	public class Level
	{
		private var terrain:Terrain;
		private var spawns:Vector.<SpawnPoint>;
		private var interactable:Vector.<IInteractable>;
		private var overlay:Vector.<IOverlay>;
		private var flags:Vector.<EventTrigger>;
		private var indoor:Vector.<Rectangle>;
		private var indoor_collision:Vector.<CollisionBox>;
		private var namables:Vector.<INamable>;
		private var ladders:Vector.<Ladder>;
		private var savables:Vector.<ISavable>;
		private var player:Player;
		private var start_hour:int;
		private var start_minute:int;
		private var start_second:int;
		private var objects_index:Dictionary;
		private var last_player_tile_x:int;
		private var objects_add_order:Vector.<*>;
		protected var far_background:RenderableObject;
		protected var middle_background:RenderableObject;
		protected var near_background:RenderableObject;
		protected var close_background:RenderableObject;
		protected var starting_point:Point;
		protected var show_paused_button:Boolean;
		
		private static var save_info:Dictionary = new Dictionary();
		
		public function Level(start_hour:int = 12, start_minute:int = 0, start_second:int = 0) 
		{
			this.start_hour = start_hour;
			this.start_minute = start_minute;
			this.start_second = start_second;
			show_paused_button = true;
			terrain = new Terrain();
			savables = new Vector.<ISavable>();
			objects_add_order = new Vector.<*>();
			spawns = new Vector.<SpawnPoint>();
			interactable = new Vector.<IInteractable>();
			overlay = new Vector.<IOverlay>();
			flags = new Vector.<EventTrigger>();
			indoor = new Vector.<Rectangle>();
			indoor_collision = new Vector.<CollisionBox>();
			objects_index = new Dictionary();
			ladders = new Vector.<Ladder>();
			last_player_tile_x = -1;
			namables = new Vector.<INamable>();
			close_background = new RenderableObject();// new SunSprite();
			far_background = new ForestBackground(HillSprite, 540);
			middle_background = new ForestBackground(BackgroundRockSprite, 540);
			near_background = new ForestBackground(BackgroundTreeSprite, 540);
			starting_point = new Point();
		}
		public function get OverrideBGM():Class { return null; }
		public function get FarBG():RenderableObject { return far_background; }
		public function get MiddleBG():RenderableObject { return middle_background; }
		public function get NearBG():RenderableObject { return near_background; }
		public function get CloseBG():RenderableObject { return close_background; }
		public function get ShowPauseMenu():Boolean { return show_paused_button; }
		public function get FreezeTime():Boolean { return !Time.IsAdvancing; }
		public function Start():void
		{
			ControlCenter.Start();
			player = new Player();
			this.savables.unshift(player);
			player.Center = starting_point;
			GameFrame.AddChild(player);
			Time.SetTime(start_hour, start_minute, start_second);
			Bed.Used = false;
			Lever.Enabled = true
			var i:int = 0;
			var savables:Vector.<ISavable> = this.savables.concat();
			for (i = 0; i < savables.length; i++)
			{
				savables[i].ID = i;
				if (save_info["has_save"] == true)
				{
					if (save_info[i] != null)
					{
						savables[i].SetInfo(save_info[i]);
					}
					else
					{
						RemoveObject(savables[i]);
					}
				}
			}
			for (i = 0; i < objects_add_order.length; i++)
			{
				var obj:* = objects_add_order[i];
				if (obj is IMVC)
					GameFrame.AddChild(obj as IMVC);
				else 
					GameSystem.AddGameObject(obj);
			}
			CheckAndResumeBGM();
		}
		public function CheckAndResumeBGM():void
		{
			if (OverrideBGM != null)
				SoundCenter.PlayBGM(OverrideBGM);
			else if (!Time.IsAdvancing)
			{
				if(Time.Hour < 18 && Time.Hour > 6)
					SoundCenter.PlayBGM(SunnyWeatherSound);
				else
					SoundCenter.PlayBGM(NightLevelBGM);
			}
			else
			{
				if(Time.TargetHour < 18 && Time.TargetHour > 6)
					SoundCenter.PlayBGM(SunnyWeatherSound);
				else
					SoundCenter.PlayBGM(NightLevelBGM);
			}
		}
		public function Stop():void
		{
			var start:int = last_player_tile_x - Math.ceil(GameSystem.UpdateRange / Tile.Size.x) - 2;
			var end:int = last_player_tile_x + Math.ceil(GameSystem.UpdateRange / Tile.Size.x) + 2;
			
			for (var i:int = start ; i < end; i++)
			{
				if (objects_index[i] == null) continue;
				
				for each(var obj:* in objects_index[i])
				{
					if (obj is IMVC)
					{
						if (GameFrame.ContainChild(obj))
							GameFrame.RemoveChild(obj);
					}
					else
					{
						if (GameSystem.ContainsGameObject(obj))
							GameSystem.RemoveGameObject(obj);
					}
				}
			}
			GameFrame.RemoveChild(player);
		}
		public function GetObjectByName(name:String):INamable
		{
			for each(var nam:INamable in namables)
				if (nam.Name == name) return nam;
			return terrain.GetObjectByName(name);
		}
		public function GetObjectByNames(name:String):Vector.<INamable>
		{
			var outgoing:Vector.<INamable> = new Vector.<INamable>();
			for each(var nam:INamable in namables)
				if (nam.Name == name) outgoing.push(nam);
			for each(var nam2:INamable in terrain.GetObjectByNames(name))
			{
				outgoing.push(nam);
			}
			return outgoing;
		}
		public function AddSpawnPoint(spawnPoint:SpawnPoint):void
		{
			spawns.push(spawnPoint);
			var x:int = int(spawnPoint.SpawnLocation.x / Tile.Size.x);
			if (objects_index[x] == null)
				objects_index[x] = new Vector.<*>();
				
			objects_index[x].push(spawnPoint);
			if (spawnPoint is INamable) namables.push(spawnPoint as INamable);
			if (spawnPoint is ISavable) savables.push(spawnPoint as ISavable);
			
			objects_add_order.push(spawnPoint);
		}
		public function AddInteractable(interactable:IInteractable, left:Number = NaN, top:Number = NaN):void
		{
			if (!isNaN(left))
				interactable.Left = left;
			if (!isNaN(top))
				interactable.Top = top;
			this.interactable.push(interactable);
			var x:int = int(interactable.CenterX / Tile.Size.x);
			if (objects_index[x] == null)
				objects_index[x] = new Vector.<*>();
				
			objects_index[x].push(interactable);
			if (interactable is INamable) namables.push(interactable as INamable);
			if (interactable is ISavable) savables.push(interactable as ISavable);
			
			objects_add_order.push(interactable);
		}
		public function AddObjectsLeftBottom(obj:IMVC, left:Number = NaN, bottom:Number = NaN):void
		{
			AddObjects(obj, left, bottom - obj.View.scaledHeight);
		}
		public function AddObjectsCenterXBottom(obj:IMVC, center_x:Number = NaN, bottom:Number = NaN):void
		{
			AddObjects(obj, center_x - (obj.View.scaledWidth / 2), bottom - obj.View.scaledHeight);
		}
		public function AddObjects(obj:IMVC, left:Number = NaN, top:Number = NaN):void
		{
			if (!isNaN(left))
				obj.Left = left;
			if (!isNaN(top))
				obj.Top = top;
			var x:int = int(obj.CenterX / Tile.Size.x);
			if (objects_index[x] == null)
				objects_index[x] = new Vector.<*>();
				
			objects_index[x].push(obj);
			if (obj is INamable) namables.push(obj as INamable);
			if (obj is ISavable) savables.push(obj as ISavable);
			
			objects_add_order.push(obj);
		}
		public function AddLadder(ladder:Ladder, left:Number = NaN, top:Number = NaN):void
		{
			if (!isNaN(left))
				ladder.Left = left;
			if (!isNaN(top))
				ladder.Top = top;
			this.ladders.push(ladder);
			var x:int = int(ladder.CenterX / Tile.Size.x);
			if (objects_index[x] == null)
				objects_index[x] = new Vector.<*>();
				
			objects_index[x].push(ladder);
			if (ladder is INamable) namables.push(ladder as INamable);
			if (ladder is ISavable) savables.push(ladder as ISavable);
			
			objects_add_order.push(ladder);
		}
		public function AddOverlayLeftBottom(obj:IOverlay, left:Number, bottom:Number):void
		{
			AddOverlay(obj, left, bottom - obj.View.scaledHeight);
		}
		public function AddOverlayCenterXTop(obj:IOverlay, center_x:Number, top:Number):void
		{
			AddOverlay(obj, center_x - obj.View.scaledWidth / 2, top);
		}
		public function AddOverlay(obj:IOverlay, left:Number = NaN, top:Number = NaN):void
		{
			if (!isNaN(left))
				obj.Left = left;
			if (!isNaN(top))
				obj.Top = top;
			overlay.push(obj);
			
			var x:int = int(obj.View.x / Tile.Size.x);
			
			var len:int = Math.max(obj.View.width / Tile.Size.x, 1);
			for (var i:int = x; i < x + len; i++)
			{
				if (objects_index[int(i)] == null)
					objects_index[int(i)] = new Vector.<*>();
					
				objects_index[int(i)].push(obj);
			}
			if (obj is INamable) namables.push(obj as INamable);
			if (obj is ISavable) savables.push(obj as ISavable);
			objects_add_order.push(obj);

		}
		public function GetInfo():LevelInfo
		{
			var inf:LevelInfo = new LevelInfo();
			inf.hour = Time.Hour;
			inf.minute = Time.Minute;
			inf.second = Time.Second;
			inf.spawnPoint_counts = new Vector.<int>();
			for each( var spawn:SpawnPoint in spawns)
			{
				inf.spawnPoint_counts.push(spawn.RemainingMonsterWave);
			}
			return inf;
		}
		public function RemoveObject(obj:*):void
		{
			var arr:Vector.<*> = Vector.<*>([spawns, interactable, overlay, flags, indoor, namables, ladders]);
			for each(var a:* in arr)
			{
				for (var i:int = 0; i < a.length; i++)
				{
					if (a[i] == obj)
						a.splice(i--, 1);
				}
			}
			for (var index:* in objects_index)
			{
				for (var j:int = 0; j < objects_index[index].length; j++)
				{
					if (objects_index[index][j] == obj)
						objects_index[index].splice(j--, 1);
				}
			}
			if (obj is ISavable) savables.splice(savables.indexOf(obj as ISavable), 1);
			if (objects_add_order.indexOf(obj) >= 0) objects_add_order.splice(objects_add_order.indexOf(obj), 1);
		}
		public function CheckTile(playerTileX:int):void
		{
			if (playerTileX == last_player_tile_x) return;
			//
			//var start:int = playerTileX - Math.ceil(GameSystem.UpdateRange / Tile.Size.x) - 2;
			//var end:int = playerTileX + Math.ceil(GameSystem.UpdateRange / Tile.Size.x) + 2;
			//var toRemove:Vector.<int> = new Vector.<int>();
			//var range:int = 0;
			//if (playerTileX < last_player_tile_x)
			//{
			//	range = last_player_tile_x - playerTileX;
			//	var oldEnd:int = last_player_tile_x + Math.ceil(GameSystem.UpdateRange / Tile.Size.x) + 2;
			//	for (var a:int = oldEnd - range; a <= oldEnd; a++)
			//	{
			//		toRemove.push(a);
			//	}
			//}
			//else if (playerTileX > last_player_tile_x)
			//{
			//	range = playerTileX - last_player_tile_x;
			//	var oldStart:int = last_player_tile_x - Math.ceil(GameSystem.UpdateRange / Tile.Size.x) - 2;
			//	for (var b:int = oldStart; b <= oldStart + range; b++)
			//	{
			//		toRemove.push(b);
			//	}
			//}
			//for (var i:int = start ; i < end; i++)
			//{
			//	if (objects_index[i] == null) continue;
			//	
			//	for each(var tile:* in objects_index[i])
			//	{
			//		if (tile is IMVC)
			//		{
			//			if (!(tile is EventTrigger) || !(tile as EventTrigger).IsFlagTriggered)
			//			{
			//				if (!GameFrame.ContainChild(tile))
			//					GameFrame.AddChild(tile);
			//			}
			//		}
			//		else
			//		{
			//			if (!GameSystem.ContainsGameObject(tile))
			//				GameSystem.AddGameObject(tile);
			//		}
			//	}
			//}
			//for each(var re:int in toRemove)
			//{
			//	if (objects_index[re] != null) for each(var t:* in objects_index[re])
			//	{
			//		if (IsObjectInIndexRange(t, start, end)) continue;
			//		
			//		if (t is IMVC)
			//		{
			//			if (GameFrame.ContainChild(t)) 
			//				GameFrame.RemoveChild(t);
			//		}
			//		else
			//		{
			//			if(GameSystem.ContainsGameObject(t))
			//				GameSystem.RemoveGameObject(t);
			//		}
			//	}
			//}
			terrain.CheckTile(playerTileX);
			last_player_tile_x = playerTileX;
		}
		public function get IndoorAreas():Vector.<Rectangle> { return indoor; }
		public function get Overlays():Vector.<IOverlay> { return overlay; }
		public function get TerrainLayer():Terrain { return terrain; }
		public function get SpawnPoints():Vector.<SpawnPoint> { return spawns; }
		public function get Interactables():Vector.<IInteractable> { return interactable; }
		public function get Flags():Vector.<EventTrigger> { return flags; }
		public function get Ladders():Vector.<Ladder> { return ladders; }
		
		protected function AddFlag(flag:EventTrigger):void
		{
			flags.push(flag);
			var x:int = int(flag.CenterX / Tile.Size.x);
			if (objects_index[x] == null)
				objects_index[x] = new Vector.<*>();
				
			objects_index[x].push(flag);
			
			objects_add_order.push(flag);
		}
		public function AddIndoorArea(rect:*):void
		{
			if(rect is Rectangle)
				indoor.push(rect as Rectangle);
			else if (rect is CollisionBox)
				indoor_collision.push(rect as CollisionBox);
		}
		public function RemoveIndoorArea(rect:*):void
		{
			if (rect is Rectangle)
			{
				for (var i:int = 0; i < indoor.length; i++)
				{
					if (rect == indoor[i])
					{
						indoor.splice(i--, 1);
					}
				}
			}
			else if (rect is CollisionBox)
			{
				for (var j:int = 0; j < indoor_collision.length; j++)
				{
					if (rect == indoor_collision[j])
					{
						indoor_collision.splice(j--, 1);
					}
				}
			}
		}
		protected function AddGrassPatch(start:int, end:int, bottom:int, amount:int, front_class:Vector.<Class>, back_class:Vector.<Class>):void
		{
			var the_x:int = 0;
			var i:int = 0;
			var c1:Class;
			var c2:Class;
			if(front_class != null && front_class.length > 0) for (i = 0; i < amount; i++)
			{ 
				the_x = Utility.Random(start, end);
				c1 = front_class[Utility.Random(0, 100) % front_class.length];
				AddOverlayLeftBottom(new Decoration(c1, Overlay.Location_Foreground), the_x, bottom);
			}
			if(back_class != null && back_class.length > 0) for (i = 0; i < amount; i++)
			{
				the_x = Utility.Random(start, end);
				c1 = back_class[Utility.Random(0, 100) % back_class.length];
				if (Utility.Random(0, 100) > 50) c1 = DecorationSprite.BackgroundGrass1;
				else c1 = DecorationSprite.BackgroundGrass2;
				AddOverlayLeftBottom(new Decoration(c1), the_x, bottom);
			}
		}
		public function IsIndoor(position:Point):Boolean 
		{
			
			for each(var p:Rectangle in indoor)
			{
				if (p.containsPoint(position)) return true;
			}
			for each(var c:CollisionBox in indoor_collision)
			{
				if (c.IntersectsPoint(position, null)) return true;
			}
			return false;
		}
		protected function LoadJSON(data:Object, next_level:int):void
		{
			SetupCollisionFromJSON(data);
			SetupIndoorAreaFromJSON(data);
			SetupTilesFromJSON(data);
			//SetupAnimatedTilesFromJSON(data);
			SetupItemPositionsFromJSON(data, next_level);
		}
		public static function ClearObjectInfo():void
		{
			save_info = new Dictionary();
			save_info["has_save"] = false;
		}
		public function SaveObjectInfo():void
		{
			SetObjectInfo(savables);
		}
		private static function SetObjectInfo(to_save:Vector.<ISavable>):void
		{
			var still_exist:Vector.<int> = new Vector.<int>();
			for (var i:int = 0; i < to_save.length; i++)
			{
				save_info[to_save[i].ID] = to_save[i].GetInfo();
				still_exist.push(to_save[i].ID);
			}
			for (var k:String in save_info)
			{
				var n:Number = parseInt(k);
				
				if (!isNaN(n) && still_exist.indexOf(n) < 0)
					save_info[n] = null;
			}
			save_info["has_save"] = true;
		}
		public static function LoadObjectSpriteFromTile(tile_number:int, map:Object, firstgid:int, origin:BitmapData):RenderableObject
		{
			var node:Object = map[(tile_number - firstgid).toString()];
			var add_node:Function = function(obj:Object, n:Object, value:int, x:int, y:int):void
			{ 
				if (obj[x] == null) obj[x] = { };
				
				obj[x][y] = value;
				if (n != null)
				{
					if (n.bottom != null) add_node(obj, map[n.bottom], n.bottom, x, y + 1);
					if (n.top != null) add_node(obj, map[n.top], n.top, x, y - 1);
					if (n.left != null) add_node(obj, map[n.left], n.left, x - 1, y);
					if (n.right != null) add_node(obj, map[n.right], n.right, x + 1, y);
				}
			};
			var create_single_bmp:Function = function(tn:String):Bitmap
			{
				var node_pos_raw:Object = { };
				add_node(node_pos_raw, map[tn], parseInt(tn), 0, 0);
				var min_x:int = 0;
				var min_y:int = 0;
				var max_x:int = 0;
				var max_y:int = 0;
				for (var key:String in node_pos_raw)
				{
					if (parseInt(key) < min_x)
						min_x = parseInt(key);
					if (parseInt(key) > max_x)
						max_x = parseInt(key);
						
					for (var k:String in node_pos_raw[key])
					{
						if (parseInt(k) < min_y)
							min_y = parseInt(k);
						if (parseInt(k) > max_y)
							max_y = parseInt(k);
					}
				}
				var dat:BitmapData = new BitmapData((Tile.Size.x / 2) * ((max_x - min_x) + 1), (Tile.Size.y / 2) * ((max_y - min_y) + 1), true, 0);
				for (var key2:String in node_pos_raw)
				{
					for (var k2:String in node_pos_raw[key2])
					{
						var id:int = node_pos_raw[key2][k2];
						var pos:Point = GetTilePosition(id, 0, 1, origin);
						
						dat.copyPixels(origin, 
							new Rectangle(pos.x * ((Tile.Size.x / 2) + 1), pos.y * ((Tile.Size.y / 2) + 1), Tile.Size.x / 2, Tile.Size.y / 2),
							new Point((Tile.Size.x / 2) * (parseInt(key2) - min_x),
										(parseInt(k2) - min_y) * (Tile.Size.y / 2)));
					}
				}
				var bmp:Bitmap = new Bitmap(dat);
				bmp.scaleX = 2;
				bmp.scaleY = 2;
				return bmp;
			}
			var outgoing:RenderableObject = null;
			if (node.activated == null && node.deactivated == null && node.charge == null)
			{
				outgoing = new Image();
				outgoing.addChild(create_single_bmp((tile_number - firstgid).toString()));
			}
			else
			{
				var bmps:Vector.<Bitmap> = Vector.<Bitmap>([create_single_bmp((tile_number - firstgid).toString())]);
				if (node.charge != null)
					bmps.push(create_single_bmp(node.charge));
				if (node.activated != null)
					bmps.push(create_single_bmp(node.activated));
				if (node.deactivated != null)
					bmps.unshift(create_single_bmp(node.deactivated));
				outgoing = new DynamicFlipbook(bmps,
					Vector.<Vector.<int>>([
					Vector.<int>([0]),
					Vector.<int>([1]),
					Vector.<int>([2])
					]), node.charge == null ? 0 : 4);
			}
			return outgoing;
		}
		private function SetupItemPositionsFromJSON(data:Object, next_level:int):void
		{
			var spikes:Vector.<Vector.<int>> = new Vector.<Vector.<int>>();
			var max_spike_length:int = 0;
			var water:Vector.<Vector.<int>> = new Vector.<Vector.<int>>();
			var max_water_length:int = 0;
			var ladder:Vector.<Vector.<int>> = new Vector.<Vector.<int>>();
			var max_ladder_length:int = 0;
			
			var i:int = 0;
			for each(var layer:Object in data.layers)
			{
				if (layer.data == null || layer.properties == null || layer.properties.position == null) continue;
				
				if (layer.properties.position == "cannon")
				{
					AddObjects(new Cannon(layer, data.tilesets));
					continue;
				}
				i = 0;
				var j:int = 0;
				for each(var tile_number:int in layer.data)
				{	
					
					var map:Object = null;
					for each(var m:Object in data.tilesets)
					{
						if (parseInt(m.firstgid) <= tile_number && (map == null || parseInt(map.firstgid) < parseInt(m.firstgid)))
						{
							map = m;
						}
					}
					if (map != null)
					{
						var node:Object = map.tileproperties[(tile_number - parseInt(map.firstgid)).toString()];
						if (node != null && node.position != null)
						{
							var spawn:SpawnPoint;
							var ren:RenderableObject = LoadObjectSpriteFromTile(tile_number, map.tileproperties, map.firstgid, Tiles.GetBitmapData(map.image));
							switch(node.position)
							{
								case "checkpoint": AddObjectsCenterXBottom(new CheckPoint(ren), (i + 0.5) * Tile.Size.x, (j + 1) * Tile.Size.y); break;
								case "treasure":
									if (next_level - 1 > 50 || !DataCenter.SecretItemCollected(next_level >= 0 ? next_level - 1 : 50))
									{
										var chest:TreasureChest = new TreasureChest(ren, next_level - 1);
										chest.Bottom = (j + 1) * Tile.Size.y;
										chest.CenterX = (i + 0.5) * Tile.Size.x;
										AddObjects(chest);
									}
									break;
								case "option": AddObjects(new Toolboard(ren), i * Tile.Size.x, j * Tile.Size.y); break;
								case "credit": AddObjects(new Documents(ren), i * Tile.Size.x, j * Tile.Size.y); break;
								case "achievement": AddObjects(new TrophyShelf(ren), i * Tile.Size.x, j * Tile.Size.y); break;
								case "level_selection": AddObjects(new Desk(), i * Tile.Size.x, j * Tile.Size.y); break;
								case "button": AddObjects(new PressurePlatform(ren, node.object), i * Tile.Size.x, j * Tile.Size.y); break;
								case "bed": AddObjects(new Bed(ren), i * Tile.Size.x, j * Tile.Size.y); break;
								case "pushable": AddObjects(new BoxTile(ren), i * Tile.Size.x, j * Tile.Size.y); break;
								case "spring": AddObjects(new Bumper(ren), i * Tile.Size.x, j * Tile.Size.y); break;
								case "baty": 
									spawn = new SpawnPoint(Tile.GetTopLeft(i, j), 100, 1);
									spawn.SetSpawns(Vector.<Class>([Baty]), Vector.<Class>([Baty]));
									AddSpawnPoint(spawn);
									break;
								case "yellow_gegon": 
									spawn = new SpawnPoint(Tile.GetTopLeft(i, j), 100, 1);
									spawn.SetSpawns(Vector.<Class>([YellowGegon]), Vector.<Class>([YellowGegon]));
									AddSpawnPoint(spawn);
									break;
								case "green_gegon":
									spawn = new SpawnPoint(Tile.GetTopLeft(i, j), 100, 1);
									spawn.SetSpawns(Vector.<Class>([GreenGegon]), Vector.<Class>([GreenGegon]));
									AddSpawnPoint(spawn);
									break;
								case "walldo":
									spawn = new SpawnPoint(Tile.GetTopLeft(i, j), 100, 1);
									spawn.SetSpawns(Vector.<Class>([Walldo]), Vector.<Class>([Walldo]));
									AddSpawnPoint(spawn);
									break;
								case "fire_launcher": case "water_launcher":
									var directions:Array = node.directions.split(',');
									AddObjects(new RocketLauncherTile(
										directions.indexOf("0") >= 0, 
										directions.indexOf("3") >= 0,
										directions.indexOf("2") >= 0,
										directions.indexOf("1") >= 0,
										node.position == "fire_launcher" ? Rocket : WaterBall, ren), i * Tile.Size.x, j * Tile.Size.y);
									break;
								case "burnable": AddObjects(new BurnableTiles(CropBitmapData(tile_number,
									map.firstgid, map.spacing, Tiles.GetBitmapData(map.image), new Point())), 
									i * Tile.Size.x, j * Tile.Size.y);  break;
								case "player": starting_point = new Point((i + 0.5) * Tile.Size.x, (j + 0.5) * Tile.Size.y); break;
								case "exit": AddInteractable(new WarpSpot(next_level, ren), i * Tile.Size.x, j * Tile.Size.y); break;
								case "switch":
									AddInteractable(new Lever(ren, node.object), i * Tile.Size.x, j * Tile.Size.y); break;
								case "BlueDoor": case "RedDoor": case "GreenDoor": case "YellowDoor":
									AddObjects(new DoorTile(node.position, ren as Animation, node.activated == null), i * Tile.Size.x, j * Tile.Size.y); break;
								case "water_potion": AddInteractable(new WaterBottle(CropBitmapData(tile_number, 
									map.firstgid, map.spacing, Tiles.GetBitmapData(map.image), new Point())),
									Tile.Size.x * i, Tile.Size.y * j); break;
								case "fire_potion": AddInteractable(new FirePotion(CropBitmapData(tile_number, 
									map.firstgid, map.spacing, Tiles.GetBitmapData(map.image), new Point())),
									Tile.Size.x * i, Tile.Size.y * j); break;
								case "ladder":
									while (ladder.length <= j) ladder.push(new Vector.<int>());
									while (ladder[j].length <= i) ladder[j].push( -1);
									ladder[j][i] = 1;
									if (i + 1 > max_ladder_length) max_ladder_length = i + 1;
									break;
								case "spike":
									while (spikes.length <= j) spikes.push(new Vector.<int>());
									while (spikes[j].length <= i) spikes[j].push( -1);
									spikes[j][i] = 1;
									if (i + 1 > max_spike_length) max_spike_length = i + 1;
									break;
								case "water":
									while (water.length <= j) water.push(new Vector.<int>());
									while (water[j].length <= i) water[j].push( -1);
									water[j][i] = 1;
									if (i + 1 > max_water_length) max_water_length = i + 1;
									break;
							}
						}
					}

					if (++i % layer.width == 0 && i > 0)
					{
						i -= parseInt(layer.width);
						j++;
					}
				}
			}
			for (i = 0; i < spikes.length; i++)
				while (spikes[i].length < max_spike_length) spikes[i].push( -1);
			for (i = 0; i < water.length; i++)
				while (water[i].length < max_water_length) water[i].push( -1);
			for (i = 0; i < ladder.length; i++)
				while (ladder[i].length < max_ladder_length) ladder[i].push( -1);
				
			if (spikes.length > 0) AddObjects(new SpikeTile(spikes));
			if (water.length > 0) AddObjects(new WaterBlock(water));
			if (ladder.length > 0) AddObjects(new Ladder(ladder));
		}
		public static function GetTilePosition(id:int, firstgid:int, spacing:int, origin:BitmapData):Point
		{
			var x:int = (id - firstgid) % ((origin.width + spacing) / ((Tile.Size.x / 2) + spacing));
			var y:int = (id - firstgid) / ((origin.width + spacing) / ((Tile.Size.y / 2) + spacing));
			return new Point(x, y);
		}
		public static function CropBitmapData(id:int, firstgid:int, spacing:int, origin:BitmapData, location:Point):BitmapData
		{
		    var outgoing:BitmapData = new BitmapData(Tile.Size.x, Tile.Size.y, true, 0);
			
			var pos:Point = GetTilePosition(id, firstgid, spacing, origin);
			
			outgoing.copyPixels(origin, new Rectangle(pos.x * ((Tile.Size.x / 2) + spacing), pos.y * ((Tile.Size.y / 2) + spacing), Tile.Size.x / 2, Tile.Size.y / 2),
				new Point(((Tile.Size.x / 2)) * location.x, ((Tile.Size.y / 2)) * location.y));
			return outgoing;
		}
		private function SetupAnimatedTilesFromJSON(data:Object):void
		{
			var animations:Object = { };
			var i:int = 0;
			var j:int = 0;
			var pos:Point = null;
			for each(var layer:Object in data.layers)
			{
				if (layer.data == null || layer.properties == null) continue;
				i = 0;
				j = 0;
				
				for each(var tile_number:int in layer.data)
				{	
					var map:Object = null;
					for each(var m:Object in data.tilesets)
					{
						if (parseInt(m.firstgid) < tile_number && (map == null || parseInt(map.firstgid) < parseInt(m.firstgid)))
						{
							map = m;
						}
					}
					if (map != null && map.tiles != null)
					{
						var node:Object = map.tiles[(tile_number - parseInt(map.firstgid)).toString()];
						if (node != null && node.animation != null)
						{
							var origin:BitmapData = Tiles.GetBitmapData(map.image);
							pos = new Point(i % layer.width, j);
							for (var k:int = 0; k < node.animation.length; k++)
							{
								var n:Object = null;
								if ((n = animations[node.animation[0].duration]) == null) 
									n = animations[node.animation[0].duration] = { };
								if ((n = n[pos.x + "," + pos.y]) == null) 
									n = animations[node.animation[0].duration][pos.x + "," + pos.y] = { };
								if ((n = n[layer.properties.location]) == null)
									n = animations[node.animation[0].duration][pos.x + "," + pos.y][layer.properties.location] = { };
								if ((n = n[k]) == null) 
									n = animations[node.animation[0].duration][pos.x + "," + pos.y][layer.properties.location][k] = new Vector.<BitmapData>();
								
								var dat:BitmapData = null;
								if (animations[node.animation[0].duration][(pos.x - 1) + "," + pos.y] != null)
								{
									
								}
								else
								{
								}
								dat = CropBitmapData(node.animation[k].tileid, 0, map.spacing, origin, new Point());
								
								n.push(dat);
							}
						}
					}
					if (++i % layer.width == 0 && i > 0)
					{
						i -= parseInt(layer.width);
						j++;
					}
				}
			}
			for (var interval:String in animations) 
			{
				
				for (var position:String in animations[interval]) 
				{
					pos = new Point(parseFloat(position.split(',')[0]), parseFloat(position.split(',')[1]));
					for (var location:String in animations[interval][position]) 
					{
						var loc:int = 0;
						switch(location)
						{
							case "floor": loc = Overlay.Location_Floor; break;
							case "floor_background": loc = Overlay.Location_FloorBackground; break;
							case "foreground": loc = Overlay.Location_Foreground; break;
						}
						var obj:Vector.<Vector.<Bitmap>> = new Vector.<Vector.<Bitmap>>();
						for(var frame:String in animations[interval][position][location])
						{
							var vec:Vector.<BitmapData> = animations[interval][position][location][frame] as Vector.<BitmapData>;
							
							for (i = 0; i < vec.length; i++)
							{
								var bmp:Bitmap = new Bitmap(vec[i]);
								bmp.scaleX = 2;
								bmp.scaleY = 2;
								while (i >= obj.length) obj.push(new Vector.<Bitmap>());
								while (parseInt(frame) >= obj[i].length) obj[i].push(bmp);
							}
						}
						
						for (i = 0; i < obj.length; i++)
						{
							var anim:Vector.<Vector.<int>> = Vector.<Vector.<int>>([new Vector.<int>()]);
							for (j = 0; j < obj[i].length; j++)
								anim[0].push(j);
							var ren:DynamicFlipbook = new DynamicFlipbook(obj[i], anim, Utility.SecondToFrame(parseInt(interval) / 1000.0));
							AddOverlay(new Overlay(ren, loc), Tile.Size.x * pos.x, Tile.Size.y * pos.y);
						}
					}
				}
			}
		}
		private function SetupTilesFromJSON(data:Object):void
		{
			/*
			 * animations
			 * - intervals (0, 1, 2, 3, ...)
			 * --- position (x + "," + y)
			 * ----- location ("floor", "floor_background", ...)
			 * ------- frames (0, 1, 2, 3, ...)
			 * --------- bitmapdata (Vector.<BitmapData>)
			 * */
			var animations:Object = { };
			for each(var layer:Object in data.layers)
			{
				if (layer.data == null || layer.properties == null || layer.properties.location == null) continue;
				
				var max_width:int;
				var max_height:int;
				
				var i:int = 0;
				var j:int = 0;
				var tile_number:int = 0;
				for each(tile_number in layer.data)
				{	
					
					if (tile_number > 0)
					{
						if (j > max_height) max_height = j;
						if (i > max_width) max_width = i;
					}

					if (++i % layer.width == 0 && i > 0)
					{
						i -= parseInt(layer.width);
						j++;
					}
				}
				
				var dat:BitmapData = new BitmapData(max_width * Tile.Size.x, max_height * Tile.Size.y, true, 0);
				
				i = 0;
				j = 0;
				for each(tile_number in layer.data)
				{
					if (tile_number > 0)
					{
						var map:Object = null;
						for each(var m:Object in data.tilesets)
						{
							if (parseInt(m.firstgid) <= tile_number && (map == null || parseInt(map.firstgid) < parseInt(m.firstgid)))
							{
								map = m;
							}
						}
						
						var origin:BitmapData = Tiles.GetBitmapData(map.image);
						var node:Object = map.tiles[(tile_number - parseInt(map.firstgid)).toString()];
						if (map != null && (map.tiles == null || map.tiles[(tile_number - map.firstgid).toString()] == null || 
							map.tiles[(tile_number - map.firstgid).toString()].animation == null))
						{
							var pos:Point = GetTilePosition(tile_number, map.firstgid, map.spacing, origin);
							dat.copyPixels(origin, new Rectangle(pos.x * ((Tile.Size.x / 2) + map.spacing), pos.y * ((Tile.Size.y / 2) + map.spacing), Tile.Size.x / 2, Tile.Size.y / 2),
								new Point(((Tile.Size.x / 2)) * (i % layer.width), ((Tile.Size.y / 2)) * j));
						}
						if (node != null && node.animation != null)
						{
							pos = new Point(i % layer.width, j);
							for (var k:int = 0; k < node.animation.length; k++)
							{
								var n:Object = null;
								if ((n = animations[node.animation[0].duration]) == null) 
									n = animations[node.animation[0].duration] = { };
								if ((n = n[pos.x + "," + pos.y]) == null) 
									n = animations[node.animation[0].duration][pos.x + "," + pos.y] = { };
								if ((n = n[layer.properties.location]) == null)
									n = animations[node.animation[0].duration][pos.x + "," + pos.y][layer.properties.location] = { };
								if ((n = n[k]) == null) 
									n = animations[node.animation[0].duration][pos.x + "," + pos.y][layer.properties.location][k] = new Vector.<BitmapData>();
								
								var animation_dat:BitmapData = null;
								animation_dat = CropBitmapData(node.animation[k].tileid, 0, map.spacing, origin, new Point());
								
								n.push(animation_dat);
							}
						}
						
					}
					if (++i % layer.width == 0 && i > 0)
					{
						i -= parseInt(layer.width);
						j++;
					}
				}
				
				var loc:int;
				if (layer.properties.location == "floor") loc = Overlay.Location_Floor;
				else if (layer.properties.location == "floor_background") loc = Overlay.Location_FloorBackground;
				else if (layer.properties.location == "foreground") loc = Overlay.Location_Foreground;
				
				var pivot:Point = null;
				var p:Array;
				if (layer.properties.pivot != null)
				{
					p = layer.properties.pivot.split(',');
					pivot = Tile.GetTopLeft(parseFloat(p[0])/2, parseFloat(p[1])/2);
				}
				var ren:RenderableObject = new RenderableObject(pivot);
				ren.addChild(new Bitmap(dat));
				ren.scaleX = 2;
				ren.scaleY = 2;
				ren.Left = 0;
				ren.Top = 0;
				if (layer.properties.offset != null)
				{
					p = layer.properties.offset.split(',');
					ren.Left = parseFloat(p[0]) * Tile.Size.x;
					ren.Top = parseFloat(p[1]) * Tile.Size.y;
				}
				if (layer.properties.rotation != null)
					AddOverlay(new SpinnableTile(ren, loc, parseFloat(layer.properties.rotation)));
				else
					AddOverlay(new Overlay(ren, loc));
					
				for (var interval:String in animations) 
				{
					
					for (var position:String in animations[interval]) 
					{
						pos = new Point(parseFloat(position.split(',')[0]), parseFloat(position.split(',')[1]));
						for (var location:String in animations[interval][position]) 
						{
							var animation_loc:int = 0;
							switch(location)
							{
								case "floor": animation_loc = Overlay.Location_Floor; break;
								case "floor_background": animation_loc = Overlay.Location_FloorBackground; break;
								case "foreground": animation_loc = Overlay.Location_Foreground; break;
							}
							var obj:Vector.<Vector.<Bitmap>> = new Vector.<Vector.<Bitmap>>();
							for(var frame:String in animations[interval][position][location])
							{
								var vec:Vector.<BitmapData> = animations[interval][position][location][frame] as Vector.<BitmapData>;
								
								for (i = 0; i < vec.length; i++)
								{
									var bmp:Bitmap = new Bitmap(vec[i]);
									bmp.scaleX = 2;
									bmp.scaleY = 2;
									while (i >= obj.length) obj.push(new Vector.<Bitmap>());
									while (parseInt(frame) >= obj[i].length) obj[i].push(bmp);
								}
							}
							
							for (i = 0; i < obj.length; i++)
							{
								var anim:Vector.<Vector.<int>> = Vector.<Vector.<int>>([new Vector.<int>()]);
								for (j = 0; j < obj[i].length; j++)
									anim[0].push(j);
								var dynamic_flipbook:DynamicFlipbook = new DynamicFlipbook(obj[i], anim, Utility.SecondToFrame(parseInt(interval) / 1000.0));
								AddOverlay(new Overlay(dynamic_flipbook, animation_loc), Tile.Size.x * pos.x, Tile.Size.y * pos.y);
							}
						}
					}
				}
				animations = { };
			}
		}
		private function SetupIndoorAreaFromJSON(data:Object):void
		{
			var collision_info:Vector.<Vector.<int>> = new Vector.<Vector.<int>>();
			
			var max_length:int = 0;
			for each(var layer:Object in data.layers)
			{
				if (layer.data == null) continue;
				
				var i:int = 0;
				var j:int = 0;
				for each(var tile_number:int in layer.data)
				{	
					
					var map:Object = null;
					for each(var m:Object in data.tilesets)
					{
						if (parseInt(m.firstgid) <= tile_number && (map == null || parseInt(map.firstgid) < parseInt(m.firstgid)))
						{
							map = m;
						}
					}
					if (map != null)
					{
						var node:Object = map.tileproperties[(tile_number - parseInt(map.firstgid)).toString()];
						if (node != null && node.indoor != null)
						{
							while (j >= collision_info.length)
								collision_info.push(new Vector.<int>());
							while (i >= collision_info[j].length)
								collision_info[j].push( -1);
							collision_info[j][i] = 1;
							if (i > max_length) max_length = i;
						}
					}

					if (++i % layer.width == 0 && i > 0)
					{
						i -= parseInt(layer.width);
						j++;
					}
				}
			}
			for each(var col:Vector.<int> in collision_info)
			{
				while (col.length < max_length)
					col.push( -1);
			}
			AddIndoorArea(new CollisionTile(collision_info));
		}
		private function SetupCollisionFromJSON(data:Object):void
		{
			var collision_info:Vector.<Vector.<int>> = new Vector.<Vector.<int>>();
			var up_collision_info:Vector.<Vector.<int>> = new Vector.<Vector.<int>>();
			
			var max_length:int = 0;
			var max_up_length:int = 0;
			for each(var layer:Object in data.layers)
			{
				if (layer.data == null) continue;
				
				var i:int = 0;
				var j:int = 0;
				for each(var tile_number:int in layer.data)
				{	
					
					var map:Object = null;
					for each(var m:Object in data.tilesets)
					{
						if (parseInt(m.firstgid) <= tile_number && (map == null || parseInt(map.firstgid) < parseInt(m.firstgid)))
						{
							map = m;
						}
					}
					if (map != null && map.tiles != null)
					{
						var node:Object = map.tiles[(tile_number - parseInt(map.firstgid)).toString()];
						if (node != null && node.terrain != null && node.terrain.indexOf(0) >= 0)
						{
							if (layer.properties == null || layer.properties.collision_direction != "up")
							{
								while (j >= collision_info.length)
									collision_info.push(new Vector.<int>());
								while (i >= collision_info[j].length)
									collision_info[j].push( -1);
								collision_info[j][i] = 1;
								if (i + 1 > max_length) max_length = i + 1;
							}
							else
							{
								while (j >= up_collision_info.length)
									up_collision_info.push(new Vector.<int>());
								while (i >= up_collision_info[j].length)
									up_collision_info[j].push( -1);
								up_collision_info[j][i] = 1;
								if (i + 1 > max_up_length) max_up_length = i + 1;
							}
						}
					}

					if (++i % layer.width == 0 && i > 0)
					{
						i -= parseInt(layer.width);
						j++;
					}
				}
			}
			for each(var col:Vector.<int> in collision_info)
			{
				while (col.length < max_length)
					col.push( -1);
			}
			for each(var up:Vector.<int> in up_collision_info)
			{
				while (up.length < max_up_length)
					up.push( -1);
			}
			TerrainLayer.SetTileAt(0, 0, new StaticTile(collision_info));
			if (up_collision_info.length > 0) TerrainLayer.SetTileAt(0, 0, new StaticTile(up_collision_info, 
				CollisionBox.IntersectDirection_Bottom));
		}
		private function IsObjectInIndexRange(obj:*, start:int, end:int):Boolean
		{
			var s:int = 0;
			var e:int = 0;
			if (obj is ICollidable)
			{
				var col:CollisionBox = (obj as ICollidable).Collision;
				s = Math.floor(col.Left / Tile.Size.x);
				e = s + Math.ceil(col.Width / Tile.Size.x);
			}
			else if (obj is IMVC)
			{
				var view:RenderableObject = (obj as IMVC).View;
				s = Math.floor(view.Left / Tile.Size.x);
				e = s + Math.ceil(view.width / Tile.Size.x);
			}
			else if (obj is SpawnPoint)
			{
				var spnt:SpawnPoint = SpawnPoint(obj);
				s = Math.floor(spnt.SpawnLocation.x / Tile.Size.x);
				e = s + Math.ceil(10 / Tile.Size.x);
			}
			else
			{
				var dis:DisplayObject = obj as DisplayObject;
				
				s = Math.floor(dis.x / Tile.Size.x);
				e = s + Math.ceil(dis.width / Tile.Size.x);
				
			}
			return start <= e && s <= end;
		}
	}
}