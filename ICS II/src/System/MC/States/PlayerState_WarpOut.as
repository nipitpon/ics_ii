package System.MC.States 
{
	import _Base.MySound;
	import fl.transitions.easing.Regular;
	import fl.transitions.easing.Strong;
	import flash.geom.ColorTransform;
	import Resources.lib.Sound.SFX.WarpOutSound;
	import System.MC.Characters.Character;
	import System.MC.Characters.Player;
	import System.SoundCenter;
	
	/**
	 * ...
	 * @author ss
	 */
	public class PlayerState_WarpOut extends PlayerState_Idle 
	{
		private static const MaxCount:int = Utility.FramePerSecond * 0.5;
		private static const HalfCount:int = Utility.FramePerSecond * 0.25;
		private var count:int;
		public function PlayerState_WarpOut(owner:Player) 
		{
			super(owner);
			canMove = false;
			canSkill = false;
			canToggleSkill = false;
			canProne = false;
			canJump = false;
			canDefaultAtk = false;
			canReload = false;
			canUseNoAnimSkill = false;
		}
		public override function OnUpdate():void
		{
			if (count >= 0)
			{
				if(--count < 0)
				{
					WarpOut();
					Owner.View.visible = false;
				}
				else
				{
					var c:int = MaxCount - count;
					var mul:Number = Owner.View.scaleX < 0 ? -1 : 1;
					if (c <= HalfCount)
					{
						Owner.View.scaleX = Regular.easeOut(c, 1, 1, HalfCount) * mul;
						Owner.View.scaleY = Regular.easeOut(c, 1, -0.5, HalfCount);
						var color_mul:Number = Utility.Interpolate(0, 1, c / HalfCount);
						Owner.View.transform.colorTransform = new ColorTransform(color_mul, color_mul, color_mul, 1,
							227 * (1 - color_mul), 255 * (1 - color_mul), 255 * (1 - color_mul));
					}
					else
					{
						Owner.View.scaleX = Regular.easeIn(c - HalfCount, 2, -1.8, HalfCount) * mul;
						Owner.View.scaleY = Regular.easeIn(c - HalfCount, 0.5, 1.5, HalfCount);
						Owner.View.alpha = Strong.easeIn(c - HalfCount, 1, -1, HalfCount);
					}
				}
			}
		}
		public override function OnActivating():void
		{
			var owner:Player = Owner as Player;
			owner.SetAnimation(Character.Idle);
			owner.Freeze = true;
			count = MaxCount;
			SoundCenter.PlaySFX(WarpOutSound);
		}
		private function WarpOut():void
		{
			Main.ChangeToGamePage();
		}
	}

}