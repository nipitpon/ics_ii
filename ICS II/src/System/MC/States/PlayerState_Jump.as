package System.MC.States 
{
	import HardCode.Effects.JumpingDustEffect;
	import System.ControlCenter;
	import System.GameFrame;
	import System.Interfaces.IAnimatable;
	import System.MC.Characters.Character;
	import System.MC.Characters.Player;
	/**
	 * ...
	 * @author Knight
	 */
	public class PlayerState_Jump extends PlayerState_Idle 
	{
		
		public function PlayerState_Jump(owner:Player) 
		{
			super(owner);
			canMove = false;
			canSkill = true;
			canToggleSkill = true;
			canJump = false;
			canProne = false;
			canDefaultAtk = true;
			canReload = true;
		}
		public override function OnUpdate():void
		{
			super.OnUpdate();
			var anim:IAnimatable = Owner.View as IAnimatable;
			if (anim.CurrentAnimation == Player.Jump && (Owner as Character).ForceY > -7)
				anim.SetAnimation(Player.MidAir);
		}
		public override function OnKeyPress(code:int):void
		{
			var owner:Player = Owner as Player;
			if (!owner.IsMovingLeft && code == ControlCenter.PlayerWalkLeft)
				owner.BeginMoveLeft(false);
			else if (!owner.IsMovingRight && code == ControlCenter.PlayerWalkRight)
				owner.BeginMoveRight(false);
		}
		public override function OnKeyRelease(code:int):void
		{
			var owner:Player = Owner as Player;
			
			if (owner.IsMovingLeft && !ControlCenter.PressingKey(ControlCenter.PlayerWalkLeft))
				owner.StopMovingLeft(false);
			if (owner.IsMovingRight && !ControlCenter.PressingKey(ControlCenter.PlayerWalkRight))
				owner.StopMovingRight(false);
		}
		public override function OnActivating():void
		{
			var owner:Player = Owner as Player;
			owner.SetAnimation(Player.Jump);
			var effect:JumpingDustEffect = new JumpingDustEffect();
			effect.CenterX = Owner.CenterX;
			effect.Bottom = Owner.Bottom;
			GameFrame.AddChild(effect);
		}
		public override function OnDeactivated():void
		{
			
		}
	}
}