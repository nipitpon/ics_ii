package System.MC.States 
{
	import flash.events.Event;
	import System.ControlCenter;
	import System.Interfaces.ISkill;
	import System.MC.Characters.Character;
	import System.MC.Skills.Skill;
	/**
	 * ...
	 * @author Knight
	 */
	public class CharacterState_Skill extends CharacterState_Idle 
	{
		private var skill:ISkill = null;
		public function CharacterState_Skill(owner:Character) 
		{
			super(owner);
		}
		public function get CurrentSkill():ISkill { return skill; }
		public function SetSkill(skill:ISkill):void
		{
			if (this.skill != null)
				this.skill.removeEventListener(Skill.Deactivating, OnDeactivating);
			this.skill = skill;
			
			this.skill.addEventListener(Skill.Deactivating, OnDeactivating);
		}
		public override function OnActivating():void
		{
			var owner:Character = Owner as Character;
			if (owner.IsMovingLeft)
				owner.StopMovingLeft(false);
			if (owner.IsMovingRight)
				owner.StopMovingRight(false);
			skill.Activate();
		}
		public override function OnDeactivating(e:Event = null):void
		{
			var owner:Character = Owner as Character;
			owner.SetState(Character.Idle);
		}
		public override function OnDeactivated():void
		{
			super.OnDeactivated();
			if (!skill.IsDeactivated) skill.Deactivate();
		}
	}

}