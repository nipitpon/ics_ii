package System.MC.States 
{
	import flash.events.Event;
	import Resources.Animation;
	import System.GameFrame;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IMovable;
	import System.MC.Characters.Character;
	import System.MC.Characters.Monster;
	import System.MC.Characters.Player;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class PlayerState_DieFromMonster extends PlayerState_Idle 
	{
		
		public function PlayerState_DieFromMonster(owner:Player) 
		{
			super(owner);
		}
		public override function OnUpdate():void
		{
			super.OnUpdate();
			Owner.MoveByOffset(0, -1);
		}
		public override function OnActivating():void
		{
			(Owner as Character).StopMoving(false);
			(Owner.View as IAnimatable).SetAnimation(Player.DieAngle);
			Owner.View.addEventListener(Animation.FinishAnimation, OnDeactivating);
			(Owner as IMovable).ClearAllForce();
		}
		public override function OnDeactivating(e:Event = null):void
		{
			Main.ChangeToGamePage();
			(Owner.View as IAnimatable).Freeze = true;
		}
	}

}