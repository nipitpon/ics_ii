package System.MC.States 
{
	import System.Interfaces.IAnimatable;
	import System.MC.Characters.Character;
	import System.MC.Characters.Player;
	import System.Time;
	
	/**
	 * ...
	 * @author ss
	 */
	public class PlayerState_Sleep extends PlayerState_Idle 
	{
		
		public function PlayerState_Sleep(owner:Player) 
		{
			super(owner);
			canMove = false;
			canSkill = false;
			canToggleSkill = false;
			canProne = false;
			canJump = false;
			canDefaultAtk = false;
			canReload = false;
			canUseNoAnimSkill = false;
		}
		public override function OnUpdate():void
		{
			if (!Time.IsAdvancing)
				(Owner as Player).SetState(Character.Idle);
		}
		public override function OnActivating():void
		{
			(Owner as Player).TurnRight();
			var anim:IAnimatable = Owner.View as IAnimatable;
			anim.SetAnimation(Player.Sleep);
			(Owner as Player).ExtinguishFire();
		}
	}

}