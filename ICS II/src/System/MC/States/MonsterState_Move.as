package System.MC.States 
{
	import flash.events.Event;
	import System.Interfaces.IHaveCooldown;
	import System.Interfaces.ISkill;
	import System.MC.Characters.Character;
	import System.MC.Characters.Monster;
	/**
	 * ...
	 * @author Knight
	 */
	public class MonsterState_Move extends MonsterState_Idle 
	{
		private var count:uint = 0;
		public function MonsterState_Move(owner:Monster) 
		{
			super(owner);
		}
		public override function OnUpdate():void
		{
		}
		public override function OnActivating():void
		{
			var owner:Monster = Owner as Monster;
			//if (owner.IsTurningLeft) owner.Push(-owner.Speed, 0);
			//else owner.Push(owner.Speed, 0);
			
			count = 0;
			owner.SetAnimation(Character.Move);
		}
		public override function OnDeactivated():void
		{
			var owner:Monster = Owner as Monster;
			//if (owner.IsTurningLeft) owner.Push(owner.Speed, 0);
			//else owner.Push( -owner.Speed, 0);
			
			if (owner.Invoked)
			{
				if (owner.Target.CenterX > owner.CenterX && !owner.IsTurningRight) owner.TurnRight();
				else if (owner.Target.CenterX < owner.CenterX && !owner.IsTurningLeft) owner.TurnLeft();
			}
		}
	}

}