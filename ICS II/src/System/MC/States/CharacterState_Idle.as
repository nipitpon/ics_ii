package System.MC.States 
{
	import flash.events.Event;
	import System.Interfaces.IAnimatable;
	import System.MC.Characters.Character;
	/**
	 * ...
	 * @author Knight
	 */
	public class CharacterState_Idle extends ObjectState 
	{
		
		public function CharacterState_Idle(owner:Character) 
		{
			super(owner);
		}
		
		public function OnUpdate():void {}
		public override function OnActivating():void 
		{
			(Owner.View as IAnimatable).SetAnimation(Character.Idle);
		}
	}

}