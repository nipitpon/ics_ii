package System.MC.States 
{
	import flash.events.Event;
	import flash.geom.Point;
	import Resources.lib.Environment.BoxSprite;
	import System.ControlCenter;
	import System.GameSystem;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IMovable;
	import System.MC.Characters.Character;
	import System.MC.Characters.Player;
	import System.MC.Ladder.Ladder;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class PlayerState_ClimbLadder extends PlayerState_Idle 
	{
		private var current_ladder:Ladder;
		public function PlayerState_ClimbLadder(owner:Player) 
		{
			super(owner);
		}
		public function SetLadder(ladder:Ladder):void
		{
			current_ladder = ladder;
		}
		public override function OnUpdate():void
		{
			var owner:Player = Owner as Player;
			//if (owner.Collision.CenterY < current_ladder.Top ||
			//	owner.Collision.Bottom >= current_ladder.Bottom)
			if (GameSystem.GetIntersectedLadder(Owner.Center) != current_ladder)
			{
				if (ControlCenter.PressingKey(ControlCenter.PlayerWalkLeft))
					owner.BeginMoveLeft(true);
				else if (ControlCenter.PressingKey(ControlCenter.PlayerWalkRight))
					owner.BeginMoveRight(true);
				else if (owner.ForceY < 0)
				{
					owner.ClearForceY();
					owner.BeginJump( -15);
				}
				else
					owner.SetState(Character.Idle);
			}
			else
			{
				Owner.Center = new Point((Math.floor(Owner.CenterX / Tile.Size.x) + 0.5) * Tile.Size.x, Owner.CenterY);
			}
		}
		public override function OnKeyPress(code:int):void
		{
			var owner:Player = Owner as Player;
			owner.Freeze = false;
			if (code == ControlCenter.Drop || code == ControlCenter.PlayerClimb)
			{
				owner.ClearForceY();
			}
			if (code == ControlCenter.Drop)
			{
				owner.SetAnimation(Player.ClimbDownLadder);
				owner.Push(0, owner.Speed * 2);
			}
			else if (code == ControlCenter.PlayerClimb)
			{
				owner.SetAnimation(Player.ClimbUpLadder);
				owner.Push(0, -owner.Speed);
			}
			else if (code == ControlCenter.PlayerWalkLeft)
				owner.BeginMoveLeft(true);
			else if (code == ControlCenter.PlayerWalkRight)
				owner.BeginMoveRight(true);
			else if (code == ControlCenter.PlayerJump)
				owner.BeginJump();
		}
		public override function OnKeyRelease(code:int):void
		{
			var owner:Player = Owner as Player;
			if (code == ControlCenter.Drop || code == ControlCenter.PlayerClimb)
			{
				(Owner as IMovable).ClearForceY();
				if (ControlCenter.PressingKey(ControlCenter.PlayerWalkLeft))
					owner.BeginMoveLeft(true);
				else if (ControlCenter.PressingKey(ControlCenter.PlayerWalkRight))
					owner.BeginMoveRight(true);
				else
					(Owner as IAnimatable).Freeze = true;
				(Owner as IAnimatable).SetAnimation(Player.ClimbUpLadder);
				
			}
			if (code == ControlCenter.PlayerWalkLeft || code == ControlCenter.PlayerWalkRight)
			{
				(Owner as IAnimatable).Freeze = !ControlCenter.PressingKey(ControlCenter.Drop) && !ControlCenter.PressingKey(ControlCenter.PlayerClimb);
				
				(Owner as IMovable).ClearForceY();
				if (ControlCenter.PressingKey(ControlCenter.Drop))
				{
					(Owner as IAnimatable).SetAnimation(Player.ClimbDownLadder);
					(Owner as IMovable).Push(0, owner.Speed * 2);
				}
				else if (ControlCenter.PressingKey(ControlCenter.PlayerClimb))
				{
					(Owner as IMovable).Push(0, -owner.Speed);
					(Owner as IAnimatable).SetAnimation(Player.ClimbUpLadder);
				}
			}
		}
		public override function OnActivating():void
		{
			var owner:Player = Owner as Player;
			super.OnActivating();
			//Owner.Center = new Point(current_ladder.CenterX, owner.CenterY);
			Owner.Center = new Point((Math.floor(Owner.CenterX / Tile.Size.x) + 0.5) * Tile.Size.x, Owner.CenterY);
			
			owner.ClearAllForce();
			if (ControlCenter.PressingKey(ControlCenter.PlayerClimb))
			{
				owner.Push(0, -owner.Speed);
				owner.SetAnimation(Player.ClimbUpLadder);
			}
			else if (ControlCenter.PressingKey(ControlCenter.Drop))
			{
				owner.Push(0, owner.Speed * 2);
				owner.SetAnimation(Player.ClimbDownLadder);
			}
			owner.TurnRight();
		}
		public override function OnDeactivated():void
		{
			super.OnDeactivated();
			var owner:Player = Owner as Player;
			
			if((Owner as IAnimatable).Freeze)
				(Owner as IAnimatable).Freeze = false;
			
			if (!owner.IsDying)
			{
				if (ControlCenter.PressingKey(ControlCenter.PlayerWalkLeft) && !owner.IsMovingLeft)
					owner.BeginMoveLeft(false);
				else if (ControlCenter.PressingKey(ControlCenter.PlayerWalkRight) && !owner.IsMovingRight)
					owner.BeginMoveRight(false);
			}
		}
	}

}