package System.MC.States 
{
	import flash.events.Event;
	import Resources.AnimationEvent;
	import System.Interfaces.IAnimatable;
	import System.MC.Characters.Character;
	import System.MC.Characters.Player;
	import System.ControlCenter;
	import System.SoundCenter;
	/**
	 * ...
	 * @author Knight
	 */
	public class PlayerState_Move extends PlayerState_Idle 
	{
		
		public function PlayerState_Move(owner:Player) 
		{
			super(owner);
			canMove = false;
			canSkill = true;
			canToggleSkill = true;
			canJump = true;
			canProne = true;
			canDefaultAtk = true;
			canReload = true;
		}
		public override function OnKeyPress(code:int):void
		{
		}
		public override function OnKeyRelease(code:int):void
		{
		}
		public override function OnActivating():void
		{
			var owner:Player = Owner as Player;
			//var anim:uint = Player_LowerPart.Move;
			//if (owner.IsReloading) anim |= Player_UpperPart.Reload;
			//else if (owner.IsUsingSkill) anim |= (owner.View as IAnimatable).CurrentAnimation & 0x00ff00;
			//else anim |= Player_UpperPart.Move;
			owner.SetAnimation(Character.Move);
		}
	}

}