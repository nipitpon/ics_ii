package System.MC.States 
{
	import flash.events.Event;
	import Resources.Animation;
	import System.GameFrame;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IMovable;
	import System.MC.Characters.Monster;
	/**
	 * ...
	 * @author Knight
	 */
	public class MonsterState_Die extends CharacterState_Idle 
	{
		
		public function MonsterState_Die(owner:Monster) 
		{
			super(owner);
		}
		public override function OnActivating():void
		{
			(Owner.View as IAnimatable).SetAnimation(Monster.Die);
			Owner.View.addEventListener(Animation.FinishAnimation, OnDeactivating);
			(Owner as IMovable).ClearAllForce();
		}
		public override function OnDeactivating(e:Event = null):void
		{
			Owner.View.removeEventListener(Animation.FinishAnimation, OnDeactivating);
			if(GameFrame.ContainChild(Owner))
				GameFrame.RemoveChild(Owner);
		}
	}

}