package System.MC.States 
{
	import flash.events.Event;
	import flash.geom.Point;
	import flash.utils.getTimer;
	import Resources.Animation;
	import System.ControlCenter;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.ICollidable;
	import System.MC.Characters.Character;
	import System.MC.Characters.Player;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.MC.etc.ForceStruct;
	import System.MC.Tile;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class PlayerState_Prone extends PlayerState_Idle 
	{
		public function PlayerState_Prone(owner:Player) 
		{
			super(owner);
			canMove = true;
			canSkill = true;
			canToggleSkill = true;
			canJump = true;
			canProne = false;
			canDefaultAtk = false;
			canReload = false;
		}
		public override function OnUpdate():void
		{
			super.OnUpdate();
			if (!ControlCenter.PressingKey(ControlCenter.Drop))
			{
				var col:CollisionBox = (Owner as Player).Collision;
				var box:CollisionBox = new CollisionRectangle(col.Left, col.Top - 22, 22, 39);
				var collide:Vector.<ICollidable> = GameSystem.GetIntersectedCollidable(box, null);
				var found:Boolean = false;
				for each(var c:ICollidable in collide)
				{
					if (c is Tile)
					{
						found = true;
						break;
					}
				}
				if ((Owner as Player).CanStand())
				{
					if (ControlCenter.PressingKey(ControlCenter.PlayerWalkLeft))
						(Owner as Player).BeginMoveLeft(true);
					else if (ControlCenter.PressingKey(ControlCenter.PlayerWalkRight))
						(Owner as Player).BeginMoveRight(true);
					else
						(Owner as Player).SetState(Character.Idle); 
				}
			}
		}
		public override function OnKeyRelease(code:int):void
		{
			
		}
		public override function OnActivating():void
		{
			var owner:Player = Owner as Player;
			owner.SetAnimation(Player.Prone);
			owner.Collision.Adjust(0, 25, 22, 20);
			
			if (owner.IsMoving) owner.StopMoving(false);
			owner.View.addEventListener(Animation.FinishAnimation, onFinishAnimation);
		}
		public override function OnDeactivated():void
		{
			var owner:Player = Owner as Player;
			owner.Collision.Adjust(0, -25, 22, 45);
			owner.View.removeEventListener(Animation.FinishAnimation, onFinishAnimation);
			(owner.View as IAnimatable).Freeze = false;
		}
		private function onFinishAnimation(e:Event):void
		{
			(Owner.View as IAnimatable).Freeze = true;
		}
	}
}