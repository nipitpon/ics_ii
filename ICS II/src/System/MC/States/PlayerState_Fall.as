package System.MC.States 
{
	import flash.events.Event;
	import System.Interfaces.IAnimatable;
	import System.MC.Characters.Character;
	import System.MC.Characters.Player;
	import System.ControlCenter;
	/**
	 * ...
	 * @author Knight
	 */
	public class PlayerState_Fall extends PlayerState_Idle 
	{
		
		public function PlayerState_Fall(owner:Player) 
		{
			super(owner);
			canMove = false;
			canSkill = true;
			canToggleSkill = true;
			canJump = false;
			canProne = false;
			canDefaultAtk = true;
			canReload = true;
		}
		public override function OnUpdate():void
		{
			super.OnUpdate();
			
			var anim:IAnimatable = Owner.View as IAnimatable;
			if (anim.CurrentAnimation == Player.MidAir && (Owner as Character).ForceY > 7)
				anim.SetAnimation(Player.Fall);
		}
		public override function OnKeyPress(code:int):void
		{
			var owner:Player = Owner as Player;
			if (!owner.IsMovingLeft && code == ControlCenter.PlayerWalkLeft)
				owner.BeginMoveLeft(false);
			else if (!owner.IsMovingRight && code == ControlCenter.PlayerWalkRight)
				owner.BeginMoveRight(false);
		}
		public override function OnKeyRelease(code:int):void
		{
			var owner:Player = Owner as Player;
			
			if (owner.IsMovingLeft && !ControlCenter.PressingKey(ControlCenter.PlayerWalkLeft))
				owner.StopMovingLeft(false);
			if (owner.IsMovingRight && !ControlCenter.PressingKey(ControlCenter.PlayerWalkRight))
				owner.StopMovingRight(false);
		}
		public override function OnDeactivating(e:Event = null):void
		{
		}
		public override function OnActivating():void
		{
			var owner:Player = Owner as Player;
			//var anim:uint = Player_LowerPart.Jump;
			//if (owner.IsReloading) anim |= Player_UpperPart.Reload;
			//else if (owner.IsUsingSkill) anim |= (owner.View as IAnimatable).CurrentAnimation & 0x00ff00;
			//else anim |= Player_UpperPart.Idle;
			
			owner.SetAnimation(Player.MidAir);
		}
		public override function OnDeactivated():void
		{
			var owner:Player = Owner as Player;
			
			if (!ControlCenter.PressingKey(ControlCenter.Drop))
			{
				if (owner.IsMovingLeft && !ControlCenter.PressingKey(ControlCenter.PlayerWalkLeft))
					owner.StopMovingLeft(false);
				if (owner.IsMovingRight && !ControlCenter.PressingKey(ControlCenter.PlayerWalkRight))
					owner.StopMovingRight(false);
			}
		}
	}
}