package System.MC.States 
{
	import System.Interfaces.IAnimatable;
	import System.MC.Characters.Character;
	/**
	 * ...
	 * @author Knight
	 */
	public class CharacterState_KO extends CharacterState_Idle
	{
		private var startCount:Boolean = false;
		private var count:uint = 0;
		public function CharacterState_KO(owner:Character) 
		{
			super(owner);
		}
		public override function OnUpdate():void
		{
			if ((Owner as Character).ForceX == 0) 
				startCount = true;
			else
			{
				startCount = false;
				count = 0;
			}
			
			if (startCount)
			{
				if(++count == 5)
					(Owner as Character).SetState(Character.Idle);
			}
		}
		public override function OnActivating():void 
		{
			var owner:Character = Owner as Character;
			owner.ClearForceX();
			if (owner.IsTurningLeft) 
				owner.PushWithDecay(10, 0, 0.5);
			else if (owner.IsTurningRight) 
				owner.PushWithDecay( -10, 0, 0.5);
			(Owner.View as IAnimatable).SetAnimation(Character.KO);
		}
		public override function OnDeactivated():void
		{
			startCount = false;
			count = 0;
			(Owner.View as IAnimatable).SetAnimation(Character.Idle);
		}
	}
}