package System.MC.States 
{
	import flash.events.Event;
	import Resources.Animation;
	import Resources.AnimationEvent;
	import System.Interfaces.IAnimatable;
	import System.MC.Characters.Character;
	import System.MC.Characters.Player;
	import System.SoundCenter;
	/**
	 * ...
	 * @author Knight
	 */
	public class PlayerState_Reload extends PlayerState_Idle 
	{
		private var reloading:Boolean = false;
		public function PlayerState_Reload(owner:Player) 
		{
			super(owner);
			canMove = true;
			canSkill = false;
			canToggleSkill = true;
			canJump = true;
			canProne = false;
			canDefaultAtk = false;
			canReload = false;
		}
		public override function OnActivating():void
		{
			var owner:Player = Owner as Player;
			reloading = true;
			owner.SetAnimation(Character.Idle);
			(owner.View).addEventListener(Animation.FinishAnimation, OnDeactivating);
			(owner.View).addEventListener(AnimationEvent.Progress, onReloadProgress);
		}
		public override function OnDeactivating(e:Event = null):void
		{
			var owner:Player = Owner as Player;
			(owner.View).removeEventListener(Animation.FinishAnimation, OnDeactivating);
			(owner.View).removeEventListener(AnimationEvent.Progress, onReloadProgress);
			reloading = false;
			//There's event back when animation is finished accordingly ONLY.
			(Owner as Player).FinishReloading(e != null);
		}
		public function get IsReloading():Boolean { return reloading; }
		private function onReloadProgress(e:AnimationEvent):void
		{
			switch(e.Position)
			{
				case 14: (Owner as Player).RefillMagazine(); break;
			}
		}
	}
}