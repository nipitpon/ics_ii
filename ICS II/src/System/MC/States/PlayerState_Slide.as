package System.MC.States 
{
	import _Base.MySound;
	import flash.events.Event;
	import HardCode.Effects.SlidingDustParticle;
	import Resources.lib.Sound.SFX.SlideSound;
	import System.ControlCenter;
	import System.GameFrame;
	import System.Interfaces.ICachable;
	import System.Interfaces.ICacher;
	import System.MC.Characters.Character;
	import System.MC.Characters.Player;
	import System.SoundCenter;
	
	/**
	 * ...
	 * @author ss
	 */
	public class PlayerState_Slide extends PlayerState_Idle implements ICacher
	{
		private var cache:Vector.<ICachable>;
		public function PlayerState_Slide(owner:Player) 
		{
			super(owner);
			canMove = false;
			canSkill = false;
			canToggleSkill = false;
			canProne = false;
			canJump = false;
			canDefaultAtk = false;
			canReload = false;
			canUseNoAnimSkill = false;
			cache = new Vector.<ICachable>();
		}
		public function Cache(obj:ICachable):void
		{
			if (cache.indexOf(obj) < 0)
				cache.push(obj);
		}
		public override function OnUpdate():void 
		{
			super.OnUpdate();
			var player:Player = Owner as Player;
			if (Math.abs(player.ForceX) < 0.05)
			{
				if (player.CanStand())
				{
					if (ControlCenter.PressingKey(ControlCenter.PlayerWalkLeft))
						player.BeginMoveLeft(true);
					else if (ControlCenter.PressingKey(ControlCenter.PlayerWalkRight))
						player.BeginMoveRight(true);
					else if(ControlCenter.PressingKey(ControlCenter.Drop))
						player.SetState(Player.Prone);
					else 
						player.SetState(Character.Idle);
				}
				else
				{
					player.SetState(Player.Prone);
				}
			}
			else
			{
				var particle:ICachable = null;
				if (cache.length > 0)
					particle = cache.shift();
				else
					particle = new SlidingDustParticle(this);
				particle.View.scaleX = 1;
				(particle as SlidingDustParticle).CenterX = Owner.CenterX;
				(particle as SlidingDustParticle).Bottom = (Owner as Player).Collision.Bottom;
				particle.View.scaleX = Owner.View.scaleX;
				GameFrame.AddChild(particle);
			}
		}
		public override function OnDeactivated():void
		{
			super.OnDeactivated();
			var owner:Player = Owner as Player;
			owner.Collision.Adjust(0, -25, 22, 45);
		}
		public override function OnActivating():void
		{
			var owner:Player = Owner as Player;
			owner.SetAnimation(Player.Slide);
			owner.Collision.Adjust(0, 25, 22, 20);
			SoundCenter.PlaySFXFromInstance(new SlideSound());
			if (owner.IsTurningLeft)
			{
				owner.PushWithDecay(-30, 0, 0.7);
			}
			else
			{
				owner.PushWithDecay( 30, 0, 0.7);
			}
		}
	}
}