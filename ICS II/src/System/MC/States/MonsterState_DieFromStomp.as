package System.MC.States 
{
	import flash.events.Event;
	import System.GameFrame;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IMovable;
	import System.MC.Characters.Monster;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class MonsterState_DieFromStomp extends MonsterState_Idle 
	{
		
		public function MonsterState_DieFromStomp(owner:Monster) 
		{
			super(owner);
			
		}
		public override function OnUpdate():void
		{
			if (Owner.CenterY > 700) OnDeactivating();
		}
		public override function OnActivating():void
		{
			(Owner.View as IAnimatable).Freeze = true;
			(Owner as IMovable).ClearAllForce();
			(Owner as IMovable).Push(0, -20);
			Owner.View.scaleY *= -1;
		}
		public override function OnDeactivating(e:Event = null):void
		{
			GameFrame.RemoveChild(Owner);
		}
	}

}