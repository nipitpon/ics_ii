package System.MC.States 
{
	import System.MC.Characters.Character;
	import System.MC.Characters.Monster;
	/**
	 * ...
	 * @author Knight
	 */
	public class MonsterState_Idle extends CharacterState_Idle 
	{
		
		public function MonsterState_Idle(owner:Monster) 
		{
			super(owner);
		}
		public override function OnUpdate():void
		{
			var mon:Monster = Owner as Monster;
			
			//if(Utility.Random(0, 100) >= 50)
			//	mon.BeginMoveRight(true);
			//else
			mon.BeginMoveLeft(true);
		}
	}

}