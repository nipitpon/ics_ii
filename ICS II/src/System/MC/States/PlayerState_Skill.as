package System.MC.States 
{
	import flash.events.Event;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.ISkill;
	import System.MC.Characters.Character;
	import System.MC.Characters.Player;
	import System.ControlCenter;
	import System.MC.Skills.Skill;
	/**
	 * ...
	 * @author Knight
	 */
	public class PlayerState_Skill extends PlayerState_Idle
	{
		private var skill:ISkill = null;
		public function PlayerState_Skill(owner:Player) 
		{
			super(owner);
			canMove = false;
			canSkill = false;
			canToggleSkill = true;
			canJump = false;
			canProne = false;
			canDefaultAtk = false;
			canReload = false;
		}
		public function SetSkill(skill:ISkill):void
		{
			if (this.skill != null)
				this.skill.removeEventListener(Skill.Deactivating, OnDeactivating);
			this.skill = skill;
			
			this.skill.addEventListener(Skill.Deactivating, OnDeactivating);
		}
		public function get CurrentSkill():ISkill { return skill; }
		public function get IsActivating():Boolean { return CurrentSkill != null; }
		public override function OnKeyPress(code:int):void
		{
			
		}
		public override function OnActivating():void
		{
			var owner:Player = Owner as Player;
			if (!skill.AllowMovement || !owner.AllowMoveAndShoot)
			{
				if (owner.IsMoving) owner.StopMoving(false);
			}
			skill.Activate();
			canMove = skill.AllowMovement;
		}
		public override function OnDeactivating(e:Event = null):void
		{
			skill.removeEventListener(Skill.Deactivating, OnDeactivating);
			skill = null;
			var owner:Player = Owner as Player;
			if (ControlCenter.PressingKey(ControlCenter.PlayerWalkLeft))
			{
				(owner.View as IAnimatable).SetAnimation(Character.Move);
				owner.BeginMoveLeft(true);
			}
			else if (ControlCenter.PressingKey(ControlCenter.PlayerWalkRight))
			{
				(owner.View as IAnimatable).SetAnimation(Character.Move);
				owner.BeginMoveRight(true);
			}
			else if (ControlCenter.PressingKey(ControlCenter.Drop))
			{
				owner.SetState(Player.Prone);
			}
			else
			{
				(owner.View as IAnimatable).SetAnimation(Character.Idle);
				//It won't change animation if the state is already idle.
				owner.SetState(Character.Idle);
			}
		}
	}
}