package System.MC.States 
{
	import flash.events.Event;
	import Resources.Animation;
	import System.ControlCenter;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IStatusHolder;
	import System.MC.Characters.Character;
	import System.MC.Characters.Player;
	/**
	 * ...
	 * @author Knight
	 */
	public class PlayerState_Idle extends CharacterState_Idle
	{
		protected var canMove:Boolean;
		protected var canSkill:Boolean;
		protected var canToggleSkill:Boolean;
		protected var canProne:Boolean;
		protected var canJump:Boolean;
		protected var canDefaultAtk:Boolean;
		protected var canReload:Boolean;
		protected var canUseNoAnimSkill:Boolean;
		public function PlayerState_Idle(owner:Player) 
		{
			super(owner);
			canMove = true;
			canSkill = true;
			canToggleSkill = true;
			canProne = true;
			canJump = true;
			canDefaultAtk = true;
			canReload = true;
			canUseNoAnimSkill = true;
		}
		public override function OnUpdate():void
		{
		}
		public function get CanMove():Boolean { return canMove; }
		public function get CanUseActiveSkill():Boolean { return canSkill; }
		public function get CanUseToggleSkill():Boolean { return canToggleSkill; }
		public function get CanProne():Boolean { return canProne && !(Owner as Player).IsReloading; }
		public function get CanJump():Boolean { return canJump; }
		public function get CanUseDefaultAttack():Boolean { return canDefaultAtk; }
		public function get CanReload():Boolean { return canReload && !(Owner as Player).IsReloading; }
		public function get CanUseNoAnimationSkill():Boolean { return canUseNoAnimSkill; }
		public function OnKeyPress(code:int):void
		{
		}
		public function OnKeyRelease(code:int):void
		{
			
		}
		public override function OnDeactivating(e:Event = null):void
		{
			
		}
		public override function OnActivating():void
		{
			var owner:Player = Owner as Player;
			owner.SetAnimation(Character.Idle);
		}
	}
}