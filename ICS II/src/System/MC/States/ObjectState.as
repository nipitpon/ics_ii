package System.MC.States 
{
	import flash.events.Event;
	import System.Interfaces.IMVC;
	/**
	 * ...
	 * @author Knight
	 */
	public class ObjectState 
	{
		private var owner:IMVC = null;
		public function ObjectState(owner:IMVC)
		{
			this.owner = owner;
		}
		public function OnActivating():void 
		{}
		public function OnActivated():void
		{}
		public function OnDeactivating(e:Event = null):void
		{}
		public function OnDeactivated():void
		{}
		public function get Owner():IMVC { return owner; }
	}
}