package System.MC.States 
{
	import flash.events.Event;
	import HardCode.Collidable.Wall;
	import Resources.lib.Sound.SFX.TurretMissileRelease;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.ICollidable;
	import System.MC.Characters.Character;
	import System.MC.Characters.Monster;
	import System.SoundCenter;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class WallState_Move extends CharacterState_Idle 
	{
		
		public function WallState_Move(owner:Character) 
		{
			super(owner);
			
		}
		public override function OnUpdate():void
		{
			(Owner as Wall).Push(0, -GameSystem.Gravity);
			if (Owner.CenterY < -1000)
				OnDeactivating();
		}
		public override function OnActivating():void
		{
			(Owner as Wall).SetAnimation(Character.Move);
			SoundCenter.PlaySFXFromInstance(new TurretMissileRelease());
		}
		public override function OnDeactivating(e:Event = null):void
		{
			GameFrame.RemoveChild(Owner);
			var col:Vector.<ICollidable> = GameSystem.GetCollidables();
			for each(var c:ICollidable in col)
			{
				if (c is Monster)
				{
					var mon:Monster = c as Monster;
					if (mon.Invoked && mon.Target == Owner)
					{
						mon.Invoke(GameSystem.PlayerInstance);
					}
				}
			}
		}
	}

}