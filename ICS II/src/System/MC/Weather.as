package System.MC 
{
	import flash.events.Event;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.IUpdatable;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Weather extends ModelViewController
	{
		
		public function Weather(view:RenderableObject) 
		{
			super(view);
		}
		public function get AmbientStrength():Number { return 1; }
		public function Start():void
		{
			GameFrame.AddChild(this);
			if (!GameSystem.IsHavingBossBattle && !GameSystem.HasLevelBGM && BackgroundMusic != null)
				SoundCenter.PlayBGM(BackgroundMusic);
		}
		public function Stop():void
		{
			GameFrame.RemoveChild(this);
			//New weather will stop previous weather's bgm itself.
			//if (!GameSystem.IsHavingBossBattle && !GameSystem.HasLevelBGM && BackgroundMusic != null)
			//	SoundCenter.StopBGM();
		}
		public function Update():void
		{
			
		}
		public function get BackgroundMusic():Class { return null; }
	}

}