package System.MC.Buttons 
{
	import flash.display.Shape;
	import System.MC.Collision.CollisionBox;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FrameButton extends Button 
	{
		private var shape:Shape;
		public function FrameButton(view:RenderableObject, collisionBox:CollisionBox) 
		{
			super(view, collisionBox);
			shape = new Shape();
			View.addChild(shape);
		}
		protected override function OnMouseOver():void
		{
			super.OnMouseOver();
			shape.graphics.clear();
			shape.graphics.lineStyle(2, 0);
			shape.graphics.drawRect(Collision.Left - View.x - View.parent.x, Collision.Top - View.Top - View.parent.y, Collision.Width, Collision.Height);
		}
		protected override function OnMouseDown():void
		{
			super.OnMouseDown();
			shape.graphics.clear();
			shape.graphics.lineStyle(3, 0);
			shape.graphics.drawRect(Collision.Left - View.x - View.parent.x, Collision.Top - View.Top - View.parent.y, Collision.Width, Collision.Height);
		}
		protected override function OnMouseLeave():void
		{
			super.OnMouseLeave();
			shape.graphics.clear();
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			shape.graphics.clear();
			shape.graphics.lineStyle(2, 0);
			shape.graphics.drawRect(Collision.Left - View.x - View.parent.x, Collision.Top - View.Top - View.parent.y, Collision.Width, Collision.Height);
		}
		
	}

}