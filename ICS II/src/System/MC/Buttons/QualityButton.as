package System.MC.Buttons 
{
	import flash.display.StageQuality;
	import flash.events.Event;
	import HardCode.Pages.Game.PauseMenu;
	import HardCode.Pages.Game.PauseMenuButton;
	import Resources.lib.GUI.QualityButtonImage;
	import System.GamePage;
	import System.GUI;
	import System.Interfaces.IDescriptor;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class QualityButton extends PauseMenuButton 
	{
		private var image:QualityButtonImage;
		public function QualityButton() 
		{
			super(image = new QualityButtonImage(), "QUALITY: HIGH");
			View.addEventListener(Event.ADDED_TO_STAGE, function(e:Event):void
			{
				switch(image.stage.quality.toLowerCase())
				{
					case StageQuality.HIGH: 
						image.SetToHigh();
						SetText("QUALITY: HIGH");
						break;
					case StageQuality.MEDIUM:
						image.SetToMedium();
						SetText("QUALITY: MEDIUM");
						break;
					case StageQuality.LOW:
						image.SetToLow();
						SetText("QUALITY: LOW");
						break;
				}
			});
		}
		public override function get IsReceivingControl():Boolean { return super.IsReceivingControl && (!GamePage.IsShowingPopup || GamePage.IsShowingMainSettings); }
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			switch(image.stage.quality.toLowerCase())
			{
				case StageQuality.HIGH: 
					image.SetToLow();
					image.stage.quality = StageQuality.LOW;
					SetText("QUALITY: LOW");
					break;
				case StageQuality.MEDIUM:
					image.SetToHigh();
					image.stage.quality = StageQuality.HIGH;
					SetText("QUALITY: HIGH");
					break;
				case StageQuality.LOW:
					image.SetToMedium();
					image.stage.quality = StageQuality.MEDIUM;
					SetText("QUALITY: MEDIUM");
					break;
				default: throw new Error("Unknown quality: " + image.stage.quality.toLowerCase()); break;
			}
		}
		
	}

}