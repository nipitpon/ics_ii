package System.MC.Buttons 
{
	import flash.events.Event;
	import flash.geom.Point;
	import System.GameFrame;
	import System.GamePage;
	import System.GUI;
	import System.MC.Collision.CollisionBox;
	import System.V.RenderableObject;
	/**
	 * ...
	 * @author Knight
	 */
	public class GUIButton extends BrightnessButton 
	{
		public function GUIButton(view:RenderableObject, collisionBox:CollisionBox, over:int = 96, click:int = -96) 
		{
			super(view, collisionBox, over, click);
			View.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		public override function get Active():Boolean { return GUI.IsShowingPopup; }
		public override function get IsReceivingControl():Boolean { return super.IsReceivingControl && !GamePage.IsShowingPopup && !GamePage.IsFadingOut; }
		private function onAddedToStage(e:Event):void
		{
			View.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			var globalPoint:Point = View.localToGlobal(new Point());
			Collision.Top = globalPoint.y;
			Collision.Left = globalPoint.x;
		}
	}

}