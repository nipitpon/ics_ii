package System.MC.Buttons 
{
	import flash.events.Event;
	import HardCode.Pages.Game.PauseMenu;
	import HardCode.Pages.Game.PauseMenuButton;
	import Resources.lib.GUI.MusicButtonImage;
	import System.GamePage;
	import System.GUI;
	import System.Interfaces.IDescriptor;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.SoundCenter;
	/**
	 * ...
	 * @author Knight
	 */
	public class MusicButton extends PauseMenuButton 
	{
		private var musicButton:MusicButtonImage;
		public function MusicButton() 
		{
			super((musicButton = new MusicButtonImage()), "MUSIC: ON");
			View.addEventListener(Event.ADDED_TO_STAGE, function(e:Event):void
			{
				if (SoundCenter.IsMutingBGM)
					DisallowMusic();
				else
					AllowMusic();
			});
		}
		public function DisallowMusic():void
		{
			SetText("MUSIC: OFF");
		}
		public function AllowMusic():void
		{
			SetText("MUSIC: ON");
		}
		public override function get IsReceivingControl():Boolean { return super.IsReceivingControl && (!GamePage.IsShowingPopup || GamePage.IsShowingMainSettings); }
		public function get IsAllowingMusic():Boolean { return !SoundCenter.IsMutingBGM; }
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			if (!SoundCenter.IsMutingBGM)
			{
				SoundCenter.MuteBGM();
				SetText("MUSIC: OFF");
			}
			else
			{
				SoundCenter.UnmuteBGM();
				SetText("MUSIC: ON");
			}
		}
	}

}