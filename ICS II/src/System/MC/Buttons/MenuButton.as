package System.MC.Buttons 
{
	import Resources.lib.GUI.MenuButtonImage;
	import System.GamePage;
	import System.GameSystem;
	import System.GUI;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	/**
	 * ...
	 * @author Knight
	 */
	public class MenuButton extends BrightnessButton 
	{
		
		public function MenuButton() 
		{
			super(new MenuButtonImage(), new CollisionRectangle(0, 0, 60, 60, CollisionBox.IntersectDirection_All), 96, -96);
		}
		protected override function OnMouseOver():void
		{
			super.OnMouseOver();
			var p:String = "Pause";
			if (GUI.IsShowingPauseMenu)
				p = "Resume";
			GUI.ShowDescriptionAsHtml(CenterX, CenterY, p + " the game");
		}
		public override function get IsReceivingControl():Boolean { return super.IsReceivingControl && !GamePage.IsShowingPopup; }
		protected override function OnMouseLeave():void
		{
			super.OnMouseLeave();
			GUI.HideDescription();
		}
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			if (!GUI.IsShowingPauseMenu)
			{
				GUI.ShowPauseMenu();
			}
			else
			{
				GUI.HidePauseMenu();
			}
			super.OnMouseLeave();
		}
	}

}