package System.MC.Buttons 
{
	import flash.events.Event;
	import HardCode.Pages.Game.PauseMenu;
	import HardCode.Pages.Game.PauseMenuButton;
	import Resources.lib.GUI.SoundButtonImage;
	import System.GamePage;
	import System.GUI;
	import System.Interfaces.IDescriptor;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionRectangle;
	import System.SoundCenter;
	/**
	 * ...
	 * @author Knight
	 */
	public class SoundButton extends PauseMenuButton 
	{
		private var soundButton:SoundButtonImage
		public function SoundButton() 
		{
			super((soundButton = new SoundButtonImage()), "SOUND: ON");
			View.addEventListener(Event.ADDED_TO_STAGE, function(e:Event):void
			{
				if (SoundCenter.IsMutingSFX)
					SetText("SOUND: OFF");
				else
					SetText("SOUND: ON");
			});
		}
		public function DisallowSound():void
		{
			SetText("SOUND: OFF");
		}
		public function AllowSound():void
		{
			SetText("SOUND: ON");
		}
		public override function get IsReceivingControl():Boolean { return super.IsReceivingControl && (!GamePage.IsShowingPopup || GamePage.IsShowingMainSettings); }
		public function get IsAllowingSound():Boolean { return !SoundCenter.IsMutingSFX; }
		protected override function OnMouseClick():void
		{
			super.OnMouseClick();
			if (!SoundCenter.IsMutingSFX)
			{
				SetText("SOUND: OFF");
				SoundCenter.MuteSFX();
			}
			else
			{
				SetText("SOUND: ON");
				SoundCenter.UnmuteSFX();
			}
		}
		
	}

}