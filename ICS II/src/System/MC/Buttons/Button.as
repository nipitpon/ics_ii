package System.MC.Buttons 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Point;
	import Resources.lib.Sound.SFX.ButtonClick;
	import Resources.lib.Sound.SFX.ButtonMouseOver;
	import System.ControlCenter;
	import System.Interfaces.IReceiveControl;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionBox;
	import System.SoundCenter;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Button extends CollidableObject implements IReceiveControl 
	{
		private var last_mouse:Point;
		private static const movement:Point = new Point();
		private var pressing:Boolean = false;
		private var mouse_over:Boolean = false;
		private var enabled:Boolean;
		private static var to_enter:Button = null;
		protected var priority:int;
		protected var on_mouse_over_sound:Class;
		protected var on_mouse_click_sound:Class;
		protected static var current_focus:Button = null;
		
		public function Button(view:RenderableObject, collisionBox:CollisionBox) 
		{
			priority = 0;
			super(view, collisionBox);
			last_mouse = new Point();
			on_mouse_over_sound = ButtonMouseOver;
			on_mouse_click_sound = ButtonClick;
			Enabled = true;
		}
		public function set Enabled(val:Boolean):void
		{
			enabled = val;
			if (val)
			{
				View.alpha = 1;
			}
			else
			{
				if (HasFocus)
				{
					pressing = false;
					ClearFocus();
				}
				View.alpha = 0.5;
			}
		}
		public function get Enabled():Boolean { return enabled; }
		public function KeyPress(id:int):void 
		{
			if (HasFocus && id == ControlCenter.ActivateButton)
			{
				OnMouseDown();
				pressing = true;
			}
		}
		public function KeyRelease(id:int):void
		{
			if (HasFocus && id == ControlCenter.ActivateButton)
			{
				OnMouseUp();
				if(pressing)
					OnMouseClick();
				pressing = false;
			}
		}
		public function MousePress(x:Number, y:Number):void
		{
			if (Enabled && Collision.IntersectsPoint(new Point(x, y), movement))
			{
				OnMouseDown();
				pressing = true;
			}
		}
		public function MouseMove(x:Number, y:Number):void
		{
			if (!Enabled) return;
			
			var point:Point = new Point(x, y);
			movement.x = last_mouse.x - x;
			movement.y = last_mouse.y - y;
			if (Collision.IntersectsPoint(last_mouse, movement))
			{
				if(to_enter == null || to_enter.priority <= priority)
					to_enter = this;
			}
			last_mouse.x = x;
			last_mouse.y = y;
		}
		public function MouseRelease(x:Number, y:Number):void
		{
			if (Collision.IntersectsPoint(new Point(x, y), movement))
			{
				OnMouseUp();
				if (!Button.HasFocus || !current_focus.IsReceivingControl)
					current_focus = this;
				if (pressing && HasFocus)
					OnMouseClick();
			}
			pressing = false;
		}
		public function get IsReceivingControl():Boolean { return IsVisible(View) && Enabled; }
		public function get HasFocus():Boolean { return current_focus == this; }
		public function get IsMouseOver():Boolean { return mouse_over; }
		protected function OnMouseOver():void
		{
			mouse_over = true;
			if(on_mouse_over_sound != null)
				SoundCenter.PlaySFXFromInstance(new on_mouse_over_sound());
		}
		protected function OnMouseLeave():void
		{
			mouse_over = false;
		}
		protected function OnMouseClick():void
		{
			if (HasFocus)
			{
				if(on_mouse_click_sound != null)
					SoundCenter.PlaySFXFromInstance(new on_mouse_click_sound());
			}
		}
		protected function OnMouseUp():void
		{}
		protected function OnMouseDown():void
		{}
		private function IsVisible(obj:DisplayObject):Boolean
		{
			if (!obj.visible) return false;
			else if (obj.parent == null || obj == View.root) return true;
			else return IsVisible(obj.parent);
		}
		
		public static function get HasFocus():Boolean 
		{ 
			if (current_focus != null && !current_focus.IsVisible(current_focus.View))
			{
				current_focus = null;
			} 
			return current_focus != null;
		}
		public static function ClearFocus():void 
		{
			if (current_focus != null)
			{
				current_focus.OnMouseLeave();
				current_focus.last_mouse = new Point();
			}
			current_focus = null; 
		}
		public static function SetFocus(button:Button):void
		{
			if (button == current_focus) return;
			
			if (current_focus != null)
			{
				if(Utility.IsObjectVisible(current_focus.View))
					current_focus.OnMouseLeave();
				current_focus.last_mouse = new Point();
			}
			current_focus = button;
			if(current_focus != null)
				current_focus.OnMouseOver();
		}
		public static function UpdateFocus():void
		{
			if (current_focus != null)
			{
				if (current_focus != to_enter)
				{
					current_focus.OnMouseLeave();
					if (to_enter != null)
					{
						to_enter.OnMouseOver();
					}
				}
				current_focus = to_enter;
			}
			else if (to_enter != null)
			{
				current_focus = to_enter;
				current_focus.OnMouseOver();
			}
			to_enter = null;
		}
	}
}