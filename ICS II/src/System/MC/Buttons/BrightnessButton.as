package System.MC.Buttons 
{
	import flash.geom.ColorTransform;
	import System.MC.Collision.CollisionBox;
	import System.V.RenderableObject;
	/**
	 * ...
	 * @author Knight
	 */
	public class BrightnessButton extends Button 
	{
		
		private var onMouseOver:int;
		private var onMouseClick:int;
		private var onIdle:int;
		public function BrightnessButton(view:RenderableObject, collisionBox:CollisionBox, over:int, click:int, idle:int = 0) 
		{
			super(view, collisionBox);
			onMouseOver = over;
			onMouseClick = click;
			onIdle = idle;
			View.transform.colorTransform = new ColorTransform(1, 1, 1, 1, idle, idle, idle);
		}
		protected override function OnMouseOver():void
		{
			super.OnMouseOver();
			View.transform.colorTransform = new ColorTransform(1, 1, 1, 1, onMouseOver, onMouseOver, onMouseOver);
		}
		protected override function OnMouseDown():void
		{
			if (!HasFocus) return;
			
			super.OnMouseDown();
			View.transform.colorTransform = new ColorTransform(1, 1, 1, 1, onMouseClick, onMouseClick, onMouseClick);
		}
		protected override function OnMouseLeave():void
		{
			super.OnMouseLeave();
			View.transform.colorTransform = new ColorTransform(1, 1, 1, 1, onIdle, onIdle, onIdle);
		}
		protected override function OnMouseClick():void
		{
			if (!HasFocus) return;
			
			super.OnMouseClick();
			View.transform.colorTransform = new ColorTransform(1, 1, 1, 1, onMouseOver, onMouseOver, onMouseOver);
		}
	}

}