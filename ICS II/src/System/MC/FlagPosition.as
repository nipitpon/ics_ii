package System.MC 
{
	/**
	 * ...
	 * @author Knight
	 */
	public final class FlagPosition 
	{
		public static const Horde1:int = 0;
		public static const Horde2:int = 1;
		public static const Horde3:int = 2;
		public static const Horde4:int = 3;
		public static const Horde5:int = 4;
		public static const Horde6:int = 5;
		public static const Horde7:int = 6;
		public static const Horde8:int = 7;
		public static const Horde9:int = 8;
		public static const Horde10:int = 9;
		public static const Horde11:int = 10;
		public static const Horde12:int = 11;
		public static const Horde13:int = 12;
		public static const EnableWeather:int = 13;
		public static const BlueBall:int = 14;
		public static const Elephant:int = 15;
		public static const SolarBramm:int = 16;
		public static const WhiteSpirit:int = 17;
		public static const Block1:int = 18;
		public static const Block2:int = 19;
		public static const Block3:int = 20;
		public static const Block4:int = 21;
		public static const Block5:int = 22;
		public static const Block6:int = 23;
		public static const Chest1:int = 24;
		public static const Chest2:int = 25;
		public static const Chest3:int = 26;
		public static const Chest4:int = 27;
		public static const Chest5:int = 28;
		public static const Chest6:int = 29;
		public static const Chest7:int = 30;
		public static const Chest8:int = 31;
		public static const Chest9:int = 32;
		public static const Chest10:int = 33;
		public static const Chest11:int = 34;
		public static const Chest12:int = 35;
		public static const Chest13:int = 36;
		public static const Extension6Left:int = 37;
		public static const Extension6Right:int = 38;
		public static const Cave1:int = 39;
		public static const Cave2:int = 40;
		public static const Extension9:int = 41;
		public static const CheckPoint1:int = 42;
		public static const CheckPoint2:int = 43;
		public static const CheckPoint3:int = 44;
		public static const CheckPoint4:int = 45;
		public static const CheckPoint5:int = 46;
		public static const CheckPoint6:int = 47;
		public static const CheckPoint7:int = 48;
		public static const CheckPoint8:int = 49;
		public static const CheckPoint9:int = 50;
		public static const CheckPoint10:int = 51;
		public static const Switch1:int = 52;
		public static const Switch2:int = 53;
		public static const Chest14:int = 54;
		public static const Chest15:int = 55;
		public static const Chest16:int = 56;
		public static const Chest17:int = 57;
		public static const Chest18:int = 58;
		public static const Chest19:int = 59;
		public static const Chest20:int = 60;
		public static const AmbushTurret:int = 61;
		public static const Note1:int = 81;
		public static const Note2:int = 62;
		public static const Note3:int = 63;
		public static const Note4:int = 64;
		public static const Note5:int = 65;
		public static const Note6:int = 66;
		public static const Note7:int = 67;
		public static const Note8:int = 68;
		public static const Note9:int = 69;
		public static const Note10:int = 70;
		public static const Note11:int = 71;
		public static const Note12:int = 72;
		public static const Note13:int = 73;
		public static const Note14:int = 74;
		public static const Note15:int = 75;
		public static const Note16:int = 76;
		public static const Note17:int = 77;
		public static const Note18:int = 78;
		public static const Note19:int = 79;
		public static const Note20:int = 80;
		//Note1 uses 81
		public static const CheckPoint11:int = 82;
		public static const CheckPoint12:int = 83;
		public static const CheckPoint13:int = 84;
		public static const CheckPoint14:int = 85;
		public static const CheckPoint15:int = 86;
		public static const CheckPoint16:int = 87;
		public static const CheckPoint17:int = 88;
		public static const CheckPoint18:int = 89;
		public static const CheckPoint19:int = 90;
		public static const CheckPoint20:int = 91;
		public static const VanillaFinished:int = 92;
		public static const Chest21:int = 93;
		public static const Chest22:int = 94;
		public static const Chest23:int = 95;
		public static const Chest24:int = 96;
		public static const Chest25:int = 97;
		public static const Chest26:int = 98;
		
		public function FlagPosition() 
		{
			throw new Error("Cannot instantiate FlagPosition");
		}
	}
}