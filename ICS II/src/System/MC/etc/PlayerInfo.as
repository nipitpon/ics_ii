package System.MC.etc 
{
	import flash.geom.Point;
	import System.ControlCenter;
	import System.MC.Characters.Player;
	/**
	 * ...
	 * @author Knight
	 */
	public final class PlayerInfo extends CharacterInfo
	{
		public var ap:int;
		public var ap_level:int;
		public var ap_point:int;
		public var sp_point:int;
		public var sp_level:int;
		public var sp:int;
		public var fire_mode:int = Player.Auto;
		public var shortcuts:Vector.<Point>;
		public var skills_state:Vector.<Vector.<int>>;
		public var ammo:int;
		public function PlayerInfo() 
		{
			
		}
	}
}