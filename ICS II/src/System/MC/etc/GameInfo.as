package System.MC.etc 
{
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	/**
	 * ...
	 * @author Knight
	 */
	public final class GameInfo
	{
		public var time:uint;
		public var items:Vector.<Boolean>;
		public var level_time:Vector.<int>;
		public var deaths:Vector.<int>;
		public var total_level_death:Vector.<int>;
		public var achievements_unlocked:Vector.<Boolean>;
		public var total_death:int;
		public var total_goat_burn:int;
		public var death_streak:int;
		public var last_death_reason:int;
		public var fire_potion_used:int;
		public function GameInfo() 
		{
			time = 0;
			items = new Vector.<Boolean>();
			deaths = new Vector.<int>();
			total_level_death = new Vector.<int>();
			level_time = new Vector.<int>();
			achievements_unlocked = new Vector.<Boolean>();
			total_death = 0;
			total_goat_burn = 0;
			fire_potion_used = 0;
			for (var i:int = 0; i < 60; i++)
			{
				items.push(false);
				deaths.push( -1);
				total_level_death.push(0);
				achievements_unlocked.push(false);
				level_time.push( -1);
			}
			items.push(false);
		}
		
	}

}