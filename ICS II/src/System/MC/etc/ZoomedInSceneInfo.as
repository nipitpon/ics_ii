package System.MC.etc 
{
	import HardCode.BlackMirror.ZoomedInScene.ZoomedInButton;
	import System.V.RenderableObject;
	/**
	 * ...
	 * @author Knight
	 */
	public class ZoomedInSceneInfo 
	{
		public var background:RenderableObject;
		public var buttons:Vector.<ZoomedInButton>;
		public function ZoomedInSceneInfo() 
		{
			
		}
		
	}

}