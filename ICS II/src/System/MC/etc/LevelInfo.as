package System.MC.etc 
{
	import System.GameSystem;
	/**
	 * ...
	 * @author Knight
	 */
	public class LevelInfo 
	{
		public var hour:int = 9;
		public var minute:int = 0;
		public var second:int = 0;
		public var spawnPoint_counts:Vector.<int> = new Vector.<int>();
		public var mode:int = GameSystem.GameMode_Vanilla;
		public function LevelInfo() 
		{
			
		}
	}
}