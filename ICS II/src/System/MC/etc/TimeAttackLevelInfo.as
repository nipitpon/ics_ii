package System.MC.etc 
{
	import System.GameSystem;
	/**
	 * ...
	 * @author Knight
	 */
	public class TimeAttackLevelInfo extends LevelInfo 
	{
		public var tick:int = 0;
		public function TimeAttackLevelInfo(inf:LevelInfo = null) 
		{
			super();
			mode = GameSystem.GameMode_TimeAttack;
			if (inf != null)
			{
				hour = inf.hour;
				minute = inf.minute;
				second = inf.second;
				spawnPoint_counts = inf.spawnPoint_counts;
			}
		}
	}
}