package System.MC.etc 
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author Knight
	 */
	public class CharacterInfo 
	{
		public var position:Point;
		public var hp:int;
		public var atk:int;
		public var def:int;
		public var vit:int;
		public var mp:int;
		public function CharacterInfo() 
		{
			
		}
		
	}

}