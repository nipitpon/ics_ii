package System.MC.etc 
{
	/**
	 * ...
	 * @author Knight
	 */
	public class ForceStruct 
	{
		public var x:Number;
		public var y:Number;
		public var decayRate:Number
		public function ForceStruct(x:Number, y:Number, decayRate:Number) 
		{
			this.x = x;
			this.y = y;
			this.decayRate = decayRate;
		}
		
	}

}