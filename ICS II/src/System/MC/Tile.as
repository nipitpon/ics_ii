package System.MC 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import System.Interfaces.ICollidable;
	import System.Interfaces.IMVC;
	import System.MC.Characters.Character;
	import System.MC.Characters.Player;
	import System.MC.Collision.*;
	import System.V.RenderableObject;
	import Resources.Resource;
	import System.V.TileSprite;
	/**
	 * ...
	 * @author Knight
	 */
	public class Tile extends MovableObject
	{
		public static const Size:Point = new Point(32, 32);
		protected var bottom_most:Boolean;
		private var allowDrop:Boolean
		
		public function Tile(allowDrop:Boolean, customBox:CollisionBox = null, length:int = 1, height:int = 1) 
		{
			if (customBox == null)
			{
				var direction:uint = CollisionBox.IntersectDirection_All;
				if (allowDrop)
					direction = CollisionBox.IntersectDirection_Bottom;
				customBox = new CollisionRectangle(0, 0, Size.x * length, Size.y * (height), direction);
			}
			super(new TileSprite(), customBox);
			this.allowDrop = allowDrop;
			bottom_most = false;
			speed = 0;
		}
		public function get AllowDrop():Boolean { return allowDrop; }
		public function set BottomMost(val:Boolean):void { bottom_most = val; }
		public function get BottomMost():Boolean { return bottom_most; }
		//public override function Intersects(obj:CollisionBox, movement:Point):ICollidable
		//{
		//	if (Math.abs(obj.CenterX - Collision.CenterX) > Collision.Width + obj.Width + Math.abs(movement.x)) return null;
		//	else if (Math.abs(obj.CenterY - Collision.CenterY) > Collision.Height + obj.Height + Math.abs(movement.y)) return null;
		//	else return super.Intersects(obj, movement);
		//}
		public override function TakenHit(obj:ICollidable, movement:Point):void
		{
			super.TakenHit(obj, movement);
			if (obj is Character)
			{
				if (Intersects(obj.Collision, new Point(movement.x, 0)) != null)
				{
					if (movement.x > 0)
					{
						movement.x = Collision.GetLeftAtPos(obj.CenterX, obj.Top, obj.Bottom) - obj.Collision.Right + ForceX;
					}
					else if (movement.x < 0)
					{
						movement.x = Collision.GetRightAtPos(obj.CenterX, obj.Top, obj.Bottom) - obj.Collision.Left + ForceX;
					}
				}
			}
		}
		public function IsInIndexRange(start:int, end:int):Boolean
		{
			if (Collision == null) return false;
			
			var s:int = Math.floor(Collision.Left / Tile.Size.x);
			var e:int = s + Math.ceil(Collision.Width / Tile.Size.x);
			
			return start <= e && s <= end;
		}
		public static function GetLeft(tile:Number):Number { return Size.x * tile; }
		public static function GetTop(tile:Number):Number { return Size.y * tile; }
		public static function GetTopLeft(x:Number, y:Number):Point { return new Point(GetLeft(x), GetTop(y)); }
	}
}