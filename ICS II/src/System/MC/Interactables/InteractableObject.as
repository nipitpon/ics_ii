package System.MC.Interactables 
{
	import _Base.MySound;
	import _Base.MyTextField;
	import flash.events.Event;
	import flash.filters.GlowFilter;
	import flash.geom.ColorTransform;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import Resources.lib.NPC.InteractionTextFieldBackground;
	import System.GameFrame;
	import System.GameSystem;
	import System.Interfaces.IInteractable;
	import System.Interfaces.IInteractor;
	import System.Interfaces.IUpdatable;
	import System.MC.Collision.CollidableObject;
	import System.MC.Collision.CollisionBox;
	import System.MC.Collision.CollisionSphere;
	import System.MC.ModelViewController;
	import System.MC.MovableObject;
	import System.MC.Overlay;
	import System.V.InteractionText;
	import System.V.RenderableObject;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class InteractableObject extends CollidableObject implements IInteractable, IUpdatable
	{
		private var interactionTextField:Overlay;
		private var cooldown_count:int;
		private var interaction_text:String;
		private var highlight_forever:Boolean;
		public function InteractableObject(view:RenderableObject, collisionBox:CollisionBox, interactionText:String) 
		{
			super(view, collisionBox);
			interaction_text = interactionText;
			var interactionTextField:InteractionText = new InteractionText();
			interactionTextField.text = interactionText.toUpperCase();
			interactionTextField.visible = true;
			interactionTextField.y = (interactionTextField.textHeight - 12) / 2;
			interactionTextField.width = interactionTextField.textWidth + 20;
			var ren:RenderableObject = new RenderableObject();
			ren.addChild(new InteractionTextFieldBackground(interactionTextField.width));
			ren.addChild(interactionTextField);
			interactionTextField.x = 8;
			this.interactionTextField = new Overlay(ren, Overlay.Location_Foreground);
			
			collide_with = Vector.<Class>([IInteractor]);
			//view.addChild(interactionTextField);
			cooldown_count = -1;
			addEventListener(Removed, onRemoved);
			highlight_forever = false;
		}
		public function get IsInteractable():Boolean { return cooldown_count < 0; }
		public function get UpdateWhenAdvancingTime():Boolean { return true; }
		public function Update():void
		{
			if (cooldown_count >= 0)
			{
				cooldown_count--;
				if (cooldown_count == 0)
				{
					cooldown_count = -1;
					View.transform.colorTransform = new ColorTransform();
				}
			}
		}
		public function GetInteracted(interactor:IInteractor):void
		{
			Unhighlight();
			cooldown_count = Utility.SecondToFrame(1);
			View.transform.colorTransform = new ColorTransform(0.5, 0.5, 0.5);
		}
		public function get Caption():String { return interaction_text; }
		public function ShowNameTag():void
		{
			highlight_forever = true;
			if (!GameFrame.ContainChild(interactionTextField))
			{
				interactionTextField.CenterX = CenterX;
				interactionTextField.Top = Bottom;
				if (interactionTextField.Right > GameSystem.CurrentLevel.TerrainLayer.Right)
					interactionTextField.Right = GameSystem.CurrentLevel.TerrainLayer.Right;
				if (interactionTextField.Left < GameSystem.CurrentLevel.TerrainLayer.Left)
					interactionTextField.Left = GameSystem.CurrentLevel.TerrainLayer.Left;
				GameFrame.AddChild(interactionTextField);
			}
		}
		public function Highlight(stay_on_forever:Boolean):void
		{
			//interactionTextField.visible = true;
			//if (!View.contains(interactionTextField))
			//	View.addChild(interactionTextField);
			ShowNameTag();
			if (View.filters == null || View.filters.length == 0)
				View.filters = [GameFrame.objectGlow];
			highlight_forever = stay_on_forever;
		}
		public function Unhighlight():void
		{
			//if (View.contains(interactionTextField))
			//	View.removeChild(interactionTextField);
			//interactionTextField.visible = false;
			if (highlight_forever) return;
			
			if (GameFrame.ContainChild(interactionTextField))
				GameFrame.RemoveChild(interactionTextField);
			if (View.filters != null && View.filters.length != 0)
				View.filters = null;
		}
		private function onRemoved(e:Event):void
		{
			if (GameFrame.ContainChild(this.interactionTextField))
				GameFrame.RemoveChild(this.interactionTextField);
		}
	}
}