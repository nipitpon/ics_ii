package System 
{
	import adobe.utils.CustomActions;
	import flash.utils.Dictionary;
	import HardCode.*;
	import System.Interfaces.ISkill;
	/**
	 * ...
	 * @author Knight
	 */
	public final class Overseer 
	{
		private static var skills:Dictionary = new Dictionary();
		
		public function Overseer() 
		{
			throw new Error("Cannot instantiate Overseer");
		}
		public static function Initialize():void
		{
			skills[ExecutionCode.Skill_Level1] = Skill_StartLevel1;
			skills[ExecutionCode.Skill_Level2] = Skill_StartLevel1;
		}
		public static function Flush():void
		{
			skills = new Dictionary();
		}
		public static function Execute(code:uint):void
		{
			if (skills[code] != null)
				(new skills[code]() as ISkill).Activate();
		}
	}
}