package System 
{
	import _Base.MySound;
	import _Base.MyTextField;
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.TextInteractionMode;
	import HardCode.GUI.InventoryButton;
	import HardCode.GUI.MonsterMarker;
	import HardCode.GUI.MoreGameButton;
	import HardCode.GUI.Sponsor300x80;
	import HardCode.Levels.DynamicLevel;
	import HardCode.Pages.Game.PauseMenu;
	import Resources.lib.GUI.BurningScreenImage;
	import Resources.lib.GUI.PauseButtonBackground;
	import Resources.lib.GUI.QualityButtonImage;
	import System.Interfaces.IBuff;
	import System.Interfaces.IReceiveControl;
	import System.MC.Buttons.Button;
	import System.MC.Buttons.MenuButton;
	import System.MC.Characters.Character;
	import System.MC.Characters.Monster;
	import System.MC.Characters.Player;
	import System.MC.etc.ZoomedInSceneInfo;
	import System.MC.ModelViewController;
	import System.V.BossHPBar;
	import System.V.DeathCount;
	import System.V.FloatingText;
	import System.V.InteractionText;
	import System.V.RenderableObject;
	import System.V.SlottedProgressBar;
	import System.V.SurvivalModeText;
	import System.V.TimeAttackText;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class GUI extends RenderableObject
	{
		private static var instance:GUI = null;
		private var more_game:MoreGameButton;
		
		private var menuButton:MenuButton;
		private var description:MyTextField;
		private var pauseMenu:PauseMenu;
		private var freezeLayer:RenderableObject;
		private var effects:RenderableObject;
		private var level_text:MyTextField;
		private var death_count:DeathCount;
		private var burning_screen:BurningScreenImage;
		
		private var interaction_text:MyTextField;
		
		private var map_text:TextField;
		private var map_button:MyTextField;
		public function GUI() 
		{
			instance = this;
			menuButton = new MenuButton();
			description = new MyTextField();
			pauseMenu = new PauseMenu();
			freezeLayer = new RenderableObject();
			effects = new RenderableObject();
			level_text = new MyTextField(80, 0xffffff, false);
			death_count = new DeathCount();
			burning_screen = new BurningScreenImage();
			more_game = new MoreGameButton();
			
			interaction_text = new MyTextField();
			if (false && CONFIG::debug)
			{
				map_text = new TextField();
				map_text.width = 200;
				map_text.height = 50;
				map_text.background = true;
				map_text.backgroundColor = 0xffffff;
				map_text.border = true;
				map_text.borderColor = 0;
				map_text.x = 260;
				map_text.selectable = true;
				map_text.multiline = true;
				map_text.type = TextFieldType.INPUT;
			}
			Initialize();
			if(false && CONFIG::debug)
			{
				map_button = new MyTextField(20);
				map_button.text = "load map";
				map_button.width = 200;
				map_button.height = 20;
				map_button.border = true;
				map_button.borderColor = 0;
				map_button.background = true;
				map_button.backgroundColor = 0x444444;
				map_button.x = 260;
				map_button.y = 50;
				map_button.addEventListener(MouseEvent.CLICK, function(e:Event):void
				{
					DynamicLevel.MapCode = map_text.text;
					GameSystem.SetGameMode(102);
					Main.ChangeToGamePage();
				});
				addChild(map_text);
				addChild(map_button);
			}
		}
		public static function ShowDescriptionAsHtml(x:int, y:int, html:String):void
		{
			if (html == null)
				throw new ArgumentError();
				
			instance.description.visible = true;
			instance.description.htmlText = html;
			instance.description.x = x;
			instance.description.y = y;
			if (instance.description.x + instance.description.width > instance.stage.stageWidth)
				instance.description.x = x - instance.description.width;
			if (instance.description.y + instance.description.height > instance.stage.stageHeight)
				instance.description.y = y - instance.description.height;
			instance.setChildIndex(instance.description, instance.numChildren-1);
		}
		public static function ShowDescription(x:int, y:int, message:String):void
		{
			if (message == null)
				throw new ArgumentError();
				
			instance.description.visible = true;
			instance.description.text = message;
			instance.description.x = x;
			instance.description.y = y;
			if (instance.description.x + instance.description.width > instance.stage.stageWidth)
				instance.description.x = x - instance.description.width;
			if (instance.description.y + instance.description.height > instance.stage.stageHeight)
				instance.description.y = y - instance.description.height;
			instance.setChildIndex(instance.description, instance.numChildren-1);
		}
		public static function HideDescription():void
		{
			instance.description.visible = false;
		}
		public static function ShowPauseMenu():void
		{
			if (GamePage.IsFadingOut) return;
			
			if (!GameSystem.IsFreezing) GameSystem.Freeze();
			instance.menuButton.View.visible = false;
			instance.pauseMenu.visible = true;
			GamePage.ShowPauseMenu();
			SoundCenter.PauseAllSFX();
		}
		public static function get IsShowingPauseMenu():Boolean { return instance.pauseMenu.visible; }
		public static function HidePauseMenu():void
		{
			GameSystem.Unfreeze();
			instance.pauseMenu.visible = false;
			instance.menuButton.View.visible = true;
			HideDescription();
			Button.ClearFocus();
			GamePage.HidePauseMenu();
			SoundCenter.ResumeAllSFX();
		}
		public static function get IsShowingPopup():Boolean { return instance.pauseMenu.visible; }
		public static function set BurningScreenAlpha(val:Number):void { instance.burning_screen.alpha = val; }
		public static function get BurningScreenAlpha():Number { return instance.burning_screen.alpha; }
		public static function AddEffect(obj:RenderableObject):void
		{
			instance.effects.addChild(obj);
		}
		public static function OnPlayerAdded():void
		{
			
		}
		public static function ShowFreezeLayer():void { instance.freezeLayer.visible = true;  }
		public static function HideFreezeLayer():void { instance.freezeLayer.visible = false; }
		public static function ShowBossHP():void 
		{ 
		}
		public static function UpdateBossHP(val:Number, max:Number):void 
		{
		}
		public static function HideBossHP():void { }
		public static function ShowInteractionText(text:String):void
		{
			instance.interaction_text.text = text;
		}
		public static function HideInteractionText():void
		{
			ShowInteractionText("");
		}
		public static function SetLevelText(level:int):void
		{
			if (level < 0) instance.level_text.text = "";
			else if (level == 55) instance.level_text.text = "LEVEL ???";
			else instance.level_text.text = "LEVEL " + (level < 10 ? "0" : "") + level;
			
			instance.death_count.visible = level >= 0;
		}
		public static function SetDeathCount(count:int):void
		{
			instance.death_count.NumOfDeath = count;
		}
		private function Initialize():void
		{
			pauseMenu.visible = false;
			
			menuButton.Center = new Point(36, 36);
			more_game.Top = 10;
			more_game.Left = 10;
			
			description.visible = false;
			description.multiline = true;
			description.defaultTextFormat = new TextFormat("Pixel Font", 14, 0xb2b2b2);
			description.embedFonts = true;
			description.background = true;
			description.backgroundColor = 0;
			description.border = true;
			description.borderColor = 0xb2b2b2;
			description.width = 200;
			description.autoSize = TextFieldAutoSize.LEFT;
			description.height = 200;
			description.text = "hohoh\nhehehee\nhahahaha";
			
			interaction_text.x = 110;
			interaction_text.y = 371;
			interaction_text.embedFonts = true;
			interaction_text.defaultTextFormat = new TextFormat("Pixel Font", 30, 0xffffff, true,
				null, null, null, null, TextFormatAlign.CENTER);
			interaction_text.width = 500;
			interaction_text.wordWrap = true;
			level_text.defaultTextFormat = new TextFormat("Pixel Font", 60, 0xffffff, true);
			level_text.text = "LEVEL 0";
			level_text.autoSize = TextFieldAutoSize.LEFT;
			level_text.x = 18;
			level_text.y = 487;
			level_text.width = 400;
			death_count.Right = 702;
			death_count.Bottom = 524;
			//interaction_text.filters = [new GlowFilter(0, 1, 3, 3, 3)];
			
			addChild(level_text);
			addChild(death_count);
			addChild(interaction_text);
			addChild(burning_screen);
			
			addChild(freezeLayer);
			
			addChild(pauseMenu);
			addChild(description);
			addChild(effects);
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		private function onEnterFrame(e:Event):void
		{
		}
		private function onAddedToStage(e:Event):void
		{
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			HideBossHP();
			if (GameSystem.CurrentLevel.ShowPauseMenu)
			{
				addChild(menuButton.View);
				ControlCenter.AddGameObject(menuButton);
			}
			else
			{
				addChild(more_game.View);
				ControlCenter.AddGameObject(more_game);
			}
			BurningScreenAlpha = 0;
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			freezeLayer.graphics.clear();
			freezeLayer.graphics.beginFill(0, 0.5);
			freezeLayer.graphics.drawRect(0, 0, instance.stage.stageWidth, instance.stage.stageHeight);
			freezeLayer.graphics.endFill();
			freezeLayer.visible = false;
			
			if(false && CONFIG::debug)
			{
				map_text.text = "";
			}
		}
		private function onRemovedFromStage(e:Event):void
		{
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			HidePauseMenu();
			HideDescription();
			if (GameSystem.CurrentLevel.ShowPauseMenu)
			{
				removeChild(menuButton.View);
				ControlCenter.RemoveGameObject(menuButton);
			}
			else
			{
				removeChild(more_game.View);
				ControlCenter.RemoveGameObject(more_game);
			}
			effects.removeChildren();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
	}
}