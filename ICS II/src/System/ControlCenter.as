package System 
{
	import flash.display.Stage;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	import flash.utils.Dictionary;
	import System.MC.Buttons.Button;
	
	import System.Interfaces.IMVC;
	import System.Interfaces.IReceiveControl;
	/**
	 * ...
	 * @author Knight
	 */
	public final class ControlCenter 
	{
		public static const PlayerWalkLeft:int = 1;
		public static const PlayerWalkRight:int = 2;
		public static const DefaultATK:int = 3;
		public static const Skill1:int = 4;
		public static const Skill2:int = 5;
		public static const Skill3:int = 6;
		public static const Skill4:int = 7;
		public static const Skill5:int = 13;
		public static const Restart:int = 21;
		public static const PlayerJump:int = 8;
		public static const PlayerReload:int = 9;
		public static const Interact:int = 10;
		public static const Drop:int = 11;
		public static const PlayerMaleeATK:int = 12;
		public static const SwitchLeft:int = 14;
		public static const SwitchRight:int = 15;
		public static const Pause:int = 18;
		public static const SkillWindow:int = 19;
		public static const Inventory:int = 20;
		public static const PlayerClimb:int = 22;
		
		public static const ActivateButton:int = Interact;
		public static const SelectLowerButton:int = Drop;
		public static const SelectLeftButton:int = PlayerWalkLeft;
		public static const SelectRightButton:int = PlayerWalkRight;
		public static const SelectUpperButton:int = PlayerClimb;
				
		private static var stage:Stage = null;
		private static var control:Vector.<IReceiveControl>;
		private static var key:Dictionary;
		private static var pressing_mouse:Boolean;
		private static var pressing_key:Dictionary;
		private static var send_event:Boolean = true;
		
		private static var to_add:Vector.<IReceiveControl>;
		private static var looping_event:Boolean = false;
		
		public function ControlCenter() 
		{
			throw new Error("Cannot instantiate ControlCenter");
		}
		
		public static function Initialize(stage:Stage):void
		{
			ControlCenter.stage = stage;
			pressing_mouse = false;
			control = new Vector.<IReceiveControl>();
			to_add = new Vector.<IReceiveControl>();
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			InitializeKeyLibrary();
		}
		public static function get IsReceivingControl():Boolean { return send_event; }
		public static function Start():void
		{
			send_event = true;
		}
		public static function Stop():void
		{
			send_event = false;
			pressing_mouse = false;
			for(var i:String in pressing_key)
				pressing_key[i] = false;
		}
		public static function AddGameObject(obj:IReceiveControl):void
		{
			if (obj == null)
				throw ArgumentError("obj cannot be null");
			
			if(!looping_event)
				control.push(obj);
			else
				to_add.push(obj);
		}
		public static function ContainsGameObject(obj:IReceiveControl):Boolean
		{
			for (var i:int = 0; i < control.length; i++)
			{
				if (control[i] == obj) return true;
			}
			return false;
		}
		public static function RemoveGameObject(obj:IReceiveControl):void
		{
			for (var i:int = 0; i < control.length; i++)
			{
				if (control[i] == obj)
				{
					control.splice(i, 1);
					i--;
				}
			}
		}
		public static function Flush():void
		{
			stage = null;
			control = null;
		}
		public static function ReleaseAllKey():void
		{
			for (var key:* in pressing_key)
			{
				pressing_key[key] = false;
			}
		}
		public static function get PressingMouse():Boolean { return pressing_mouse; }
		public static function PressingKey(id:int):Boolean { return pressing_key[id] != null && pressing_key[id] == true; }
		
		private static function UpdateList():void
		{
			while (to_add.length > 0)
				control.push(to_add.shift());
		}
		private static function onKeyDown(e:KeyboardEvent):void
		{
			if (!send_event || Main.IsFading) return;
			
			var k:int = GetKey(e.keyCode);
			if (k >= 0 && !PressingKey(k))
			{
				pressing_key[k] = true;
				looping_event = true;
				for each(var c:IReceiveControl in control)
				{
					if(c.IsReceivingControl)
						c.KeyPress(k);
				}
				looping_event = false;
				UpdateList();
			}
			if (k == Interact)
				GameSystem.Interacts();
		}
		private static function onKeyUp(e:KeyboardEvent):void
		{
			if (!send_event || Main.IsFading) return;
			
			var k:int = GetKey(e.keyCode);
			if (k >= 0)
			{
				pressing_key[k] = false;
				looping_event = true;
				for each(var c:IReceiveControl in control)
				{
					if(c.IsReceivingControl)
						c.KeyRelease(k);
				}
				looping_event = false;
				UpdateList();
			}
		}
		private static function onMouseDown(e:MouseEvent):void
		{
			if (!send_event || Main.IsFading) return;
			
			pressing_mouse = true;
			looping_event = true;
			for each(var c:IReceiveControl in control)
			{
				if(c.IsReceivingControl)
					c.MousePress(e.stageX, e.stageY);
			}
			looping_event = false;
			UpdateList();
		}
		private static function onMouseMove(e:MouseEvent):void
		{
			if (!send_event) return;
			
			looping_event = true;
			for each(var c:IReceiveControl in control)
			{
				if(c.IsReceivingControl)
					c.MouseMove(e.stageX, e.stageY);
			}
			looping_event = false;
			UpdateList();
			Button.UpdateFocus();
		}
		private static function onMouseUp(e:MouseEvent):void
		{
			if (!send_event || Main.IsFading) return;
			
			pressing_mouse = false;
			looping_event = true;
			for each(var c:IReceiveControl in control)
			{
				if(c.IsReceivingControl)
					c.MouseRelease(e.stageX, e.stageY);
			}
			looping_event = false;
			UpdateList();
		}
		private static function GetKey(keyCode:int):int
		{
			if (key[keyCode] == null) return -1;
			else return key[keyCode];
		}
		private static function InitializeKeyLibrary():void
		{
			key = new Dictionary();
			
			key[Keyboard.LEFT] = PlayerWalkLeft;
			key[Keyboard.RIGHT] = PlayerWalkRight;
			key[Keyboard.UP] = PlayerClimb;
			key[Keyboard.Z] = PlayerJump;
			key[Keyboard.X] = Interact;
			key[Keyboard.DOWN] = Drop;
			key[Keyboard.ESCAPE] = Pause;
			key[Keyboard.R] = Restart;
			key[Keyboard.ENTER] = ActivateButton;
			
			key[Keyboard.W] = PlayerClimb;
			key[Keyboard.A] = PlayerWalkLeft;
			key[Keyboard.S] = Drop;
			key[Keyboard.D] = PlayerWalkRight;
			key[Keyboard.K] = PlayerJump;
			key[Keyboard.J] = Interact;
			key[Keyboard.P] = Pause;
			key[Keyboard.SPACE] = PlayerJump;
						
			pressing_key = new Dictionary();
			pressing_key[PlayerWalkLeft] = false;
			pressing_key[PlayerWalkRight] = false;
			pressing_key[DefaultATK] = false;
			pressing_key[Skill1] = false;
			pressing_key[Skill2] = false;
			pressing_key[Skill3] = false;
			pressing_key[Skill4] = false;
			pressing_key[Skill5] = false;
			pressing_key[PlayerJump] = false;
			pressing_key[PlayerReload] = false;
			pressing_key[Interact] = false;
			pressing_key[Drop] = false;
			pressing_key[PlayerMaleeATK] = false;
			pressing_key[SwitchLeft] = false;
			pressing_key[SwitchRight] = false;
			pressing_key[Pause] = false;
			pressing_key[SkillWindow] = false;
			pressing_key[Inventory] = false;
			pressing_key[Restart] = false;
		}
	}

}