package System.V 
{
	import _Base.MyTextField;
	import flash.display.Shape;
	import flash.filters.GlowFilter;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	/**
	 * ...
	 * @author Knight
	 */
	public class SlottedProgressBar extends RenderableObject 
	{
		private var shape:Shape;
		private var slot:Vector.<RenderableObject>;
		private var slotType:Class;
		private var max:Number = 100;
		private var min:Number = 0;
		private var current:Number = max;
		private var _width:Number;
		private var _height:Number;
		private var border:Shape;
		private var text:MyTextField;
		public function SlottedProgressBar(slot:Class, width:Number, height:Number) 
		{
			shape = new Shape();
			border = new Shape();
			slotType = slot;
			this.slot = new Vector.<RenderableObject>();
			text = new MyTextField();
			Initialize(width, height);
		}
		public function get Max():Number { return max; }
		public function get Min():Number { return min; }
		public function get CurrentValue():Number { return current; }
		public override function get width():Number { return _width; }
		public override function get height():Number { return _height; }
		public function get ShowText():Boolean { return text.visible; }
		public function set ShowText(val:Boolean):void { text.visible = val; }
		public function set Max(val:Number):void 
		{
			max = val;
			Refresh(false);
		}
		public function set Min(val:Number):void
		{
			min = val;
			Refresh(false);
		}
		public function set CurrentValue(val:Number):void
		{
			current = val;
			Refresh(false);
		}
		private function Initialize(width:Number, height:Number):void
		{
			addChild(shape);
			addChild(border);
			addChild(text);
			shape.graphics.lineStyle(1);
			shape.graphics.moveTo(0, 0);
			shape.graphics.lineTo(0, height);
			shape.graphics.lineTo(width, height);
			text.defaultTextFormat = new TextFormat("Tahoma", 14, 0xffffff, true);
			text.autoSize = TextFieldAutoSize.CENTER;
			text.filters = [new GlowFilter(0, 1, 3, 3, 2)];
			text.x = (width - text.textWidth) / 2;
			text.text = "20";
			text.y = (height - text.textHeight) / 2;
			
			this._width = width;
			this._height = height;
			Refresh(true);
		}
		private function Refresh(slot:Boolean):void
		{
			var i:int = 0;
			if (slot)
			{
				while (this.slot.length > 0)
				{
					var out:RenderableObject = this.slot.shift();
					if(contains(out))
						removeChild(out);
				}
					
				var sample:RenderableObject = new slotType();
				var num:uint = uint(Math.floor(width / sample.width));
				for (i = 0; i < num; i++)
				{
					var s:RenderableObject = new slotType();
					s.x = s.width * i + 1;
					s.y = height - s.height;
					this.slot.push(s);
				}
			}
			var percent:int = int(Math.ceil(((current - min) / (max - min)) * this.slot.length));
			
			border.graphics.clear();
			border.graphics.lineStyle(1);
			for (i = 0; i < this.slot.length; i++)
			{
				addChild(this.slot[i]);
				if (i < percent)
				{
					this.slot[i].visible = true;
					border.graphics.drawRect(this.slot[i].x, this.slot[i].y, this.slot[i].width, this.slot[i].height);
				}
				else
					this.slot[i].visible = false;
			}
			
			text.text = int(current).toString() + " / " + Max;
			setChildIndex(border, numChildren-1);
			setChildIndex(text, numChildren-1);
		}
	}
}