package System.V 
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import System.DataCenter;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class HoldingItem extends RenderableObject 
	{
		private var code:int;
		public function HoldingItem(code:int) 
		{
			super();
			this.code = code;
			addChild(DataCenter.GetItemIcon(code));
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		public function get ItemCode():int { return code; }
		private function onAddedToStage(e:Event):void
		{
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			x = stage.mouseX;
			y = stage.mouseY;
		}
		private function onRemovedFromStage(e:Event):void
		{
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		private function onMouseMove(e:MouseEvent):void
		{
			x = e.stageX;
			y = e.stageY;
		}
	}

}