package System.V 
{
	import _Base.MyTextField;
	import flash.filters.GlowFilter;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class TimeAttackText extends RenderableObject
	{
		private static const Timer_Format:TextFormat = new TextFormat("Tahoma", 30, 0xffffff, false, false, false, null, null, TextFormatAlign.LEFT);
		private static const Start_Format:TextFormat = new TextFormat("Tahoma", 100, 0xffffff, true, false, false, null, null, TextFormatAlign.CENTER);
		private static const Border:Array = [new GlowFilter(0, 1, 3, 3, 2)];
		private var tf:MyTextField;
		public function TimeAttackText() 
		{
			super();
			tf = new MyTextField();
			tf.x = 5;
			tf.y = 500;
			tf.text = "hahaha";
			tf.width = 800;
			tf.height = 600;
			tf.filters = Border;
			addChild(tf);
		}
		public function SetStartCount(count:int):void
		{
			if(tf.defaultTextFormat != Start_Format)
				tf.defaultTextFormat = Start_Format;
			tf.text = count.toString();
			tf.textColor = 0xffffff;
			tf.y = 230
		}
		public function SetTimerCount(tick:int, hasBorder:Boolean):void
		{
			if(tf.defaultTextFormat != Timer_Format)
				tf.defaultTextFormat = Timer_Format;
			tf.y = 500;
			tf.htmlText = "Please implement!";// TimeCounter.TickToString(tick);
			if (hasBorder)
				tf.textColor = 0xcccccc;
			else if (!hasBorder)
				tf.textColor = 0xffffff;
		}
	}
}