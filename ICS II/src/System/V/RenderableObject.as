package System.V 
{
	import _Base.MySprite;
	import flash.display.DisplayObject;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import Resources.Animation;
	import System.Interfaces.IAnimatable;
	import System.Interfaces.IUpdatable;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class RenderableObject extends MySprite
	{
		private var scaleCenter:Point
		private var rot:Number;
		private var flash_length:uint;
		private var flash_count:uint;
		private var flash_start:uint;
		private var updatables:Vector.<IUpdatable>;
		public function RenderableObject(scaleCenter:Point = null) 
		{
			if (scaleCenter == null)
				scaleCenter = new Point();
				
			this.scaleCenter = scaleCenter;
			updatables = new Vector.<IUpdatable>();
			flash_length = 0;
			flash_count = 0;
			rot = 0;
		}
		public override function addChild(obj:DisplayObject):DisplayObject
		{
			if (obj is IUpdatable) updatables.push(obj as IUpdatable);
			return super.addChild(obj);
		}
		public override function removeChild(obj:DisplayObject):DisplayObject
		{
			if (obj is IUpdatable) for (var i:int = 0; i < updatables.length; i++)
			{
				if (obj == updatables[i])
					updatables.splice(i--, 1);
			}
			return super.removeChild(obj);
		}
		public function NegateAmbient(red:Number, green:Number, blue:Number):void
		{
			var t:ColorTransform = transform.colorTransform;
			t.redMultiplier = 1 / red;
			t.greenMultiplier = 1 / green;
			t.blueMultiplier = 1 / blue;
			transform.colorTransform = t;
		}
		public function Update():void
		{
			for each(var animatable:IUpdatable in updatables)
				animatable.Update();
			if (flash_length != 0)
			{
				var t:ColorTransform = transform.colorTransform;
				if (flash_count > 0)
				{
					flash_count--;
					var progress:Number = (flash_count) / Number(flash_length);
					t.redOffset = int(((flash_start & 0xff0000) >> 16) * progress);
					t.greenOffset = int(((flash_start & 0x00ff00) >> 8) * progress);
					t.blueOffset = int((flash_start & 0x0000ff) * progress);
				}
				else
				{
					flash_length = 0;
					t.redOffset = 0;
					t.greenOffset = 0;
					t.blueOffset = 0;
				}
				transform.colorTransform = t;
			}
		}
		public function MoveToLocationByScalingPoint(point:Point):void
		{
			var X:int = 0;
			var Y:int = 0; 
			if (scaleX > 0)
			{
				Y = scaleCenter.y + Top;
				X = scaleCenter.x + Left;
			}
			else
			{
				Y = scaleCenter.y + Top;
				X = Right - scaleCenter.x;
			}
			var difX:int = point.x - X;
			var difY:int = point.y - Y;
			MoveByOffset(difX, difY);
		}
		public function set Center(val:Point):void
		{
			if (val == null)
				throw new Error("value cannot be null");

			x = val.x - scaledWidth / 2;
			y = val.y - scaledHeight / 2;
		}
		public override function set scaleX(val:Number):void
		{
			var scale:Number = (val / scaleX);
			if (scale == 1) return;
			
			var centerX:Number = (Math.abs(scaleX) * scaleCenter.x) + Left;
			
			super.scaleX = val;
			
			Left = (centerX - scaleCenter.x * Math.abs(scaleX));
		}
		public override function set scaleY(val:Number):void
		{
			var scale:Number = (val / scaleY);
			if (scale == 1) return;
			
			var centerY:Number = 0;
			
			if (super.scaleY > 0) centerY = (scaleY * scaleCenter.y) + Top;
			else centerY = Math.abs(scaledHeight) - scaleCenter.y + Top;
			
			var distanceFromTop:Number = centerY - Top;
			var distanceFromBottom:Number = Bottom - centerY;
			
			super.scaleY = val;
			
			if(scale > 0)
				Top = centerY - distanceFromTop * Math.abs(scale);
			else	
				Top = centerY - distanceFromBottom * Math.abs(scale);
		}
		public function set Bottom(val:Number):void 
		{
			if (scaledHeight > 0) y = val - scaledHeight; 
			else y = val;
		}
		public function set Left(val:Number):void
		{ 
			if (scaledWidth > 0) x = val; 
			else x = val + (scaledWidth * scaleX);
		}
		public function set Right(val:Number):void
		{ 
			var minus:int = 0;
			for (var i:int = 0; i < numChildren; i++)
			{
				var obj:DisplayObject = getChildAt(i);
				if (obj.x < 0 && minus > obj.x)
					minus = obj.x;
			}
			if (scaledWidth > 0) x = val - (scaledWidth + minus); 
			else x = val;
		}
		public override function set rotation(val:Number):void
		{			
			var rotVal:Number = val - rot;
			
			var matrix:Matrix = new Matrix();
			matrix.translate( -scaleCenter.x, -scaleCenter.y);
			matrix.rotate(Utility.ToRadian(rotVal));
			matrix.translate( scaleCenter.x, scaleCenter.y);
			matrix.concat(transform.matrix);
			transform.matrix = matrix;
			
			rot = val;
		}
		public override function get rotation():Number { return rot; }
		public function Flash(startRed:uint, startGreen:uint, startBlue:uint, length:uint):void
		{
			flash_length = length;
			flash_count = length;
			flash_start = (startRed << 16) | (startGreen << 8) | startBlue;
		}
		public function set Top(val:Number):void 
		{
			if (scaledHeight > 0) y = val; 
			else y = val - scaledHeight;
		}
		public function set CenterX(val:Number):void
		{
			Left = val - scaledWidth / 2;
		}
		public function set CenterY(val:Number):void
		{
			Top = val - scaledHeight / 2;
		}
		public function get Center():Point { return new Point(CenterX, CenterY); }
		public function get CenterX():Number { return (Left + Right) / 2; }
		public function get CenterY():Number { return (Top + Bottom) / 2; }
		public function get Top():Number 
		{ 
			if (scaledHeight > 0) return y; 
			else return y + scaledHeight;
		}
		public function get Left():Number 
		{ 
			if (scaledWidth > 0) return x; 
			else return x - (scaledWidth * scaleX);
		}
		public function get Right():Number 
		{
			if (scaledWidth > 0) return x + scaledWidth; 
			else return x;
		}
		public function get Bottom():Number 
		{
			if (scaledHeight > 0) return y + scaledHeight; 
			else return y;
		}
		public function MoveByOffset(x:int, y:int):void
		{
			this.x += x;
			this.y += y;
		}
		public function get scaledWidth():Number { return scaleX > 0 ? width : -width; }
		public function get scaledHeight():Number { return scaleY > 0 ? height : -height; }
	}

}