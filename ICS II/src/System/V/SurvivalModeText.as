package System.V 
{
	import _Base.MyTextField;
	import flash.events.Event;
	import flash.filters.GlowFilter;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class SurvivalModeText extends MyTextField 
	{
		private var count:int;
		public function SurvivalModeText() 
		{
			super();
			defaultTextFormat = new TextFormat("Tahoma", 40, 0xffffff, true, false, false, null, null, TextFormatAlign.CENTER);
			x = 0;
			y = 100;
			width = 800;
			filters = [new GlowFilter(0, 1, 3, 3)];
			visible = false;
		}
		public function Show(text:String):void
		{
			if (stage == null) return;
			
			visible = true;
			this.text = text;
			count = 0;
			if(!hasEventListener(Event.ENTER_FRAME))
				addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		private function onEnterFrame(e:Event):void
		{
			if (++count == Utility.SecondToFrame(3))
			{
				removeEventListener(Event.ENTER_FRAME, onEnterFrame);
				visible = false;
			}
		}
	}

}