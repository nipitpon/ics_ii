package System.V 
{
	import flash.geom.Point;
	import Resources.Image;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class FarBackground extends Image 
	{
		
		public function FarBackground() 
		{
			super();
			graphics.beginFill(0x8bd200);
			graphics.drawRect(0, 300, 800, 300);
			graphics.endFill();
			
			graphics.beginFill(0x772e00);
			graphics.drawRect(0, 600, 800, 700);
			graphics.endFill();
			cacheAsBitmap = true;
		}
		
	}

}