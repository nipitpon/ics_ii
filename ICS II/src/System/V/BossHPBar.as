package System.V 
{
	/**
	 * ...
	 * @author Knight
	 */
	public class BossHPBar extends MonsterHealthBar 
	{
		public function BossHPBar() 
		{
			super(700, 30);
			x = 50;
			y = 60;
			ShowText = true;
		}
	}
}