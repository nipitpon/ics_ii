package System.V 
{
	import flash.events.Event;
	import flash.geom.Point;
	import Resources.Image;
	import System.GameSystem;
	import System.Time;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class ForestBackground extends RenderableObject 
	{
		private var bg:Vector.<Image>;
		private var accel:Number;
		public function ForestBackground(sprite:Class, bottom:int, accel:Number = 0) 
		{
			super();
			bg = new Vector.<Image>();
			bg.push(new sprite());
			bg.push(new sprite());
			for each(var b:RenderableObject in bg)
			{
				b.Bottom = bottom;
				addChild(b);
			}
			this.accel = accel;
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		public override function Update():void
		{
			super.Update();
			var offsetX:Number = parent.x + x + parent.parent.x;
			if (accel != 0) for (var i:int = 0; i < bg.length; i++)
			{
				if(Time.IsAdvancing)
					bg[i].x += accel * 3;
				else
					bg[i].x += accel;
			}
			
			if(bg[0].Left + offsetX > 0) while(bg[0].Left + offsetX > 0)
			{
				var temp:Image = bg[0];
				bg[1].Right = bg[0].Left;
				bg[0] = bg[1];
				bg[1] = temp;
			}
			if(bg[1].Right + offsetX < GameSystem.ScreenWidth) while (bg[1].Right + offsetX < GameSystem.ScreenWidth)
			{
				var temp2:Image = bg[1];
				bg[0].Left = bg[1].Right;
				bg[1] = bg[0];
				bg[0] = temp2;
			}
		}
		private function onAddedToStage(e:Event):void
		{
			bg[0].Right = 0;
			bg[1].Left = 0;
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		}
		private function onRemovedFromStage(e:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		}
	}

}