package System.V 
{
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import Resources.BlackMirror.GUI.BarSquare;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Bar extends RenderableObject 
	{
		public static const ValueChanged:String = "ValueChanged";
		private var line:Shape;
		private var button:BarSquare;
		private var buttonWidth:Number;
		private var buttonHeight:Number;
		private var _width:Number;
		private var min:Number;
		private var max:Number;
		private var current:Number;
		public function Bar(width:Number, buttonWidth:Number, buttonHeight:Number, min:Number, max:Number) 
		{
			super();
			_width = width;
			this.min = min;
			this.max = max;
			this.buttonWidth = buttonWidth;
			this.buttonHeight = buttonHeight;
			line = new Shape();
			line.graphics.lineStyle(2, 0xffffff, 0.5);
			line.graphics.moveTo(0, buttonHeight / 2);
			line.graphics.lineTo(width, buttonHeight / 2);
			button = new BarSquare();
			button.x = -buttonWidth / 2;
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			addChild(line);
			addChild(button);
			current = 0;
		}
		public function set CurrentValue(value:Number):void
		{
			if (value == current) return;
			
			if (value > max) value = max;
			if (value < min) value = min;
			current = value;
			//var c:Number = Utility.Interpolate(max, min, (button.x + button.width / 2) / _width);
			//c = (weightTowardV1) * v1 + (1 - weightTowardV1) * v2;
			//c = (weightTowardV1 * v1) + v2 - (weightTowardV1 * v2);
			//c - v2 = (weightTowardV1) * (v1 + v2);
			//(c - v2) / weightTowardV1 = v1 + v2;
			//weightTowardV1 = (c - v2) / (v1 + v2);
			
			//weightTowardV1 = (button.x + button.width / 2) / _width
			button.x = (((current - min) / (max + min)) * _width) - (button.width / 2);
		}
		public function get CurrentValue():Number { return current; }
		private function onAddedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, button_onMouseDown);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		}
		private function onRemovedFromStage(e:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			stage.removeEventListener(MouseEvent.MOUSE_DOWN, button_onMouseDown);
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		private function button_onMouseDown(e:MouseEvent):void
		{
			if (!Utility.IsObjectVisible(button)) return;
			
			var r:Rectangle = button.getRect(root);
			if (r.intersects(new Rectangle(e.stageX, e.stageY, 1, 1)))
			{
				button.transform.colorTransform = new ColorTransform(0.5, 0.5, 0.5);
				stage.addEventListener(MouseEvent.MOUSE_UP, button_onMouseUp);
				stage.addEventListener(MouseEvent.MOUSE_MOVE, button_onMouseMove);
			}
		}
		private function button_onMouseUp(e:Event):void
		{
			button.transform.colorTransform = new ColorTransform();
			stage.removeEventListener(MouseEvent.MOUSE_UP, button_onMouseUp);
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, button_onMouseMove);
		}
		private function button_onMouseMove(e:MouseEvent):void
		{
			var offset:Point = localToGlobal(new Point());
			button.x = e.stageX - button.width / 2 - offset.x;
			if (button.x < -button.width / 2) button.x = -button.width / 2;
			else if (button.x > _width - button.width / 2) button.x = _width - button.width / 2;
			
			var c:Number = Utility.Interpolate(max, min, (button.x + button.width / 2) / _width);
			if (c != current)
			{
				current = c;
				dispatchEvent(new Event(ValueChanged));
			}
		}
	}
}