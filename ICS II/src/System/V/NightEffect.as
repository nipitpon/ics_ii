package System.V 
{
	import flash.display.Bitmap;
	import flash.display.BlendMode;
	import flash.display.Shape;
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class NightEffect extends RenderableObject 
	{
		[Embed(source = "../../../lib/Darkness.png")]
		private static const map:Class;
		private var circle:Shape;
		public function NightEffect() 
		{
			super();
			var bmp:Bitmap = new map();
			bmp.scaleX = 2;
			bmp.scaleY = 2;
			bmp.x = -bmp.width / 2;
			bmp.y = -bmp.height / 2;
			addChild(bmp);
			//circle = new Shape();
			//circle.graphics.beginGradientFill("radial", [0xffffff, 0xffffff], [1, 0], [128, 255]);
			//circle.graphics.drawCircle(0, 0, 100);
			//circle.graphics.endFill();
			//circle.x = 0;
			//circle.y = 0;
			//circle.scaleX = 3;
			//circle.scaleY = 3;
			//graphics.beginFill(0);
			//graphics.drawRect( -1000, -1000, 2000, 2000);
			//graphics.endFill();
			//addChild(circle);
			//blendMode = BlendMode.LAYER;
			//circle.blendMode = BlendMode.ERASE;
			//cacheAsBitmap = true;
			
		}
		
	}

}