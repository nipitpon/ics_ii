package System.V 
{
	import fl.transitions.easing.None;
	import fl.transitions.easing.Regular;
	import fl.transitions.easing.Strong;
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import flash.events.Event;
	import flash.filters.GlowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import System.GameSystem;
	/**
	 * ...
	 * @author Knight
	 */
	public class DamageNumber extends RenderableObject 
	{
		private static const filter:GlowFilter = new GlowFilter(0, 1, 3, 3, 2);
		private var count:uint;
		private var textBox:TextField;
		private var aTween:Tween;
		public function DamageNumber(x:Number, y:Number, color:uint, damage:uint) 
		{
			textBox = new TextField();
			super();
			Initialize(x, y, color, damage);
			count = 0;
		}
		private function Initialize(x:Number, y:Number, color:uint, damage:uint):void
		{
			var format:TextFormat = new TextFormat("Tahoma", 14, color, true, false, false);
			format.align = TextFormatAlign.CENTER;
			
			textBox.defaultTextFormat = format;
			textBox.text = damage.toString();
			textBox.textColor = color;
			textBox.width = 200;
			textBox.height = 20;
			textBox.x = -100;
			textBox.y = -20;
			textBox.filters = [filter];
			textBox.selectable = false;
			addChild(textBox);
			this.x = x;
			this.y = y;
			
			aTween = new Tween(this, "alpha", Strong.easeOut, 100, 0, 3, true);
			aTween.start();
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		private function onEnterFrame(e:Event):void
		{
			if (GameSystem.IsFreezing)
			{
				if (aTween.isPlaying) aTween.stop();
				return;
			}
			if (!aTween.isPlaying) aTween.resume();
			
			this.y -= 75 / 90.0;
			if (++count == 90)
				onFinishAnimation(null);
		}
		private function onFinishAnimation(e:Event):void
		{
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			parent.removeChild(this);
		}
	}

}