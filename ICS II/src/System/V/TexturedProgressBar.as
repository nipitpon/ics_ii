package System.V 
{
	import flash.filters.GlowFilter;
	/**
	 * ...
	 * @author Knight
	 */
	public class TexturedProgressBar extends RenderableObject 
	{
		private var bg:RenderableObject;
		private var fg:RenderableObject;
		private var _width:Number;
		private var _height:Number;
		private var min:Number = 0;
		private var max:Number = 100;
		private var current:Number = max;
		private static const filter:GlowFilter = new GlowFilter(0, 1, 2, 2, 2);
		public function TexturedProgressBar(background:RenderableObject, foreground:RenderableObject, width:Number, height:Number) 
		{
			bg = background;
			fg = foreground;
			_width = width;
			_height = height;
			Initialize();
		}
		public function get Max():Number { return max; }
		public function get Min():Number { return min; }
		public function get CurrentValue():Number { return current; }
		public override function get width():Number { return _width; }
		public override function get height():Number { return _height; }
		public function set Max(val:Number):void 
		{
			max = val;
			Refresh();
		}
		public function set Min(val:Number):void
		{
			min = val;
			Refresh();
		}
		public function set CurrentValue(val:Number):void
		{
			current = val;
			Refresh();
		}
		
		private function Initialize():void
		{
			addChild(bg);
			addChild(fg);
			bg.width = width;
			filters = [filter];
			Refresh();
		}
		private function Refresh():void
		{
			fg.width = ((current - min) / (max - min)) * width;
		}
	}
}