package System.V 
{
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import Resources.BlackMirror.Level.Level1.CloseBackground1;
	import Resources.Image;
	import System.GameFrame;
	import System.GameSystem;
	import System.Time;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class Sky extends Image 
	{
		private var prevColor:int;
		private var dark_cloud:Number;
		private var dark_cloud_change:Boolean;
		public function Sky() 
		{
			super();
			addChild(new CloseBackground1());
			graphics.beginFill(0xffffff, 1);
			graphics.drawRect( -100, -100, 1000, 800);
			graphics.endFill();
			prevColor = 0x123456;
			dark_cloud = 0;
			dark_cloud_change = false;
			cacheAsBitmap = true;
		}
		public function set DarkCloudRate(val:Number):void
		{
			if (val != dark_cloud)
			{
				dark_cloud = val;
				dark_cloud_change = true;
			}
		}
		public override function Update():void
		{
			super.Update();
			var color:int = Time.SkyColor;
			if (color != prevColor || dark_cloud_change)
			{
				dark_cloud_change = false;
				
				prevColor = color;
				
				var cTransform:ColorTransform = transform.colorTransform;
				var red:Number = ((color & 0xff0000) >> 16) / 255.0;
				var green:Number = ((color & 0x00ff00) >> 8) / 255.0;
				var blue:Number = ((color & 0x0000ff) / 255.0);
				var greyScale:Number = (red + green + blue) / 6;
				
				red = Utility.Interpolate(greyScale, red, dark_cloud);
				green = Utility.Interpolate(greyScale, green, dark_cloud);
				blue = Utility.Interpolate(greyScale, blue, dark_cloud);
				
				cTransform.redMultiplier = red;
				cTransform.greenMultiplier = green;
				cTransform.blueMultiplier = blue;
				
				transform.colorTransform = cTransform;
			}
		}
	}
}