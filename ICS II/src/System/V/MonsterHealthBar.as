package System.V 
{
	/**
	 * ...
	 * @author Knight
	 */
	public class MonsterHealthBar extends ProgressBar 
	{
		public function MonsterHealthBar(width:Number, height:Number) 
		{
			super(width, height, 4, 0xff0000, 0x7ABA1B);
		}
	}
}