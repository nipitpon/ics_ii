package System.V 
{
	import _Base.MyTextField;
	import flash.display.Bitmap;
	import flash.display.Shape;
	import flash.filters.GlowFilter;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	/**
	 * ...
	 * @author Knight
	 */
	public class ProgressBar extends RenderableObject 
	{
		[Embed(source = "../../../lib/GUI/Heat Bar BG.png")]
		private static const healthbar_bg:Class;
		[Embed(source = "../../../lib/GUI/Heat Bar.png")]
		private static const healthbar_fg:Class;
		private var background:Bitmap;
		private var foreground:Shape;
		private var border:Bitmap;
		private var min:Number = 0;
		private var max:Number = 100;
		private var current:Number = max;
		private var padding:Number;
		private var bgColor:uint;
		private var fgColor:uint;
		private var text:MyTextField;
		public function ProgressBar(width:Number, height:Number, padding:Number, bgColor:uint, fgColor:uint) 
		{
			background = new healthbar_bg();
			foreground = new Shape();
			border = new healthbar_fg();
			background.scaleX = 2;
			background.scaleY = 2;
			border.scaleX = 2;
			border.scaleY = 2;
			text = new MyTextField();
			this.padding = padding;
			this.bgColor = bgColor;
			this.fgColor = fgColor;
			Initialize(width, height, padding, bgColor, fgColor);
		}
		public function set Max(val:Number):void 
		{
			max = val;
			RefreshForeground();
		}
		public function set Min(val:Number):void
		{
			min = val;
			RefreshForeground();
		}
		public function set CurrentValue(val:Number):void
		{
			current = val;
			RefreshForeground();
		}
		public function get Max():Number { return max; }
		public function get Min():Number { return min; }
		public function get CurrentValue():Number { return current; }
		
		protected function set ShowText(val:Boolean):void { text.visible = val; }
		protected function get ShowText():Boolean { return text.visible; }
		private function Initialize(width:Number, height:Number, padding:Number, bgColor:uint, fgColor:uint):void
		{
			addChild(background);
			addChild(foreground);
			addChild(border);
			addChild(text);
			
			foreground.graphics.beginFill(fgColor);
			foreground.graphics.drawRect(padding / 2, padding / 2, width - padding, height - padding / 2);
			foreground.graphics.endFill();
			
			//border.graphics.lineStyle(padding, (40 << 16) | (15 << 8) | 9);
			//border.graphics.drawRect(padding / 2, padding / 2, width - padding, height - padding);
			
			text.visible = false;
			text.defaultTextFormat = new TextFormat("Tahoma", 14, 0xffffff, true);
			text.autoSize = TextFieldAutoSize.CENTER;
			text.filters = [new GlowFilter(0, 1, 3, 3, 2)];
			text.text = "20";
			text.x = (width - text.textWidth) / 2;
			text.y = (height - text.textHeight) / 2;
		}
		private function RefreshForeground():void
		{
			var percent:Number = (current - min) / (max - min);
			var newWidth:Number = (background.width - (padding)) * percent;
			foreground.graphics.clear();
			foreground.graphics.beginFill(fgColor);
			foreground.graphics.drawRect(padding, padding, newWidth, background.height - (padding * 2));
			foreground.graphics.endFill();
			text.text = int(current).toString() + " / " + Max;
		}
	}
}