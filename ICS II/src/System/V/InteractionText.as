package System.V 
{
	import _Base.MyTextField;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	/**
	 * ...
	 * @author Knight
	 */
	public class InteractionText extends MyTextField 
	{
		
		public function InteractionText() 
		{
			//0xAF814D
			var format:TextFormat = new TextFormat("Pixel Font", 24, 0xffffff, false, false, false, null, null, TextFormatAlign.CENTER);
			x = 0;
			y = 0;
			this.embedFonts = true;
			defaultTextFormat = format;
			text = "LOLOL";
			width = 800;
			filters = [new DropShadowFilter(2, 90, 0x492313, 1, 2, 2, 2)];
		}
	}
}