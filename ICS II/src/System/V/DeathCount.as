package System.V 
{
	import _Base.MyTextField;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import Resources.lib.GUI.SkullImage;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class DeathCount extends RenderableObject 
	{
		private var text:MyTextField;
		public function DeathCount() 
		{
			super();
			addChild(new SkullImage());
			text = new MyTextField();
			text.defaultTextFormat = new TextFormat("Pixel Font", 60, 0xffffff, true);
			text.autoSize = TextFieldAutoSize.LEFT;
			text.text = "99";
			text.x = 40;
			text.y = 3;
			addChild(text);
		}
		public function set NumOfDeath(val:int):void 
		{ 
			if (val >= 0)
				text.text = (val < 100 ? "0" : "") + (val < 10 ? "0" : "") + val.toString(); 
			else
				text.text = "-";
		}
		public function get NumOfDeath():int { return int(text.text); }
	}

}