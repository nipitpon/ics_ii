package System.V 
{
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	import System.MC.Tile;
	/**
	 * ...
	 * @author Knight
	 */
	public class TileSprite extends RenderableObject 
	{
		
		public function TileSprite() 
		{
			
		}
		public override function get width():Number { return Tile.Size.x; }
		public override function get height():Number { return Tile.Size.y; }
	}

}