package System.V 
{
	import fl.transitions.easing.None;
	import fl.transitions.easing.Regular;
	import fl.transitions.easing.Strong;
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.filters.GlowFilter;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import System.GameSystem;
	/**
	 * ...
	 * @author Knight
	 */
	public class FloatingText extends RenderableObject 
	{
		private static const filter:GlowFilter = new GlowFilter(0, 1, 3, 3, 2);
		private var count:uint;
		private var textBox:TextField;
		private var aTween:Tween;
		private var destinationSize:Number;
		public function FloatingText(x:Number, y:Number, color:uint, text:String, textSize:int = 20, bounce:Boolean = false, destSize:Number = 1) 
		{
			textBox = new TextField();
			super();
			Initialize(x, y, color, text, bounce, textSize);
			count = 0;
			destinationSize = destSize;
		}
		private function Initialize(x:Number, y:Number, color:uint, text:String, bounce:Boolean, textSize:int):void
		{
			var format:TextFormat = new TextFormat("Tahoma", textSize, color, true, false, false);
			format.align = TextFormatAlign.CENTER;
			
			textBox.defaultTextFormat = format;
			textBox.autoSize = TextFieldAutoSize.LEFT;
			textBox.text = text;
			textBox.textColor = color;
			textBox.width = 200;
			textBox.x = -100;
			textBox.y = -20;
			textBox.filters = [filter];
			textBox.selectable = false;
			this.x = x - textBox.width / 2;
			this.y = y - textBox.height / 2;
			
			var dat:BitmapData = new BitmapData(textBox.width, textBox.height, true, 0);
			dat.draw(textBox);
			addChild(new Bitmap(dat));
			aTween = new Tween(this, "alpha", Strong.easeOut, 100, 0, 3, true);
			aTween.start();
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
			
			if (false && bounce)
			{
				var hTween:Tween = new Tween(this, "scaleY", None.easeNone, 1, 0.5, 0.1, true);
				hTween.addEventListener(TweenEvent.MOTION_FINISH, tweenBounce_Finish1);
				hTween.start();
				var vTween:Tween = new Tween(this, "scaleX", None.easeNone, 1, 0.5, 0.1, true);
				vTween.start();
			}
		}
		private function tweenBounce_Finish1(e:Event):void
		{
			var hTween:Tween = new Tween(this, "scaleY", None.easeNone, 0.5, destinationSize, 0.2, true);
			hTween.start();
			var vTween:Tween = new Tween(this, "scaleX", None.easeNone, 0.5, destinationSize, 0.2, true);
			vTween.start();
		}
		private function onEnterFrame(e:Event):void
		{
			if (GameSystem.IsFreezing && !GameSystem.IsGameOver)
			{
				if (aTween.isPlaying) aTween.stop();
				return;
			}
			if (!aTween.isPlaying) aTween.resume();
			
			this.y -= 75 / 90.0;
			if (++count == 90)
				onFinishAnimation(null);
		}
		private function onFinishAnimation(e:Event):void
		{
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			//Could be already removed from stage.
			if(parent != null)
				parent.removeChild(this);
		}
	}

}