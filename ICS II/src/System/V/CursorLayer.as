package System.V 
{
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import Resources.BlackMirror.GUI.CursorImage;
	import System.Interfaces.IReceiveControl;
	
	/**
	 * ...
	 * @author Knight
	 */
	public class CursorLayer extends RenderableObject implements IReceiveControl 
	{
		private var cursor:CursorImage;
		private var hotspot:Shape;
		public function CursorLayer() 
		{
			super();
			addChild(cursor = new CursorImage());
			visible = false;
			cursor.SetAnimation(0);
			//addChild(hotspot = new Shape());
			//hotspot.graphics.beginFill(0);
			//hotspot.graphics.drawCircle(0, 0, 5);
			//hotspot.graphics.endFill();
		}
		public function KeyPress(id:int):void {}
		public function KeyRelease(id:int):void {}
		public function MousePress(x:Number, y:Number):void { cursor.SetAnimation(1); }
		public function MouseMove(x:Number, y:Number):void
		{ 
			cursor.x = x; 
			cursor.y = y;
			//hotspot.x = x;
			//hotspot.y = y;
		}
		public function MouseRelease(x:Number, y:Number):void { cursor.SetAnimation(0); }
		public function get IsReceivingControl():Boolean { return visible; }
	}

}