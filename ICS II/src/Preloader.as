package 
{
	import _Base.MyTextField;
	import com.spilgames.gameapi.components.Logo;
	import com.spilgames.gameapi.GameAPI;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.filters.BlurFilter;
	import flash.geom.ColorTransform;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.ui.Keyboard;
	import flash.utils.getDefinitionByName;
	import HardCode.Pages.Preloader.Logo370x100;
	import HardCode.Pages.Preloader.Sponsor500x500;
	import Resources.lib.Other_pages.PreloaderBackground;
	import Resources.lib.Other_pages.PreloaderGoImage;
	import Resources.lib.Other_pages.PreloaderLoadBar;
	import Resources.lib.Other_pages.PreloaderLogo;
	//import com.newgrounds.API;
	/**
	 * ...
	 * @author Knight
	 */
	public class Preloader extends MovieClip 
	{
		private var go:PreloaderGoImage;
		private var load_bar:PreloaderLoadBar;
		private var finished:Boolean;
		private var fader:Fader;
		private var error_text:TextField;
		public function Preloader() 
		{
			var api:GameAPI = GameAPI.getInstance();
			if (stage) {
				stage.scaleMode = StageScaleMode.EXACT_FIT;
				stage.align = StageAlign.TOP_LEFT;
				api.addEventListener(Event.COMPLETE, onAPICompleted);
				api.loadAPI("576742227280298418", stage);
				//API.connect(root, "42879:orxPNm7a", "xof9KjtV6kt7HcrlpxlF70bhf29sWyU6");
			}
			addEventListener(Event.ENTER_FRAME, checkFrame);
			loaderInfo.addEventListener(ProgressEvent.PROGRESS, progress);
			loaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioError);
			addChild(new PreloaderBackground());
			fader = new Fader();
			load_bar = new PreloaderLoadBar();
			load_bar.x = 130;
			load_bar.y = 492;
			finished = false;
			addChild(load_bar);
			var logo:PreloaderLogo = new PreloaderLogo();
			logo.x = 124;
			logo.y = 316;
			addChild(logo);
			go = new PreloaderGoImage();
			go.x = load_bar.x;
			go.y = load_bar.y;
			addChild(go);
			go.visible = false;
			error_text = new TextField();
			error_text.defaultTextFormat = new TextFormat(null, null, 0xffffff);
			addChild(error_text);
		}
		
		private function ioError(e:IOErrorEvent):void 
		{
			error_text.appendText("Error: " + e.text + "\n");
		}
		
		private function progress(e:ProgressEvent):void 
		{
			// TODO update loader
			var progress:Number = e.bytesLoaded / e.bytesTotal;
			//progressText.text = Math.round(e.bytesLoaded / 1024) + "kb / " + Math.round(e.bytesTotal / 1024) + "kb";
			load_bar.scaleX = progress;
		}
		
		private function checkFrame(e:Event):void 
		{
			if (currentFrame == totalFrames) 
			{
				stop();
				loadingFinished();
			}
		}
		
		private function loadingFinished():void 
		{
			try
			{
			finished = true;
			removeEventListener(Event.ENTER_FRAME, checkFrame);
			loaderInfo.removeEventListener(ProgressEvent.PROGRESS, progress);
			loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, ioError);
			
			load_bar.scaleX = 1;
			if (GameAPI.getInstance().isReady)
			{
				// TODO hide loader
					go.visible = true;
					load_bar.visible = false;
					go.addEventListener(MouseEvent.CLICK, go_onClick);
					go.addEventListener(MouseEvent.ROLL_OVER, go_onMouseOver);
					go.addEventListener(MouseEvent.ROLL_OUT, go_onMouseLeave);
				//startup();
			}
			}
			catch (error:Error)
			{
				error_text.text = "Error: " + error.message + "\n";
			}
		}
		private function onAPICompleted(e:Event):void
		{
			try
			{
			var bmp:Logo = GameAPI.getInstance().Branding.getLogo();
			bmp.x = 520;
			bmp.y = 429;
			if(GameAPI.getInstance().Branding.isReady)
				addChild(bmp);
			bmp.addEventListener(MouseEvent.ROLL_OVER, function(e:Event):void
			{
				bmp.transform.colorTransform = new ColorTransform(1, 1, 1, 1, 64, 64, 64);
			});
			bmp.addEventListener(MouseEvent.ROLL_OUT, function(e:Event):void
			{
				bmp.transform.colorTransform = new ColorTransform();
			});
			bmp.addEventListener(MouseEvent.MOUSE_DOWN, function(e:Event):void
			{
				bmp.transform.colorTransform = new ColorTransform(1, 1, 1, 1, -64, -64, -64);
			});
			bmp.addEventListener(MouseEvent.MOUSE_UP, function(e:Event):void
			{
				bmp.transform.colorTransform = new ColorTransform(1, 1, 1, 1, 64, 64, 64);
			});
			
			if (load_bar.scaleX == 1)
				loadingFinished();
			}
			catch (error:Error)
			{
				error_text.text = error.message;
			}
		}
		private function go_onMouseOver(e:Event):void
		{
			go.transform.colorTransform = new ColorTransform(1, 1, 1, 1, 96, 96, 96);
		}
		private function go_onMouseLeave(e:Event):void
		{
			go.transform.colorTransform = new ColorTransform();
		}
		private function go_onClick(e:Event):void
		{
			if (false && CONFIG::release && this.loaderInfo.url.indexOf("knight52.somee.com") < 0) 
			{
				var txt:MyTextField = new MyTextField(40, 0xff0000);
				txt.text = "Stolen property!!!";
				txt.x = Utility.Random(0, 600);
				txt.y = Utility.Random(0, 500);
				txt.width = 800;
				addChild(txt);
			}
			else
			{
				go.removeEventListener(MouseEvent.CLICK, go_onClick);
				go.removeEventListener(MouseEvent.ROLL_OVER, go_onMouseOver);
				go.removeEventListener(MouseEvent.ROLL_OUT, go_onMouseLeave);
				addChild(fader);
				fader.addEventListener(Fader.FaderFadedIn, startup);
				fader.FadeIn(null, 0, "");
			}
		}
		private function startup(e:Event = null):void 
		{
			var main:Class = getDefinitionByName("Main") as Class;
			stage.addChild(new main() as DisplayObject);
			visible = false;
		}
	}
}