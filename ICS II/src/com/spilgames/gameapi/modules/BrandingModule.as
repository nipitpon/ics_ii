package com.spilgames.gameapi.modules
{
	import com.spilgames.gameapi.GameAPI;
	import com.spilgames.gameapi.GameAPIEvent;
	import com.spilgames.gameapi.GameAPIModule;
	import com.spilgames.gameapi.components.GameAPIBtn;
	import com.spilgames.gameapi.components.Logo;
	import com.spilgames.gameapi.components.SplashScreen;
	
	import flash.display.Sprite;

	public class BrandingModule extends GameAPIModule
	{
		/*
		* The Module name
		*/
		public static const MODULE_NAME:String = "Branding";
		
		/*
		 * The available GameAPI methods
		*/
		public static const GET_LOGO:String = "getLogo";
		public static const GET_LINK:String = "getLink";
		public static const GET_SPLASH_SCREEN:String = "getSplashScreen";
		
		public function BrandingModule()
		{
			
		}
		
		public function getLogo():Logo
		{
			return new Logo();
		}
		
		public function getLink(label:String):GameAPIBtn
		{
			return new GameAPIBtn(label);
		}
		
		public function getSplashScreen():Boolean
		{
			var result:Boolean = true;
			
			if (GameAPI.getInstance().isReady && !GameAPI.getInstance().isDebug) {
				var splashscreen:SplashScreen = new SplashScreen();
				result = splashscreen.show;
			}
			
			return result;
		}
	}
}