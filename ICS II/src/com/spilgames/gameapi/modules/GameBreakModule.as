package com.spilgames.gameapi.modules
{
	import com.spilgames.gameapi.GameAPI;
	import com.spilgames.gameapi.GameAPIModule;

	public class GameBreakModule extends GameAPIModule
	{
		/*
		* The Module name
		*/
		public static const MODULE_NAME:String = "GameBreak";
		
		/*
		* The available GameAPI methods
		*/
		public static const REQUEST:String = "request";
		
		public function GameBreakModule()
		{
			
		}
		
		public function request(pause:Function, resume:Function):void
		{
			if (GameAPI.getInstance().isReady && !GameAPI.getInstance().isDebug) {
				var params:Object = {};
				params.pause = pause;
				params.resume = resume;
				
				GameAPI.getInstance().callGameAPI(GameBreakModule.MODULE_NAME, GameBreakModule.REQUEST, params);
			} else {
				resume();
			}
		}
	}
}