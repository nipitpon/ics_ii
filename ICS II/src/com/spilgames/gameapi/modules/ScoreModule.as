package com.spilgames.gameapi.modules
{
	import com.spilgames.gameapi.GameAPI;
	import com.spilgames.gameapi.GameAPIModule;

	public class ScoreModule extends GameAPIModule
	{
		/*
		* The Module name
		*/
		public static const MODULE_NAME:String = "Score";
		
		/*
		* The available GameAPI methods
		*/
		public static const SUBMIT:String = "submit";
		
		public function ScoreModule()
		{
			
		}
		
		public function submit(score:int = 0):void
		{
			if (GameAPI.getInstance().isReady && !GameAPI.getInstance().isDebug) {
				GameAPI.getInstance().callGameAPI(ScoreModule.MODULE_NAME, ScoreModule.SUBMIT, score);
			}
		}
	}
}