package com.spilgames.gameapi.modules
{
	import com.spilgames.gameapi.GameAPI;
	import com.spilgames.gameapi.GameAPIModule;
	
	import flash.external.ExternalInterface;
	
	public class GameModule extends GameAPIModule
	{
		/*
		* The Module name
		*/
		public static const MODULE_NAME:String = "Game";
		
		/*
		* The available GameAPI methods
		*/
		public static const IS_SITE_LOCK:String = "isSiteLock";
		
		public function GameModule()
		{
			
		}
		
		public function isSiteLock():Boolean
		{
			var sitelock:Boolean = true;
			
			if (GameAPI.getInstance().isReady && !GameAPI.getInstance().isDebug) {
				sitelock = GameAPI.getInstance().callGameAPI(GameModule.MODULE_NAME, GameModule.IS_SITE_LOCK);
			}
			
			return sitelock;
		}
	}
}