package com.spilgames.gameapi.modules
{
	import com.spilgames.gameapi.GameAPI;
	import com.spilgames.gameapi.GameAPIModule;

	public class AwardModule extends GameAPIModule
	{
		/*
		* The Module name
		*/
		public static const MODULE_NAME:String = "Award";
		
		/*
		* The available GameAPI methods
		*/
		public static const SUBMIT:String = "submit";
		
		public function AwardModule()
		{
			
		}
		
		public function submit(awardLabel:String = ""):void
		{
			if (GameAPI.getInstance().isReady && !GameAPI.getInstance().isDebug) {
				GameAPI.getInstance().callGameAPI(AwardModule.MODULE_NAME, AwardModule.SUBMIT, awardLabel);
			}
		}
	}
}