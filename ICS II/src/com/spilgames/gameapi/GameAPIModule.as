package com.spilgames.gameapi
{
	import flash.events.EventDispatcher;

	public class GameAPIModule extends EventDispatcher
	{
		// ====================================
		// private properties
		// ====================================
		
		/*
		* The Module name
		*/
		private var _moduleName:String = "";
		
		// ====================================
		// public properties
		// ====================================
		
		/*
		* Indicates if the Module is initialized and ready to use
		*/
		public var isReady:Boolean = false;
		
		// ====================================
		// public methods
		// ====================================
		
		public function GameAPIModule()
		{
			
		}
		
		public function init():void
		{
			isReady = true;
		}
	}
}