package com.spilgames.gameapi
{
	import flash.events.Event;
	
	public class GameAPIEvent extends Event
	{
		public static const GAMEAPI_CALL:String = "game.API.call";
		
		private var _moduleName:String;
		private var _methodName:String;
		private var _args:Object;
		
		public function GameAPIEvent(type:String, moduleName:String, methodName:String, args:Object = null, bubbles:Boolean=false)
		{
			super(type, bubbles);
			_moduleName = moduleName;
			_methodName = methodName;
			_args = args;
		}
		
		public function get moduleName():String { return _moduleName; }
		public function get methodName():String { return _methodName; }
		public function get args():Object { return _args; }
	}
}
