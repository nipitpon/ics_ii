package com.spilgames.gameapi.components
{
	import com.spilgames.gameapi.GameAPI;
	import com.spilgames.gameapi.modules.BrandingModule;
	
	import flash.display.Sprite;

	public class Logo extends Sprite
	{
		private var _gameapiLogoImpl:*;
		
		public function Logo()
		{
			if (GameAPI.getInstance().isReady && !GameAPI.getInstance().isDebug) {
				_gameapiLogoImpl = GameAPI.getInstance().callGameAPI(BrandingModule.MODULE_NAME, BrandingModule.GET_LOGO);
				addChild(_gameapiLogoImpl);
			}
		}
		
		public function action():void
		{
			if (GameAPI.getInstance().isReady && !GameAPI.getInstance().isDebug) {
				if (_gameapiLogoImpl && _gameapiLogoImpl['action']) _gameapiLogoImpl['action']();
			}
		}
	}
}