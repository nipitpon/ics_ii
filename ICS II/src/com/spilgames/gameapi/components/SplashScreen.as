package com.spilgames.gameapi.components
{
	import com.spilgames.gameapi.GameAPI;
	import com.spilgames.gameapi.modules.BrandingModule;
	
	import flash.display.Sprite;
	
	public class SplashScreen extends Sprite
	{
		private var _gameapiSplashScreenImpl:*;
		public var _show:Boolean = true;
		
		public function SplashScreen()
		{
			if (GameAPI.getInstance().isReady && !GameAPI.getInstance().isDebug) {
				_gameapiSplashScreenImpl = GameAPI.getInstance().callGameAPI(BrandingModule.MODULE_NAME, BrandingModule.GET_SPLASH_SCREEN);
				_show = _gameapiSplashScreenImpl.show;
			}
		}
		
		public function get show():Boolean
		{
			return _show;
		}
	}
}