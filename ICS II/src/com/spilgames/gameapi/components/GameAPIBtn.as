package com.spilgames.gameapi.components
{
	import com.spilgames.gameapi.GameAPI;
	import com.spilgames.gameapi.modules.BrandingModule;
	
	import flash.display.Sprite;
	
	public class GameAPIBtn extends Sprite
	{
		private var _gameapiBtnImpl:*;
		
		public function GameAPIBtn(label:String = "")
		{
			if (GameAPI.getInstance().isReady && !GameAPI.getInstance().isDebug) {
				_gameapiBtnImpl = GameAPI.getInstance().callGameAPI(BrandingModule.MODULE_NAME, BrandingModule.GET_LINK, label);
				addChild(_gameapiBtnImpl);
			}
		}
		
		public function action():void
		{
			if (GameAPI.getInstance().isReady && !GameAPI.getInstance().isDebug) {
				if (_gameapiBtnImpl && _gameapiBtnImpl['action']) _gameapiBtnImpl['action']();
			}
		}
	}
}