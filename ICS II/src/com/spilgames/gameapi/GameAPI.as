package com.spilgames.gameapi
{
	import com.spilgames.gameapi.modules.AwardModule;
	import com.spilgames.gameapi.modules.BrandingModule;
	import com.spilgames.gameapi.modules.GameBreakModule;
	import com.spilgames.gameapi.modules.ScoreModule;
	import com.spilgames.gameapi.modules.GameModule;
	import com.spilgames.gameapi.utils.APILoader;

	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	
	public class GameAPI extends Sprite
	{
		// ====================================
		// private properties
		// ====================================
		
		/*
		* Contains a reference to the GameAPI instance
		*/
		private static const NOT_READY_ERROR_MESSAGE:String = "GameAPI not ready";
		
		/*
		* Contains a reference to the GameAPI instance
		*/
		private static var _instance:GameAPI;
		
		/*
		* The reference to the remote API client
		*/
		private var _apiClient:*;
		
		/*
		 * The instance of APILoader class responsible for loading the remote API client
		*/
		private var _apiLoader:APILoader;
		
		/*
		* Contains a reference to the callback from the game
		*/
		private var _loadCallback:Function;
		
		/*
		* Contains the Id for the Game's configuration
		*/
		private var _gameId:String = "";
		
		/*
		* Contains a reference to the Stage
		*/
		private var _stg:Stage;
		
		/*
		* The group of all API modules
		*/
		private var _modules:Array = [];
		
		// ====================================
		// public properties
		// ====================================
		
		/*
		* Contains the Flash distribution API client version
		*/
		public const version:String = '';
		
		/*
		* Indicates if the API is loaded, initialized and ready to use
		*/
		public var isReady:Boolean = false;
		
		/*
		* Indicates if the API is on debug mode or not
		* when on debug mode the remote client will not be loaded and no calls will be done
		*/
		public var isDebug:Boolean = false;
		
		// ====================================
		// GameAPI modules
		// ====================================
		public var Branding:BrandingModule;
		public var Score:ScoreModule;
		public var Award:AwardModule;
		public var GameBreak:GameBreakModule;
		public var Game:GameModule;
		
		public function GameAPI(access:Private = null)
		{
			super();
			initConstructor(access);
		}
		
		// ====================================
		// STATIC METHODS
		// ====================================
		
		/**
		 * Returns the sole instance of the GameAPI class.
		 * 
		 * @return Instance of the GameAPI class.
		 */
		public static function getInstance():GameAPI
		{
			if (!_instance)
			{
				_instance = new GameAPI(new Private());
			}
			
			return _instance;
		}
		
		// ====================================
		// PUBLIC METHODS
		// ====================================
		
		/**
		 * @public
		 * 
		 * @param apiLocation
		 * @param spilData
		 * @param debugMode
		 * 
		 */
		public function loadAPI(gameId:String, stage:Stage, debugMode:Boolean = false):void
		{
			if (!isReady) {
				_gameId = gameId;
				_stg = stage;
				isDebug = debugMode;
				
				_apiLoader = new APILoader();
				_stg.addChild(_apiLoader);
				_apiLoader.addEventListener(Event.COMPLETE, onAPILoadComplete);
				_apiLoader.loadAPI();
			} else {
				dispatchEvent(new Event(Event.COMPLETE));
			}
		}
		
		public function callGameAPI(moduleName:String, methodName:String, args:Object = null):*
		{
			if (isReady) {
				return _apiClient["callGameAPI"](moduleName, methodName, args);
			}
		}
		
		// ====================================
		// PRIVATE METHODS
		// ====================================
		
		private function addHandlers():void
		{
			_apiLoader.addEventListener(Event.COMPLETE, onAPILoadComplete);
			_apiLoader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			_apiLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
		}
		
		private function removeHandlers():void
		{
			_apiLoader.removeEventListener(Event.COMPLETE, onAPILoadComplete);
			_apiLoader.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			_apiLoader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
		}
		
		private function ioErrorHandler(e:IOErrorEvent):void
		{
			dispatchEvent(e);
		}
		
		private function securityErrorHandler(e:SecurityErrorEvent):void
		{
			dispatchEvent(e);
		}
		
		private function onAPILoadComplete(e:Event):void
		{
			var options:Object = {};
			options.version = version;
			
			_apiClient = _apiLoader.loadedRem;
			_apiClient.addEventListener(Event.COMPLETE, onAPIInitialized);
			_apiClient["init"](_gameId, _stg, options);
		}
		
		private function onAPIInitialized(e:Event):void
		{
			isReady = true;
			
			for (var i:int = 0; i < _modules.length; i++)
			{
				_modules[i].init();
			}
			
			dispatchEvent(e);
		}
		
		/**
		 * Initialize the constructor.
		 * 
		 * @param access
		 * @throws Error Cannot instantiate this class directly, use
		 *               <code>GameAPI.getInstance()</code> instead.
		 */		
		private function initConstructor(access:Private):void
		{
			if (!access)
			{
				throw new Error("Cannot instantiate this class directly, " +
					"use GameAPI.getInstance() instead.");
			} else {
				
				Branding = new BrandingModule();
				_modules.push(Branding);
				
				Score = new ScoreModule();
				_modules.push(Score);
				
				Award = new AwardModule();
				_modules.push(Award);
				
				GameBreak = new GameBreakModule();
				_modules.push(Award);
				
				Game = new GameModule();
				_modules.push(Game);
			}
		}
	}
}

/**
 * @private
 */
internal class Private {}