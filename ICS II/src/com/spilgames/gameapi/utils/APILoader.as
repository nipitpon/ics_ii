package com.spilgames.gameapi.utils
{
	import flash.display.Sprite;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	import flash.system.SecurityDomain;
	import flash.system.Security;
	
	public class APILoader extends Sprite
	{
		/*
		* Contains the location where the remove API client lives
		*/
		private const GAMEAPI_LOCATION:String = "http://files.cdn.spilcloud.com/gameapi_flash";
		private const GAMEAPI_FILE:String = "/GameAPI_AS3_Rem.swf";
		
		/*
		* The instance of the Loader class responsible for loading the remote GameAPI client
		*/
		private var _loader:Loader;

		/*
		* The instance of the Loaded remote
		*/
		private var _loadedRem:*;
		
		/*
		* Contains a Boolean indicating if the remote client was already loaded
		*/
		private var _loadComplete:Boolean = false;
		
		public function APILoader()
		{
			_loader = new Loader();
		}
		
		// ====================================
		// PUBLIC METHODS
		// ====================================
		
		public function get loadedRem():* 
		{
			return _loadedRem;
		}
		
		public function get loadComplete():Boolean
		{
			return _loadComplete;
		}
		
		public function loadAPI():void
		{	
			Security.allowDomain("*");
			Security.allowInsecureDomain("*");
			
			addHandlers();
			
			var noCache:String = "?nocache=" + String(Math.floor(Math.random() * 99999));
			
			var ldrContext:LoaderContext = new LoaderContext(false, ApplicationDomain.currentDomain);
			if (Security.sandboxType == Security.REMOTE) 
				ldrContext.securityDomain = SecurityDomain.currentDomain;
			
			addChild(_loader);
			
			var urlReq:URLRequest = new URLRequest(GAMEAPI_LOCATION + GAMEAPI_FILE + noCache);
			
			try {
				_loader.load(urlReq, ldrContext);
			}catch(e:*){
				dispatchEvent(new Event(Event.COMPLETE));
			}
		}
		
		// ====================================
		// PRIVATE METHODS
		// ====================================
		
		private function addHandlers():void
		{
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadComplete);
			_loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			_loader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
		}
		
		private function removeHandlers():void
		{
			_loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onLoadComplete);
			_loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			_loader.contentLoaderInfo.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
		}
		
		private function ioErrorHandler(e:IOErrorEvent):void
		{
			removeHandlers();
			dispatchEvent(e);
		}
		
		private function securityErrorHandler(e:SecurityErrorEvent):void
		{
			removeHandlers();
			dispatchEvent(e);
		}
		
		private function onLoadComplete(e:Event):void
		{
			removeHandlers();
			
			_loadedRem = LoaderInfo(e.target).content;
			
			_loadComplete = true;
			dispatchEvent(e);
		}
	}
}