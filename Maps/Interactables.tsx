<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Interactables" tilewidth="16" tileheight="16" spacing="1">
 <image source="../../../Dropbox/ICS II/Tiles/Interactables.png" width="118" height="492"/>
 <terraintypes>
  <terrain name="New Terrain" tile="-1"/>
 </terraintypes>
 <tile id="0">
  <properties>
   <property name="position" value="water_potion"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="position" value="fire_potion"/>
  </properties>
 </tile>
 <tile id="2" terrain="0,0,0,0">
  <properties>
   <property name="position" value="pushable"/>
  </properties>
 </tile>
 <tile id="3" terrain="0,0,0,0">
  <properties>
   <property name="position" value="pushable"/>
  </properties>
 </tile>
 <tile id="4" terrain="0,0,0,0">
  <properties>
   <property name="position" value="pushable"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="position" value="spike"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="position" value="spike"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="activated" value="8"/>
   <property name="object" value="RedDoor"/>
   <property name="position" value="switch"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="activated" value="10"/>
   <property name="object" value="RedDoor"/>
   <property name="position" value="button"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="activated" value="12"/>
   <property name="position" value="RedDoor"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="deactivated" value="11"/>
   <property name="position" value="RedDoor"/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="directions" value="0,1,2,3"/>
   <property name="position" value="fire_launcher"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="activated" value="15"/>
   <property name="object" value="GreenDoor"/>
   <property name="position" value="switch"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="activated" value="17"/>
   <property name="object" value="GreenDoor"/>
   <property name="position" value="button"/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="activated" value="19"/>
   <property name="position" value="GreenDoor"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="deactivated" value="18"/>
   <property name="position" value="GreenDoor"/>
  </properties>
 </tile>
 <tile id="20">
  <properties>
   <property name="directions" value="0,1"/>
   <property name="position" value="fire_launcher"/>
  </properties>
 </tile>
 <tile id="21">
  <properties>
   <property name="activated" value="22"/>
   <property name="object" value="BlueDoor"/>
   <property name="position" value="switch"/>
  </properties>
 </tile>
 <tile id="23">
  <properties>
   <property name="activated" value="24"/>
   <property name="object" value="BlueDoor"/>
   <property name="position" value="button"/>
  </properties>
 </tile>
 <tile id="25">
  <properties>
   <property name="activated" value="26"/>
   <property name="position" value="BlueDoor"/>
  </properties>
 </tile>
 <tile id="26">
  <properties>
   <property name="deactivated" value="25"/>
   <property name="position" value="BlueDoor"/>
  </properties>
 </tile>
 <tile id="27">
  <properties>
   <property name="directions" value="1,2"/>
   <property name="position" value="fire_launcher"/>
  </properties>
 </tile>
 <tile id="28">
  <properties>
   <property name="bottom" value="35"/>
   <property name="position" value="fountain"/>
  </properties>
  <animation>
   <frame tileid="28" duration="120"/>
   <frame tileid="29" duration="120"/>
   <frame tileid="30" duration="120"/>
  </animation>
 </tile>
 <tile id="31">
  <properties>
   <property name="directions" value="1,3"/>
   <property name="position" value="fire_launcher"/>
  </properties>
 </tile>
 <tile id="32">
  <properties>
   <property name="directions" value="0,2"/>
   <property name="position" value="fire_launcher"/>
  </properties>
 </tile>
 <tile id="33">
  <properties>
   <property name="directions" value="1,2,3"/>
   <property name="position" value="fire_launcher"/>
  </properties>
 </tile>
 <tile id="34">
  <properties>
   <property name="directions" value="2,3"/>
   <property name="position" value="fire_launcher"/>
  </properties>
 </tile>
 <tile id="35">
  <animation>
   <frame tileid="35" duration="120"/>
   <frame tileid="36" duration="120"/>
   <frame tileid="37" duration="120"/>
  </animation>
 </tile>
 <tile id="38">
  <properties>
   <property name="directions" value="0,2,3"/>
   <property name="position" value="fire_launcher"/>
  </properties>
 </tile>
 <tile id="39">
  <properties>
   <property name="directions" value="0,1,3"/>
   <property name="position" value="fire_launcher"/>
  </properties>
 </tile>
 <tile id="40">
  <properties>
   <property name="directions" value="0,1,2"/>
   <property name="position" value="fire_launcher"/>
  </properties>
 </tile>
 <tile id="41">
  <properties>
   <property name="directions" value="0,3"/>
   <property name="position" value="fire_launcher"/>
  </properties>
 </tile>
 <tile id="42">
  <properties>
   <property name="bottom" value="49"/>
   <property name="position" value="exit"/>
  </properties>
 </tile>
 <tile id="43" terrain="0,0,0,0">
  <properties>
   <property name="position" value="ladder"/>
  </properties>
 </tile>
 <tile id="44">
  <properties>
   <property name="directions" value="0,1,2,3"/>
   <property name="position" value="water_launcher"/>
  </properties>
 </tile>
 <tile id="45">
  <properties>
   <property name="directions" value="0"/>
   <property name="position" value="fire_launcher"/>
  </properties>
 </tile>
 <tile id="46">
  <properties>
   <property name="directions" value="1"/>
   <property name="position" value="fire_launcher"/>
  </properties>
 </tile>
 <tile id="47">
  <properties>
   <property name="directions" value="2"/>
   <property name="position" value="fire_launcher"/>
  </properties>
 </tile>
 <tile id="48">
  <properties>
   <property name="directions" value="3"/>
   <property name="position" value="fire_launcher"/>
  </properties>
 </tile>
 <tile id="50">
  <properties>
   <property name="position" value="ladder"/>
  </properties>
 </tile>
 <tile id="51">
  <properties>
   <property name="directions" value="0,1"/>
   <property name="position" value="water_launcher"/>
  </properties>
 </tile>
 <tile id="52">
  <properties>
   <property name="directions" value="1,3"/>
   <property name="position" value="water_launcher"/>
  </properties>
 </tile>
 <tile id="53">
  <properties>
   <property name="directions" value="0,2"/>
   <property name="position" value="water_launcher"/>
  </properties>
 </tile>
 <tile id="54">
  <properties>
   <property name="directions" value="1,2,3"/>
   <property name="position" value="water_launcher"/>
  </properties>
 </tile>
 <tile id="55">
  <properties>
   <property name="directions" value="2,3"/>
   <property name="position" value="water_launcher"/>
  </properties>
 </tile>
 <tile id="56" terrain="0,0,0,0">
  <properties>
   <property name="position" value="platform"/>
  </properties>
 </tile>
 <tile id="57" terrain="0,0,0,0">
  <properties>
   <property name="position" value="platform"/>
  </properties>
 </tile>
 <tile id="58">
  <properties>
   <property name="directions" value="1,2"/>
   <property name="position" value="water_launcher"/>
  </properties>
 </tile>
 <tile id="59">
  <properties>
   <property name="directions" value="0,2,3"/>
   <property name="position" value="water_launcher"/>
  </properties>
 </tile>
 <tile id="60">
  <properties>
   <property name="directions" value="0,1,3"/>
   <property name="position" value="water_launcher"/>
  </properties>
 </tile>
 <tile id="61">
  <properties>
   <property name="directions" value="0,1,2"/>
   <property name="position" value="water_launcher"/>
  </properties>
 </tile>
 <tile id="62">
  <properties>
   <property name="directions" value="0,3"/>
   <property name="position" value="water_launcher"/>
  </properties>
 </tile>
 <tile id="63">
  <properties>
   <property name="activated" value="65"/>
   <property name="charge" value="64"/>
   <property name="position" value="spring"/>
  </properties>
 </tile>
 <tile id="66">
  <properties>
   <property name="directions" value="0"/>
   <property name="position" value="water_launcher"/>
  </properties>
 </tile>
 <tile id="67">
  <properties>
   <property name="directions" value="1"/>
   <property name="position" value="water_launcher"/>
  </properties>
 </tile>
 <tile id="68">
  <properties>
   <property name="directions" value="2"/>
   <property name="position" value="water_launcher"/>
  </properties>
 </tile>
 <tile id="69">
  <properties>
   <property name="directions" value="3"/>
   <property name="position" value="water_launcher"/>
  </properties>
 </tile>
 <tile id="70" terrain="0,0,0,0">
  <properties>
   <property name="position" value="platform"/>
  </properties>
 </tile>
 <tile id="71" terrain="0,0,0,0">
  <properties>
   <property name="position" value="platform"/>
  </properties>
 </tile>
 <tile id="72" terrain="0,0,0,0">
  <properties>
   <property name="position" value="platform"/>
  </properties>
 </tile>
 <tile id="73">
  <properties>
   <property name="position" value="burnable"/>
  </properties>
 </tile>
 <tile id="74">
  <properties>
   <property name="position" value="burnable"/>
  </properties>
 </tile>
 <tile id="75">
  <properties>
   <property name="position" value="burnable"/>
  </properties>
 </tile>
 <tile id="76">
  <properties>
   <property name="position" value="burnable"/>
  </properties>
 </tile>
 <tile id="77">
  <properties>
   <property name="position" value="bed"/>
   <property name="right" value="78"/>
  </properties>
 </tile>
 <tile id="79" terrain="0,0,0,0">
  <properties>
   <property name="position" value="platform"/>
  </properties>
 </tile>
 <tile id="80" terrain="0,0,0,0">
  <properties>
   <property name="position" value="platform"/>
  </properties>
 </tile>
 <tile id="84">
  <properties>
   <property name="position" value="player"/>
  </properties>
 </tile>
 <tile id="85">
  <properties>
   <property name="direction" value="left"/>
   <property name="position" value="yellow_gegon"/>
  </properties>
 </tile>
 <tile id="86">
  <properties>
   <property name="direction" value="right"/>
   <property name="position" value="yellow_gegon"/>
  </properties>
 </tile>
 <tile id="87" terrain="0,0,,">
  <properties>
   <property name="direction" value="left"/>
   <property name="position" value="walldo"/>
  </properties>
 </tile>
 <tile id="88">
  <properties>
   <property name="direction" value="right"/>
   <property name="position" value="walldo"/>
  </properties>
 </tile>
 <tile id="98">
  <properties>
   <property name="direction" value="right"/>
   <property name="place" value="end"/>
   <property name="position" value="moving_platform"/>
  </properties>
 </tile>
 <tile id="99">
  <properties>
   <property name="direction" value="left,right"/>
   <property name="place" value="grab"/>
   <property name="position" value="moving_platform"/>
  </properties>
 </tile>
 <tile id="100">
  <properties>
   <property name="direction" value="left,right"/>
   <property name="place" value="middle"/>
   <property name="position" value="moving_platform"/>
  </properties>
 </tile>
 <tile id="101">
  <properties>
   <property name="direction" value="left"/>
   <property name="place" value="end"/>
   <property name="position" value="moving_platform"/>
  </properties>
 </tile>
 <tile id="102">
  <properties>
   <property name="direction" value="none"/>
   <property name="place" value="grab,end"/>
   <property name="position" value="moving_platform"/>
  </properties>
 </tile>
 <tile id="103">
  <properties>
   <property name="direction" value="right"/>
   <property name="place" value="grab,end"/>
   <property name="position" value="moving_platform"/>
  </properties>
 </tile>
 <tile id="104">
  <properties>
   <property name="direction" value="left"/>
   <property name="place" value="grab,end"/>
   <property name="position" value="moving_platform"/>
  </properties>
 </tile>
 <tile id="105">
  <properties>
   <property name="direction" value="1,2"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="106">
  <properties>
   <property name="direction" value="3,1"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="107">
  <properties>
   <property name="direction" value="3,2"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="108">
  <properties>
   <property name="direction" value="2,1"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="109">
  <properties>
   <property name="direction" value="3,2"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="110">
  <properties>
   <property name="direction" value="2,1"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="111">
  <properties>
   <property name="direction" value="3,2"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="112">
  <properties>
   <property name="direction" value="0,2"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="114">
  <properties>
   <property name="direction" value="0,2"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="115">
  <properties>
   <property name="direction" value="0,1"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="116">
  <properties>
   <property name="direction" value="0,3"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="117">
  <properties>
   <property name="direction" value="0,1"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="118">
  <properties>
   <property name="direction" value="0,3"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="119">
  <properties>
   <property name="direction" value="0,1"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="120">
  <properties>
   <property name="direction" value="3,1"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="121">
  <properties>
   <property name="direction" value="0,3"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="122">
  <properties>
   <property name="direction" value="1"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="123">
  <properties>
   <property name="direction" value="3"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="124">
  <properties>
   <property name="direction" value="1"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="125">
  <properties>
   <property name="direction" value="3"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="129">
  <properties>
   <property name="direction" value="2"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="130">
  <properties>
   <property name="direction" value="0"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="131">
  <properties>
   <property name="direction" value="2"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
 <tile id="132">
  <properties>
   <property name="direction" value="0"/>
   <property name="position" value="rail"/>
  </properties>
 </tile>
</tileset>
