<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Tileset #1" tilewidth="16" tileheight="16" spacing="1">
 <image source="../../../Dropbox/ICS II/Tiles/Tileset #1.png" width="118" height="747"/>
 <terraintypes>
  <terrain name="solid tiles" tile="-1"/>
 </terraintypes>
 <tile id="0" terrain="0,0,0,0"/>
 <tile id="1" terrain="0,0,0,0"/>
 <tile id="2" terrain="0,0,0,0"/>
 <tile id="3" terrain="0,0,0,0"/>
 <tile id="4" terrain="0,0,0,0"/>
 <tile id="5" terrain="0,0,0,0"/>
 <tile id="6" terrain="0,0,0,0"/>
 <tile id="7" terrain="0,0,0,0"/>
 <tile id="8" terrain="0,0,0,0"/>
 <tile id="9" terrain="0,0,0,0"/>
 <tile id="10" terrain="0,0,0,0"/>
 <tile id="11" terrain="0,0,0,0"/>
 <tile id="12" terrain="0,0,0,0"/>
 <tile id="13" terrain="0,0,0,0"/>
 <tile id="14" terrain="0,0,0,0"/>
 <tile id="15" terrain="0,0,0,0"/>
 <tile id="16" terrain="0,0,0,0"/>
 <tile id="17" terrain="0,0,0,0"/>
 <tile id="18" terrain="0,0,0,0"/>
 <tile id="19" terrain="0,0,0,0"/>
 <tile id="20" terrain="0,0,0,0"/>
 <tile id="21" terrain="0,0,0,0"/>
 <tile id="22" terrain="0,0,0,0"/>
 <tile id="23" terrain="0,0,0,0"/>
 <tile id="24" terrain="0,0,0,0"/>
 <tile id="25" terrain="0,0,0,0"/>
 <tile id="26" terrain="0,0,0,0"/>
 <tile id="27" terrain="0,0,0,0"/>
 <tile id="28" terrain="0,0,0,0"/>
 <tile id="29" terrain="0,0,0,0"/>
 <tile id="30" terrain="0,0,0,0"/>
 <tile id="31" terrain="0,0,0,0"/>
 <tile id="32" terrain="0,0,0,0"/>
 <tile id="33" terrain="0,0,0,0"/>
 <tile id="34" terrain="0,0,0,0"/>
 <tile id="35" terrain="0,0,0,0"/>
 <tile id="36" terrain="0,0,0,0"/>
 <tile id="37" terrain="0,0,0,0"/>
 <tile id="38" terrain="0,0,0,0"/>
 <tile id="39">
  <properties>
   <property name="indoor" value=""/>
  </properties>
 </tile>
 <tile id="40">
  <properties>
   <property name="indoor" value=""/>
  </properties>
 </tile>
 <tile id="41">
  <properties>
   <property name="indoor" value=""/>
  </properties>
 </tile>
 <tile id="42" terrain="0,0,0,0"/>
 <tile id="43" terrain="0,0,0,0"/>
 <tile id="44" terrain="0,0,0,0"/>
 <tile id="45" terrain="0,0,0,0"/>
 <tile id="46">
  <properties>
   <property name="indoor" value=""/>
  </properties>
 </tile>
 <tile id="47">
  <properties>
   <property name="indoor" value=""/>
  </properties>
 </tile>
 <tile id="48">
  <properties>
   <property name="indoor" value=""/>
  </properties>
 </tile>
 <tile id="52">
  <properties>
   <property name="indoor" value=""/>
  </properties>
 </tile>
 <tile id="53">
  <properties>
   <property name="indoor" value=""/>
  </properties>
 </tile>
 <tile id="54">
  <properties>
   <property name="indoor" value=""/>
  </properties>
 </tile>
 <tile id="55">
  <properties>
   <property name="indoor" value=""/>
  </properties>
 </tile>
 <tile id="59" terrain="0,0,0,0"/>
 <tile id="60" terrain="0,0,0,0"/>
 <tile id="61">
  <properties>
   <property name="indoor" value=""/>
  </properties>
 </tile>
 <tile id="62">
  <properties>
   <property name="indoor" value=""/>
  </properties>
 </tile>
 <tile id="63" terrain="0,0,0,0"/>
 <tile id="64" terrain="0,0,0,0"/>
 <tile id="65" terrain="0,0,0,0"/>
 <tile id="66" terrain="0,0,0,0"/>
 <tile id="67" terrain="0,0,0,0"/>
 <tile id="68">
  <properties>
   <property name="indoor" value=""/>
  </properties>
 </tile>
 <tile id="69">
  <properties>
   <property name="indoor" value=""/>
  </properties>
 </tile>
 <tile id="70" terrain="0,0,0,0"/>
 <tile id="71" terrain="0,0,0,0"/>
 <tile id="72" terrain="0,0,0,0"/>
 <tile id="73" terrain="0,0,0,0"/>
 <tile id="74" terrain="0,0,0,0"/>
 <tile id="75" terrain="0,0,0,0"/>
 <tile id="76" terrain="0,0,0,0"/>
 <tile id="77">
  <properties>
   <property name="position" value="water"/>
  </properties>
  <animation>
   <frame tileid="77" duration="100"/>
   <frame tileid="78" duration="100"/>
   <frame tileid="79" duration="100"/>
   <frame tileid="78" duration="100"/>
  </animation>
 </tile>
 <tile id="78">
  <properties>
   <property name="position" value="water"/>
  </properties>
  <animation>
   <frame tileid="78" duration="100"/>
   <frame tileid="79" duration="100"/>
   <frame tileid="78" duration="100"/>
   <frame tileid="77" duration="100"/>
  </animation>
 </tile>
 <tile id="79">
  <properties>
   <property name="position" value="water"/>
  </properties>
  <animation>
   <frame tileid="79" duration="100"/>
   <frame tileid="78" duration="100"/>
   <frame tileid="77" duration="100"/>
   <frame tileid="78" duration="100"/>
  </animation>
 </tile>
 <tile id="80" terrain="0,0,0,0"/>
 <tile id="81" terrain="0,0,0,0"/>
 <tile id="82" terrain="0,0,0,0"/>
 <tile id="83" terrain="0,0,0,0"/>
 <tile id="84">
  <properties>
   <property name="position" value="water"/>
  </properties>
 </tile>
 <tile id="87" terrain="0,0,0,0"/>
 <tile id="88" terrain="0,0,0,0"/>
 <tile id="91">
  <properties>
   <property name="position" value="player"/>
  </properties>
 </tile>
 <tile id="94" terrain="0,0,0,0"/>
 <tile id="95" terrain="0,0,0,0"/>
 <tile id="101" terrain="0,0,0,0"/>
 <tile id="102" terrain="0,0,0,0"/>
 <tile id="103" terrain="0,0,0,0"/>
 <tile id="104" terrain="0,0,0,0"/>
 <tile id="105" terrain="0,0,0,0"/>
 <tile id="106" terrain="0,0,0,0"/>
 <tile id="107" terrain="0,0,0,0"/>
 <tile id="108" terrain="0,0,0,0"/>
 <tile id="109" terrain="0,0,0,0"/>
 <tile id="110" terrain="0,0,0,0"/>
 <tile id="111" terrain="0,0,0,0"/>
 <tile id="112" terrain="0,0,0,0"/>
 <tile id="113" terrain="0,0,0,0"/>
 <tile id="114" terrain="0,0,0,0"/>
 <tile id="115" terrain="0,0,0,0"/>
 <tile id="116" terrain="0,0,0,0"/>
 <tile id="117" terrain="0,0,0,0"/>
 <tile id="118" terrain="0,0,0,0"/>
 <tile id="119" terrain="0,0,0,0"/>
 <tile id="120" terrain="0,0,0,0"/>
 <tile id="121" terrain="0,0,0,0"/>
 <tile id="126" terrain="0,0,0,0"/>
 <tile id="127" terrain="0,0,0,0"/>
 <tile id="128" terrain="0,0,0,0"/>
 <tile id="133" terrain="0,0,0,0"/>
 <tile id="134" terrain="0,0,0,0"/>
 <tile id="135" terrain="0,0,0,0"/>
 <tile id="140" terrain="0,0,0,0"/>
 <tile id="141" terrain="0,0,0,0"/>
 <tile id="142" terrain="0,0,0,0"/>
 <tile id="147" terrain="0,0,0,0"/>
 <tile id="148" terrain="0,0,0,0"/>
 <tile id="149" terrain="0,0,0,0"/>
 <tile id="154" terrain="0,0,0,0"/>
 <tile id="155" terrain="0,0,0,0"/>
 <tile id="156" terrain="0,0,0,0"/>
</tileset>
